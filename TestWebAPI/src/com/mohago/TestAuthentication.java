package com.mohago;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.junit.Test;

public class TestAuthentication {

	private int NUM_MACHINES = 1000;
	private int NUM_REPLICAS = 1024;
	private long FIRST_HALF_RANGE = Integer.MAX_VALUE;
	private long SECOND_HALF_RANGE = Integer.MAX_VALUE;
	
//	private int NUM_MACHINES = 3;
//	private int NUM_REPLICAS = 8;
//	private long FIRST_HALF_RANGE = 180;
//	private long SECOND_HALF_RANGE = 180;

	@Test
	public void test() {
		// Authentication.isAuthorisedByCloudCix();
		// uploadLgfPair("C:\\Users\\Dylan\\Desktop\\api_test\\201503301709450248_MSNZR2.lgmf",
		// "http://dblaptop:8080/web-api/dataset/lgf",
		// "declan.gordon@mohago.com", "MohagoCix121!", "p1");

		//HashMap<Long, String> nodeSet = new HashMap<Long, String>();
		HashSet<Node> nodeSet = new HashSet<Node>();
		final long RANGE = FIRST_HALF_RANGE + SECOND_HALF_RANGE;
		
//		for (int i = 0; i < (RANGE / NUM_REPLICAS); i += (RANGE / (NUM_REPLICAS * NUM_MACHINES))) {
//			String ip = "a." + i + ":3306";
//			nodeSet.add(new Node(i, ip));
//			//System.out.println("i: " + i + " , ip: " + ip);
//		}
		
		
		
		for (int i = 0; i < (RANGE / (NUM_REPLICAS)); i += (RANGE / (NUM_REPLICAS * NUM_MACHINES))) {
			String ip = "a." + i + ":3306";
			//nodeSet.add(new Node(i, ip));
			
			//System.out.println("i: " + i + " , ip: " + ip);
		}
		
		// nodeSet.add(new Node(1, "a.b.c:3306"));
		// nodeSet.add(new Node(2, "d.e.f:3306"));
		// nodeSet.add(new Node(3, "a.g.t:3306"));
		// nodeSet.add(new Node(4, "a.h.k:3306"));

		long before = System.currentTimeMillis();
		NodeManager.refresh(nodeSet);
		long after = System.currentTimeMillis();
		System.out.println("time: " + (after - before) + " millis");
		
		long val = -1;

		for (int i = 0; i < 10; i += 1) {
			//
			 val = MD5Hash.hash(String.valueOf(i));
			System.out.println("address id: " + i + "hash: " + val);
			System.out.println("Node ip: "
					+ NodeManager.getNodeByObjectId(String.valueOf(i)));
		}
//		Iterator it = nodeSet.entrySet().iterator();
//		while(it.hasNext()){
//			Entry entry = (Entry) it.next();
//			if((Long)entry.getKey() == 1723734){
//				it.remove();
//			}
//		}
		System.out.println();
		System.out.println("Remove node");
		System.out.println();
		NodeManager.refresh(nodeSet);

		for (int i = 0; i < 10; i += 1) {
			//
			 val = MD5Hash.hash(String.valueOf(i));
				System.out.println("After address id: " + i + "hash: " + val);
			System.out.println("After  Node ip: "
					+ NodeManager.getNodeByObjectId(String.valueOf(i)));

		}
//
//		nodeSet.remove(1);
//		NodeManager.refresh(nodeSet);
//
//		for (int i = 0; i < 20; i += 1) {
//			//
//			// val = MD5Hash.hash(String.valueOf(i));
//			System.out.println("After 2 address id: " + i);
//			System.out.println("After 2 Node ip: "
//					+ NodeManager.getNodeByObjectId(String.valueOf(i)));
//			System.out.println();
//		}

		// long a = Long.MAX_VALUE;
		// System.out.println("Long max value: " + a);
		// int b = Integer.MAX_VALUE;
		// long c = Integer.MAX_VALUE;
		// long d = Integer.MAX_VALUE;
		// System.out.println("int Range: " + (c + d));
		// System.out.println("Integer max value: " + b);
		// System.out.println("Range: " + String.valueOf(Integer.MAX_VALUE *
		// 2));
		// long val;
		// long min = Integer.MAX_VALUE;
		// long max = Integer.MIN_VALUE;
		//
		// for (int i = 0; i < 10000; i += 1) {
		//
		// val = MD5Hash.hash(String.valueOf(i));
		// if (val < min) {
		// min = val;
		// }
		// if (val > max) {
		// max = val;
		// }
		// if (val <= 0) {
		// System.out.println("Neg: " + val);
		// }
		// }
		// System.out.println("Min: " + min);
		// System.out.println("Max: " + max);
		// Dataset dataset = new Dataset();
		// dataset.setName("name");
		// DateFormat formatter = new SimpleDateFormat(
		// "yyyyMMdd'T'HHmmssSSSZ");
		// Date date = new Date();
		// formatter.format(date);
		// dataset.setDate(formatter.format(date));
		// dataset.setProject("p1");//this should be "unassigned" unless it
		// matches an existing project which the
		// dataset.setTask("t1");
		// dataset.setSource("s1");
		// dataset.setUser("me");
		// Item item = new Item();
		// item.getParameters().add(new ParameterBase<Double>("test1", 0,
		// 23.6));
		// item.getParameters().add(new ParameterBase<Double>("test2", 0,
		// 23.3));
		// // dataset.getItems().add(item);
		// item = new Item();
		// item.getParameters().add(new ParameterBase<Double>("test3", 0,
		// 23.8));
		// item.getParameters().add(new ParameterBase<Double>("test4", 0,
		// 23.5));
		// // dataset.getItems().add(item);
		// //dataset.getItems().add(new Item<String>("b", 0, "test"));
		// Gson gson = new Gson();
		// Type datasetType = new TypeToken<Dataset>(){}.getType();
		// String datasetStr = gson.toJson(dataset, datasetType);
		// uploadDataset("https://dblaptop:8443/web-api/dataset",
		// "declan.gordon@mohago.com", "MohagoCix121!", datasetStr);
		// fail("Not yet implemented");
	}

	private boolean uploadLgfPair(String metaPath, String uploadUrl,
			String userName, String password, String project) {

		boolean ret = false;
		HttpResponse response = null;
		HttpClient client = null;
		try {
			client = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(uploadUrl);
			FileBody lgmf = new FileBody(new File(metaPath));
			FileBody lgdf = new FileBody(new File(metaPath.replace(".lgmf",
					".lgdf")));
			HttpEntity reqEntity = MultipartEntityBuilder.create()
					.addPart("lgmf", lgmf).addPart("lgdf", lgdf).build();
			httppost.setEntity(reqEntity);
			BasicHeader userHeader = new BasicHeader("user_name", userName);
			BasicHeader passwordHeader = new BasicHeader("password", password);
			BasicHeader projectHeader = new BasicHeader("project", project);
			httppost.addHeader(userHeader);
			httppost.addHeader(passwordHeader);
			httppost.addHeader(projectHeader);
			response = client.execute(httppost);
			ret = true;
		} catch (IOException e) {
			// LOG.error("Error posting lgf pair: " + e);
			int i = 0;
		} finally {
			HttpEntity result = response.getEntity();
			try {
				// LOG.info("Result: " + readStream(result.getContent(),
				// result.getContentLength()));
			} catch (IllegalStateException e) {
			}
		}
		return ret;

	}

	private boolean uploadDataset(String uploadUrl, String userName,
			String password, String json) {

		boolean ret = false;
		HttpResponse response = null;
		HttpClient client = null;
		try {
			client = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(uploadUrl);
			StringEntity params = new StringEntity(json);

			BasicHeader userHeader = new BasicHeader("user_name", userName);
			BasicHeader passwordHeader = new BasicHeader("password", password);
			httppost.addHeader("content-type", "application/json");
			httppost.setEntity(params);
			httppost.addHeader(userHeader);
			httppost.addHeader(passwordHeader);
			response = client.execute(httppost);
			ret = true;
		} catch (IOException e) {
			// LOG.error("Error posting lgf pair: " + e);
		} finally {
			HttpEntity result = response.getEntity();
			try {
				// LOG.info("Result: " + readStream(result.getContent(),
				// result.getContentLength()));
			} catch (IllegalStateException e) {
			}
		}
		return ret;

	}

}
