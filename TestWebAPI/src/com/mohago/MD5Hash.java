package com.mohago;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Hash {
    private static final MessageDigest INSTANCE;
    static{
    	try {
            INSTANCE = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
        	 throw new RuntimeException("Could not init class.", e);
        }
    }

    static long hash(String key) {
        INSTANCE.reset();
        INSTANCE.update(key.getBytes());
        byte[] digest = INSTANCE.digest();

        long h = 0;
        for (int i = 0; i < 4; i++) {
            h <<= 8;
            h |= ((int) digest[i]) & 0xFF;
        }
        return h;
    }
};
