package com.mohago.lgf;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mohago.config.DatabaseConnection;
import com.mohago.node.NodeManager;
import com.mohago.persist.DatasetUpdateService;
import com.mohago.request.DatabaseHelpers;
import com.mohago.request.MasterTable;
import com.mohago.services.CleanupJob;
import com.mohago.sql.SQLPreparedStatements;
import com.mohago.utilities.HashUtils;
import com.mohago.utilities.SQLHelpers;

public class DataSetImportTest {

	private int NUM_MACHINES = 1000;
	private int NUM_REPLICAS = 1024;
	private long FIRST_HALF_RANGE = Integer.MAX_VALUE;
	private long SECOND_HALF_RANGE = Integer.MAX_VALUE;
	private static final Random rand = new Random();
	
	static final String CONFIG = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<configuration>"
			+ "<dbconnparams>"
			+ "<type>postgresql</type>"
			+ "<host>127.0.0.1</host>"
			+ "<!--<host>91.103.6.47</host> qa-->"
			+ "<!--<host>185.49.61.53</host>prod-public-->"
			+ "<!--<host>192.168.1.4</host>prod-->"
			+ "<!--<host>localhost</host>dev/local-->"
			+ "<port>5432</port>"
			+ "<user_name>postgres</user_name>"
			+ "<!-- <password>BananaShuttle1!</password>-->"
			+ "<password>BananaShuttle1!</password>"
			+ "<mohago_db>mohago_db_3</mohago_db><!-- Provide name for data database -->"
			+ "<mohago_local>mohago_local</mohago_local><!-- Provide name for user database -->"
			+ "</dbconnparams>" + "<administrator>" + "<user_name></user_name>"
			+ "<password></password>" + "</administrator>" + "</configuration>";
	static final String LGMF = "<?xml version=\"1.0\" ?><Meta><ParameterCollections><ParamCol Name=\"a\" Type=\"double_precision\" Index=\"0\"></ParamCol><ParamCol Name=\"b\" Type=\"date_time\" Index=\"1\"></ParamCol></ParameterCollections><string_value Name=\"cloudCIXUserName\" Value=\"declan.gordon@mohago.com\"></string_value><integer Name=\"cloudCIXIDMember\" Value=\"11\"></integer><integer Name=\"cloudCIXIDUser\" Value=\"205040\"></integer><integer Name=\"cloudCIXIDAddress\" Value=\"205396\"></integer><double_precision Name=\"voltage\" Value=\"26.5\"></double_precision><double_precision Name=\"temp\" Value=\"14.2\"></double_precision><DataSetName Value=\"Unit tests\"></DataSetName><DataSetDate Value=\"201508280841130052\"></DataSetDate><DataSetProject Value=\"p1\"></DataSetProject><DataSetId Value=\"" + rand.nextInt(Integer.MAX_VALUE) + "\"></DataSetId><Done>0</Done><Hash>md5Token</Hash><Imported>0</Imported></Meta>";
	static final String LGDF = "215.45,201508201977400628\n"
			+ "213.45,201508191955400628\n" + "211.45,201508192055400628\n";
	private static final String IP = "127.0.0.1";
	private static Connection conn = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DatabaseConnection.initDBConnectionParamsFromString(CONFIG);
		DatabaseConnection.initJDBCDriver();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@Before
	public void setUp() throws Exception {
		conn = DatabaseConnection.getConnection(IP);
		SQLHelpers.setAutoCommit(conn, false);
		
	}

	@After
	public void tearDown() throws Exception {
		SQLHelpers.closeConn(conn);
	}
	
	public static boolean insertNodeEntry(final Connection conn, final int id) {
		boolean result = false;
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = conn
					.prepareStatement("INSERT INTO mohago_db_3.node(id) values(?)");
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			conn.commit();
			result = true;
		} catch (SQLException e) {
			SQLHelpers.rollBack(conn, "inserting into node");
		} finally {
			SQLHelpers.closeResources(preparedStatement, null);
		}
		return result;

	}
	
	@Test
	public void prePopulateNodeTable(){
		int i;
		int j = 2;//E.g if this is two, will be HALF of pie slice representing: ((RANGE / NUM_REPLICAS))
		for (i = 0; i < ((FIRST_HALF_RANGE + SECOND_HALF_RANGE) / (NUM_REPLICAS)) / j; i += ((FIRST_HALF_RANGE + SECOND_HALF_RANGE) / (NUM_REPLICAS * NUM_MACHINES))) {
			//insertNodeEntry(conn, i);
		}
		System.out.print(i);
	}
	
	@Test
	public void testUidGenerator(){
		for(int i = 0; i < 100; i ++){
		long a = NodeManager.getDatasetUid(11);
		System.out.println(a);
		}
	}
	
	@Test
	public void testNodeManager(){
		NodeManager.refresh(MasterTable.getNodeSet());
		String address = NodeManager.getNodeAddressByAddressId("1");//205928
		System.out.println(address
				);
		int count = 0;
		for(int i = 0; i <1000; i++){
			address = NodeManager.getNodeAddressByAddressId(String.valueOf(i));
			if(!address.equals("192.168.1.12")){
				count ++;
				
			}
		}
		System.out.println(count
				);
	}
	
	@Test
	public void testDeleteRedundantDatasets(){
		String shardIP = "192.168.1.17";
		NodeManager.refresh(MasterTable.getNodeSet());
		try(Connection conn = DatabaseConnection.getConnection("185.49.61.52")){
			SQLHelpers.setAutoCommit(conn, false);
			ResultSet reader;
			PreparedStatement ps = conn.prepareStatement("SELECT d_id,device_token FROM mohago_db_3.dataset");
			reader = ps.executeQuery();
			while(reader.next()){
				if(!NodeManager.getNodeAddressByDeviceToken(reader.getString("device_token")).equals(shardIP)){
					DatasetUpdateService.markDatasetAsDeleted(reader.getLong("d_id"), conn);
				}
			}
			CleanupJob.cleanupOldDatasets(0, conn);
		}catch(SQLException e){
			System.out.println(e
					);
		}
		NodeManager.refresh(MasterTable.getNodeSet());
		String address = NodeManager.getNodeAddressByAddressId("1");//205928
		System.out.println(address
				);
		int count = 0;
		for(int i = 0; i <1000; i++){
			address = NodeManager.getNodeAddressByAddressId(String.valueOf(i));
			if(!address.equals("192.168.1.17")){
				count ++;
				
			}
		}
		System.out.println(count
				);
	}

	@Test
	public void testImportStoreLgmfLgdfPair() {
		
		NodeManager.refresh(MasterTable.getNodeSet());
		String devToken = "";
		File dataFile = null;
		File metaFile = null;
		try {
			dataFile = new File("c:\\users\\dylan\\test\\abc.lgdf");
			BufferedWriter bw = new BufferedWriter(new FileWriter(dataFile));
			
			bw.write(LGDF);
			bw.close();

			metaFile = new File("c:\\users\\dylan\\test\\abc.lgmf");
			bw = new BufferedWriter(new FileWriter(metaFile));
			bw.write(LGMF.replace("md5Token", HashUtils.getMD5(dataFile.getAbsolutePath())));
			bw.close();
			DataSetImportManager dsi = new DataSetImportManager();
			devToken = dsi.importStoreLgmfLgdfPair(metaFile.getAbsolutePath());
			Long did = DatabaseHelpers.getDidFromDeviceToken(devToken, conn);
			DatasetUpdateService.markDatasetAsDeleted(did, conn);
			CleanupJob.cleanupOldDatasets(0, conn);
			System.out.println("Done");

		} catch (IOException e) {

			e.printStackTrace();

		}finally{
			dataFile.delete();
			metaFile.delete();
		}
		assertNotEquals(devToken, "");///TODO: Add more tests on the Dataset that was persisted
	}
	
	@Test
	public void testImportStoreLgmfLgdfPairStream() {
		fail("Not implemented yet!");
	}

}
