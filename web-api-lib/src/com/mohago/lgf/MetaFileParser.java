package com.mohago.lgf;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channel;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.Part;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.log4j.Logger;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.mohago.node.NodeManager;
import com.mohago.serialization.Dataset;
import com.mohago.serialization.LookingGlassType;
import com.mohago.serialization.Meta;
import com.mohago.serialization.MetaDataBase;
import com.mohago.serialization.Pair;
import com.mohago.utilities.HashUtils;
import com.mohago.utilities.SQLHelpers;

public class MetaFileParser {

	private static final Logger LOG = Logger.getLogger(MetaFileParser.class);

	synchronized static HashMap<String, Object> parseLgmf(String metaPath) {
		HashMap<String, Object> ret = new HashMap<String, Object>();
		ArrayList<Triple<Integer, String, LookingGlassType>> paramList = new ArrayList<Triple<Integer, String, LookingGlassType>>();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		Document doc = null;

		try (InputStream is = new FileInputStream(metaPath)) {
			db = dbf.newDocumentBuilder();
			doc = db.parse(is);
			is.close();
			
		} catch (ParserConfigurationException | IOException
				| IllegalArgumentException | SAXException e) {
			LOG.error("Error parsing lgmf: " + e);
		}finally{
			String delPath = metaPath.replace("lgdf", "lgdf" + File.separator
					+ "delete");
			// Move meta file to delete folder
			try {
				Files.move(Paths.get(metaPath), Paths.get(delPath),
						StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				LOG.error("Error parsing lgmf: " + e);
			}
		}

		ret = parseMetaFileDocForDatasetAndParameters(doc, paramList, metaPath);
		return ret;
	}

	public static HashMap<String, Object> parseMetaFileDocForDatasetAndParameters(Document doc,
			ArrayList<Triple<Integer, String, LookingGlassType>> paramList,
			String metaPath) {

		HashMap<String, Object> ret = new HashMap<String, Object>();
		if (doc != null) {

			Date now = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");
			String formattedDate = sdf.format(now);
			Dataset importedDataSet = new Dataset();
			importedDataSet.setProject("local");

			importedDataSet.setMetaData(new Meta(importedDataSet));
			doc.getDocumentElement().normalize();

			NodeList nList = doc.getDocumentElement().getChildNodes();

			if (parseMetaNodes(importedDataSet, nList, paramList, metaPath)) {
				ret.put("dataset", importedDataSet);
				ret.put("params", paramList);
			}
		}
		return ret;
	}

	public static void parseMetaNodes(InputPart part, String fileSavePath,
			int idMember, int idUser, String userName, int idAddress) {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		Document doc = null;

		try (InputStream is = part.getBody(InputStream.class, null)) {
			db = dbf.newDocumentBuilder();
			doc = db.parse(is);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getDocumentElement().getChildNodes();
			boolean foundIdMember = false;
			boolean foundIdUser = false;
			boolean foundUserName = false;
			boolean foundIdAddress = false;
			for (int temp = 0; temp < nList.getLength(); temp++) {
				String type;
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					String nodeName = eElement.getNodeName();
					String name = eElement.getAttribute("Name");
					String value = eElement.getAttribute("Value");

					if ("integer".equalsIgnoreCase(nodeName)
							&& "cloudCIXIDMember".equalsIgnoreCase(name)) {
						eElement.setAttribute("Value", String.valueOf(idMember));
						foundIdMember = true;
					} else if ("integer".equalsIgnoreCase(nodeName)
							&& "cloudCIXIDIDUser".equalsIgnoreCase(name)) {
						eElement.setAttribute("Value", String.valueOf(idUser));
						foundIdUser = true;
					} else if ("string_value".equalsIgnoreCase(nodeName)
							&& "cloudCIXIDUserName".equalsIgnoreCase(name)) {
						eElement.setAttribute("Value", String.valueOf(userName));
						foundUserName = true;
					} else if ("integer".equalsIgnoreCase(nodeName)
							&& "cloudCIXIDAddress".equalsIgnoreCase(name)) {
						eElement.setAttribute("Value",
								String.valueOf(idAddress));
						foundIdAddress = true;
					}
					else if ("DataSetId".equalsIgnoreCase(nodeName)) {
						eElement.setAttribute("Value",
								String.valueOf(NodeManager.getDatasetUid(idAddress)));
					}
				}
			}
			if (!foundIdMember) {

				Element idMemberElement = doc.createElement("integer");
				idMemberElement.setAttribute("Name", "cloudCIXIDMember");
				idMemberElement.setAttribute("Value", String.valueOf(idMember));
				doc.getDocumentElement().appendChild(idMemberElement);

			}
			if (!foundIdUser) {

				Element idMemberElement = doc.createElement("integer");
				idMemberElement.setAttribute("Name", "cloudCIXIDUser");
				idMemberElement.setAttribute("Value", String.valueOf(idUser));
				doc.getDocumentElement().appendChild(idMemberElement);

			}
			if (!foundUserName) {

				Element idMemberElement = doc.createElement("string_value");
				idMemberElement.setAttribute("Name", "cloudCIXUserName");
				idMemberElement.setAttribute("Value", String.valueOf(userName));
				doc.getDocumentElement().appendChild(idMemberElement);

			}
			if (!foundIdAddress) {

				Element idMemberElement = doc.createElement("integer");
				idMemberElement.setAttribute("Name", "cloudCIXIDAddress");
				idMemberElement
						.setAttribute("Value", String.valueOf(idAddress));
				doc.getDocumentElement().appendChild(idMemberElement);

			}
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(fileSavePath));
			transformer.transform(source, result);
			is.close();

		} catch (ParserConfigurationException | IOException
				| IllegalArgumentException | SAXException
				| TransformerException e) {
			LOG.error("Error parsing lgmf: " + e);

		} finally {
			// No need to close is as its in try block?
		}

	}

	private static boolean parseMetaNodes(Dataset importedDataSet,
			NodeList nList,
			ArrayList<Triple<Integer, String, LookingGlassType>> paramList,
			String metaPath) {

		for (int temp = 0; temp < nList.getLength(); temp++) {
			String type;
			Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				String nodeName = eElement.getNodeName();
				String name = eElement.getAttribute("Name");
				String value = eElement.getAttribute("Value");

				if ("double_precision".equalsIgnoreCase(nodeName)) {
					importedDataSet
							.getMetaData()
							.getDoubleData()
							.add(new MetaDataBase<Double>(name, Double
									.parseDouble(value), importedDataSet));
				} else if ("integer".equalsIgnoreCase(nodeName)) {
					importedDataSet
							.getMetaData()
							.getIntData()
							.add(new MetaDataBase<Integer>(name, Integer
									.parseInt(value), importedDataSet));
				} else if ("string_value".equalsIgnoreCase(nodeName)) {
					importedDataSet
							.getMetaData()
							.getStringData()
							.add(new MetaDataBase<String>(name, value,
									importedDataSet));
				} else if ("date_time".equalsIgnoreCase(nodeName)) {
					DateFormat formatter = new SimpleDateFormat(
							"yyyyMMddHHmmssSSSS");
					Date date = null;
					try {
						date = formatter.parse(value);
					} catch (ParseException e) {
						LOG.error("Error formatting date during lgmf import: "
								+ e);
					}
					importedDataSet
							.getMetaData()
							.getDateTimeData()
							.add(new MetaDataBase<Date>(name, date,
									importedDataSet));
				} else if ("DataSetName".equalsIgnoreCase(nodeName)) {
					importedDataSet.setName(value);
				} else if ("DataSetProject".equalsIgnoreCase(nodeName)) {
					importedDataSet.setProject(value);
				
				} else if ("DataSetId".equalsIgnoreCase(nodeName)) {
					importedDataSet.setUid(Long.parseLong(value));
				} else if ("DataSetDeviceToken".equalsIgnoreCase(nodeName)) {
					importedDataSet.setDeviceToken(value);
				}else if ("DataSetParentId".equalsIgnoreCase(nodeName)) {
					importedDataSet.setParentUid(Long.parseLong(value));
				} else if ("DataSetDate".equalsIgnoreCase(nodeName)) {
					String da = value;
					DateFormat formatter = new SimpleDateFormat(
							"yyyyMMddHHmmssSSSS");
					Date date = null;
					try {
						date = formatter.parse(da);
					} catch (ParseException e) {
						LOG.error("Error formatting date during lgmf import: "
								+ e);
					}
					importedDataSet.setDate(da);
					importedDataSet.getMetaData().getDateTimeData()
							.add(new MetaDataBase<Date>("Timestamp", date));
				}

				else if ("Hash".equalsIgnoreCase(nodeName)) {

					String fpath = metaPath.replace(".lgmf", ".lgdf");
					final String hash = HashUtils.getMD5(fpath);

					importedDataSet
							.setFileHash(new ArrayList<Pair<String, String>>());
					importedDataSet.getFileHash().add(
							new Pair<String, String>(importedDataSet
									.getName(), hash));// DB

					String hashValue = eElement.getFirstChild()
							.getTextContent();
					LOG.info("Locally made hash: " + hash
							+ " , imported hash from lgmf: " + hashValue);
					// /TODO: split method here:
					if (!hash.equalsIgnoreCase(hashValue)) {
						// /Delete and return false
						try {
							Files.delete(Paths.get(metaPath));
							Files.delete(Paths.get(metaPath.replace(".lgmf",
									".lgdf")));
						} catch (IOException e) {
							LOG.error("Error deleting corrupted lgf pair: " + e);
						}
						String error = "Possible corrupted file, error occured importing batch file using API.";
						LOG.error("Error, imported lgmf hash does not match local hash. Lgf pair deleted."
								+ error);
						return false;
					}

				} else if ("ParameterCollections"// /TODO: split method here
				.equalsIgnoreCase(nodeName)) {
					// paramsInMeta = true;
					NodeList nodeList = nNode.getChildNodes();
					for (int i = 0; i < nodeList.getLength(); i++) {
						Node current = nodeList.item(i);
						Element currElement = (Element) current;
						String currName = currElement.getAttribute("Name");
						int currIndex = Integer.parseInt(currElement
								.getAttribute("Index"));
						if ("ParamCol".equalsIgnoreCase(currElement
								.getNodeName())) {
							type = (String) currElement.getAttribute("Type");
							if ("double_precision".equalsIgnoreCase(type)) {
								paramList
										.add(new ImmutableTriple<Integer, String, LookingGlassType>(
												currIndex,
												currName,
												LookingGlassType.double_precision));
							} else if ("integer".equalsIgnoreCase(type)) {
								paramList
										.add(new ImmutableTriple<Integer, String, LookingGlassType>(
												currIndex, currName,
												LookingGlassType.integer));
							} else if ("string_value".equalsIgnoreCase(type)) {
								paramList
										.add(new ImmutableTriple<Integer, String, LookingGlassType>(
												currIndex, currName,
												LookingGlassType.string_value));
							} else if ("date_time".equalsIgnoreCase(type)) {
								paramList
										.add(new ImmutableTriple<Integer, String, LookingGlassType>(
												currIndex, currName,
												LookingGlassType.date_time));
							}
						}
					}
				}
			}
		}
		return true;
	}

}
