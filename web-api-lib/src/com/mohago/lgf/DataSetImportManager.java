package com.mohago.lgf;

import java.io.File;
import java.io.IOException;
import java.nio.channels.Channel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import org.apache.commons.lang3.tuple.Triple;
import org.apache.log4j.Logger;
//import org.owasp.esapi.ESAPI;
//import org.owasp.esapi.codecs.Codec;
//import org.owasp.esapi.codecs.MySQLCodec;
//import org.owasp.esapi.codecs.OracleCodec;

import org.quartz.utils.DBConnectionManager;

import com.mohago.config.Configuration;
import com.mohago.config.DatabaseConnection;
import com.mohago.config.Configuration.Database;
import com.mohago.node.NodeManager;
import com.mohago.persist.MetaStorageService;
import com.mohago.request.DatabaseHelpers;
import com.mohago.request.MasterTable;
import com.mohago.serialization.Dataset;
import com.mohago.serialization.LookingGlassType;
import com.mohago.serialization.MetaDataBase;
import com.mohago.serialization.Parameter;
import com.mohago.services.ScanService;
import com.mohago.sql.SQLPreparedStatements;
import com.mohago.utilities.Constants;
import com.mohago.utilities.SQLHelpers;
import com.mysql.jdbc.StringUtils;

/**
 * 
 * @author Dylan
 *
 */
public class DataSetImportManager {//Need to refactor

	private static final Logger LOG = Logger.getLogger(DataSetImportManager.class);
	public static final HashMap<LookingGlassType, String> LG_TYPE_DICT;
	static {
		LG_TYPE_DICT = new HashMap<LookingGlassType, String>();
		LG_TYPE_DICT.put(LookingGlassType.floating_point, "FLOAT");
		LG_TYPE_DICT.put(LookingGlassType.integer, "INTEGER");
		LG_TYPE_DICT.put(LookingGlassType.string_value, "VARCHAR(255)");
		LG_TYPE_DICT.put(LookingGlassType.date_time, "TIMESTAMP(4)");
		LG_TYPE_DICT.put(LookingGlassType.double_precision, "DOUBLE PRECISION");
	}
	static final int TOKEN_SIZE_TABLE_NAME_AND_GLOBAL_ID = 5;
	static final int TOKEN_SIZE_DEVICE_TOKEN = 24;

	public DataSetImportManager() {

	}

	public String importStoreLgmfLgdfPair(String lgmfFileName) {
		Long dId = -1l;
		String deviceToken = "";

		// parse lgmf, get parameters index, meta.
		HashMap<String, Object> dataObject = MetaFileParser.parseLgmf(lgmfFileName);// This
																			// method
																			// moves
																			// the
																			// Meta
																			// file
																			// into
																			// delete
																			// folder
		// dataObject is made up of top level Dataset as well as a "Params" object
		// which is a list of Parameter information entries for that Dataset
		Dataset dataset = null;
		if (dataObject != null) {

			dataset = (Dataset) dataObject.get("dataset");
			// get the address_id from the dataset
			int addressId = -1;
			for (MetaDataBase<Integer> mdbi : dataset.getMetaData()
					.getIntData()) {
				if (mdbi.name.equals("cloudCIXIDAddress")) {
					addressId = mdbi.value;
				}
			}

			String ip = "";// Need to do some logic here for device_token
			final DatasetTablesView dtv = new DatasetTablesView();
			// This is a new Dataset, it has no parent (First of stream)
			if (dataset.getParentUid() == null
					|| dataset.getParentUid()
							.equals(dataset.getUid())) {// Not stream

				if (!StringUtils.isNullOrEmpty(ip = NodeManager
						.getNodeAddressByAddressId(String.valueOf(addressId)))) {
					try (Connection conn = DatabaseConnection
							.getPooledConnection(ip)) {

						// keep
						// pools
						// once
						// we
						// know
						// the
						// ips
						SQLHelpers.setAutoCommit(conn, false);

						deviceToken = processNewDataset((ArrayList<Triple<Integer, String, LookingGlassType>>) dataObject
								.get("params"), dataset,
								lgmfFileName, dtv, ip,
								String.valueOf(addressId), conn);
						//DatabaseHelpers.getDeviceTokenFromDid(dId, conn);
						conn.commit();
					} catch (SQLException e) {
						LOG.error("Error with master db conn: " + e);
					}
					
				} else {
					moveMetaFileToErrorFolderFromDelete(lgmfFileName);
				}

			} else if (dataset// This IS a Stream
															// type dataset
															// (Has a
															// parent)
					.getParentUid() != null
					&& !dataset.getParentUid().equals(
							dataset.getUid())) {// Stream

				if (addressId != -1) {
					ip = NodeManager.getNodeAddressByDeviceToken(String
							.valueOf(addressId));
				} else {
					ip = NodeManager.getNodeAddressByDeviceToken(dataset
							.getDeviceToken());
				}

				if (!StringUtils.isNullOrEmpty(ip)) {
					try (Connection conn = DatabaseConnection
							.getPooledConnection(ip)) {
						SQLHelpers.setAutoCommit(conn, false);
						deviceToken = processDataStream(dataObject, dataset, lgmfFileName,
								dtv, conn);

						conn.commit();
					} catch (SQLException e) {
						LOG.error("Error with master db conn: " + e);
					}

				} else {
					moveMetaFileToErrorFolderFromDelete(lgmfFileName);
				}

			} else {// Neither stream NOR non-stream, some sort of error

				// SQLHelpers.closeConn(conn);// No longer needed
				moveMetaFileToMainFolderFromDelete(lgmfFileName);
			}

		}

		return deviceToken;
	}

	private void insertIntoDataAccess(Long dId, Dataset dataset, Connection conn) {
		int memberId = -1;
		for (MetaDataBase<Integer> mdb : dataset.getMetaData().getIntData()) {
			if (mdb.name.equalsIgnoreCase("cloudCIXIDMember")) {
				memberId = mdb.value;
			}
		}

		PreparedStatement preparedStatement = null;
		try {

			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Insert.INSERT_DATASET_ACCESS_ENTRY
							.getValue());
			preparedStatement.setLong(1, dId);// did
			preparedStatement.setInt(2, memberId);// m_id
			preparedStatement.setInt(3, memberId);// owner_m_id
			preparedStatement.execute();
			conn.commit();
		} catch (SQLException e) {
			LOG.error("inserting dataset access entry");
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, null);
		}

	}

	private void setItemsCountInDB(Long dId, int maxItemsCountFromTables,
			Connection conn) {

		PreparedStatement preparedStatement = null;
		try {

			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Update.SET_ITEM_COUNT
							.getValue());
			preparedStatement.setInt(1, maxItemsCountFromTables);
			preparedStatement.setLong(2, dId);
			preparedStatement.execute();
			conn.commit();
		} catch (SQLException e) {
			LOG.info("Setting item count in Db");
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, null);
		}
	}

	static int getMaxItemsCountFromTables(Long dId, Connection conn) {
		ArrayList<String> tables = new ArrayList<String>();
		ArrayList<Integer> lengths = new ArrayList<Integer>();

		PreparedStatement preparedStatement = null;
		ResultSet reader = null;
		LOG.info("Getting max items count ");
		try {

			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_TABLE_NAMES
							.getValue());
			preparedStatement.setLong(1, dId);
			reader = preparedStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				tables.add(reader.getString("table_name"));
			}

		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
		}

		if (!tables.isEmpty()) {
			for (String tname : tables) {
				LOG.info("Max items count ");// Is this correct??? should be max
												// row id maybe
				try {

					preparedStatement = conn.prepareStatement(String.format(
							SQLPreparedStatements.Select.GET_ITEM_COUNT
									.getValue(), tname));
					reader = preparedStatement.executeQuery();
					conn.commit();
					while (reader.next()) {
						lengths.add(reader.getInt(1));
					}

				} catch (SQLException e) {
					LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
				} finally {
					SQLHelpers.closeResources(preparedStatement, reader);
				}
			}
			int max = 0;
			for (Integer size : lengths) {

				if (size > max) {
					max = size;
				}
			}
			return max + 1;// need not count not index
		} else {
			return 0;
		}
	}

	Long buildStorageForDataSet(final Dataset dataset, DatasetTablesView dtv, boolean isStream,
			Connection conn) {

		if (!isStream) {

			insertDataset(dataset, conn);
		}

		for (TableIndex dt : dtv.getTables()) {

			if (!dt.isBuiltAlready()) {
				String tableName = SQLHelpers
						.ranToken(DataSetImportManager.TOKEN_SIZE_TABLE_NAME_AND_GLOBAL_ID)
						+ "a" + dataset.getUid();
				int tId = nextTid(dataset.getUid(), conn);
				storeTableIndex(dataset.getUid(), tId, tableName, dt
						.getParameters().size(), conn);

				String createString = "CREATE TABLE IF NOT EXISTS "
						+ Configuration.Database.MOHAGO_DB_NAME.getValue()
						+ "." + tableName + "(id INTEGER NOT NULL ";

				Collections.sort(dt.getParameters(),
						new Comparator<Parameter>() {
							@Override
							public int compare(Parameter param1,
									Parameter param2) {

								return Integer.compare(
										param1.getStorageIndex(),
										param2.getStorageIndex());
							}
						});
				PreparedStatement storeparamNameIndex = null;
				try {
					storeparamNameIndex = conn
							.prepareStatement(SQLPreparedStatements.Insert.INSERT_PARAM_NAME_INDEX
									.getValue());
				} catch (SQLException e2) {
					LOG.error("Error executing insert param name batch: " + e2);
				}
				int i = 0;
				for (Parameter param : dt.getParameters()) {
					String pseudonym = "p" + String.valueOf(i);
					param.setStorageIndex(i);
					try {
						storeparamNameIndex.setLong(1, dataset.getUid());
						storeparamNameIndex.setInt(2, tId);
						storeparamNameIndex.setString(3, param.getName());
						storeparamNameIndex.setInt(4, i);
						storeparamNameIndex.setInt(5, param.getType()
								.getValue());
						storeparamNameIndex.addBatch();
					} catch (SQLException e) {
						LOG.error("Error executing insert param name batch: "
								+ e);
					}

					createString += ", "
							+ pseudonym
							+ " "
							+ LG_TYPE_DICT.get((LookingGlassType) param
									.getType()) + " NULL";
					i++;
				}
				try {
					storeparamNameIndex.executeBatch();
				} catch (SQLException e2) {
					LOG.error("Error executing insert param name batch: " + e2);
				}
				createString += ");";
				Statement createTable = null;
				try {
					createTable = conn.createStatement();
					createTable.execute(createString);// No need to sql encode
														// as a
														// look up table is now
														// used
					conn.commit();
				} catch (SQLException e1) {
					LOG.error("Error creating stmt: " + e1);
				} finally {
					if (createTable != null) {
						try {
							createTable.close();
						} catch (SQLException e) {
							LOG.error("Error closing stmt: " + e);
						}
					}
				}
				dt.setTableName(tableName);
			}
		}

		dtv.setDIdInternal(dataset.getUid());

		return dataset.getUid();
	}

	static void storeTableIndex(Long dId, final int tId, String tableName,
			final int paramCount, Connection conn) {
		PreparedStatement storeTableIndex = null;
		try {
			storeTableIndex = conn
					.prepareStatement(SQLPreparedStatements.Insert.INSERT_TABLE_INDEX
							.getValue());
			storeTableIndex.setLong(1, dId);
			storeTableIndex.setInt(2, tId);
			storeTableIndex.setString(3, tableName);
			storeTableIndex.setInt(4, 0);// Should this always be
											// zero????// it is now.
			storeTableIndex.setInt(5, paramCount);
			storeTableIndex.execute();

		} catch (SQLException e) {
			LOG.error("Error storing table index: " + e);
			SQLHelpers.rollBack(conn, "storing table index");
		} finally {
			SQLHelpers.closeResources(storeTableIndex, null);
		}
	}

	static void createEmptyObfTable(String tableName, Connection conn) {

		String createString = "CREATE TABLE IF NOT EXISTS "
				+ Configuration.Database.MOHAGO_DB_NAME.getValue() + "."
				+ tableName + "(id INTEGER NOT NULL)";
		PreparedStatement createTable = null;
		try {
			createTable = conn.prepareStatement(createString);

			createTable.execute();
		} catch (SQLException e) {
			LOG.error("Error creating table: " + e);
			SQLHelpers.rollBack(conn, "creating table");
		} finally {
			SQLHelpers.closeResources(createTable, null);
		}
	}

	private int nextTid(Long uid, Connection conn) {
		int ret = -1;
		PreparedStatement getNextTid = null;
		ResultSet reader = null;
		try {
			getNextTid = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_NEXT_TID
							.getValue());
			getNextTid.setLong(1, uid);
			reader = getNextTid.executeQuery();
			reader.next();
			ret = reader.getInt("t_id");

		} catch (SQLException e) {
			LOG.error("Error getting next t_id: " + e);
			SQLHelpers.rollBack(conn, "getting next t_id");
			ret = -1;
		} finally {
			SQLHelpers.closeResources(getNextTid, reader);
		}
		if (ret != -1) {
			return ret + 1;
		} else {
			return 0;
		}
	}



	private void insertDataset(Dataset dataset, Connection conn) {
		if (conn != null) {
			PreparedStatement insertDataSet = null;
			try {
				insertDataSet = conn
						.prepareStatement(SQLPreparedStatements.Insert.INSERT_DATASET
								.getValue());
				insertDataSet.setLong(1, dataset.getUid());
				insertDataSet.setString(2, dataset.getName());
				insertDataSet.setString(3, dataset.getProject());

				DateFormat dateFormat = new SimpleDateFormat(
						"yyyyMMddHHmmssSSSS");
				java.util.Date date = null;
				try {
					date = dateFormat.parse(dataset.getDate());
				} catch (ParseException e) {
					LOG.error("error parsing date: " + e);
				}
				final long time = date.getTime();
				final Timestamp timeStamp = new Timestamp(time);
				insertDataSet.setTimestamp(4, timeStamp);
				insertDataSet.setString(5, dataset.getDeviceToken());
				insertDataSet.execute();
				conn.commit();
			} catch (SQLException e) {
				LOG.error("Error storing batch: " + e);
				SQLHelpers.rollBack(conn, "storing batch");
			} finally {
				SQLHelpers.closeResources(insertDataSet, null);
			}
		} else {
			LOG.error("No connection");
		}

	}

	private String processNewDataset(ArrayList<Triple<Integer,String,LookingGlassType>> params,
			Dataset dataset, String lgmfFileName, DatasetTablesView dtv,
			String ip, String addressId, Connection conn) {
		Long dId;
		dtv.buildFromExternalSourceTriple(params);

		// insert new dataset entry. build tables for this dataset
		dataset.setDeviceToken(addressId + "a"
				+ SQLHelpers.ranToken(TOKEN_SIZE_DEVICE_TOKEN));
		dId = buildStorageForDataSet(dataset, dtv, false, conn);
		MetaStorageService.insertDatasetMeta(dId, dataset.getMetaData(), conn);
		MetaStorageService.insertfileHash(dataset, conn);
		// /parse lgdf, line by line into the new dataset using a
		// DataSetTablesViewObject.
		char[] delim = new char[] { ',' };
		DataFileParser.parseLgdfFile(dtv, lgmfFileName, delim, conn);
		setItemsCountInDB(dId, getMaxItemsCountFromTables(dId, conn), conn);
		insertIntoDataAccess(dId, dataset, conn);
		// insertIntoDatasetHistory(dId, dataset, 0, conn);

		// MasterTable.insertConnectionIPForDataset(dataset.getGlobalId(), ip,
		// dataset.getDeviceToken());// new

		// dataset
		return dataset.getDeviceToken();
	}

	private String processDataStream(HashMap<String, Object> dOb, Dataset dataset,
			String lgmfFileName, DatasetTablesView dtv, Connection conn) {

		// Check if parent has been imported yet
		Long dId = dataset.getParentUid();
		if (dId != -1) {

			long timeOut = System.currentTimeMillis() + (10 * 1000);
			while (isDatasetBeingStreamedTo(dId, conn) || (System.currentTimeMillis() > timeOut)) {// Add in countdown or
															// timeout code
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					LOG.error("Wait for dataset stream interrupted");
				}
			}
			setDatasetBeingStreamedTo(dId, true, conn);
			dtv.buildFromExternalSourceTripleStream(dataset,
					(ArrayList<Triple<Integer, String, LookingGlassType>>) dOb
							.get("params"), conn);
			dId = buildStorageForDataSet(dataset, dtv, true, conn);
			char[] delim = new char[] { ',' };
			DataFileParser.parseLgdfFile(dtv, lgmfFileName, delim, conn);
			setItemsCountInDB(dId, getMaxItemsCountFromTables(dId, conn), conn);
			setDatasetBeingStreamedTo(dId, false, conn);
		} else {// Put Meta File back into scan folder for
				// processing,
				// (parent hasn't been imported yet so re-add the
				// file
				// at the bottom of queue until parent is imported)
			// Note: data file hasn't been processed or moved into delete folder
			// yet
			moveMetaFileToMainFolderFromDelete(lgmfFileName);
			ScanService.getFilesToProcess().add(lgmfFileName);
			LOG.debug("Parent hasn't been imported yet");
		}
		return DatabaseHelpers.getDeviceTokenFromDid(dId, conn);
	}

	private boolean isDatasetBeingStreamedTo(Long dId, Connection conn) {
		int ret = -1;
		PreparedStatement preparedStatement = null;
		ResultSet reader = null;
		try {
			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.IS_DATASET_BEING_STREAMED_TO
							.getValue());
			preparedStatement.setLong(1, dId);
			reader = preparedStatement.executeQuery();
			conn.commit();
			reader.next();
			ret = reader.getInt("d_id");
		} catch (SQLException e) {
			LOG.error("Error checking if did busy: " + e);
			// SQLHelpers.rollBack(conn, "checking if did busy");
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
		}
		return ret == dId;
	}

	private boolean setDatasetBeingStreamedTo(Long dId,
			boolean isBeingStreamedTo, Connection conn) {
		int rowsAffected = -1;
		PreparedStatement preparedStatement = null;
		try {
			if (isBeingStreamedTo) {

				preparedStatement = conn
						.prepareStatement(SQLPreparedStatements.Insert.INSERT_DATASET_STREAM_FLAG
								.getValue());

			} else {
				preparedStatement = conn
						.prepareStatement(SQLPreparedStatements.Delete.REMOVE_DATASET_STREAM_FLAG
								.getValue());
			}
			preparedStatement.setLong(1, dId);
			rowsAffected = preparedStatement.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOG.error("Error setting did: " + e);
			SQLHelpers.rollBack(conn, "setting did");
		} finally {
			SQLHelpers.closeResources(preparedStatement, null);
		}
		return rowsAffected == 1;

	}

	private void moveMetaFileToErrorFolderFromDelete(String lgmfFileName) {
		LOG.warn("Dataset null on import");

		try {// Move meta file into error folder from delete
			Files.move(
					Paths.get(lgmfFileName.replace("lgdf", "lgdf"
							+ File.separator + "delete")),
					Paths.get(lgmfFileName.replace("lgdf", "lgdf"
							+ File.separator + "error")),
					StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			LOG.error("Error moving meta into error folder: " + e);
		}
	}

	private void moveMetaFileToMainFolderFromDelete(String lgmfFileName) {
		try {// Move meta file into error folder from delete
			Files.move(
					Paths.get(lgmfFileName.replace("lgdf", "lgdf"
							+ File.separator + "delete")),
					Paths.get(lgmfFileName.replace("lgdf", "lgdf"
							+ File.separator + "error")),
					StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			LOG.error("Error moving meta back in to scan folder from delete: "
					+ e);
		}
	}

}
