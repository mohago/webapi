package com.mohago.lgf;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mohago.config.Configuration;
import com.mohago.serialization.Pair;
import com.mohago.serialization.Parameter;

public class TableIndex {
	private String tableName;
	private int tabIndex; // /table order in dataset.
	private List<Parameter> parameters; 
	private int rowIndex;// /current row for insert purposes.
	private boolean isBuiltAlready = false;
	
	private String inS = "";
	private ArrayList<Pair<String, String>> insertPairs = new ArrayList<Pair<String, String>>();

	private String parsedValue = "";
	private double pd;
	private String ps;
	private Date pdate;
	private int internalIndex;
	private StringBuilder sbNames = new StringBuilder();
	private StringBuilder sbValues = new StringBuilder();
	private StringBuilder sbFin = new StringBuilder();
	
	private String insertString;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

//	public int getIndexMin() {
//		return indexMin;
//	}
//
//	public void setIndexMin(int indexMin) {
//		this.indexMin = indexMin;
//	}

	public boolean isBuiltAlready() {
		return isBuiltAlready;
	}

	public void setBuiltAlready(boolean isBuiltAlready) {
		this.isBuiltAlready = isBuiltAlready;
	}

	public int getTabIndex() {
		return tabIndex;
	}

	public void setTabIndex(int tabIndex) {
		this.tabIndex = tabIndex;
	}

	public List<Parameter> getParameters() {
		return parameters;
	}

	public void setParameters(ArrayList<Parameter> parameters) {
		this.parameters = parameters;
	}

	public int getRowIndex() {
		return rowIndex;
	}

	public void setRowIndex(int rowIndex) {
		this.rowIndex = rowIndex;
	}

	public String getInsertString() {
		return insertString;
	}

	public void setInsertString(String insertString) {
		this.insertString = insertString;
	}

	public String getInS() {
		return inS;
	}

	public void setInS(String inS) {
		this.inS = inS;
	}

	public ArrayList<Pair<String, String>> getInsertPairs() {
		return insertPairs;
	}

	public void setInsertPairs(ArrayList<Pair<String, String>> insertPairs) {
		this.insertPairs = insertPairs;
	}

	public String getParsedValue() {
		return parsedValue;
	}

	public void setParsedValue(String parsedValue) {
		this.parsedValue = parsedValue;
	}

	public double getPd() {
		return pd;
	}

	public void setPd(double pd) {
		this.pd = pd;
	}



	public String getPs() {
		return ps;
	}

	public void setPs(String ps) {
		this.ps = ps;
	}

	public Date getPdate() {
		return pdate;
	}

	public void setPdate(Date pdate) {
		this.pdate = pdate;
	}

	public int getInternalIndex() {
		return internalIndex;
	}

	public void setInternalIndex(int internalIndex) {
		this.internalIndex = internalIndex;
	}

	public StringBuilder getSbNames() {
		return sbNames;
	}

	public void setSbNames(StringBuilder sbNames) {
		this.sbNames = sbNames;
	}

	public StringBuilder getSbValues() {
		return sbValues;
	}

	public void setSbValues(StringBuilder sbValues) {
		this.sbValues = sbValues;
	}

	public StringBuilder getSbFin() {
		return sbFin;
	}

	public void setSbFin(StringBuilder sbFin) {
		this.sbFin = sbFin;
	}
	
	public TableIndex(int tabInd, List<Parameter> prs) {
		parameters = prs;
		//indexMin = min;
		//indexMax = max;
		tabIndex = tabInd;
		int i = 0;
		for (Parameter param : prs) {
			param.setStorageIndex(i);
			param.setParent(this);
			i++;
		}
	}
	//storage indices already set
	public TableIndex(String tableName, int tabIndex, List<Parameter> paramList) {

		for (Parameter param : paramList) {
			param.setParent(this);
		}
		if(parameters == null){
			parameters = new ArrayList<Parameter>();
		}
		this.parameters.addAll(paramList);
		this.setTableName(tableName);
		this.tabIndex = tabIndex;
		isBuiltAlready = true;
	}

	public void buildInsertString() {
		// /build insert statement using insertpairs.
		insertString = "";
		sbNames.setLength(0);
		sbValues.setLength(0);
		sbFin.setLength(0);
		if (!insertPairs.isEmpty()) {
			sbNames.append("( id");
			sbValues.append("(" + rowIndex);

			for (int n = 0; n < insertPairs.size(); n++) {
				sbNames.append(","
						+ "" + ((Pair<String, String>) insertPairs.toArray()[n])
								.getFirst() + "") ;
				sbValues.append(","
						+ ((Pair<String, String>) insertPairs.toArray()[n])
								.getSecond());
			}
			sbNames.append(')');
			sbValues.append(");");

			sbFin.append("INSERT INTO "+ Configuration.Database.MOHAGO_DB_NAME.getValue() + "." + tableName);
			sbFin.append(sbNames.toString());
			sbFin.append(" VALUES");
			sbFin.append(sbValues.toString());
			insertString = sbFin.toString();
		}
		insertPairs.clear();
		// /execute
	}
}
