package com.mohago.lgf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.mohago.utilities.SQLHelpers;
import com.mysql.jdbc.StringUtils;

public class DataFileParser {

	private static final Logger LOG = Logger.getLogger(DataFileParser.class);

	static void parseLgdfFile(DatasetTablesView dtv,
			final String lgmfFileName, char[] delim/*can remove*/, Connection conn) {

		String lgdfFilePath = lgmfFileName.replace(".lgmf", ".lgdf");
		File lgdfFile = new File(lgdfFilePath);
		if (lgdfFile.exists() && !isFileEmpty(lgdfFilePath)) {

			try (BufferedReader reader = new BufferedReader(new FileReader(
					lgdfFilePath))) {
				String line;
				String[] items;
				Statement insertTableData = null;
				try {
					insertTableData = conn.createStatement();
				} catch (SQLException e1) {
					LOG.error("Error creating stmt: " + e1);
				}
				while ((line = reader.readLine()) != null) {
					items = line.split(",");
					dtv.parseDelimitedLineItemsToInsertStatements(
							dtv.getRowIndex(), items);

					// /do insert
					for (TableIndex t : dtv.getTables()) {
						if (!StringUtils.isNullOrEmpty(t.getInsertString())) {
							insertTableData.addBatch((t.getInsertString()));
						}

					}

					dtv.setRowIndex(dtv.getRowIndex() + 1);
				}
				try {
					insertTableData.executeBatch();
					conn.commit();
				} catch (SQLException e) {
					LOG.error("Error trying to execute batch: " + e);
					SQLHelpers.rollBack(conn,
							" execute batch creating parameter table");
				} finally {
					try {
						if (insertTableData != null) {
							insertTableData.close();
						}
					} catch (SQLException e) {
						LOG.error("Error cleaning up" + e);
					}
				}
			} catch (IOException e1) {
				LOG.error("Error parsing lgdf: " + e1);
			} catch (SQLException e1) {
				LOG.error("Error parsing lgdf: " + e1);
			}
			// Move to delete folder
			String delPath = lgdfFilePath.replace(File.separator + "lgdf",
					File.separator + "lgdf" + File.separator + "delete");
			try {
				Files.move(Paths.get(lgdfFilePath), Paths.get(delPath),
						StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				LOG.error("Error moving lgdf file to delete folder after scan: "
						+ e);
			}
		} else {
			LOG.error("Data file empty");
		}

	}

	static boolean isFileEmpty(String filePath) {

		boolean ret = false;
		try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
			ret = br.readLine() == null;
		} catch (IOException e) {
			ret = true;
		}
		return ret;
	}

}
