package com.mohago.lgf;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.log4j.Logger;
import org.javatuples.Quartet;

import com.mohago.config.Configuration;
import com.mohago.request.DatabaseHelpers;
import com.mohago.serialization.Dataset;
import com.mohago.serialization.LookingGlassType;
import com.mohago.serialization.Parameter;
import com.mohago.sql.SQLPreparedStatements;
import com.mohago.utilities.SQLHelpers;

public class DatasetTablesView {

	private static final Logger LOG = Logger.getLogger(DatasetTablesView.class);

	private Long dIdInternal;
	private int rowIndex = 0;

	private ArrayList<Parameter> orderedParams = new ArrayList<Parameter>();

	private ArrayList<TableIndex> tables = new ArrayList<TableIndex>();

	private static final int INTERVAL = 498;// Maximum for mysql, 998 fails if
											// column names are at a maximum (64
											// characters)

	ArrayList<TableIndex> getTables() {
		return tables;
	}

	void setTables(ArrayList<TableIndex> tables) {
		this.tables = tables;
	}

	ArrayList<Parameter> getAllParameters() {
		ArrayList<Parameter> ret = new ArrayList<Parameter>();
		if (tables != null) {
			for (TableIndex table : tables) {
				ret.addAll(table.getParameters());
			}
		}
		return ret;
	}

	Long getDIdInternal() {
		return dIdInternal;
	}

	void setDIdInternal(Long dIdInternal) {
		this.dIdInternal = dIdInternal;
	}

	int getRowIndex() {
		return rowIndex;
	}

	void setRowIndex(int rowIndex) {
		this.rowIndex = rowIndex;
	}

	// / <summary>
	// / build from list of tuples of index, name, type structure.
	// conforming to enum float,int,String,datetime.
	// / Most likely use is parsing lgmf then lgdf file.
	// / Must change to DataSet type from batch
	// / </summary>
	// / <param name="parameters"></param>
	void buildFromExternalSourceTriple(
			final ArrayList<Triple<Integer, String, LookingGlassType>> pars) {
		tables = new ArrayList<TableIndex>();

		ArrayList<Parameter> parameters = new ArrayList<Parameter>();
		for (Triple<Integer, String, LookingGlassType> triple : pars) {
			parameters.add(new Parameter(triple.getLeft(), triple.getMiddle(),
					triple.getRight()));
		}
		// Sorting on dataset index
		Collections.sort(parameters, new Comparator<Parameter>() {
			@Override
			public int compare(Parameter param1, Parameter param2) {

				return Long.compare(param1.getDatasetIndex(),
						param2.getDatasetIndex());
			}
		});

		int period = 0;
		int interval = INTERVAL;
		List<Parameter> tabParams;
		int periodXInterval = 0;
		if (parameters.size() > 1) {
			while (period * interval < parameters.size() - 1) {// Hashtag GAK
				try {
					periodXInterval = period * interval;
					if ((period + 1) * interval < parameters.size()) {
						tabParams = parameters.subList(periodXInterval,
								periodXInterval + interval);
						tables.add(new TableIndex(period, tabParams));
					} else {

						tabParams = parameters.subList(periodXInterval,
								periodXInterval + parameters.size()
										- (periodXInterval));
						tables.add(new TableIndex(period, tabParams));

					}
				} catch (IllegalArgumentException | IndexOutOfBoundsException e) {
					LOG.error("Error parsing lg file on api scan import: " + e);
				}
				period++;
			}
		} else if (parameters.size() == 1) {
			tables.add(new TableIndex(0, parameters));
		}
	}

	/**
	 * 
	 * @param rowIndex
	 * @param itemsAr
	 */
	void parseDelimitedLineItemsToInsertStatements(int rowIndex,
			final String[] itemsAr) {
		ArrayList<String> items = new ArrayList<String>(Arrays.asList(itemsAr));
		if (orderedParams.isEmpty()) {
			orderedParams = this.getAllParameters();
			Collections.sort(orderedParams, new Comparator<Parameter>() {
				@Override
				public int compare(Parameter param1, Parameter param2) {

					return Long.compare(param1.getDatasetIndex(),
							param2.getDatasetIndex());
				}
			});

		}
		int n = 0;
		while (n < items.size() && n < orderedParams.size()) {
			((Parameter) orderedParams.toArray()[n])
					.parseStringAddToTableInsertQueue((String) items.toArray()[n]);
			n++;
		}
		// /set current rowIndex
		for (TableIndex tableIndex : tables) {
			tableIndex.setRowIndex(rowIndex);
			tableIndex.buildInsertString();
		}
	}

	/**
	 * 
	 * 
	 * @param dataset
	 * @param incomingPars
	 *            Array triples of paramIndex,param-name, param type which were
	 *            taken from incoming .lgmf
	 * @param conn
	 */
	///TODO: Refactor! Fragile at the moment but works
	//can get rid of type. Needs to be split up!!
	void buildFromExternalSourceTripleStream(Dataset dataset,
			ArrayList<Triple<Integer, String, LookingGlassType>> incomingPars,
			Connection conn) {

		dataset.setUid(dataset.getParentUid());
		this.rowIndex = DataSetImportManager.getMaxItemsCountFromTables(
				dataset.getUid(), conn);//Is this necessary? could use item_count from Dataset?

		HashMap<String, ArrayList<Parameter>> overlapParamGroup = new HashMap<String, ArrayList<Parameter>>();
		ArrayList<Parameter> newParams = new ArrayList<Parameter>();
		ArrayList<Quartet<String, String, LookingGlassType, Integer>> overlapParams = new ArrayList<Quartet<String, String, LookingGlassType, Integer>>();

		ArrayList<Quartet<String, String, LookingGlassType, Integer>> allExistingParams = DatabaseHelpers
				.getAllTableColumnInfo(dataset.getUid(), conn);//could do join
		for (Triple<Integer, String, LookingGlassType> incomingParam : incomingPars) {//iterates over existing params and checks against incoming
			//divides incoming into: 1. overlapping with existing params, or 2. new parameters
			boolean match = false;
			for (Quartet<String, String, LookingGlassType, Integer> existingParam : allExistingParams) {
				if (incomingParam.getMiddle().equals(existingParam.getValue1())
						&& incomingParam.getRight().equals(
								existingParam.getValue2())) {
					overlapParams.add(existingParam);
					if (!overlapParamGroup.containsKey(existingParam
							.getValue0())) {
						overlapParamGroup.put(existingParam.getValue0(),
								new ArrayList<Parameter>());
					}
					overlapParamGroup.get(existingParam.getValue0()).add(
							new Parameter(existingParam.getValue3(), existingParam
									.getValue1(), existingParam.getValue2(),
									existingParam.getValue3()));//Bug here, dataset uid is not correct here for index
					match = true;
				}

			}
			if (!match) {
				newParams.add(new Parameter(incomingParam
						.getLeft(), incomingParam
						.getMiddle(), incomingParam.getRight(), incomingParam
						.getLeft()));//Same bug here again. Need to sort out difference between storage index and dataset index
			}
		}

		Iterator  it = overlapParamGroup.entrySet().iterator();//Iterate over overlapping parameters
		int i = 0;
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			String tableName = (String) pair.getKey();
			TableIndex temp = new TableIndex(tableName, i,
					(ArrayList<Parameter>) pair.getValue());
			this.tables.add(temp);
			i++;
		}
		int maxTableIndex = i;// current max table index which is used later

		// Sorting on dataset index
		Collections.sort(newParams, new Comparator<Parameter>() {//I think this is defunct //they get resorted anyway
			@Override
			public int compare(Parameter param1, Parameter param2) {

				return Long.compare(param1.getDatasetIndex(),
						param2.getDatasetIndex());
			}
		});

		Triple<Integer, String, Integer> finalTableInfo = DatabaseHelpers
				.getMaxTidTableInfo(dataset.getUid(), conn);

		if (finalTableInfo == null) {// Empty Dataset
			String tableName = SQLHelpers.ranToken(DataSetImportManager.TOKEN_SIZE_TABLE_NAME_AND_GLOBAL_ID) + "a" + dataset.getUid();
			DataSetImportManager.createEmptyObfTable(tableName, conn);
			DataSetImportManager.storeTableIndex(dataset.getUid(), 0, tableName, 0,
					conn);
			finalTableInfo = new ImmutableTriple<Integer, String, Integer>(0,
					tableName, 0);
		}

		List<Parameter> fillFinalTableParams = new ArrayList<Parameter>();

		List<Parameter> remainingParams = new ArrayList<Parameter>();
		if (newParams.size() > (INTERVAL - finalTableInfo.getRight())) {// Fill
																		// up
																		// final
																		// table
			fillFinalTableParams = newParams.subList(0,
					(INTERVAL - finalTableInfo.getRight()));
			remainingParams = newParams.subList(
					(INTERVAL - finalTableInfo.getRight()), newParams.size());
		} else {//Table not filled
			fillFinalTableParams = newParams;
		}

		if (!fillFinalTableParams.isEmpty()) {

			TableIndex temp = null;
			boolean removeTableIndex = false;
			for (TableIndex ti : tables) {
				if (ti.getTableName().equals(finalTableInfo.getMiddle())) {// Does
																			// the
																			// current
																			// TableIndex
																			// list
																			// already
																			// contain
																			// the
																			// final
																			// table?
					temp = ti;

					removeTableIndex = true;
				}
			}
			if (removeTableIndex) {// (Avoiding concurrent mod exceptopn)    //Use iterator instead?
				this.tables.remove(temp);// Remove the TableIndex, alter it,
											// then re-add it later
			} else {
				temp = new TableIndex(finalTableInfo.getMiddle(),
						tables.size(),// Add a new table which will be added to
										// the TableIndex list
						fillFinalTableParams);

			}

			updateFinalTableDuringStream(dataset.getUid(),
					fillFinalTableParams, finalTableInfo.getMiddle(),
					finalTableInfo.getLeft(), finalTableInfo.getRight(), conn);

			for (Parameter param : fillFinalTableParams) {
				param.setParent(temp);
			}
			temp.getParameters().addAll(fillFinalTableParams);
			this.tables.add(temp);// Re-adding (or adding)
			maxTableIndex++;
		}

		int period = 0;
		int interval = INTERVAL;
		List<Parameter> tabParams;
		int periodXInterval = 0;
		if (remainingParams.size() > 1) {
			while (period * interval < remainingParams.size() - 1) {// Hashtag
																	// GAK
				try {
					periodXInterval = period * interval;
					if ((period + 1) * interval < remainingParams.size()) {
						tabParams = remainingParams.subList(periodXInterval,
								periodXInterval + interval);
						tables.add(new TableIndex((tables.size() + 1) + period,
								tabParams));
					} else {

						tabParams = remainingParams.subList(periodXInterval,
								periodXInterval + remainingParams.size()
										- (periodXInterval));
						tables.add(new TableIndex((tables.size() + 1) + period,
								tabParams));

					}
				} catch (IllegalArgumentException | IndexOutOfBoundsException e) {
					LOG.error("Error creating table structure: " + e);
				}
				period++;
			}
		} else if (remainingParams.size() == 1) {//special case
			tables.add(new TableIndex((tables.size() + 1) + 0, remainingParams));
		}

	}

	/**
	 * 
	 * @param did
	 * @param finalParams
	 * @param tableName
	 * @param tId
	 * @param finalTableParamCount
	 * @param conn
	 * @return
	 */
	boolean updateFinalTableDuringStream(Long did,
			List<Parameter> finalParams, String tableName, int tId,
			int finalTableParamCount, Connection conn) {

		boolean ret = false;
		int i = finalTableParamCount;
		String alterString = "";
		String alterStringTemp = "ALTER TABLE "
				+ Configuration.Database.MOHAGO_DB_NAME.getValue() + "."
				+ tableName + " ADD ";

		PreparedStatement prepStmt = null;
		ResultSet reader = null;
		if (conn != null) {
			try {

				prepStmt = conn
						.prepareStatement(SQLPreparedStatements.Insert.INSERT_PARAM_NAME_INDEX
								.getValue());

				for (Parameter param : finalParams) {

					String pseudonym = "p" + i;
					param.setStorageIndex(i);
					prepStmt.setLong(1, did);
					prepStmt.setInt(2, tId);
					prepStmt.setString(3, param.getName());
					prepStmt.setInt(4, i);
					prepStmt.setInt(5, param.getType().getValue());
					prepStmt.addBatch();

					alterString += alterStringTemp
							+ pseudonym
							+ " "
							+ DataSetImportManager.LG_TYPE_DICT
									.get((LookingGlassType) param.getType())
							+ " NULL;";// adding parametr
										// psuedonym and
										// parameter type to
										// table create
										// string
					i++;
				}

				prepStmt.executeBatch();
				ret = true;
				alterString += ";";

			} catch (SQLException e) {
				ret = false;
				LOG.error("updating final table structure: " + e);
				SQLHelpers.rollBack(conn, "updating final table structure");
			} finally {
				SQLHelpers.closeResources(prepStmt, reader);
			}

			Statement createTable = null;
			try {
				createTable = conn.createStatement();
				createTable.execute(alterString);// No need to sql encode as a
													// look up table is now used
				conn.commit();
			} catch (SQLException e1) {
				LOG.error("Error creating stmt: " + e1);
			} finally {
				if (createTable != null) {
					try {
						createTable.close();
					} catch (SQLException e) {
						LOG.error("Error closing stmt: " + e);
					}
				}
			}

			prepStmt = null;
			try {
				if (conn != null) {
					prepStmt = conn
							.prepareStatement(SQLPreparedStatements.Update.UPDATE_PARAM_COUNT_TABLE_INDEX
									.getValue());
					prepStmt.setInt(1, i);
					prepStmt.setInt(2, tId);
					prepStmt.setLong(3, did);
					prepStmt.execute();
					conn.commit();
				}

			} catch (SQLException e) {
				LOG.error("Error updating param count: " + e);
				SQLHelpers.rollBack(conn, "updating param count");
			} finally {
				SQLHelpers.closeResources(prepStmt, null);
			}
		}
		return ret;
	}

}
