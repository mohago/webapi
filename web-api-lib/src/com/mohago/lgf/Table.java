package com.mohago.lgf;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.javatuples.Pair;

import com.mohago.serialization.LookingGlassType;

public class Table {
	private static final Logger LOG = Logger.getLogger(Table.class);
	
	private String name;
    private int parameterCount;
    private List<Pair<String, LookingGlassType>> columns;
    private List<Row> rows;

    private int maxId;
    private int minId;

    private String nullDelimitedString;
    
    public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getParameterCount() {
		if (columns != null)
		{ 
			return columns.size(); 
			} else {
				return -1; 
				}
	}
	public void setParameterCount(int parameterCount) {
		this.parameterCount = parameterCount;
	}
	public List<Pair<String, LookingGlassType>> getColumns() {
		return columns;
	}
	public void setColumns(List<Pair<String, LookingGlassType>> columns) {
		this.columns = columns;
	}
	public List<Row> getRows() {
		return rows;
	}
	public void setRows(List<Row> rows) {
		this.rows = rows;
	}
	public int getMaxId() {
		int max = Integer.MIN_VALUE;
		if (!rows.isEmpty()) {
			
			for(Row row : rows){
				if (row.getId() > max){
					max = row.getId();
				}
			}
			
			} 
		return max;
	}

	public int getMinId() {
		int min = Integer.MAX_VALUE;
		if (!rows.isEmpty()) {
			
			for(Row row : rows){
				if (row.getId() < min){
					min = row.getId();
				}
			}
			
			}
		return min;
	}
	
	public Table(String name, List<Pair<String, LookingGlassType>> columns){
		this.setName(name);
		this.setColumns(columns);
	}
	public String getNullDelimitedString() {
		return nullDelimitedString;
	}
	public void setNullDelimitedString(String nullDelimitedString) {
		this.nullDelimitedString = nullDelimitedString;
	}

    public void clearRowBuffer()
    {
        if (rows != null)
        {
            rows.clear();
        }
    }
    public String returnDelimetedRow(int rowID)
    {

        StringBuilder sb = new StringBuilder();
        boolean containsRowId = false;
        Row current = null;
        for(Row row : rows){
        	if(row.getId() == rowID){
        		containsRowId = true;
        		current = row;
        	}
        }
        if (containsRowId)
        {
            int c = current.getItems().length;
            for (int n = 0; n < c; n++)
            {
                if (current.getItems()[n] != null)
                {
                    if (((Pair<String, LookingGlassType>)columns.toArray()[n]).getValue1() == LookingGlassType.date_time)
                    {
                        if (current.getItems()[n] != null)
                        {
                        	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");
                        	Date date = null;
							try {
								date = sdf.parse(((String)current.getItems()[n]));
							} catch (ParseException e) {
								LOG.error("Date parse e: " + e);
							}
                            sb.append(sdf.format(date));
                        } 
                    }
                    else
                    {
                        sb.append(current.getItems()[n].toString());
                    }
                } sb.append(",");
            }
            return sb.toString ();
        }
        else
        {
            if (nullDelimitedString == null)
            {
                for (int i = 0; i < parameterCount; i++) { sb.append(","); }
                nullDelimitedString = sb.toString ();
            }
            return nullDelimitedString;
        }

    }
	

}
