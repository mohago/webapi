package com.mohago.request;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.HashMap;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.log4j.Logger;
import org.javatuples.Quartet;
import org.javatuples.Triplet;
import org.joda.time.DateTime;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.mohago.config.Configuration;
import com.mohago.json.DataReturnObject;
import com.mohago.json.DateRange;
import com.mohago.json.Point;
import com.mohago.serialization.LookingGlassType;
import com.mohago.serialization.Pair;
import com.mohago.sql.SQLPreparedStatements;
import com.mohago.utilities.Constants;
import com.mohago.utilities.Constants.ErrorCode;
import com.mohago.utilities.SQLHelpers;

public class DatabaseHelpers {//Need to refactor

	private static final Logger LOG = Logger.getLogger(DatabaseHelpers.class);
	private static final String GET_VALUES = SQLPreparedStatements.Select.GET_VALUES
			.getValue();

	/**
	 * Get a list of tuples containing table-name, parameter-name,
	 * parameter-type, parameter-index for every parameter in a specified table
	 * 
	 * @param tableName
	 * @param conn
	 * @return List of table-name, param-name, type, param-index
	 */
	public static ArrayList<Quartet<String, String, LookingGlassType, Integer>> getTableInfo(
			String tableName, final Connection conn) {

		Pair<Integer, Long> tIdDid = DatabaseHelpers.getTidDidFromObfName(
				tableName, conn);
		ArrayList<Quartet<String, String, LookingGlassType, Integer>> ret = new ArrayList<Quartet<String, String, LookingGlassType, Integer>>();
		ResultSet resultSet = null;
		PreparedStatement getTableInfo = null;
		try {
			getTableInfo = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_NAME_TYPE_INDEX
							.getValue());
			getTableInfo.setInt(1, tIdDid.getFirst());
			getTableInfo.setLong(2, tIdDid.getSecond());
			resultSet = getTableInfo.executeQuery();
			conn.commit();
			while (resultSet.next()) {
				ret.add(new Quartet<String, String, LookingGlassType, Integer>(
						tableName, resultSet.getString(1), LookingGlassType
								.values()[resultSet.getInt(2)], resultSet
								.getInt(3)));
			}
		} catch (SQLException e) {
			LOG.error("Error getting table info: " + e);
		} finally {
			SQLHelpers.closeResources(getTableInfo, resultSet);
		}
		return ret;
	}

	/**
	 * 
	 * @param did
	 * @param conn
	 * @return triple: left is t_id, middle is table name, right is param-count
	 */
	public static Triple<Integer, String, Integer> getMaxTidTableInfo(Long did,
			Connection conn) {
		PreparedStatement prepStatement = null;
		ResultSet reader = null;
		Triple<Integer, String, Integer> ret = null;
		try {
			prepStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_MAX_TID_FOR_DID
							.getValue());
			prepStatement.setLong(1, did);
			reader = prepStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				ret = new ImmutableTriple<Integer, String, Integer>(
						reader.getInt("t_id"), reader.getString("table_name"),
						reader.getInt("parameter_count"));
			}
		} catch (SQLException e1) {

			LOG.error("Error retrieving name: " + e1);
		} finally {
			SQLHelpers.closeResources(prepStatement, reader);
		}
		return ret;// This will be null if empty dataset was created

	}

	/**
	 * Get a corresponding Table Id - Dataset Id pair for a given obfuscated
	 * table name
	 * 
	 * @param obfName
	 * @param conn
	 * @return
	 */
	public static Pair<Integer, Long> getTidDidFromObfName(String obfName,
			Connection conn) {
		PreparedStatement getTid = null;
		ResultSet reader = null;
		Pair<Integer, Long> ret = null;
		try {
			getTid = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_TID_DID_FROM_OBF_NAME
							.getValue());
			getTid.setString(1, obfName);
			LOG.info("Retrieving name");
			reader = getTid.executeQuery();
			conn.commit();
			while (reader.next()) {
				ret = new Pair(reader.getInt("t_id"), reader.getLong("d_id"));
			}
		} catch (SQLException e1) {

			LOG.error("Error retrieving name: " + e1);
		} finally {
			SQLHelpers.closeResources(getTid, reader);
		}
		return ret;

	}

	/**
	 * Return a list of filename-associated hash pairs which represent the Data
	 * that make up a Dataset
	 * 
	 * @param did
	 * @param conn
	 * @return
	 */
	public static ArrayList<Pair<String, String>> getHashes(Long did,
			Connection conn) {
		PreparedStatement getHahes = null;
		ResultSet reader = null;
		ArrayList<Pair<String, String>> hashes = new ArrayList<Pair<String, String>>();
		try {
			SQLHelpers.setSchema(Configuration.Database.DATABASE.getValue(),
					conn);
			getHahes = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_HASHES
							.getValue());
			getHahes.setLong(1, did);

			LOG.info("Retrieving hashes");

			reader = getHahes.executeQuery();
			conn.commit();
			while (reader.next()) {
				hashes.add(new Pair<String, String>(reader.getString("file"),
						reader.getString("hash")));
			}
		} catch (SQLException e1) {

			LOG.error("Error retrieving batch info: " + e1);
		} finally {
			SQLHelpers.closeResources(getHahes, reader);

		}
		return hashes;
	}

	/**
	 * Get all table names (obfuscated) that are associated with a Dataset Id
	 * 
	 * @param dId
	 * @param conn
	 * @return
	 */
	public static ArrayList<String> getTableNames(Long dId, Connection conn) {

		ArrayList<String> ret = new ArrayList<String>();
		PreparedStatement getTableNames = null;
		ResultSet reader = null;
		LOG.info("Getting table names");
		try {

			getTableNames = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_TABLE_NAMES
							.getValue());
			getTableNames.setLong(1, dId);
			reader = getTableNames.executeQuery();
			conn.commit();
			while (reader.next()) {
				ret.add(reader.getString("table_name").trim());
			}

		} catch (SQLException e) {
			LOG.error("Error getting table names: " + e);
		} finally {
			SQLHelpers.closeResources(getTableNames, reader);
		}
		return ret;
	}

	/**
	 * 
	 * 
	 * @param dId
	 * @param conn
	 * @return List of table-name, param-name, type, param-index for all tables
	 *         for that did
	 */
	public static ArrayList<Quartet<String, String, LookingGlassType, Integer>> getAllTableColumnInfo(
			Long dId, Connection conn) {

		ArrayList<Quartet<String, String, LookingGlassType, Integer>> ret = new ArrayList<Quartet<String, String, LookingGlassType, Integer>>();

		ArrayList<String> tableNameList = getTableNames(dId, conn);

		for (String tableName : tableNameList) {

			ret.addAll(getTableInfo(tableName, conn));
		}
		return ret;
	}

	@Deprecated
	public static ArrayList<Triplet<Integer, Double, Double>> getIDXYDoubleValues(
			String paramNameX, String paramNameY, Long did, Connection conn,
			PrintWriter printWriter) {

		ArrayList<Triplet<Integer, Double, Double>> ret = new ArrayList<Triplet<Integer, Double, Double>>();
		Pair<Integer, String> tIdPseudonymX = getTidPseudonymFromNameType(
				paramNameX, LookingGlassType.double_precision, did, conn);
		String obfNameX = getObfNameFromTid(tIdPseudonymX.getFirst(), did, conn);
		Pair<Integer, String> tIdPseudonymY = getTidPseudonymFromNameType(
				paramNameY, LookingGlassType.double_precision, did, conn);
		String obfNameY = getObfNameFromTid(tIdPseudonymY.getFirst(), did, conn);

		PreparedStatement getValues = null;
		ResultSet reader = null;
		String sql = "";
		try {

			if (obfNameX.equals(obfNameY)) {
				sql = SQLPreparedStatements.Select.GET_ID_X_Y_VALUES_SAME_TABLE
						.getValue();

			} else {
				sql = SQLPreparedStatements.Select.GET_ID_X_Y_VALUES_DIFF_TABLE
						.getValue();
			}
			sql = sql.replace("@param1", paramNameX)
					.replace("@param2", paramNameY).replace("@table", obfNameX)
					.replace("@obf1", obfNameX).replace("@obf2", obfNameY);
			getValues = conn.prepareStatement(sql.toString());
			reader = getValues.executeQuery();
			conn.commit();
			if (reader != null) {
				while (reader.next()) {
					ret.add(new Triplet<Integer, Double, Double>(Integer
							.valueOf(reader.getInt(1)), Double.valueOf(reader
							.getDouble(2)), Double.valueOf(reader.getDouble(3))));
				}
			}
		} catch (SQLException e1) {
			LOG.error("Error retrieving triplet xy values: " + e1);
		} finally {
			SQLHelpers.closeResources(getValues, reader);
		}
		return ret;

	}

	public static int maxId(String tableName, Connection conn) {
		int ret = -1;
		PreparedStatement preparedStatement = null;
		ResultSet reader = null;
		try {

			preparedStatement = conn.prepareStatement(String.format(
					SQLPreparedStatements.Select.GET_ITEM_COUNT.getValue(),
					tableName));
			reader = preparedStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				ret = reader.getInt(1);
			}

		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
		}
		return ret;
	}

	// TODO add a return id boolean option
	public static Object getValues(
			ArrayList<Pair<String, LookingGlassType>> parameters,
			DateRange dateRange, int offset, int limit, boolean returnId, Long did,
			Connection conn) {
		ArrayList<JsonArray> ret = new ArrayList<JsonArray>();
		ArrayList<Triple<Integer, String, String>> validPseudoParams = new ArrayList<Triple<Integer, String, String>>();
		String paramSelectStatement = "";
		if (parameters != null && parameters.size() > 0) {

			// /get all valid parameters.
			for (Pair<String, LookingGlassType> param : parameters) {
				Pair<Integer, String> pseudoParam = getTidPseudonymFromNameType(
						param.getFirst(), param.getSecond(), did, conn);
				if ((pseudoParam.getFirst()) != -1) {
					ImmutableTriple triple = new ImmutableTriple<Integer, String, String>(
							pseudoParam.getFirst(),
							pseudoParam.getSecond(),
							getObfNameFromTid(pseudoParam.getFirst(), did, conn));
					validPseudoParams.add(triple);
					// paramSelectStatement+=pseudoParam.getSecond()+",";
				} else {
					// /throw new
					// MohagoException(404,"Requested parameter not found");
					return null;
				}
			}
			if (paramSelectStatement.length() > 1) {
				paramSelectStatement = paramSelectStatement.substring(0,
						paramSelectStatement.length() - 1);
			} else {
				paramSelectStatement = "*";
			}

		} else {
			paramSelectStatement = "*";
		}

		PreparedStatement getValues = null;
		ResultSet reader = null;
		String sql = "";
		try {
			sql = SQLPreparedStatements.Select.SELECT_CHARTV2_WHERE.getValue();

		} catch (Exception e1) {
			LOG.error("Error retrieving pair xy values: " + e1);
		} finally {
			SQLHelpers.closeResources(getValues, reader);
		}

		return ret;
	}

	// TODO add a return id boolean option
	public static Object getValues(String[] parameters,
			DateRange dateRange, int offset, int limit, boolean returnId, Long datasetId,
			Connection conn) {
		String connCat = "";
		try {
			connCat = conn.getCatalog();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JsonObject ret = null;
		ArrayList<Triple<String, String, Integer>> validPseudoParams = new ArrayList<Triple<String, String, Integer>>();
		String paramList = "";
		String tablesList = "";
		String whereCondition = "";
		JsonObject metaInfo = null;

		HashMap<String, ArrayList<String>> hashMap = new HashMap<String, ArrayList<String>>();
		if (parameters != null && parameters.length > 0) {
			for (String param : parameters) {
				// /returns (obfTableName,pseudonym)
				Triple<String, String, Integer> pseudoParam = getObfNamePseudonymFromName(
						param, datasetId, conn);
				if (pseudoParam.getLeft() != "") {
					validPseudoParams.add(pseudoParam);
				} else {
					// /throw new
					// MohagoException(404,"Requested parameter not found");
					return null;
				}
			}

			// /distinct table locations
			for (Triple<String, String, Integer> param : validPseudoParams) {
				if (!hashMap.containsKey(param.getLeft())) {
					ArrayList<String> list = new ArrayList<String>();
					list.add(param.getMiddle());
					hashMap.put(param.getLeft(), list);
				} else {
					hashMap.get(param.getLeft()).add(param.getMiddle());
				}
			}

			// /build tables list
			String[] keys = hashMap.keySet().toArray(new String[0]);
			if (keys.length > 0) {
				for (int n = 0; n < keys.length; n++) {
					if (n == 0) {
						tablesList += "mohago_db_3." + keys[n] + " ";
					} else {
						tablesList += "INNER JOIN mohago_db_3." + keys[n]
								+ " ON (mohago_db_3." + keys[n - 1]
								+ ".id = mohago_db_3." + keys[n] + ".id) ";
					}
				}
			} else {
				return null; // TODO internal error
			}

			// /write params to the paramsList
			for (Triple<String, String, Integer> param : validPseudoParams) {
				paramList += (param.getLeft() + "." + param.getMiddle() + ",");
			}
			paramList = paramList.substring(0, paramList.length() - 1); // /trim
																		// the
																		// trailing
																		// comma
																		// from
																		// paramsList
		} else { // /should do get all, i.e. populate validPseudoParams with all
					// params and do SELECT.
			return null; // bad request
		}

		// /build the where condition
		// /precedence is given to Date object over offset & limit pair
		if (dateRange != null) {
			// /get the date params psuedo param.
			Triple<String, String, Integer> pseudoParam = getObfNamePseudonymFromName(
					dateRange.getDateParamName(), datasetId, conn);
			if (pseudoParam.getLeft() == "" || pseudoParam.getRight() != 3) {// not
																				// found,
																				// or
																				// is
																				// not
																				// of
																				// date
																				// type.
																				// //TODO
																				// untested
				return null;
			}
			if (!(hashMap.containsKey(pseudoParam.getLeft()))) {// /does this
																// params
																// obfname exist
																// in the param
																// map? If not
																// add it to the
																// tables list.
				// /if not add it to dbList
				tablesList += "," + pseudoParam.getLeft();
			}
			String obfPseudo = pseudoParam.getLeft() + "."
					+ pseudoParam.getMiddle();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			whereCondition = obfPseudo + " > '"
					+ sdf.format(dateRange.getDateTimeStart()) + "' AND "
					+ obfPseudo + " < '"
					+ sdf.format(dateRange.getDateTimeEnd()) + "'";
		} else {
			// maxId = MaxId(,conn)
			if (limit > -1 && offset > -1) {
				whereCondition = " id >= " + Integer.toString(offset)
						+ " AND id <=" + Integer.toString(limit + offset - 1);
				StringBuilder nameBuilder = new StringBuilder();
				for (String n : parameters) {
					nameBuilder.append(n).append(",");
				}
				nameBuilder.deleteCharAt(nameBuilder.length() - 1);
				int max = maxId(validPseudoParams.get(0).getLeft(), conn);
				metaInfo = new JsonObject();

				if (!((offset + limit) > max)) {
					metaInfo.addProperty("next", "?datasetId=" + datasetId
							+ "&parameters=" + nameBuilder.toString()
							+ "&offset=" + (offset + limit) + "&limit=" + limit);
				}
				if ((offset - limit) >= 0) {
					metaInfo.addProperty("prev", "?datasetId=" + datasetId
							+ "&parameters=" + nameBuilder.toString()
							+ "&offset=" + (offset - limit) + "&limit=" + limit);
				}
				metaInfo.addProperty("first", "?datasetId=" + datasetId
						+ "&parameters=" + nameBuilder.toString() + "&offset="
						+ (0) + "&limit=" + limit);
				int lastLimit = max % limit;
				int lastOffset = max - lastLimit;
				metaInfo.addProperty("last", "?datasetId=" + datasetId
						+ "&parameters=" + nameBuilder.toString() + "&offset="
						+ lastOffset + "&limit=" + lastLimit);

			} else if (limit > -1) {
				// /not supported
			} else if (offset > -1) {
				// /not supported
			}
		}

		if (returnId) {
			validPseudoParams.add(0, new ImmutableTriple("obfName", "id", 0));
			paramList = "id," + paramList;
		}

		PreparedStatement getValues = null;
		ResultSet reader = null;
		String sql = "";
		try {
			if (whereCondition == "") {
				sql = SQLPreparedStatements.Select.SELECT_CHARTV2.getValue();
				sql = sql.replace("@params", paramList).replace("@tables",
						tablesList);

			} else {
				sql = SQLPreparedStatements.Select.SELECT_CHARTV2_WHERE
						.getValue();
				sql = sql.replace("@params", paramList)
						.replace("@tables", tablesList)
						.replace("@where", whereCondition);
			}

			getValues = conn.prepareStatement(sql.toString());
			reader = getValues.executeQuery();
			conn.commit();
			int n = 1;
			if (reader != null) {
				Object val;
				Collection retCol = new ArrayList();
				while (reader.next()) {
					Collection rowCol = new ArrayList();
					n = 1;
					for (Triple<String, String, Integer> param : validPseudoParams) {
						if ((val = reader.getObject(n)) != null) {
							switch (param.getRight()) {
							case 0:
								rowCol.add(Integer.parseInt(val.toString()));
								break;
							case 1:
								rowCol.add(Double.parseDouble(val.toString()));
								break;
							case 3:
								DateTime dt = new DateTime(val.toString()
										.replace(' ', 'T'));
								rowCol.add(dt.getMillis());
								break;
							case 2:
								rowCol.add(val.toString());
								break;
							}
						} else {
							rowCol.add(null);
						}
						n++;
					}
					retCol.add(rowCol);
				}
				/*
				 * String[] tableNames = new String[validPseudoParams.size()];
				 * for( n = 0;n<validPseudoParams.size();n++){ tableNames[n] =
				 * validPseudoParams.get(n).getMiddle(); }
				 */

				if (returnId) {
					String[] parameters2 = new String[parameters.length + 1];
					parameters2[0] = "id";
					n = 1;
					for (String param : parameters) {
						parameters2[n] = param;
						n++;
					}
					parameters = parameters2;
				}
				ret = new JsonObject();
				Gson gson = new Gson();
				ret.add("data", gson.toJsonTree(retCol));
				ret.add("labels", gson.toJsonTree(parameters));
				if (metaInfo != null) {
					//ret.add("metaInfo", metaInfo);
				}
				// ret = new DataReturnObject(parameters,retCol,paginationURL);
			}
		} catch (Exception e1) {
			LOG.error("Error retrieving requested chart data from db." + e1);
		} finally {
			SQLHelpers.closeResources(getValues, reader);
		}
		return ret;
	}

	@Deprecated
	public static ArrayList<Point<Number, Number>> getXYValues(
			String paramNameX, LookingGlassType lgTypeX, String paramNameY,
			LookingGlassType lgTypeY, Long did, Connection conn) {

		ArrayList<Point<Number, Number>> nArray = new ArrayList<Point<Number, Number>>();
		Pair<Integer, String> tIdPseudonymX = getTidPseudonymFromNameType(
				paramNameX, lgTypeX, did, conn);
		String obfNameX = getObfNameFromTid(tIdPseudonymX.getFirst(), did, conn);
		Pair<Integer, String> tIdPseudonymY = getTidPseudonymFromNameType(
				paramNameY, lgTypeY, did, conn);
		String obfNameY = getObfNameFromTid(tIdPseudonymY.getFirst(), did, conn);

		PreparedStatement getValues = null;
		ResultSet reader = null;
		String sql = "";
		try {

			if (obfNameX.equals(obfNameY)) {
				sql = SQLPreparedStatements.Select.GET_X_Y_VALUES_SAME_TABLE
						.getValue();
			} else {
				sql = SQLPreparedStatements.Select.GET_X_Y_VALUES_DIFF_TABLE
						.getValue();
			}
			sql = sql.replace("@param1", tIdPseudonymX.getSecond())
					.replace("@param2", tIdPseudonymY.getSecond())
					.replace("@table", obfNameX).replace("@obf1", obfNameX)
					.replace("@obf2", obfNameY);
			if (lgTypeX.equals(LookingGlassType.date_time)) {
				sql = sql.replaceFirst(
						obfNameX + "." + tIdPseudonymX.getSecond(), "to_char("
								+ obfNameX + "." + tIdPseudonymX.getSecond()
								+ ", 'YYYY-MM-DD HH24:MI:SS.MS')");
			}
			if (lgTypeY.equals(LookingGlassType.date_time)) {
				sql = sql.replaceFirst(
						obfNameY + "." + tIdPseudonymY.getSecond(), "to_char("
								+ obfNameY + "." + tIdPseudonymY.getSecond()
								+ ", 'YYYY-MM-DD HH24:MI:SS.MS')");
			}
			getValues = conn.prepareStatement(sql.toString());
			reader = getValues.executeQuery();
			conn.commit();

			if (reader != null) {
				while (reader.next()) {

					Number val1 = null, val2 = null;
					switch (lgTypeX) {
					case integer:
						val1 = reader.getInt(1);
						break;
					case double_precision:
						val1 = reader.getDouble(1);
						break;
					case date_time:
						DateTime dt = new DateTime(reader.getString(1).replace(
								' ', 'T'));
						val1 = dt.getMillis();
						break;
					case string_value:
						val1 = null;
						break;
					default:
						break;
					}
					switch (lgTypeY) {
					case integer:
						val2 = reader.getInt(2);
						break;
					case double_precision:
						val2 = reader.getDouble(2);
						break;
					case date_time:
						DateTime dt = new DateTime(reader.getString(1).replace(
								' ', 'T'));
						val1 = dt.getMillis();
						break;
					case string_value:
						val2 = null;
						break;
					default:
						break;
					}
					Point<Number, Number> entry = new Point<Number, Number>(
							val1, val2);
					nArray.add(entry);
				}
			}
		} catch (SQLException e1) {
			LOG.error("Error retrieving pair xy values: " + e1);
		} finally {
			SQLHelpers.closeResources(getValues, reader);
		}
		return nArray;
	}

	/**
	 * Retrieve an indexed list of values for a parameter
	 * 
	 * @param paramName
	 * @param lgType
	 * @param did
	 * @param startIndex
	 * @param endIndex
	 * @param conn
	 * @return
	 */
	public static JsonObject getValues(String paramName,
			LookingGlassType lgType, Long did, Integer startIndex,
			Integer endIndex, Connection conn) {
		Pair<Integer, String> tIdPseudonym = getTidPseudonymFromNameType(
				paramName, lgType, did, conn);
		JsonObject ret = new JsonObject();
		String obfName = getObfNameFromTid(tIdPseudonym.getFirst(), did, conn);

		PreparedStatement getValues = null;
		ResultSet reader = null;
		JsonArray jList = null;
		int rowCount = -1;
		int resultCount = 0;
		try {

			LOG.info("Retrieving \"duid, data\" values");
			String getValuesPreFormat = (lgType
					.equals(LookingGlassType.date_time)) ? SQLPreparedStatements.Select.GET_TIMESTAMP_VALUES
					.getValue() : GET_VALUES;
			String sql = String
					.format(getValuesPreFormat,
							tIdPseudonym.getSecond(),
							Configuration.Database.MOHAGO_DB_NAME.getValue()
									+ "."
									+ SQLHelpers
											.removeNonAlphaNumericOrUnderscoreChars(obfName));

			getValues = conn.prepareStatement(sql);
			// getValues.setInt(1, 0);
			if (lgType.equals(LookingGlassType.date_time)) {
				rowCount = SQLHelpers.getRowCount(getValues, "id,to_char("
						+ tIdPseudonym.getSecond()
						+ ",'YYYY-MM-DD HH24:MI:SS.MS')", conn);// Need this for
																// second part
			} else {
				rowCount = SQLHelpers.getRowCount(getValues, "id,"
						+ tIdPseudonym.getSecond(), conn);
			}
			SQLHelpers.closeResources(getValues, null);
			getValuesPreFormat = (lgType.equals(LookingGlassType.date_time)) ? SQLPreparedStatements.Select.GET_SPECIFIED_TIMESTAMP_VALUES
					.getValue()
					: SQLPreparedStatements.Select.GET_SPECIFIED_VALUES
							.getValue();
			sql = String
					.format(getValuesPreFormat,
							tIdPseudonym.getSecond(),
							Configuration.Database.MOHAGO_DB_NAME.getValue()
									+ "."
									+ SQLHelpers
											.removeNonAlphaNumericOrUnderscoreChars(obfName));
			getValues = conn.prepareStatement(sql);

			if (startIndex != null) {
				getValues.setInt(1, startIndex);
			} else {
				getValues.setInt(1, 0);
			}
			if (endIndex != null) {
				getValues.setInt(2, endIndex);
			} else {
				getValues.setInt(2, rowCount);
			}

			reader = getValues.executeQuery();

			conn.commit();
			jList = new JsonArray();

			if (lgType.equals(LookingGlassType.double_precision)
					|| lgType.equals(LookingGlassType.floating_point)) {

				if (reader != null) {
					while (reader.next()) {

						JsonArray entry = new JsonArray();
						entry.add(new JsonPrimitive(reader.getInt(1)));
						entry.add(new JsonPrimitive(reader.getDouble(2)));
						if (reader.wasNull()) {
							entry = new JsonArray();
							entry.add(new JsonPrimitive(reader.getInt(1)));
							entry.add(null);
						}
						jList.add(entry);

						resultCount++;
					}
				}
			}

			else if (lgType.equals(LookingGlassType.integer)) {

				if (reader != null) {
					while (reader.next()) {
						JsonArray entry = new JsonArray();
						entry.add(new JsonPrimitive(reader.getInt(1)));
						entry.add(new JsonPrimitive(reader.getInt(2)));
						jList.add(entry);
						resultCount++;
					}
				}
			}

			else if (lgType.equals(LookingGlassType.string_value)) {

				if (reader != null) {
					while (reader.next()) {
						JsonArray entry = new JsonArray();
						entry.add(new JsonPrimitive(reader.getInt(1)));
						entry.add(new JsonPrimitive(reader.getString(2)));
						jList.add(entry);
						resultCount++;
					}
				}
			} else if (lgType.equals(LookingGlassType.date_time)) {

				if (reader != null) {
					while (reader.next()) {

						JsonArray entry = new JsonArray();
						entry.add(new JsonPrimitive(reader.getInt(1)));
						entry.add(new JsonPrimitive(reader.getTimestamp(2)
								.toString()));
						jList.add(entry);
						resultCount++;
					}
				}
			}
		} catch (SQLException e1) {
			LOG.error("Error retrieving \"duid, data\" values: " + e1);
		} finally {
			SQLHelpers.closeResources(getValues, reader);
		}
		JsonObject metaInfo = new JsonObject();
		metaInfo.addProperty("rowCount", resultCount);
		metaInfo.addProperty("totalRowCount", rowCount);
		ret.add("metaInfo", metaInfo);
		ret.add("content", jList);
		return ret;

	}

	private static Triple<String, String, Integer> getObfNamePseudonymFromName(
			String paramName, Long did, Connection conn) {
		Triple<String, String, Integer> ret = new ImmutableTriple("", "", -1);
		PreparedStatement prepStatement = null;
		ResultSet reader = null;
		try {
			prepStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_OBF_NAME_PSEUDONYM_FROM_NAME
							.getValue());

			prepStatement.setString(1, paramName);
			prepStatement.setLong(2, did);
			LOG.info("Retrieving");
			reader = prepStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				ret = new ImmutableTriple(reader.getString("table_name"), "p"
						+ String.valueOf(reader.getInt("param_index")),
						reader.getInt("type"));
			}
		} catch (SQLException e1) {

			LOG.error("Error retrieving: " + e1);
		} finally {
			SQLHelpers.closeResources(prepStatement, reader);
		}
		return ret;
	}

	private static Pair<Integer, String> getTidPseudonymFromNameType(
			String paramName, LookingGlassType lgType, Long did, Connection conn) {
		Pair<Integer, String> ret = new Pair<Integer, String>(-1, "");

		PreparedStatement prepStatement = null;
		ResultSet reader = null;
		try {
			prepStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_TID_PSEUDONYM_FROM_NAME_TYPE
							.getValue());
			prepStatement.setString(1, paramName);
			prepStatement.setInt(2, lgType.getValue());
			prepStatement.setLong(3, did);
			LOG.info("Retrieving");
			reader = prepStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				ret = new Pair(reader.getInt("t_id"), "p"
						+ String.valueOf(reader.getInt("param_index")));
			}
		} catch (SQLException e1) {

			LOG.error("Error retrieving: " + e1);
		} finally {
			SQLHelpers.closeResources(prepStatement, reader);
		}

		return ret;

	}

	/**
	 * Find an obfuscated table name of given it's t_id and d_id.
	 * 
	 * @param tid
	 * @param did
	 * @param conn
	 * @return obfuscated table name if found or else empty string
	 */
	private static String getObfNameFromTid(int tid, Long did, Connection conn) {
		String ret = "";

		PreparedStatement prepStatement = null;
		ResultSet reader = null;
		try {
			prepStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_OBF_NAME_FROM_TID
							.getValue());
			prepStatement.setInt(1, tid);
			prepStatement.setLong(2, did);
			LOG.info("Retrieving");
			reader = prepStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				ret = reader.getString("table_name");
			}

		} catch (SQLException e1) {

			LOG.error("Error retrieving: " + e1);
		} finally {
			SQLHelpers.closeResources(prepStatement, reader);
		}

		return ret;

	}



	/**
	 * Checks if Dataset exists
	 * 
	 * @param globalId
	 * @param conn
	 * @return boolean representing existing of Dataset
	 */
	public static boolean doesDatasetExist(Long dId, Connection conn) {
		int ret = -1;
		PreparedStatement preparedStatement = null;
		ResultSet reader = null;
		try {

			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.DOES_DATASET_EXIST
							.getValue());
			preparedStatement.setLong(1, dId);
			reader = preparedStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				ret = reader.getInt(1);
			}
		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
		}
		return ret > 0;
	}

	public static Long getDidFromDeviceToken(String deviceToken, Connection conn) {
		Long ret = -1l;
		PreparedStatement preparedStatement = null;
		ResultSet reader = null;
		try {

			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_DID_FROM_DEVICE_TOKEN
							.getValue());
			preparedStatement.setString(1, deviceToken);
			reader = preparedStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				ret = reader.getLong("d_id");
			}
		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
		}
		return ret;
	}

	public static String getDeviceTokenFromDid(Long did, Connection conn) {
		String ret = "";
		PreparedStatement preparedStatement = null;
		ResultSet reader = null;
		try {

			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_DEVICE_TOKEN_FROM_DID
							.getValue());
			preparedStatement.setLong(1, did);
			reader = preparedStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				ret = reader.getString("device_token");
			}
		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
		}
		return ret;
	}

}
