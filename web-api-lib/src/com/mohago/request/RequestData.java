package com.mohago.request;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.javatuples.Quartet;
import org.joda.time.DateTime;

import com.mohago.config.Configuration;
import com.mohago.serialization.CollectionBase;
import com.mohago.serialization.Dataset;
import com.mohago.serialization.LookingGlassType;
import com.mohago.serialization.ParameterCollection;
import com.mohago.serialization.ParameterCollection.ObjectOrigin;
import com.mohago.sql.SQLPreparedStatements;
import com.mohago.utilities.Constants;
import com.mohago.utilities.SQLHelpers;

public class RequestData {

	private static final Logger LOG = Logger.getLogger(RequestData.class);

	/**
	 * Get a list of parameter name-type pairs. There is only one parameter name
	 * allowed per type.
	 * 
	 * @param globalId
	 * @param conn
	 * @return HashMap of name-LookingGlassType entries
	 */
	public static HashMap<String, LookingGlassType> getParamNameTypeMap(
			Long did, Connection conn) {
		HashMap<String, LookingGlassType> ret = new HashMap<String, LookingGlassType>();
		PreparedStatement preparedStatement = null;
		ResultSet reader = null;
		try {

			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_NAME_TYPE
							.getValue());
			preparedStatement.setLong(1, did);
			reader = preparedStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				ret.put(reader.getString(1),
						LookingGlassType.values()[reader.getInt(2)]);
			}

		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
		}
		return ret;
	}

	/**
	 * Builds a collection base object which contains child collections (4, one
	 * for each LG Type) which contain entries for every parameter of that type:
	 * Table name, Parameter name, Table index, LG Type, instance and origin
	 * 
	 * @param did
	 * @param conn
	 * @return
	 */
	public static CollectionBase buildCollectionBase(Long did, Connection conn) {
		CollectionBase collectionBase = new CollectionBase();
		ArrayList<Quartet<String, String, LookingGlassType, Integer>> colInfoList = DatabaseHelpers
				.getAllTableColumnInfo(did, conn);

		for (Quartet<String, String, LookingGlassType, Integer> parameter : colInfoList) {

			if (parameter.getValue2() == LookingGlassType.double_precision) {
				ParameterCollection<Double> pd = new ParameterCollection<Double>(
						parameter.getValue0(), parameter.getValue1(),
						parameter.getValue3(),
						LookingGlassType.double_precision, 0,
						ObjectOrigin.server);
				collectionBase.getParamDoubles().add(pd);
			} else if (parameter.getValue2() == LookingGlassType.integer) {
				ParameterCollection<Integer> pi = new ParameterCollection<Integer>(
						parameter.getValue0(), parameter.getValue1(),
						parameter.getValue3(), LookingGlassType.integer, 0,
						ObjectOrigin.server);
				collectionBase.getParamInts().add(pi);
			} else if (parameter.getValue2() == LookingGlassType.string_value) {
				ParameterCollection<String> pv = new ParameterCollection<String>(
						parameter.getValue0(), parameter.getValue1(),
						parameter.getValue3(), LookingGlassType.string_value,
						0, ObjectOrigin.server);
				collectionBase.getParamStrings().add(pv);
			} else if (parameter.getValue2() == LookingGlassType.date_time) {
				ParameterCollection<java.util.Date> pda = new ParameterCollection<java.util.Date>(
						parameter.getValue0(), parameter.getValue1(),
						parameter.getValue3(), LookingGlassType.date_time, 0,
						ObjectOrigin.server);
				collectionBase.getParamDates().add(pda);
			} else if (parameter.getValue2() == LookingGlassType.floating_point) {
				ParameterCollection<Double> pda = new ParameterCollection<Double>(
						parameter.getValue0(), parameter.getValue1(),
						parameter.getValue3(), LookingGlassType.floating_point,
						0, ObjectOrigin.server);
				collectionBase.getParamDoubles().add(pda);// /TODO: add floats
															// to collection
															// base
			}
		}

		return collectionBase;
	}

	/**
	 * Returns all Datasets associated with a CloudCix member Id
	 * 
	 * @param conn
	 * @param memberId
	 * @return
	 */
	public static ArrayList<Dataset> allDatasets(final Connection conn,
			int memberId) {

		final ArrayList<Dataset> datasetList = new ArrayList<Dataset>();

		PreparedStatement preparedStatement = null;
		// PreparedStatement selectMeta = null;
		ResultSet reader = null;
		LOG.info("Retrieving allowed project list");
		try {

			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_ALL_DATASETS
							.getValue());
			preparedStatement.setInt(1, memberId);
			reader = preparedStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				final Dataset dataset = new Dataset(reader.getLong(1), reader
						.getString(2).trim(), reader.getString(4), reader
						.getString(3).trim(), reader.getInt(5),
						reader.getString(7));
				if (dataset != null) {
					dataset.setMetaInfo(RequestMeta.getMetaNameValueMap(
							reader.getLong(1), conn));
					datasetList.add(dataset);
				}
			}

		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
		}

		return datasetList;
	}

	/**
	 * Returns specified (by Global Id explicitly) Datasets associated with a
	 * CloudCix member Id
	 * 
	 * @param conn
	 * @param datasetIdList
	 * @param memberId
	 * @return
	 */
	public static ArrayList<Dataset> specifiedDatasets(String datasetIdList,
			int memberId, final Connection conn) {

		final ArrayList<Dataset> datasetList = new ArrayList<Dataset>();
		PreparedStatement preparedStatement = null;
		ResultSet reader = null;
		LOG.info("Retrieving allowed project list");
		try {

			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_SPECIFIED_DATASETS
							.getValue() + datasetIdList + ")");
			preparedStatement.setInt(1, memberId);
			reader = preparedStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				final Dataset dataset = new Dataset(reader.getLong(1), reader
						.getString(2).trim(), reader.getString(4), reader
						.getString(3).trim(), reader.getInt(5),
						reader.getString(7));
				if (dataset != null) {
					dataset.setMetaInfo(RequestMeta.getMetaNameValueMap(
							reader.getLong(1), conn));
					datasetList.add(dataset);
				}
			}

		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
		}

		return datasetList;
	}

	/**
	 * Return a Dataset found by Global Id
	 * 
	 * @param globalId
	 * @param conn
	 * @return
	 */
	public static Dataset retrieveDataset(final Long dId, final int idMember,
			final Connection conn) {
		PreparedStatement preparedStatement = null;
		ResultSet reader = null;
		Dataset ret = null;
		try {
			SQLHelpers.setSchema(
					Configuration.Database.MOHAGO_DB_NAME.getValue(), conn);
			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.RETRIEVE_DATASET
							.getValue());
			preparedStatement.setInt(1, idMember);
			preparedStatement.setLong(2, dId);

			LOG.info("Retrieving datasets");

			reader = preparedStatement.executeQuery();
			String sql = preparedStatement.toString();
			String[] sqlSplit = sql.split("wrapping ");// hack
			conn.commit();
			if (reader != null
					&& SQLHelpers.getRowCount(
							conn.prepareStatement(sqlSplit[1]), "*", conn) > 0) {
				reader.next();
				// String userName
				String deviceToken = null;
				if (reader.getInt("owner_m_id") == idMember) {
					deviceToken = reader.getString(7);
				}// Only show device token to owner
					// dId = reader.getLong(1);
				final Dataset dataset = new Dataset(reader.getLong(1), reader
						.getString(2).trim(), reader.getString(4), reader
						.getString(3).trim(), reader.getInt(5), deviceToken);
				if (dataset != null) {
					dataset.setMetaInfo(RequestMeta.getMetaNameValueMap(
							reader.getLong(1), conn));
				}
				ret = dataset;
			}

		} catch (SQLException e1) {

			LOG.error("Error retrieving dataset: " + e1);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);

		}
		if (ret != null) {
			ret.setFileHash(DatabaseHelpers.getHashes(dId, conn));
		}

		return ret;

	}

	/**
	 * Return a Dataset found by Dataset Id
	 * 
	 * @param dId
	 * @param conn
	 * @return
	 */
	public static Dataset retrieveDataset(final Long dId, final Connection conn) {
		PreparedStatement preparedStatement = null;
		ResultSet reader = null;
		Dataset ret = null;
		// int dId = -1;
		try {
			SQLHelpers.setSchema(
					Configuration.Database.MOHAGO_DB_NAME.getValue(), conn);
			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.RETRIEVE_DATASET_BY_DID
							.getValue());
			preparedStatement.setLong(1, dId);

			LOG.info("Retrieving datasets");

			reader = preparedStatement.executeQuery();
			conn.commit();
			if (reader != null
					&& SQLHelpers.getRowCount(preparedStatement, "*", conn) > 0) {
				reader.next();
				final Dataset dataset = new Dataset(reader.getLong(1), reader
						.getString(2).trim(), reader.getString(4), reader
						.getString(3).trim(), reader.getInt(5),
						reader.getString(7));
				if (dataset != null) {
					dataset.setMetaInfo(RequestMeta.getMetaNameValueMap(
							reader.getLong(1), conn));
				}

				ret = dataset;

			}

		} catch (SQLException e1) {

			LOG.error("Error retrieving dataset: " + e1);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);

		}
		if (ret != null) {
			ret.setFileHash(DatabaseHelpers.getHashes(dId, conn));
		}

		return ret;

	}

	/**
	 * Return a list of name-type pairs for all parameters in a list of Datasets
	 * 
	 * @param didSet
	 * @param conn
	 * @return
	 */
	public static HashMap<String, Integer> paramCollectionsMap(
			final HashSet<Long> didSet, final Connection conn) {

		LOG.debug("Entering coll base List for d_id set");
		HashMap<String, Integer> nameValueMap = new HashMap<String, Integer>();
		if (!didSet.isEmpty()) {

			for (final Long did : didSet) {
				CollectionBase collBase = null;
				collBase = buildCollectionBase(did, conn);
				for (ParameterCollection<String> pcs : collBase
						.getParamStrings()) {
					nameValueMap.put(pcs.getName(),
							LookingGlassType.string_value.getValue());
				}
				for (ParameterCollection<Double> pcd : collBase
						.getParamDoubles()) {

					nameValueMap.put(pcd.getName(),
							LookingGlassType.double_precision.getValue());
				}
				for (ParameterCollection<java.util.Date> pcda : collBase
						.getParamDates()) {
					nameValueMap.put(pcda.getName(),
							LookingGlassType.date_time.getValue());
				}
				for (ParameterCollection<Integer> pcs : collBase.getParamInts()) {
					nameValueMap.put(pcs.getName(),
							LookingGlassType.integer.getValue());
				}
			}

			LOG.info("Returning json  Coll base list object successfully");
		} else {
			LOG.error("Empty did set");
		}
		return nameValueMap;
	}

	public static String getDIDListFromMeta(LookingGlassType type, String name,
			String operator, Object value, int memberId, Connection conn) {

		// ArrayList<Long> ret = new ArrayList<Long>();
		String ret = "";
		PreparedStatement preparedStatement = null;
		ResultSet reader = null;
		LOG.info("Retrieving DID list");
		String sql = "";
		try {

			if (operator.equals("==")) {
				if (type.equals(LookingGlassType.integer)) {

					sql = SQLPreparedStatements.MetaQuery.INT_EQUALS.getValue();
				} else if (type.equals(LookingGlassType.double_precision)) {
					sql = SQLPreparedStatements.MetaQuery.DOUBLE_EQUALS
							.getValue();
				} else if (type.equals(LookingGlassType.date_time)) {
					sql = SQLPreparedStatements.MetaQuery.DATE_TIME_EQUALS
							.getValue();
				} else if (type.equals(LookingGlassType.string_value)) {
					sql = SQLPreparedStatements.MetaQuery.STRING_EQUALS
							.getValue();
				}

			} else if (operator.equals("!=")) {

				if (type.equals(LookingGlassType.integer)) {

					sql = SQLPreparedStatements.MetaQuery.INT_NOT_EQUALS
							.getValue();
				} else if (type.equals(LookingGlassType.double_precision)) {
					sql = SQLPreparedStatements.MetaQuery.DOUBLE_NOT_EQUALS
							.getValue();
				} else if (type.equals(LookingGlassType.date_time)) {
					sql = SQLPreparedStatements.MetaQuery.DATE_TIME_NOT_EQUALS
							.getValue();
				} else if (type.equals(LookingGlassType.string_value)) {
					sql = SQLPreparedStatements.MetaQuery.STRING_NOT_EQUALS
							.getValue();
				}
			} else if (operator.equals("<=")) {
				if (type.equals(LookingGlassType.integer)) {

					sql = SQLPreparedStatements.MetaQuery.INT_LESS_EQUALS
							.getValue();
				} else if (type.equals(LookingGlassType.double_precision)) {
					sql = SQLPreparedStatements.MetaQuery.DOUBLE_LESS_EQUALS
							.getValue();
				} else if (type.equals(LookingGlassType.date_time)) {
					sql = SQLPreparedStatements.MetaQuery.DATE_TIME_LESS_EQUALS
							.getValue();
				}
			} else if (operator.equals(">=")) {
				if (type.equals(LookingGlassType.integer)) {

					sql = SQLPreparedStatements.MetaQuery.INT_GREATER_EQUALS
							.getValue();
				} else if (type.equals(LookingGlassType.double_precision)) {
					sql = SQLPreparedStatements.MetaQuery.DOUBLE_GREATER_EQUALS
							.getValue();
				} else if (type.equals(LookingGlassType.date_time)) {
					sql = SQLPreparedStatements.MetaQuery.DATE_TIME_GREATER_EQUALS
							.getValue();
				}

			} else if (operator.equals(">")) {
				if (type.equals(LookingGlassType.integer)) {

					sql = SQLPreparedStatements.MetaQuery.INT_GREATER
							.getValue();
				} else if (type.equals(LookingGlassType.double_precision)) {
					sql = SQLPreparedStatements.MetaQuery.DOUBLE_GREATER
							.getValue();
				} else if (type.equals(LookingGlassType.date_time)) {
					sql = SQLPreparedStatements.MetaQuery.DATE_TIME_GREATER
							.getValue();
				}
			} else if (operator.equals("<")) {
				if (type.equals(LookingGlassType.integer)) {

					sql = SQLPreparedStatements.MetaQuery.INT_LESS.getValue();
				} else if (type.equals(LookingGlassType.double_precision)) {
					sql = SQLPreparedStatements.MetaQuery.DOUBLE_LESS
							.getValue();
				} else if (type.equals(LookingGlassType.date_time)) {
					sql = SQLPreparedStatements.MetaQuery.DATE_TIME_LESS
							.getValue();
				}
			}
			preparedStatement = conn.prepareStatement(sql);
			preparedStatement.setInt(1, memberId);
			preparedStatement.setString(2, name);
			if (type.equals(LookingGlassType.date_time)) {
				final long time = ((DateTime) value).getMillis();
				final Timestamp timeStamp = new Timestamp(time);
				preparedStatement.setTimestamp(3, timeStamp);
			} else {
				preparedStatement.setObject(3, value);
			}
			reader = preparedStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				ret += String.valueOf(reader.getLong("d_id")) + ",";
			}
			if (ret.length() > 0) {
				ret = ret.substring(0, ret.length() - 1);
			}

		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
		}
		return ret;

	}

}
