package com.mohago.request;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.mohago.config.Configuration.Database;
import com.mohago.config.DatabaseConnection;
import com.mohago.node.Node;
import com.mohago.sql.SQLPreparedStatements;
import com.mohago.utilities.Constants;
import com.mohago.utilities.SQLHelpers;

public class MasterTable {

	private static final Logger LOG = Logger.getLogger(MasterTable.class);
	
	public static HashSet<Node> getNodeSet() {

		long before2 = System.currentTimeMillis();
		HashSet<Node> ret = new HashSet<Node>();

		try (Connection masterConn = DatabaseConnection.getPooledConnection(Database.HOST.getValue());
				Statement statement = masterConn
						.createStatement();) {
			try (ResultSet reader = statement.executeQuery(SQLPreparedStatements.Select.GET_NODE_LIST
					.getValue());) {
				while (reader.next()) {
					ret.add(new Node(reader.getLong(1), reader.getString(2)));
				}
			} catch (SQLException e) {
				LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
			}
		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		}
		long after2 = System.currentTimeMillis();
		LOG.error("Getting node set: " + (after2 - before2) + " milliseconds");

		return ret;
	}

	// used during cleanup
	public static ArrayList<String> getAllConnectionIPList() {

		ArrayList<String> ret = new ArrayList<String>();

		try (Connection masterConn = DatabaseConnection.getMasterConnection();
				Statement statement = masterConn
						.createStatement();) {
			//SQLHelpers.setAutoCommit(masterConn, false);
			try (ResultSet reader = statement.executeQuery(SQLPreparedStatements.Select.GET_ALL_CONNECTION_IPS
					.getValue());) {
				//masterConn.commit();
				while (reader.next()) {
					ret.add(reader.getString(1));
				}
			} catch (SQLException e) {
				LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
			}
		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		}

		return ret;
	}


}
