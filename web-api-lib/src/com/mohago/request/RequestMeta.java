package com.mohago.request;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.mohago.serialization.Dataset;
import com.mohago.serialization.LookingGlassType;
import com.mohago.serialization.Meta;
import com.mohago.serialization.MetaDataBase;
import com.mohago.sql.SQLPreparedStatements;
import com.mohago.utilities.Constants;
import com.mohago.utilities.SQLHelpers;

public class RequestMeta {

	private static final Logger LOG = Logger.getLogger(RequestMeta.class);

	public static Meta getDatasetMeta(long did, Connection conn) {
		Meta metaData = new Meta();
		metaData.setDateTimeData(getMetaDate(did, conn));
		metaData.setIntData(getMetaInt(did, conn));
		metaData.setDoubleData(getMetaDouble(did, conn));
		metaData.setStringData(getMetaString(did, conn));

		return metaData;
	}

	public static ArrayList<MetaDataBase<Integer>> getMetaInt(long dId,
			Connection conn) {
		ArrayList<MetaDataBase<Integer>> ret = new ArrayList<MetaDataBase<Integer>>();
		PreparedStatement preparedStatement = null;
		ResultSet reader = null;
		LOG.info("Retrieving allowed project list");
		try {

			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_META_INT_BY_DID
							.getValue());
			preparedStatement.setLong(1, dId);
			reader = preparedStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				MetaDataBase<Integer> temp = new MetaDataBase<Integer>(
						reader.getString("name"), reader.getInt("value"));
				temp.type = LookingGlassType.integer;
				ret.add(temp);
			}

		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
		}
		return ret;

	}

	public static ArrayList<MetaDataBase<Double>> getMetaDouble(long dId,
			Connection conn) {
		ArrayList<MetaDataBase<Double>> ret = new ArrayList<MetaDataBase<Double>>();
		PreparedStatement preparedStatement = null;
		ResultSet reader = null;
		LOG.info("Retrieving allowed project list");
		try {

			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_META_DOUBLE_BY_DID
							.getValue());
			preparedStatement.setLong(1, dId);
			reader = preparedStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				MetaDataBase<Double> temp = new MetaDataBase<Double>(
						reader.getString("name"), reader.getDouble("value"));
				temp.type = LookingGlassType.double_precision;
				ret.add(temp);
			}

		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
		}
		return ret;

	}

	public static ArrayList<MetaDataBase<String>> getMetaString(long dId,
			Connection conn) {
		ArrayList<MetaDataBase<String>> ret = new ArrayList<MetaDataBase<String>>();
		PreparedStatement preparedStatement = null;
		ResultSet reader = null;
		LOG.info("Retrieving allowed project list");
		try {

			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_META_STRING_BY_DID
							.getValue());
			preparedStatement.setLong(1, dId);
			reader = preparedStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				MetaDataBase<String> temp = new MetaDataBase<String>(
						reader.getString("name"), reader.getString("value"));
				temp.type = LookingGlassType.string_value;
				ret.add(temp);
			}

		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
		}
		return ret;

	}

	public static ArrayList<MetaDataBase<java.util.Date>> getMetaDate(long dId,
			Connection conn) {
		ArrayList<MetaDataBase<java.util.Date>> ret = new ArrayList<MetaDataBase<java.util.Date>>();
		PreparedStatement preparedStatement = null;
		ResultSet reader = null;
		LOG.info("Retrieving allowed project list");
		try {

			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_META_DATE_BY_DID
							.getValue());
			preparedStatement.setLong(1, dId);
			reader = preparedStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				MetaDataBase<java.util.Date> temp = new MetaDataBase<java.util.Date>(
						reader.getString("name"), new java.util.Date(reader
								.getTimestamp("value").getTime()));
				temp.type = LookingGlassType.date_time;
				ret.add(temp);
			}

		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
		}
		return ret;

	}

	public static Map<String, LookingGlassType> getMetaNameTypeMap(
			long dId, Connection conn) {
		HashMap<String, LookingGlassType> ret = new HashMap<String, LookingGlassType>();
		Meta meta = RequestMeta.getDatasetMeta(
				dId, conn);
		for (MetaDataBase<Integer> mdb : meta.getIntData()) {
			ret.put(mdb.getName(), mdb.getType());
		}
		for (MetaDataBase<Double> mdb : meta.getDoubleData()) {
			ret.put(mdb.getName(), mdb.getType());
		}
		for (MetaDataBase<java.util.Date> mdb : meta.getDateTimeData()) {
			ret.put(mdb.getName(), mdb.getType());
		}
		for (MetaDataBase<String> mdb : meta.getStringData()) {
			ret.put(mdb.getName(), mdb.getType());
		}
		return ret;
	}

	public static HashMap<String, Object> getMetaNameValueMap(
			long dId, Connection conn) {
		HashMap<String, Object> ret = new HashMap<String, Object>();
		Meta meta = RequestMeta.getDatasetMeta(
				dId, conn);
		for (MetaDataBase<Integer> mdb : meta.getIntData()) {
			ret.put(mdb.getName(), mdb.getValue());
		}
		for (MetaDataBase<Double> mdb : meta.getDoubleData()) {
			ret.put(mdb.getName(), mdb.getValue());
		}
		for (MetaDataBase<java.util.Date> mdb : meta.getDateTimeData()) {
			ret.put(mdb.getName(), mdb.getValue());
		}
		for (MetaDataBase<String> mdb : meta.getStringData()) {
			ret.put(mdb.getName(), mdb.getValue());
		}
		return ret;
	}

	public static ArrayList<Dataset> setMeta(
			HashMap<Long, Dataset> topLevelDatasetMap, Connection conn) {
		//ArrayList<Dataset> ret = new ArrayList<Dataset>();
		PreparedStatement preparedStatement = null;
		ResultSet reader = null;
		LOG.info("Retrieving meta");
		
		String datasetIdList = "(" + StringUtils.join(topLevelDatasetMap.keySet(), ',') + ")";
		try {

			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_META_INT_MULTIPLE_DID
							.getValue() + datasetIdList);
			reader = preparedStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				topLevelDatasetMap.get(reader.getLong("d_id")).getMetaInfo().put(reader.getString("name"), reader.getObject("value"));
			}

		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
		}
		
		try {

			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_META_DOUBLE_MULTIPLE_DID
							.getValue() + datasetIdList);
			reader = preparedStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				topLevelDatasetMap.get(reader.getLong("d_id")).getMetaInfo().put(reader.getString("name"), reader.getObject("value"));
			}

		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
		}
		try {

			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_META_DATE_TIME_MULTIPLE_DID
							.getValue() + datasetIdList);
			reader = preparedStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				topLevelDatasetMap.get(reader.getLong("d_id")).getMetaInfo().put(reader.getString("name"), reader.getObject("value"));
			}

		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
		}
		
		try {

			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_META_STRING_MULTIPLE_DID
							.getValue() + datasetIdList);
			reader = preparedStatement.executeQuery();
			conn.commit();
			while (reader.next()) {
				topLevelDatasetMap.get(reader.getLong("d_id")).getMetaInfo().put(reader.getString("name"), reader.getObject("value"));
			}

		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
		}
		return new ArrayList<Dataset> (topLevelDatasetMap.values());
	}
	


}
