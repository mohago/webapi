package com.mohago.config;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.mohago.sql.SQLPreparedStatements;
import com.mohago.utilities.SQLHelpers;

public class FirstRun {
	
	private final static Logger LOG = Logger.getLogger(FirstRun.class);

	public static boolean createTable(Connection conn, String sql) {
		PreparedStatement statement = null;
		boolean wasTableCreated = false;
		if (conn != null) {
			try {
				statement = conn.prepareStatement(sql);
				statement.execute();
				conn.commit();
				wasTableCreated = true;
			} catch (SQLException e) {

				LOG.error("Error inserting new tables: " + sql + " . Error: " + e);
				SQLHelpers.rollBack(conn, "inserting new tables");
				wasTableCreated = false;

			} finally {
				SQLHelpers.closeResources(statement, null);
			}
		} else {
			LOG.error("No DB connection");
		}
		return wasTableCreated;
	}
	
	public static void createAllTables(Connection conn){



		
		
		createTable(conn,
				SQLPreparedStatements.CreateTables.DATASET.getValue());
		
		createTable(conn,
				SQLPreparedStatements.CreateTables.DATASET_ACCESS.getValue());
		if(!checkForIndex(SQLPreparedStatements.ConstraintNames.DATASET_ACCESS.getValue(), conn)){
			createTable(conn, SQLPreparedStatements.CreateTables.DATASET_ACCESS_CONSTRAINT.getValue());
		}
		

		createTable(conn,
				SQLPreparedStatements.CreateTables.DATASET_STREAM_FLAG.getValue());
		


		
		createTable(conn,
				SQLPreparedStatements.CreateTables.TABLE_INDEX.getValue());
		if(!checkForIndex(SQLPreparedStatements.IndexNames.TABLE_INDEX.getValue(), conn)){
			createTable(conn, SQLPreparedStatements.CreateTables.TABLE_INDEX_INDEX.getValue());
		}
		
		createTable(conn,
				SQLPreparedStatements.CreateTables.PARAMETER_NAME_INDEX.getValue());
		if(!checkForIndex(SQLPreparedStatements.IndexNames.PARAMETER_NAME_INDEX.getValue(), conn)){
			createTable(conn, SQLPreparedStatements.CreateTables.CREATE_PARAMETER_NAME_INDEX_INDEX.getValue());
		}
		
		createTable(conn,
				SQLPreparedStatements.CreateTables.META_DOUBLE
						.getValue());
		if(!checkForIndex(SQLPreparedStatements.ConstraintNames.META_DOUBLE.getValue(), conn)){
			createTable(conn, SQLPreparedStatements.CreateTables.META_DOUBLE_CONSTRAINT.getValue());
		}
		
		createTable(conn,
				SQLPreparedStatements.CreateTables.META_INT
						.getValue());
		if(!checkForIndex(SQLPreparedStatements.ConstraintNames.META_INT.getValue(), conn)){
			createTable(conn, SQLPreparedStatements.CreateTables.META_INT_CONSTRAINT.getValue());
		}
		
		createTable(conn,
				SQLPreparedStatements.CreateTables.META_STRING
						.getValue());
		if(!checkForIndex(SQLPreparedStatements.ConstraintNames.META_STRING.getValue(), conn)){
			createTable(conn, SQLPreparedStatements.CreateTables.META_STRING_CONSTRAINT.getValue());
		}
		if(!checkForIndex(SQLPreparedStatements.ConstraintNames.PARAMETER_NAME_INDEX.getValue(), conn)){
			createTable(conn, SQLPreparedStatements.CreateTables.PARAMETER_NAME_CONSTRAINT.getValue());
		}
		
		createTable(conn,
				SQLPreparedStatements.CreateTables.META_DATE
						.getValue());
		if(!checkForIndex(SQLPreparedStatements.ConstraintNames.META_DATE.getValue(), conn)){
			createTable(conn, SQLPreparedStatements.CreateTables.META_DATE_CONSTRAINT.getValue());
		}
		
		createTable(conn,
				SQLPreparedStatements.CreateTables.FILE_HASH
						.getValue());
		
		createTable(conn,
				SQLPreparedStatements.CreateTables.FILE_DELETE
						.getValue());

	}

	private static boolean checkForIndex(String indexName, Connection conn) {
		PreparedStatement checkForIndex = null;
		boolean success = false;
		if (conn != null) {
			try {
				checkForIndex = conn.prepareStatement("SELECT '" + indexName + "' ::regclass");
				checkForIndex.execute();
				success = true;
			} catch (SQLException e) {
				LOG.error("Index doesn't exist: " + e);
				success = false;
				try {
					conn.commit();
				} catch (SQLException e1) {
					LOG.error("Error commiting: " + e);
				}
			} finally {
				SQLHelpers.closeResources(checkForIndex, null);
			}
		} else {
			LOG.error("No DB connection");
		}
		return success;
	}


}
