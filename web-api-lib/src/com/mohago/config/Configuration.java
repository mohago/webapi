package com.mohago.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
//import org.owasp.esapi.codecs.Codec;
//import org.owasp.esapi.codecs.MySQLCodec;

import com.mohago.utilities.Constants;
import com.mohago.utilities.ResultCodes;
import com.mysql.jdbc.StringUtils;

public class Configuration {

	private static final Logger LOG = Logger.getLogger(Configuration.class);
	private static String lgdfFolderPath = "";
	private static String transferPath = "";
	private static String contextPath = "";
	private static String errorPath = "";

	public enum Database {

		TYPE(""), HOST(""), PORT(""), DATABASE(""), USER(""), PASSWORD(""), MOHAGO_DB_NAME(
				""), MOHAGO_LOCAL_NAME("");

		

		private String value;
		
		private Database(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

	}

	public enum Admin {

		USER_NAME(""), PASSWORD("");

		private Admin(String value) {
			this.value = value;
		}

		private String value;

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

	}

	public enum JDBCDriver {
		MYSQL("com.mysql.jdbc.Driver"), SQLITE(""), MARIADB(
				"com.mysql.jdbc.Driver"), POSTGRESQL("org.postgresql.Driver");

		private JDBCDriver(String value) {
			this.value = value;
		}

		private String value;

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}

	public static boolean initDBConnectionParamsFromClient(
			HttpServletRequest request) {

		boolean result = false;
		final String type = request.getParameter("type");
		final String host = request.getParameter("host");
		final String port = request.getParameter("port");
		final String user = request.getParameter(Constants.Parameters.USER_NAME
				.getValue());
		final String password = request
				.getParameter(Constants.Parameters.PASSWORD.getValue());
		if (!StringUtils.isNullOrEmpty(type)
				&& !StringUtils.isNullOrEmpty(host)
				&& !StringUtils.isNullOrEmpty(port)
				&& !StringUtils.isNullOrEmpty(user)
				&& !StringUtils.isNullOrEmpty(password)) {
			if (type.equalsIgnoreCase("mariadb")) {
				Database.TYPE.setValue("mysql");
			} else if (type.equalsIgnoreCase("mysql")) {
				Database.TYPE.setValue("mysql");
			} else if (type.equalsIgnoreCase("postgresql")) {
				Database.TYPE.setValue("postgresql");
			} else {
				Database.TYPE.setValue(type);
			}// /TODO: finish this for other databases
			Database.HOST.setValue(host);
			Database.PORT.setValue(port);
//			Database.DATABASE
//					.setValue(SQLPreparedStatements.
//							.getValue());// Remove this altogether??
			Database.USER.setValue(user);
			Database.PASSWORD.setValue(password);
			result = true;
		} else {
			result = false;
		}
		return result;
	}

	public static boolean initAdminUserFromClient(HttpServletRequest request) {

		boolean result = false;
		final String userName = request.getParameter("user_name");
		final String password = request
				.getParameter(Constants.Parameters.PASSWORD.getValue());

		if (!StringUtils.isNullOrEmpty(userName)
				&& !StringUtils.isNullOrEmpty(password)) {
			Admin.USER_NAME.setValue(userName);
			Admin.PASSWORD.setValue(password);
			result = true;
		} else {
			result = false;
		}
		return result;

	}

	/**
	 * Loads up information on an initial admin user, like user name and
	 * password and stores this initial admin in the Mohago server database
	 * 
	 * @param httpServlet
	 */
	public static String getServletVersion(HttpServlet servlet) {
		String result = "";
		ServletContext context = servlet.getServletContext();
		if (context != null) {
			final String appPath = servlet.getServletContext().getRealPath("");
			// constructs path of the directory to save uploaded file
			String manifestPath = appPath + File.separator + "META-INF"
					+ File.separatorChar + "MANIFEST.MF";

			try {
				final InputStream inputStream = new FileInputStream(
						manifestPath);
				Manifest manifest = null;
				Attributes attributes = null;
				manifest = new Manifest(inputStream);
				if (manifest != null) {
					attributes = manifest.getMainAttributes();
				}
				if (attributes != null) {
					result = attributes.getValue("Implementation-Version");
				}
			} catch (IOException e) {
				LOG.error("Error reading .war manifest: " + e);
				result = ResultCodes.Config.FAIL.getValue();
			}
		}
		return result;

	}

	public static String getLgdfFolderPath() {
		return lgdfFolderPath;
	}

	public static void setLgdfFolderPath(String lgdfFolderPath) {
		Configuration.lgdfFolderPath = lgdfFolderPath;
	}

	public static String getTransferPath() {
		return transferPath;
	}

	public static void setTransferPath(String transferPath) {
		Configuration.transferPath = transferPath;
	}

	public static String getContextPath() {
		return contextPath;
	}

	public static void setContextPath(String contextPath) {
		Configuration.contextPath = contextPath;
	}

	public static String getErrorPath() {
		return errorPath;
	}

	public static void setErrorPath(String errorPath) {
		Configuration.errorPath = errorPath;
	}

}
