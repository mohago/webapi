package com.mohago.config;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.mohago.config.Configuration.Database;
import com.mohago.utilities.Constants;
import com.mohago.utilities.ResultCodes;
import com.mohago.utilities.SQLHelpers;
import com.mysql.jdbc.StringUtils;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class DatabaseConnection {

	private static final Logger LOG = Logger
			.getLogger(DatabaseConnection.class);
	private static final ConcurrentHashMap<String, HikariDataSource> CONN_POOL_MAP;// =
																					// new
																					// HashMap<String,
	static {
		CONN_POOL_MAP = new ConcurrentHashMap<String, HikariDataSource>();

	}

	public static Connection getMasterConnection() {

		Connection conn = null;
		LOG.info("Start get connection");
		if (!StringUtils.isNullOrEmpty(Configuration.Database.TYPE.getValue())) {
			try {
				String connStr = "jdbc:"
						+ Configuration.Database.TYPE.getValue() + "://"
						+ Configuration.Database.HOST.getValue() + ":"
						+ Configuration.Database.PORT.getValue()
				// + "/" /*+ "?dontTrackOpenResources=true"*/+
				// Configuration.Database.MOHAGO_DB_NAME.getValue()
				;
				conn = (Connection) DriverManager.getConnection(connStr,
						Configuration.Database.USER.getValue(),
						Configuration.Database.PASSWORD.getValue());

			} catch (Exception e) {
				LOG.error("Error retrieving database connection: " + e);
				e.printStackTrace();
			}
			LOG.info("DB connection established");
		} else {
			LOG.error("Configuration string parameter for db type is not set, possibly others are empty too: ");
		}
		LOG.info("End get connection");

		return conn;
	}

	/**
	 * Loads up Database connection parameters from a config.xml file in web-app
	 * path. For example: mysql, localhost, port number, db user and pwd etc.
	 * 
	 * @param httpServlet
	 */

	public static boolean initDBConnectionParamsFromFile(HttpServlet httpServlet) {// /TODO:
																					// Refactor

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		Document doc = null;
		final String appPath = httpServlet.getServletContext().getRealPath("");
		// constructs path of the directory to save uploaded file
		String configPath = appPath + File.separator + "WEB-INF"
				+ File.separator + "classes" + File.separator + "config.xml";
		LOG.info("Config.xml path: " + configPath);
		try {
			db = dbf.newDocumentBuilder();

			doc = db.parse(new FileInputStream(configPath));
		} catch (ParserConfigurationException | IOException | SAXException
				| IllegalArgumentException e3) {

			LOG.error("Parser config exception loading doc builder . extract db config info from xml: "
					+ e3);
		}

		return initDBConnFromDoc(doc);
	}

	/**
	 * Used for mocking, testing
	 * 
	 * @param configString
	 * @return
	 */
	public static boolean initDBConnectionParamsFromString(String configString) {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		Document doc = null;

		try {
			db = dbf.newDocumentBuilder();

			doc = db.parse(new ByteArrayInputStream(configString
					.getBytes(StandardCharsets.UTF_8)));
		} catch (ParserConfigurationException | IOException | SAXException
				| IllegalArgumentException e3) {

			LOG.error("Parser config exception loading doc builder . extract db config info from xml: "
					+ e3);
		}

		return initDBConnFromDoc(doc);
	}

	public static boolean setDBConnParams(HttpServletRequest request) {

		boolean success = false;
		Configuration.initDBConnectionParamsFromClient(request);
		initJDBCDriver();
		Connection conn = DatabaseConnection.getMasterConnection();
		if (conn != null) {
			success = true;
		}
		SQLHelpers.closeConn(conn);
		return success;
	}

	public static String buildTables(Connection conn) {

		String result = "";
		if (conn != null) {
			SQLHelpers.setAutoCommit(conn, false);
			FirstRun.createAllTables(conn);
			try {
				conn.commit();
				result = ResultCodes.Config.SUCCESS.getValue();
			} catch (SQLException e) {
				SQLHelpers.rollBack(conn, "build tables");
				LOG.error("Error commiting create tables transaction: " + e);
				result = ResultCodes.Config.SQL_EXCEPTION.getValue();
			}
		} else {
			result = ResultCodes.Config.CONNECTION_PARAMS_INCORRECT.getValue();
		}
		return result;
	}

	public static boolean verifyDBConnParamsExist() {

		boolean success = false;

		Connection conn = DatabaseConnection.getMasterConnection();
		if (conn != null) {
			success = true;
			SQLHelpers.closeConn(conn);
		}
		return success;
	}

	public static void initJDBCDriver() {

		if (!StringUtils.isNullOrEmpty(Configuration.Database.TYPE.getValue())) {
			try {

				final String jdbcClassName = Configuration.JDBCDriver.valueOf(
						Configuration.Database.TYPE.getValue().toUpperCase())
						.getValue();
				Class.forName(jdbcClassName);
			} catch (ClassNotFoundException e) {
				LOG.error("The JDBC driver could not be loaded: " + e);
			}
		}
	}


	public static Connection getConnection(String ip) {
		LOG.info("Start get connection " + ip);
		Connection connection = null;
		String connStr = "";
		if (!StringUtils.isNullOrEmpty(Configuration.Database.TYPE.getValue())) {
			try {
				connStr = "jdbc:" + Configuration.Database.TYPE.getValue()
						+ "://" + ip
						// + ":" + Constants.SQL.PSQL_DATABASE_PORT.getValue()
						// /*+ "?dontTrackOpenResources=true"*/,
						// + Constants.SQL.PSQL_DATABASE_PORT.getValue();
						+ ":" + Configuration.Database.PORT.getValue();
				connection = (Connection) DriverManager.getConnection(connStr,
						Configuration.Database.USER.getValue(),
						Configuration.Database.PASSWORD.getValue());
				LOG.info("DB connection established");
			} catch (SQLException e) {
				LOG.error("Error retrieving database connection: "
						+ "coonstr: " + connStr + " e: " + e);
			}

		} else {
			LOG.error("Configuration string parameter for db type is not set, possibly others are empty too: ");
		}
		return connection;
	}

	private static boolean initDBConnFromDoc(Document doc) {
		boolean success = false;
		if (doc != null) {
			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("dbconnparams");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					Node jdbcTypeNode = eElement.getElementsByTagName("type")
							.item(0);
					Node hostNode = eElement.getElementsByTagName("host").item(
							0);
					Node portNode = eElement.getElementsByTagName("port").item(
							0);
					Node userNode = eElement.getElementsByTagName(
							Constants.Parameters.USER_NAME.getValue()).item(0);
					Node pwdNode = eElement.getElementsByTagName(
							Constants.Parameters.PASSWORD.getValue()).item(0);

					Node mohagoDbNameNode = eElement.getElementsByTagName(
							"mohago_db").item(0);
					Node mohagoLocalNameNode = eElement.getElementsByTagName(
							"mohago_local").item(0);

					String jdbcType = null;
					String host = null;
					String port = null;
					String user = null;
					String pwd = null;
					String mohagoDb = null;
					String mohagoLocal = null;

					if (jdbcTypeNode != null
							&& !StringUtils.isNullOrEmpty(jdbcTypeNode
									.getTextContent())) {
						jdbcType = jdbcTypeNode.getTextContent();
						if ("mariadb".equals(jdbcType)) {
							Database.TYPE.setValue("mysql");
						} else {
							Database.TYPE.setValue(jdbcType);
						}
					}
					if (hostNode != null
							&& !StringUtils.isNullOrEmpty(hostNode
									.getTextContent())) {
						host = hostNode.getTextContent();
						Database.HOST.setValue(eElement
								.getElementsByTagName("host").item(0)
								.getTextContent());
					}
					if (portNode != null
							&& !StringUtils.isNullOrEmpty(portNode
									.getTextContent())) {
						port = portNode.getTextContent();
						Database.PORT.setValue(eElement
								.getElementsByTagName("port").item(0)
								.getTextContent());
					}
					if (userNode != null
							&& !StringUtils.isNullOrEmpty(userNode
									.getTextContent())) {
						user = userNode.getTextContent();
						Database.USER.setValue(eElement
								.getElementsByTagName("user_name").item(0)
								.getTextContent());
					}
					if (pwdNode != null
							&& !StringUtils.isNullOrEmpty(pwdNode
									.getTextContent())) {
						pwd = pwdNode.getTextContent();
						Database.PASSWORD.setValue(eElement
								.getElementsByTagName(
										Constants.Parameters.PASSWORD
												.getValue()).item(0)
								.getTextContent());
					}
					if (mohagoDbNameNode != null
							&& !StringUtils.isNullOrEmpty(mohagoDbNameNode
									.getTextContent())) {
						mohagoDb = mohagoDbNameNode.getTextContent();
						Database.MOHAGO_DB_NAME.setValue(eElement
								.getElementsByTagName("mohago_db").item(0)
								.getTextContent());
					}
					if (mohagoLocalNameNode != null
							&& !StringUtils.isNullOrEmpty(mohagoLocalNameNode
									.getTextContent())) {
						mohagoLocal = mohagoLocalNameNode.getTextContent();
						Database.DATABASE.setValue(mohagoLocal);// Remove this
																// altogether??
						Database.MOHAGO_LOCAL_NAME.setValue(eElement
								.getElementsByTagName("mohago_local").item(0)
								.getTextContent());
					}

					if (!StringUtils.isNullOrEmpty(jdbcType)
							&& !StringUtils.isNullOrEmpty(host)
							&& !StringUtils.isNullOrEmpty(port)
							&& !StringUtils.isNullOrEmpty(user)
							&& !StringUtils.isNullOrEmpty(pwd)
							&& !StringUtils.isNullOrEmpty(mohagoDb)
							&& !StringUtils.isNullOrEmpty(mohagoLocal)) {

						LOG.info("Starting to load db connection params from config.xml");

						// Don't need to remove from config file
						// userNode.getFirstChild().setTextContent("");
						// pwdNode.getFirstChild().setTextContent("");
						success = true;
						LOG.info("Loaded db connection params from config.xml");
					} else {
						success = false;
					}
				}
			}
		} else {
			success = false;
		}
		return success;
	}

	public static Connection getPooledConnection(String host) {

		if (!CONN_POOL_MAP.containsKey(host)) {
			String connStr = "jdbc:" + Configuration.Database.TYPE.getValue()
					+ "://" + host + ":"
					// + Constants.SQL.PSQL_DATABASE_PORT.getValue();
					+ Configuration.Database.PORT.getValue() + "/"
					+ Configuration.Database.USER.getValue();
			HikariConfig config = new HikariConfig();
			config.setConnectionTestQuery("SELECT 1");
			config.setJdbcUrl(connStr); // jdbc url specific to your database,
										// eg
										// jdbc:mysql://127.0.0.1/yourdb
			config.setUsername(Configuration.Database.USER.getValue());
			config.setPassword(Configuration.Database.PASSWORD.getValue());
			config.setAutoCommit(false);
			// config.setMaximumPoolSize(100);
			// config.setPartitionCount(1);
			CONN_POOL_MAP.putIfAbsent(host, new HikariDataSource(config));
		}
		// connectionPool = ; // setup the connection pool
		Connection conn = null;
		try {
			conn = CONN_POOL_MAP.get(host).getConnection();
		} catch (SQLException e) {
			LOG.error("Error with hikari: " + e);

		}
		return conn;
	}

}
