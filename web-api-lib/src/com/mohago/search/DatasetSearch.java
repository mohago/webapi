package com.mohago.search;

import java.security.InvalidParameterException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.mohago.request.RequestData;
import com.mohago.request.RequestMeta;
import com.mohago.serialization.Dataset;
import com.mohago.serialization.LookingGlassType;
import com.mohago.serialization.Meta;
import com.mohago.serialization.MetaDataBase;
import com.mohago.serialization.Pair;
import com.mysql.jdbc.StringUtils;

public class DatasetSearch {
	private static final Logger LOG = Logger.getLogger(DatasetSearch.class);

	/**
	 * 
	 * 
	 * @param datasetList
	 * @param metaQuery
	 * @throws InvalidParameterException
	 *             if there's a problem with the query string
	 */
	public static void applyMetaQueryToDatasetList(List<Dataset> datasetList,
			String metaQuery, Connection conn) throws InvalidParameterException {

		if (!StringUtils.isNullOrEmpty(metaQuery)) {// replace with stringutils
													// null
			String[] conditionList = metaQuery.split(",");
			for (String condition : conditionList) {
				String[] parts = condition.split("(?<=[<>!=])|(?=[<>!=])");
				String operator = "";
				String valueStr = "";
				String name = "";
				LookingGlassType type = null;
				if (parts.length == 4 && parts[2].equals("=")) {
					operator = parts[1] + "=";

				} else if (parts.length == 3) {
					operator = parts[1];
				} else {
					throw new InvalidParameterException(
							"Error parsing meta query condition, check format matches {meta-data name}{boolean comparator}{compare value}, e.g. \"voltage>=23.0\"");
				}

				name = parts[0];
				valueStr = parts[parts.length - 1];

				Object value = null;
				try {
					value = Integer.parseInt(valueStr);
					type = LookingGlassType.integer;
				} catch (NumberFormatException e) {
					try {
						value = Double.parseDouble(valueStr);
						type = LookingGlassType.double_precision;
					} catch (NumberFormatException ne) {
						try {
							value = new DateTime(valueStr);
							type = LookingGlassType.date_time;
						} catch (IllegalArgumentException ie) {
							value = valueStr;
							type = LookingGlassType.string_value;
						}
					}
				}

				Iterator<Dataset> iter = datasetList.iterator();
				// Now loop through each dataset and remove any which don't meet
				for (Iterator<Dataset> iterator = datasetList.iterator(); iterator
						.hasNext();) {
					Dataset dataset = iterator.next();

					Meta meta = RequestMeta.getDatasetMeta(dataset.getUid(),
							conn);
					boolean metaNameMatched = false;
					if (type.equals(LookingGlassType.integer)) {
						for (MetaDataBase<Integer> mdb : meta.getIntData()) {
							if (mdb.getName().equals(name)) {
								if (operator.equals("==")) {
									if (!mdb.getValue().equals(
											(Integer) (value))) {
										iterator.remove();
									}
								} else if (operator.equals("!=")) {
									if (mdb.getValue()
											.equals((Integer) (value))) {
										iterator.remove();
									}
								} else if (operator.equals("<=")) {
									if (mdb.getValue() > (Integer) (value)) {
										iterator.remove();
									}
								} else if (operator.equals(">=")) {
									if (mdb.getValue() < (Integer) (value)) {
										iterator.remove();
									}
								} else if (operator.equals(">")) {
									if (mdb.getValue() <= (Integer) (value)) {
										iterator.remove();
									}
								} else if (operator.equals("<")) {
									if (mdb.getValue() >= (Integer) (value)) {
										iterator.remove();
									}
								}
								metaNameMatched = true;
							}
						}
					} else if (type.equals(LookingGlassType.double_precision)) {
						for (MetaDataBase<Double> mdb : meta.getDoubleData()) {
							if (mdb.getName().equals(name)) {
								if (operator.equals("==")) {
									if (!mdb.getValue()
											.equals((Double) (value))) {
										iterator.remove();
									}
								} else if (operator.equals("!=")) {
									if (mdb.getValue().equals((Double) (value))) {
										iterator.remove();
									}
								} else if (operator.equals("<=")) {
									if (mdb.getValue() > (Double) (value)) {
										iterator.remove();
									}
								} else if (operator.equals(">=")) {
									if (mdb.getValue() < (Double) (value)) {
										iterator.remove();
									}
								} else if (operator.equals(">")) {
									if (mdb.getValue() <= (Double) (value)) {
										iterator.remove();
									}
								} else if (operator.equals("<")) {
									if (mdb.getValue() >= (Double) (value)) {
										iterator.remove();
									}
								}
								metaNameMatched = true;
							}
						}
					} else if (type.equals(LookingGlassType.date_time)) {
						for (MetaDataBase<Date> mdb : meta.getDateTimeData()) {
							if (mdb.getName().equals(name)) {
								if (operator.equals("==")) {
									if (!(mdb.getValue().compareTo(
											((DateTime) value).toDate()) == 0)) {
										iterator.remove();
									}
								} else if (operator.equals("!=")) {
									if (mdb.getValue().compareTo(
											((DateTime) value).toDate()) == 0) {
										iterator.remove();
									}
								} else if (operator.equals("<=")) {
									if (mdb.getValue().compareTo(
											((DateTime) value).toDate()) > 0) {
										iterator.remove();
									}
								} else if (operator.equals(">=")) {
									if (mdb.getValue().compareTo(
											((DateTime) value).toDate()) < 0) {
										iterator.remove();
									}
								} else if (operator.equals(">")) {
									if ((mdb.getValue().compareTo(
											((DateTime) value).toDate()) < 0)
											|| (mdb.getValue()
													.compareTo(
															((DateTime) value)
																	.toDate()) == 0)) {
										iterator.remove();
									}
								} else if (operator.equals("<")) {
									if ((mdb.getValue().compareTo(
											((DateTime) value).toDate()) > 0)
											|| (mdb.getValue()
													.compareTo(
															((DateTime) value)
																	.toDate()) == 0)) {
										iterator.remove();
									}
								}
								metaNameMatched = true;
							}
						}
					} else if (type.equals(LookingGlassType.string_value)) {
						for (MetaDataBase<String> mdb : meta.getStringData()) {
							if (mdb.getName().equals(name)) {
								if (operator.equals("==")) {
									if (!(mdb.getValue().equals((String) value))) {
										iterator.remove();
									}
								} else if (operator.equals("!=")) {
									if ((mdb.getValue().equals((String) value))) {
										iterator.remove();
									}
								}
								metaNameMatched = true;
							}
						}
					}
					if (!metaNameMatched) {
						iterator.remove();// meta name not found so user not
											// interested.?? Think this is
											// fine..? ..
					}
				}
			}
		} else {
			// don't apply metaQuery
		}
	}

	/**
	 * Parses the "sortBy" query parameter and returns corresponding sql to be
	 * appended to the a base sql query
	 * 
	 * @param sortBy
	 * @return
	 */
	public static ArrayList<Pair<Boolean, String>> parseSortBy(String sortBy) {
		ArrayList<Pair<Boolean, String>> ret = new ArrayList<Pair<Boolean,String>>();
		LOG.debug("in parse sort by: " + sortBy);
		if (!StringUtils.isNullOrEmpty(sortBy)) {// replace with stringutils
				
			// null
			String[] sortList = {sortBy};
			if(sortBy.contains(",")){
				sortList = sortBy.split(",");
			}
			LOG.debug("sortList 1: " + sortList[0]);
			for (String condition : sortList) {
				String[] parts = condition.split("(?<=[+-])|(?=[+-])");
				// System.out.println(Arrays.toString(parts));
				if (parts.length == 2 && parts[0].equals("+")) {
					ret.add(new Pair<Boolean, String>(false, parts[1]));
				} else if (parts.length == 2 && parts[0].equals("-")) {
					ret.add(new Pair<Boolean, String>(true, parts[1]));
				} else if (parts.length == 1) {
					ret.add(new Pair<Boolean, String>(false, parts[0]));
				}
			}
		}
		return ret;

	}

	public static Collection<? extends Dataset> searchByMetaQuery(
			String metaQuery, int memberId, Connection conn) {
		
		ArrayList<Dataset> datasetList = new ArrayList<Dataset>();
		String[] conditionList = metaQuery.split(",");
		for (String condition : conditionList) {
			String[] parts = condition.split("(?<=[<>!=])|(?=[<>!=])");
			String operator = "";
			String valueStr = "";
			String name = "";
			LookingGlassType type = null;
			if (parts.length == 4 && parts[2].equals("=")) {
				operator = parts[1] + "=";

			} else if (parts.length == 3) {
				operator = parts[1];
			} else {
				throw new InvalidParameterException(
						"Error parsing meta query condition, check format matches {meta-data name}{boolean comparator}{compare value}, e.g. \"voltage>=23.0\"");
			}

			name = parts[0];
			valueStr = parts[parts.length - 1];

			Object value = null;
			try {
				value = Integer.parseInt(valueStr);
				type = LookingGlassType.integer;
			} catch (NumberFormatException e) {
				try {
					value = Double.parseDouble(valueStr);
					type = LookingGlassType.double_precision;
				} catch (NumberFormatException ne) {
					try {
						value = new DateTime(valueStr);
						type = LookingGlassType.date_time;
					} catch (IllegalArgumentException ie) {
						value = valueStr;
						type = LookingGlassType.string_value;
					}
				}
			}
			String didList = RequestData.getDIDListFromMeta(type, name,
					operator, value, memberId, conn);
			if (!StringUtils.isNullOrEmpty(didList)) {
				datasetList.addAll(RequestData.specifiedDatasets(didList,
						memberId, conn));
			}

		}
		
		return datasetList;
	}

}
