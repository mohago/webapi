package com.mohago.charting;

import java.util.ArrayList;

import com.mohago.json.Chart;
import com.mohago.json.Point;
import com.mohago.serialization.Pair;

public class DownSampling {

	public static Chart largestTriangleThreeBuckets(
			ArrayList<Point<Number, Number>> data, int threshold) {
		Chart ret = new Chart();

		int dataLength = data.size();
		if (threshold >= dataLength || threshold == 0) {
			ret.setPoints(data);
			return ret; // Nothing to do
		}

		ArrayList<Point<Number, Number>> sampled = new ArrayList<Point<Number, Number>>();

		// Bucket size. Leave room for start and end data points
		double every = (double) (dataLength - 2) / (threshold - 2);

		int a = 0;
		Point<Number, Number> maxAreaPoint = new Point<Number, Number>(0.0, 0.0);
		int nextA = 0;

		sampled.add(data.get(a)); // Always add the first point

		for (int i = 0; i < threshold - 2; i++) {
			// Calculate point average for next bucket (containing c)
			double avgX = 0;
			double avgY = 0;
			int avgRangeStart = (int) (Math.floor((i + 1) * every) + 1);
			int avgRangeEnd = (int) (Math.floor((i + 2) * every) + 1);
			avgRangeEnd = avgRangeEnd < dataLength ? avgRangeEnd : dataLength;

			int avgRangeLength = avgRangeEnd - avgRangeStart;

			for (; avgRangeStart < avgRangeEnd; avgRangeStart++) {
				avgX += data.get(avgRangeStart).getX().doubleValue(); //
				avgY += data.get(avgRangeStart).getY().doubleValue();
			}
			avgX /= avgRangeLength;

			avgY /= avgRangeLength;

			// Get the range for this bucket
			int rangeOffs = (int) (Math.floor((i + 0) * every) + 1);
			int rangeTo = (int) (Math.floor((i + 1) * every) + 1);

			// Point a
			double pointAx = data.get(a).getX().doubleValue(); // enforce Number
																// (value
			// may be Date)
			double pointAy = data.get(a).getY().doubleValue();

			double maxArea = -1;

			for (; rangeOffs < rangeTo; rangeOffs++) {
				// Calculate triangle area over three buckets
				double area = Math.abs((pointAx - avgX)
						* (data.get(rangeOffs).getY().doubleValue() - pointAy)
						- (pointAx - data.get(rangeOffs).getX().doubleValue())
						* (avgY - pointAy)) * 0.5;
				if (area > maxArea) {
					maxArea = area;
					maxAreaPoint = data.get(rangeOffs);
					nextA = rangeOffs; // Next a is this b
				}
			}

			sampled.add(maxAreaPoint); // Pick this point from the bucket
			a = nextA; // This a is the next a (chosen b)
		}

		sampled.add(data.get(dataLength - 1)); // Always add last

		ret.setPoints(sampled);

		return ret;
	}

	public static Number[][] largestTriangleThreeBuckets(Number[][] data,
			Integer threshold) {
		Number[][] sampled = new Number[threshold][];

		if (data.length <= 2 || data.length <= threshold) {
			return data;
		}
		int sampled_index = 0;
		double every = (double) (data.length - 2) / (double) (threshold - 2);
		System.out.println(": " + every);
		int a = 0, next_a = 0;
		Number[] max_area_point = null;
		double max_area, area;

		sampled[sampled_index++] = data[a];

		for (int i = 0; i < threshold - 2; i++) {
			double avg_x = 0.0D, avg_y = 0.0D;
			int avg_range_start = (int) Math.floor((i + 1) * every) + 1;
			int avg_range_end = (int) Math.floor((i + 2) * every) + 1;
			avg_range_end = avg_range_end < data.length ? avg_range_end
					: data.length;
			int avg_range_length = (int) (avg_range_end - avg_range_start);
			while (avg_range_start < avg_range_end) {
				avg_x = avg_x + data[avg_range_start][0].doubleValue();
				avg_y += data[avg_range_start][1].doubleValue();
				avg_range_start++;
			}
			avg_x /= avg_range_length;
			avg_y /= avg_range_length;

			int range_offs = (int) Math.floor((i + 0) * every) + 1;
			int range_to = (int) Math.floor((i + 1) * every) + 1;

			double point_a_x = data[a][0].doubleValue();
			double point_a_y = data[a][1].doubleValue();

			max_area = area = -1;

			while (range_offs < range_to) {
				area = Math.abs((point_a_x - avg_x)
						* (data[range_offs][1].doubleValue() - point_a_y)
						- (point_a_x - data[range_offs][0].doubleValue())
						* (avg_y - point_a_y)) * 0.5D;
				if (area > max_area) {
					max_area = area;
					max_area_point = data[range_offs];
					next_a = range_offs;
				}
				range_offs++;
			}
			sampled[sampled_index++] = max_area_point;
			a = next_a;
		}

		sampled[sampled_index++] = data[data.length - 1];
		return sampled;
	}

	public static Number[][] largestTriangleThreeBucketsTime(Number[][] data,
			Integer threshold) {
		Number[][] sampled = new Number[threshold][];

		if (data.length <= 2 || data.length <= threshold) {
			return data;
		}
		int bucket_interval = (int) ((data[data.length - 1][0].longValue() - data[0][0]
				.longValue()) / threshold);
		int sampled_index = 0;
		double every = (double) (data.length - 2) / (double) (threshold - 2);
		int a = 0, next_a = 0;
		Number[] max_area_point = null;
		double max_area, area;

		sampled[sampled_index++] = data[a];

		for (int i = 0; i < threshold - 2; i++) {
			double avg_x = 0.0D, avg_y = 0.0D;
			int avg_range_start = (int) Math.floor((i + 1) * every) + 1;
			int avg_range_end = (int) Math.floor((i + 2) * every) + 1;
			avg_range_end = avg_range_end < data.length ? avg_range_end
					: data.length;
			int avg_range_length = (int) (avg_range_end - avg_range_start);
			while (avg_range_start < avg_range_end) {
				avg_x = avg_x + data[avg_range_start][0].doubleValue();
				avg_y += data[avg_range_start][1].doubleValue();
				avg_range_start++;
			}
			avg_x /= avg_range_length;
			avg_y /= avg_range_length;

			int range_offs = (int) Math.floor((i + 0) * every) + 1;
			int range_to = (int) Math.floor((i + 1) * every) + 1;

			double point_a_x = data[a][0].doubleValue();
			double point_a_y = data[a][1].doubleValue();

			max_area = area = -1;

			long ending_time = data[range_offs][0].longValue()
					+ bucket_interval;

			while (range_offs < range_to) { // &&
											// data[range_offs][0].longValue() <
											// ending_time) {
				area = Math.abs((point_a_x - avg_x)
						* (data[range_offs][1].doubleValue() - point_a_y)
						- (point_a_x - data[range_offs][0].doubleValue())
						* (avg_y - point_a_y)) * 0.5D;
				if (area > max_area) {
					max_area = area;
					max_area_point = new Number[] { ending_time,
							data[range_offs][1] };
					next_a = range_offs;
				}
				range_offs++;
			}
			sampled[sampled_index++] = max_area_point;
			a = next_a;
		}

		sampled[sampled_index++] = data[data.length - 1];
		return sampled;
	}

}
