package com.mohago.sql;

import com.mohago.config.Configuration;

public class SQLPreparedStatements {

	public enum Update {
		META_INT(
				"UPDATE mohago_db_3.meta_int SET value=? WHERE d_id=? AND name=?"), META_DOUBLE(
				"UPDATE mohago_db_3.meta_double SET value=? WHERE d_id=? AND name=?"), META_STRING(
				"UPDATE mohago_db_3.meta_string SET value=? WHERE d_id=? AND name=?"), META_DATE(
				"UPDATE mohago_db_3.meta_date SET value=? WHERE d_id=? AND name=?"), SET_ITEM_COUNT(
				"UPDATE mohago_db_3.dataset SET item_count=? WHERE deleted IS NULL AND d_id=?"), UPDATE_PARAM_COUNT_TABLE_INDEX(
				"UPDATE mohago_db_3.table_index SET parameter_count=? WHERE t_id=? AND d_id=?"), MARK_DATASET_AS_DELETED(
				"UPDATE mohago_db_3.dataset SET deleted=1 WHERE d_id=? AND deleted IS null"), CHANGE_PROJECT(
				"UPDATE mohago_db_3.dataset SET project=? WHERE d_id=?"), ;
		private Update(String value) {
			this.value = value;
		}

		private String value;

		public String getValue() {
			String ret = "";
			ret = value.replace("mohago_db_3",
					Configuration.Database.MOHAGO_DB_NAME.getValue());
			return ret;
		}
	}

	public enum Delete {
		META_INT("DELETE FROM mohago_db_3.meta_int WHERE d_id=? AND name=?"), META_DOUBLE(
				"DELETE FROM mohago_db_3.meta_double WHERE d_id=? AND name=?"), META_STRING(
				"DELETE FROM mohago_db_3.meta_string WHERE d_id=? AND name=?"), META_DATE(
				"DELETE FROM mohago_db_3.meta_date WHERE d_id=? AND name=?"), REMOVE_FILE_FROM_DELETE_TABLE(
				"DELETE FROM mohago_db_3.file_delete WHERE id=?"), DELETE_PROJ_ASSOC_DATASETS(
				"DELETE FROM mohago_db_3.dataset WHERE project=?"), DELETE_OBF_TABLE(
				"DROP TABLE IF EXISTS "), DELETE_DATASET_ACCESS_ENTRY(
				"DELETE FROM mohago_db_3.dataset_access WHERE d_id=? AND m_id=? AND owner_m_id=?"), REMOVE_IP_FROM_FREE_TABLE(
				"DELETE FROM mohago_db_3.free_ip WHERE id=?"), DELETE_OLD_DATASETS(
				"DELETE FROM mohago_db_3.dataset WHERE deleted=1 AND mohago_db_3.dataset.date < (NOW() - ? * interval '1 day')"), REMOVE_DATASET_STREAM_FLAG(
				"DELETE FROM mohago_db_3.dataset_stream_flag WHERE d_id=?"),META_INT_ALL("DELETE FROM mohago_db_3.meta_int WHERE d_id=?"), META_DOUBLE_ALL(
						"DELETE FROM mohago_db_3.meta_double WHERE d_id=?"), META_STRING_ALL(
								"DELETE FROM mohago_db_3.meta_string WHERE d_id=?"), META_DATE_ALL(
								"DELETE FROM mohago_db_3.meta_date WHERE d_id=?") ;
		private Delete(String value) {
			this.value = value;
		}

		private String value;

		public String getValue() {

			String ret = "";
			ret = value.replace("mohago_db_3",
					Configuration.Database.MOHAGO_DB_NAME.getValue());
			return ret;
		}
	}

	public enum CreateTables {

		DATABASE_3("CREATE DATABASE IF NOT EXISTS mohago_db_3"), DATASET(
				"CREATE  TABLE IF NOT EXISTS mohago_db_3.dataset"
						+ " (d_id BIGINT ," + "name VARCHAR(250) NOT NULL ,"
						+ "project VARCHAR(250) NOT NULL ,"
						+ "date TIMESTAMP(4)," + "item_count INT NULL ,"
						+ "deleted INTEGER NULL ,"
						+ "device_token VARCHAR(32) NULL ,"
						+ "PRIMARY KEY (d_id))"), DATASET_ACCESS(
				"CREATE TABLE IF NOT EXISTS mohago_db_3.dataset_access (d_id BIGINT NOT NULL, m_id INT NOT NULL, owner_m_id INT NOT NULL,"
						+ " FOREIGN KEY (d_id)"
						+ " REFERENCES mohago_db_3.dataset (d_id )"
						+ " ON DELETE CASCADE " + "ON UPDATE CASCADE)"), DATASET_HISTORY(
				"CREATE TABLE IF NOT EXISTS mohago_db_3.dataset_history (d_id BIGINT NOT NULL, u_id INT NOT NULL, item_count INT NOT NULL, mod_type INT NOT NULL,"
						+ " modified_at TIMESTAMP NOT NULL,"
						+ " FOREIGN KEY (d_id)"
						+ " REFERENCES mohago_db_3.dataset (d_id )"
						+ " ON DELETE CASCADE " + "ON UPDATE CASCADE)"), TABLE_INDEX(
				"CREATE  TABLE IF NOT EXISTS mohago_db_3.table_index ("
						+ " d_id BIGINT NOT NULL ," + " t_id INT NOT NULL ,"
						+ " table_name VARCHAR(255) NULL ,"
						+ " index_start INTEGER NOT NULL ,"
						+ " parameter_count INT NOT NULL ,"
						+ " PRIMARY KEY (t_id, d_id) ,"
						+ " CONSTRAINT fk_d_id_tables FOREIGN KEY (d_id )"
						+ " REFERENCES mohago_db_3.dataset (d_id) "
						+ "ON DELETE CASCADE ON UPDATE CASCADE)"), DATASET_STREAM_FLAG(
				"CREATE TABLE IF NOT EXISTS mohago_db_3.dataset_stream_flag ("
						+ " d_id BIGINT NOT NULL," + " PRIMARY KEY (d_id) ,"
						+ " CONSTRAINT fk_d_id_stream FOREIGN KEY (d_id )"
						+ " REFERENCES mohago_db_3.dataset (d_id) "
						+ "ON DELETE CASCADE ON UPDATE CASCADE)"),

		META_STRING(
				"CREATE TABLE IF NOT EXISTS mohago_db_3.meta_string (d_id BIGINT NOT NULL , name VARCHAR(100) NOT NULL, value VARCHAR(255) NOT NULL,"
						+ " FOREIGN KEY (d_id )"
						+ " REFERENCES mohago_db_3.dataset (d_id )"
						+ " ON DELETE CASCADE " + "ON UPDATE CASCADE)" + " "), META_DOUBLE(
				"CREATE TABLE IF NOT EXISTS mohago_db_3.meta_double (d_id BIGINT NOT NULL , name VARCHAR(100) NOT NULL, value DOUBLE PRECISION NOT NULL,"
						+ " FOREIGN KEY (d_id )"
						+ " REFERENCES mohago_db_3.dataset (d_id )"
						+ " ON DELETE CASCADE " + "ON UPDATE CASCADE)" + " "), META_INT(
				"CREATE TABLE IF NOT EXISTS mohago_db_3.meta_int (d_id BIGINT NOT NULL , name VARCHAR(100) NOT NULL, value INT NOT NULL,"
						+ " FOREIGN KEY (d_id )"
						+ " REFERENCES mohago_db_3.dataset (d_id )"
						+ " ON DELETE CASCADE " + "ON UPDATE CASCADE)" + " "), META_DATE(
				"CREATE TABLE IF NOT EXISTS mohago_db_3.meta_date (d_id BIGINT NOT NULL , name VARCHAR(100) NOT NULL, value TIMESTAMP(4) NOT NULL,"
						+ " FOREIGN KEY (d_id )"
						+ " REFERENCES mohago_db_3.dataset (d_id )"
						+ " ON DELETE CASCADE " + "ON UPDATE CASCADE)" + " "), FILE_HASH(
				"CREATE TABLE IF NOT EXISTS mohago_db_3.hashes ( "
						+ "d_id BIGINT NOT NULL, " + "hash VARCHAR(50) NULL , "
						+ "file VARCHAR(255) NULL , " + "CONSTRAINT d_id "
						+ "FOREIGN KEY (d_id ) "
						+ "REFERENCES mohago_db_3.dataset (d_id ) "
						+ "ON DELETE CASCADE " + "ON UPDATE CASCADE) " + ""), FILE_DELETE(
				"CREATE TABLE IF NOT EXISTS mohago_db_3.file_delete ("
						+ "id SERIAL, " + "name VARCHAR(400) NOT NULL,"
						+ "PRIMARY KEY (id)" + ") "), PARAMETER_NAME_INDEX(
				"CREATE TABLE IF NOT EXISTS mohago_db_3.parameter_name_index ("
						+ "id SERIAL," + "d_id BIGINT NOT NULL, "
						+ "t_id INTEGER NOT NULL," + "name VARCHAR(255),"
						+ "param_index INTEGER  NOT NULL," + "type INTEGER ,"
						+ " PRIMARY KEY (id) ," + " FOREIGN KEY (d_id )"
						+ " REFERENCES mohago_db_3.dataset (d_id )"
						+ " ON DELETE CASCADE " + "ON UPDATE CASCADE)"), CREATE_PARAMETER_NAME_INDEX_INDEX(
				"CREATE INDEX index_d_id_parameters ON mohago_db_3.parameter_name_index (d_id ASC)"), PARAMETER_NAME_CONSTRAINT(
				"CREATE UNIQUE INDEX parameter_name_index_unq ON mohago_db_3.parameter_name_index (d_id, name)"), TABLE_INDEX_INDEX(
				"CREATE INDEX index_d_id_tables ON mohago_db_3.table_index (d_id ASC)"), META_INT_CONSTRAINT(
				"CREATE UNIQUE INDEX meta_int_unq ON mohago_db_3.meta_int (d_id, name)"), META_DOUBLE_CONSTRAINT(
				"CREATE UNIQUE INDEX meta_double_unq ON mohago_db_3.meta_double (d_id, name)"), META_STRING_CONSTRAINT(
				"CREATE UNIQUE INDEX meta_string_unq ON mohago_db_3.meta_string (d_id, name)"), META_DATE_CONSTRAINT(
				"CREATE UNIQUE INDEX meta_date_unq ON mohago_db_3.meta_date (d_id, name)"), DATASET_ACCESS_CONSTRAINT(
				"CREATE UNIQUE INDEX dataset_access_unq ON mohago_db_3.dataset_access (d_id, m_id, owner_m_id)"), EMPTY_TABLE(
				""), ;

		private CreateTables(String value) {
			this.value = value;
		}

		private String value;

		public String getValue() {
			String ret = "";
			ret = value.replace("mohago_db_3",
					Configuration.Database.MOHAGO_DB_NAME.getValue());
			return ret;
		}

	}

	public enum Select {

		GET_LIST_OF_TABLES("SELECT * FROM mohago_db_3.table_index"), SELECT_MAX_TID(
				"SELECT MAX(t_id) FROM mohago_db_3.table_index"), GET_MAX_DID(
				"SELECT MAX(d_id) FROM mohago_db_3.dataset"), RETRIEVE_ALL_DATASET_IDS(
				"SELECT d_id FROM mohago_db_3.dataset WHERE deleted IS NULL"), GET_DATASET_ASSOCIATED_TABLE_UIDS(
				"SELECT t_id FROM mohago_db_3.table_index WHERE d_id =?"), GET_DATASET_AND_TABLE_UIDS(
				"SELECT * FROM mohago_db_3.table_index WHERE d_id IN "), RETRIEVE_DATASET(
				"SELECT * FROM mohago_db_3.dataset INNER JOIN mohago_db_3.dataset_access"
						+ " ON (dataset.d_id=dataset_access.d_id) WHERE m_id=? AND dataset.d_id=? AND deleted IS NULL"), SELECT_DATASET_DATE_PROJ_BOUNDS(
				"SELECT d_id FROM mohago_db_3.dataset WHERE project=? AND deleted IS NULL AND dataset.date BETWEEN ? AND ?"), GET_ALL_PROJECT_NAMES(
				"SELECT DISTINCT project FROM mohago_db_3.dataset WHERE deleted IS NULL"), GET_D_ID_LIST_FROM_PROJECT_LIST(
				"SELECT d_id FROM mohago_db_3.dataset WHERE deleted IS NULL"), SET_SCHEMA(
				"SET SCHEMA '%s'"), GET_PROJ_NAME_BY_D_ID(
				"SELECT project FROM mohago_db_3.dataset WHERE deleted IS NULL AND d_id=?"), GET_META_INT_BY_DID(
				"SELECT * FROM mohago_db_3.meta_int WHERE d_id=?"), GET_META_DOUBLE_BY_DID(
				"SELECT * FROM mohago_db_3.meta_double WHERE d_id=?"), GET_META_STRING_BY_DID(
				"SELECT * FROM mohago_db_3.meta_string WHERE d_id=?"), GET_META_DATE_BY_DID(
				"SELECT * FROM mohago_db_3.meta_date WHERE d_id=?"), GET_HASHES(
				"SELECT hash,file FROM mohago_db_3.hashes WHERE d_id=?"), GET_MAX_D_ID(
				"SELECT d_id FROM mohago_db_3.dataset WHERE d_id = (select max(d_id) from mohago_db_3.dataset)"), GET_NEXT_TID(
				"SELECT t_id FROM mohago_db_3.table_index WHERE d_id=? ORDER BY t_id DESC LIMIT 1"), GET_FILE_DELETE_QUEUE_LENGTH(
				"SELECT COUNT(*) FROM mohago_db_3.file_delete"), GET_NEXT_FILE_FOR_DELETION(
				"SELECT id,name FROM mohago_db_3.file_delete LIMIT 1"), GET_ID_NEXT_FILE(
				"SELECT id FROM mohago_db_3.file_delete WHERE name=?"), GET_TABLE_INFO(
				"SHOW COLUMNS FROM mohago_db_3."), GET_NAME_TYPE_INDEX(
				"SELECT name,type,param_index FROM mohago_db_3.parameter_name_index WHERE t_id=? AND d_id=?"), GET_NAME_TYPE(
				"SELECT name,type FROM mohago_db_3.parameter_name_index WHERE d_id=?"), GET_TABLE_NAMES(
				"SELECT table_name FROM mohago_db_3.table_index WHERE d_id=?"), GET_SPECIFIED_VALUES(
				"SELECT id,%s FROM %s WHERE id >=? AND id <=?"), GET_VALUES(
				"SELECT id,%s FROM %s"), GET_SPECIFIED_TIMESTAMP_VALUES(
				"SELECT id,to_char(%s,'YYYY-MM-DD HH24:MI:SS.MS') FROM %s WHERE id >=? AND id <=?"), GET_TIMESTAMP_VALUES(
				"SELECT id,to_char(%s,'YYYY-MM-DD HH24:MI:SS.MS') FROM %s"), GET_ITEM_COUNT(
				"SELECT id FROM mohago_db_3.%s ORDER BY id DESC LIMIT 1"), GET_OBF_TABLE_NAME(
				"SELECT table_name FROM mohago_db_3.table_index WHERE d_id=?"), GET_PARAM_NAME_FROM_PSEUDONYM(
				"SELECT name FROM mohago_db_3.parameter_name_index WHERE obf_table_name=? AND pseudonym=?"), GET_PSEUDONYM_FROM_PARAM_NAME(
				"SELECT param_index FROM mohago_db_3.parameter_name_index WHERE t_id=? AND name=? AND d_id=?"), GET_TID_FROM_OBF_NAME(
				"SELECT t_id FROM mohago_db_3.table_index WHERE table_name=?"), GET_TID_DID_FROM_OBF_NAME(
				"SELECT t_id,d_id FROM mohago_db_3.table_index WHERE table_name=?"), SELECT_D_ID_BY_PROJ(
				"SELECT d_id FROM mohago_db_3.dataset WHERE deleted IS NULL AND project=?"), GET_ALLOWED_READ_DID_LIST_FOR_MEMBER(
				"SELECT d_id FROM mohago_db_3.dataset_access WHERE m_id=?"), SELECT_DATASET_DATE_BOUNDS(
				"SELECT d_id FROM mohago_db_3.dataset WHERE deleted IS NULL AND dataset.date BETWEEN ? AND ?"), SELECT_CHARTV2_WHERE(
				"SELECT @params FROM @tables WHERE @where"), SELECT_CHARTV2(
				"SELECT @params FROM @tables"), GET_OBF_NAME_PSEUDONYM_FROM_NAME(
				"SELECT table_index.table_name,parameter_name_index.param_index,parameter_name_index.type FROM mohago_db_3.parameter_name_index,mohago_db_3.table_index WHERE  parameter_name_index.name=? AND table_index.d_id=? AND parameter_name_index.t_id=table_index.t_id"), GET_TID_PSEUDONYM_FROM_NAME_TYPE(
				"SELECT t_id,param_index FROM mohago_db_3.parameter_name_index WHERE name=? AND type=? AND d_id=?"), GET_OBF_NAME_FROM_TID(
				"SELECT table_name FROM mohago_db_3.table_index WHERE t_id=? AND d_id=?"), GET_ID_X_Y_VALUES_SAME_TABLE(
				"SELECT @table.id, @table.@param1, @table.@param2 FROM mohago_db_3.@table WHERE @table.@param1 IS NOT NULL AND @table.@param2 IS NOT NULL"), GET_ID_X_Y_VALUES_DIFF_TABLE(
				"SELECT @obf1.id, @obf1.@param1, @obf2.@param2 FROM mohago_db_3.@obf1, mohago_db_3.@obf2 WHERE @obf1.@param1 IS NOT NULL AND @obf2.@param2 IS NOT NULL"), GET_X_Y_VALUES_SAME_TABLE(
				"SELECT @table.@param1, @table.@param2 FROM mohago_db_3.@table WHERE @table.@param1 IS NOT NULL AND @table.@param2 IS NOT NULL"), GET_X_Y_VALUES_DIFF_TABLE(
				"SELECT @obf1.@param1, @obf2.@param2 FROM mohago_db_3.@obf1, mohago_db_3.@obf2 WHERE @obf1.@param1 IS NOT NULL AND @obf2.@param2 IS NOT NULL"), GET_MAX_TID_FOR_DID(
				"SELECT t_id,table_name,parameter_count FROM mohago_db_3.table_index WHERE d_id=? ORDER BY t_id DESC LIMIT 1"), GET_ALLOWED_WRITE_DID_LIST_FOR_MEMBER(
				"SELECT d_id FROM mohago_db_3.dataset_access WHERE owner_m_id=?"), DOES_DATASET_EXIST(
				"SELECT COUNT(*) FROM mohago_db_3.dataset WHERE deleted IS NULL AND d_id=?"), GET_CONNECTION_IP_FOR_MEMBER(
				"SELECT conn_ip FROM mohago_db_3.address_conn_lookup WHERE member_id = ANY (?)"), GET_IP_FOR_ADDRESS(
				"SELECT COUNT(*) FROM mohago_db_3.address_conn_lookup WHERE address_id=?"), GET_FREE_IP(
				"SELECT id,ip FROM mohago_db_3.free_ip ORDER BY id DESC LIMIT 1"), GET_CONNECTION_IP_FOR_ADDRESS(
				"SELECT conn_ip FROM mohago_db_3.address_conn_lookup WHERE address_id = ANY(?)"), GET_TABLE_NAMES_FOR_DELETION(
				"SELECT string_agg(table_name,',mohago_db_3.') FROM mohago_db_3.table_index WHERE d_id in(SELECT d_id FROM mohago_db_3.dataset WHERE deleted=1 AND mohago_db_3.dataset.date < (NOW() - ? * interval '1 day'))"), GET_ALL_CONNECTION_IPS(
				"SELECT DISTINCT ip_address FROM mohago_db_3.node"), SELECT_DATASET(
				"SELECT * FROM mohago_db_3.dataset INNER JOIN mohago_db_3.dataset_access"
						+ " ON (dataset.d_id=dataset_access.d_id) WHERE m_id=? AND UPPER(name) LIKE UPPER('%' || ? || '%') "
						+ "AND UPPER(project) LIKE UPPER('%' || ? || '%') "
						+ "AND deleted IS NULL "
						+ "AND dataset.date BETWEEN ? AND ?"), GET_ALL_DATASETS(
				"SELECT * FROM mohago_db_3.dataset INNER JOIN mohago_db_3.dataset_access"
						+ " ON (dataset.d_id=dataset_access.d_id) WHERE m_id=? AND deleted IS NULL"), GET_SPECIFIED_DATASETS(
				"SELECT * FROM mohago_db_3.dataset INNER JOIN mohago_db_3.dataset_access"
						+ " ON (dataset.d_id=dataset_access.d_id) WHERE m_id=? AND deleted IS NULL AND dataset.d_id IN ("), RETRIEVE_DATASET_BY_DID(
				"SELECT * FROM mohago_db_3.dataset WHERE d_id=? AND deleted IS NULL"), GET_CONNECTION_IP_FOR_DATASET_BY_TOKEN(
				"SELECT conn_ip FROM mohago_db_3.dataset_conn_lookup WHERE device_token=?"), GET_DEVICE_TOKEN_FROM_DID(
				"SELECT device_token FROM mohago_db_3.dataset WHERE d_id=? AND deleted IS NULL"), IS_DATASET_BEING_STREAMED_TO(
				"SELECT d_id FROM mohago_db_3.dataset_stream_flag WHERE d_id=?"), GET_ALL_NODE_CONNECTION_IP_ADDRESSES(
				"SELECT conn_ip FROM mohago_db_3.address_conn_lookup"), GET_NODE_LIST(
				"SELECT id,ip_address FROM mohago_db_3.node"), GET_DID_FROM_DEVICE_TOKEN(
				"SELECT d_id FROM mohago_db_3.dataset WHERE device_token=? AND deleted IS NULL"), GET_META_INT_MULTIPLE_DID(
				"SELECT * FROM mohago_db_3.meta_int WHERE d_id IN "), GET_META_DOUBLE_MULTIPLE_DID(
				"SELECT * FROM mohago_db_3.meta_double WHERE d_id IN "), GET_META_STRING_MULTIPLE_DID(
				"SELECT * FROM mohago_db_3.meta_string WHERE d_id IN "), GET_META_DATE_TIME_MULTIPLE_DID(
				"SELECT * FROM mohago_db_3.meta_date WHERE d_id IN ");

		private Select(String value) {
			this.value = value;
		}

		private String value;

		public String getValue() {
			String ret = "";
			ret = value.replace("mohago_db_3",
					Configuration.Database.MOHAGO_DB_NAME.getValue());
			return ret;
		}

	}

	public enum Insert {

		INSERT_DATASET(
				"INSERT INTO mohago_db_3.dataset(d_id,name,project,date,device_token) values(?,?,?,?,?)"), META_DOUBLE(
				"INSERT INTO mohago_db_3.meta_double(d_id,name,value) values(?,?,?)"), META_STRING(
				"INSERT INTO mohago_db_3.meta_string(d_id,name,value) values(?,?,?)"), META_INT(
				"INSERT INTO mohago_db_3.meta_int(d_id,name,value) values(?,?,?)"), META_DATE(
				"INSERT INTO mohago_db_3.meta_date(d_id,name,value) values(?,?,?)"), INSERT_FILE_HASH(
				"INSERT INTO mohago_db_3.hashes(d_id,hash,file) values(?,?,?)"), INSERT_TABLE_INDEX(
				"INSERT INTO mohago_db_3.table_index(d_id,t_id,table_name,index_start,parameter_count) values(?,?,?,?,?)"), ADD_FILE_TO_DELETE_TABLE(
				"INSERT INTO mohago_db_3.file_delete(name) values(?)"), INSERT_PARAM_NAME_INDEX(
				"INSERT INTO mohago_db_3.parameter_name_index(d_id,t_id,name,param_index,type) values(?,?,?,?,?)"), INSERT_DATASET_ACCESS_ENTRY(
				"INSERT INTO mohago_db_3.dataset_access(d_id,m_id, owner_m_id) values(?,?,?)"), INSERT_DATASET_HISTORY_ENTRY(
				"INSERT INTO mohago_db_3.dataset_history(d_id,u_id,item_count,mod_type,modified_at) values(?,?,?,?,NOW())"), INSERT_DATASET_STREAM_FLAG(
				"INSERT INTO mohago_db_3.dataset_stream_flag (d_id) values(?)"), ;

		private Insert(String value) {
			this.value = value;
		}

		private String value;

		public String getValue() {
			String ret = "";
			ret = value.replace("mohago_db_3",
					Configuration.Database.MOHAGO_DB_NAME.getValue());
			return ret;
		}

	}

	public enum IndexNames {

		PARAMETER_NAME_INDEX("mohago_db_3.index_d_id_parameters"), TABLE_INDEX(
				"mohago_db_3.index_d_id_tables"), ;

		private IndexNames(String value) {
			this.value = value;
		}

		private String value;

		public String getValue() {
			String ret = "";
			ret = value.replace("mohago_db_3",
					Configuration.Database.MOHAGO_DB_NAME.getValue());

			return value;
		}

	}

	public enum ConstraintNames {

		PARAMETER_NAME_INDEX("mohago_db_3.parameter_name_index_unq"), META_DATE(
				"mohago_db_3.meta_date_unq"), META_DOUBLE(
				"mohago_db_3.meta_double_unq"), META_INT(
				"mohago_db_3.meta_int_unq"), META_STRING(
				"mohago_db_3.meta_string_unq"), DATASET_ACCESS(
				"mohago_db_3.dataset_access_unq");

		private ConstraintNames(String value) {
			this.value = value;
		}

		private String value;

		public String getValue() {
			String ret = "";
			ret = value.replace("mohago_db_3",
					Configuration.Database.MOHAGO_DB_NAME.getValue());

			return value;
		}

	}

	public enum MetaQuery {
		INT_EQUALS(
				"SELECT meta_int.d_id FROM mohago_db_3.meta_int INNER JOIN mohago_db_3.dataset_access ON (meta_int.d_id=dataset_access.d_id) WHERE m_id=? AND meta_int.name=? AND meta_int.value =?"), INT_NOT_EQUALS(
				"SELECT meta_int.d_id FROM mohago_db_3.meta_int INNER JOIN mohago_db_3.dataset_access ON (meta_int.d_id=dataset_access.d_id) WHERE m_id=? AND meta_int.name=? AND meta_int.value !=?"), INT_GREATER(
				"SELECT meta_int.d_id FROM mohago_db_3.meta_int INNER JOIN mohago_db_3.dataset_access ON (meta_int.d_id=dataset_access.d_id) WHERE m_id=? AND meta_int.name=? AND meta_int.value >?"), INT_LESS(
				"SELECT meta_int.d_id FROM mohago_db_3.meta_int INNER JOIN mohago_db_3.dataset_access ON (meta_int.d_id=dataset_access.d_id) WHERE m_id=? AND meta_int.name=? AND meta_int.value <?"), INT_GREATER_EQUALS(
				"SELECT meta_int.d_id FROM mohago_db_3.meta_int INNER JOIN mohago_db_3.dataset_access ON (meta_int.d_id=dataset_access.d_id) WHERE m_id=? AND meta_int.name=? AND meta_int.value >=?"), INT_LESS_EQUALS(
				"SELECT meta_int.d_id FROM mohago_db_3.meta_int INNER JOIN mohago_db_3.dataset_access ON (meta_int.d_id=dataset_access.d_id) WHERE m_id=? AND meta_int.name=? AND meta_int.value <=?"), DOUBLE_EQUALS(
				"SELECT meta_double.d_id FROM mohago_db_3.meta_double INNER JOIN mohago_db_3.dataset_access ON (meta_double.d_id=dataset_access.d_id) WHERE m_id=? AND meta_double.name=? AND meta_double.value =?"), DOUBLE_NOT_EQUALS(
				"SELECT meta_double.d_id FROM mohago_db_3.meta_double INNER JOIN mohago_db_3.dataset_access ON (meta_double.d_id=dataset_access.d_id) WHERE m_id=? AND meta_double.name=? AND meta_double.value !=?"), DOUBLE_GREATER(
				"SELECT meta_double.d_id FROM mohago_db_3.meta_double INNER JOIN mohago_db_3.dataset_access ON (meta_double.d_id=dataset_access.d_id) WHERE m_id=? AND meta_double.name=? AND meta_double.value >?"), DOUBLE_LESS(
				"SELECT meta_double.d_id FROM mohago_db_3.meta_double INNER JOIN mohago_db_3.dataset_access ON (meta_double.d_id=dataset_access.d_id) WHERE m_id=? AND meta_double.name=? AND meta_double.value <?"), DOUBLE_GREATER_EQUALS(
				"SELECT meta_double.d_id FROM mohago_db_3.meta_double INNER JOIN mohago_db_3.dataset_access ON (meta_double.d_id=dataset_access.d_id) WHERE m_id=? AND meta_double.name=? AND meta_double.value >=?"), DOUBLE_LESS_EQUALS(
				"SELECT meta_double.d_id FROM mohago_db_3.meta_double INNER JOIN mohago_db_3.dataset_access ON (meta_double.d_id=dataset_access.d_id) WHERE m_id=? AND meta_double.name=? AND meta_double.value <=?"), DATE_TIME_EQUALS(
				"SELECT meta_date.d_id FROM mohago_db_3.meta_date INNER JOIN mohago_db_3.dataset_access ON (meta_date.d_id=dataset_access.d_id) WHERE m_id=? AND meta_date.name=? AND meta_date.value =?"), DATE_TIME_NOT_EQUALS(
				"SELECT meta_date.d_id FROM mohago_db_3.meta_date INNER JOIN mohago_db_3.dataset_access ON (meta_date.d_id=dataset_access.d_id) WHERE m_id=? AND meta_date.name=? AND meta_date.value !=?"), DATE_TIME_GREATER(
				"SELECT meta_date.d_id FROM mohago_db_3.meta_date INNER JOIN mohago_db_3.dataset_access ON (meta_date.d_id=dataset_access.d_id) WHERE m_id=? AND meta_date.name=? AND meta_date.value >?"), DATE_TIME_LESS(
				"SELECT meta_date.d_id FROM mohago_db_3.meta_date INNER JOIN mohago_db_3.dataset_access ON (meta_date.d_id=dataset_access.d_id) WHERE m_id=? AND meta_date.name=? AND meta_date.value <?"), DATE_TIME_GREATER_EQUALS(
				"SELECT meta_date.d_id FROM mohago_db_3.meta_date INNER JOIN mohago_db_3.dataset_access ON (meta_date.d_id=dataset_access.d_id) WHERE m_id=? AND meta_date.name=? AND meta_date.value >=?"), DATE_TIME_LESS_EQUALS(
				"SELECT meta_date.d_id FROM mohago_db_3.meta_date INNER JOIN mohago_db_3.dataset_access ON (meta_date.d_id=dataset_access.d_id) WHERE m_id=? AND meta_date.name=? AND meta_date.value <=?"), STRING_EQUALS(
				"SELECT meta_string.d_id FROM mohago_db_3.meta_string INNER JOIN mohago_db_3.dataset_access ON (meta_string.d_id=dataset_access.d_id) WHERE m_id=? AND meta_string.name=? AND meta_string.value =?"), STRING_NOT_EQUALS(
				"SELECT meta_string.d_id FROM mohago_db_3.meta_string INNER JOIN mohago_db_3.dataset_access ON (meta_string.d_id=dataset_access.d_id) WHERE m_id=? AND meta_string.name=? AND meta_string.value !=?");
		private MetaQuery(String value) {
			this.value = value;
		}

		private String value;

		public String getValue() {
			String ret = "";
			ret = value.replace("mohago_db_3",
					Configuration.Database.MOHAGO_DB_NAME.getValue());

			return value;
		}
	}

}
