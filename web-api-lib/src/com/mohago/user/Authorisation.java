package com.mohago.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;

import org.apache.log4j.Logger;

import com.mohago.sql.SQLPreparedStatements;
import com.mohago.utilities.Constants;
import com.mohago.utilities.SQLHelpers;

public class Authorisation {
	private final static Logger LOG = Logger.getLogger(Authorisation.class);

	public static HashSet<Long> getAllowedReadDidList(int idMember,
			Connection conn) {
		HashSet<Long> ret = new HashSet<Long>();
		LOG.info("Getting access for m_id: " + idMember);
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_ALLOWED_READ_DID_LIST_FOR_MEMBER
							.getValue());
			preparedStatement.setInt(1, idMember);
			resultSet = preparedStatement.executeQuery();
			conn.commit();
			while(resultSet.next()){
				ret.add(resultSet.getLong("d_id"));
			}

		} catch (SQLException e) {
			LOG.error("Error getting dataset ids for m_id (read access): " + e);
			//SQLHelpers.rollBack(conn, "getting dataset ids for m_id (read access)");
		} finally {
			SQLHelpers.closeResources(preparedStatement, resultSet);
		}
		return ret;
	}
	
	public static HashSet<Long> getAllowedWriteDidList(int idMember,
			Connection conn) {
		HashSet<Long> ret = new HashSet<Long>();
		LOG.info("Getting access for m_id: " + idMember);
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_ALLOWED_WRITE_DID_LIST_FOR_MEMBER
							.getValue());
			preparedStatement.setInt(1, idMember);
			resultSet = preparedStatement.executeQuery();
			conn.commit();
			while(resultSet.next()){
				ret.add(resultSet.getLong("d_id"));
			}

		} catch (SQLException e) {
			LOG.error("Error getting dataset ids for m_id (read access): " + e);
			//SQLHelpers.rollBack(conn, "getting dataset ids for m_id (read access)");
		} finally {
			SQLHelpers.closeResources(preparedStatement, resultSet);
		}
		return ret;
	}
	
	public static boolean shareDataset(Long dId, int ownerMID, int shareMID, boolean share, Connection conn) {

		boolean ret = false;
		PreparedStatement preparedStatement = null;
		try {

			if(share){
			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Insert.INSERT_DATASET_ACCESS_ENTRY
							.getValue());
			
			}else{
				preparedStatement = conn
						.prepareStatement(SQLPreparedStatements.Delete.DELETE_DATASET_ACCESS_ENTRY
								.getValue());
			}
			preparedStatement.setLong(1, dId);// did
			preparedStatement.setInt(2, shareMID);// m_id
			preparedStatement.setInt(3, ownerMID);// owner_m_id
			preparedStatement.execute();
			conn.commit();
			ret = true;
		} catch (SQLException e) {
			LOG.error("inserting dataset access entry");
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, null);
		}
		return ret;

	}

}
