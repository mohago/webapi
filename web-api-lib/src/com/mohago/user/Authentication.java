package com.mohago.user;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.simple.JSONObject;

import cloudcix.api.*;
import cloudcix.base.*;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mohago.config.Configuration.Database;
import com.mohago.config.DatabaseConnection;
import com.mohago.json.CloudCIXUser;
import com.mohago.json.Token;
import com.mohago.request.MasterTable;
import com.mohago.sql.SQLPreparedStatements;
import com.mohago.utilities.Constants;
import com.mohago.utilities.SQLHelpers;
import com.mysql.jdbc.StringUtils;

public class Authentication {

	private final static Logger LOG = Logger.getLogger(Authentication.class);
	// private final static String SELECT_PWD =
	// SQLPreparedStatements.User.SELECT_PWD
	// .getValue();
	// private final static String SELECT_AUTH_DETAILS =
	// SQLPreparedStatements.User.SELECT_AUTH_DETAILS
	// .getValue();
	private static Utilities authUtils;
	private static Auth client;

	public static Utilities getAuthUtils() {
		return authUtils;
	}

	public static void setAuthUtils(Utilities utilities) {
		Authentication.authUtils = utilities;
	}

	public static Auth getClient() {
		return client;
	}

	public static void setClient(Auth cli) {
		Authentication.client = cli;
	}


	/**
	 * Passes in user credentials which it sends to CloudCIX to obtain a header
	 * auth-token ("X-Subject-Token")
	 * 
	 * @param username
	 * @param password
	 * @param memberId
	 * @param token
	 * @return Returns a user auth token.
	 * @throws IOException
	 */
	public static CloudCIXUser authUser(String username, String password,
			String memberId, String token) throws SocketTimeoutException {
		CloudCIXAuth credentials = new CloudCIXAuth(username, password,
				memberId, token);
		return authUser(credentials);
	}

	/**
	 * Passes in user credentials which it sends to CloudCIX to authenticate
	 * 
	 * @param username
	 * @param password
	 * @param memberId
	 * @param token
	 *            If passesd the method will just use this token and not the
	 *            rest of the creds to get a refreshed token
	 * @param domainId
	 * @return Returns a user with auth token.
	 * @throws IOException
	 */
	public static CloudCIXUser authScopedUser(String username, String password,
			String memberId, String domainId, String token) throws IOException {
		CloudCIXAuth credentials = new CloudCIXAuth(username, password,
				memberId, token);
		credentials.scope_domain(domainId);
		return authUser(credentials);
	}

	/**
	 * Passes in user credentials which it sends to CloudCIX to authenticate
	 * 
	 * @param credentials
	 * @return Returns a user with auth token.
	 * @throws IOException
	 */
	private static CloudCIXUser authUser(CloudCIXAuth credentials)
			throws SocketTimeoutException {

		long before = System.currentTimeMillis();
		CloudCIXUser cixUser = null;
		// System.out.println(credentials.get_auth_ref().toJSONString());
		LOG.info("creds: " + credentials.toString());
		Response response = Authentication.getClient().keystone
				.create(credentials);
		LOG.info(response.code + " " + response.message);
		if (response.body != null) {
			LOG.info("Body: " + (response.body.toJSONString()));
		}
		if (response.code == 201) {
			cixUser = parseResponse(response);
		} else {
			LOG.error("Response from cix is null");
		}
		long after = System.currentTimeMillis();
		LOG.error("Total AUth took: " + (after - before) + "milliseconds");
		return cixUser;
	}

	private static CloudCIXUser parseResponse(Response response) {

		CloudCIXUser cixUser = null;
		String userToken = "";
		Gson gson = new Gson();
		JSONObject jToken = (JSONObject) response.body.get("token");
		Type tokenType = new TypeToken<Token>() {
		}.getType();
		Token token = gson.fromJson(jToken.toString(), tokenType);
		JSONObject user = (JSONObject) jToken.get("user");
		Type userType = new TypeToken<CloudCIXUser>() {
		}.getType();
		if (user != null) {
			cixUser = gson.fromJson(user.toString(), userType);
			LOG.info("idAdress: " + user.get("idAddress"));
			userToken = response.headers.get("X-Subject-Token").get(0);
			LOG.info("X-Subject-Token: " + userToken);
			token.setValue(userToken);
			cixUser.setToken(token);
		} else {
			LOG.info("parse response fails: " + jToken);
		}

		return cixUser;
	}

	/**
	 * Reads a users token in json form.
	 * 
	 * @param adminToken
	 * @param userToken
	 * @return
	 * @throws IOException
	 */
	private static CloudCIXUser readUserToken(String userToken)
			throws SocketTimeoutException {

		CloudCIXUser cixUser = null;
		// Token token = null;
		String adminToken = Authentication.getAuthUtils().get_admin_token();
		Response response = Authentication.getClient().keystone.read(
				adminToken, userToken);
		if (response.code == 200) {// 200 is for Read, 201 for Create
			cixUser = parseResponse(response);
		}
		if (cixUser != null) {
			boolean shouldRefresh = false;
			SimpleDateFormat formatter = new SimpleDateFormat(
					"yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
			formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
			Date expiry = null;
			try {
				expiry = formatter.parse(cixUser.getToken().getExpires_at());
				Calendar cal = Calendar
						.getInstance(TimeZone.getTimeZone("GMT")); // creates
																	// calendar
				cal.setTime(new Date()); // sets calendar time/date
				cal.add(Calendar.MINUTE, 30); // adds half hour
				Date testTime = cal.getTime();
				shouldRefresh = testTime.compareTo(expiry) > 0 ? true : false;
			} catch (ParseException e) {
				LOG.debug("Error parsing date: " + e);
			}
			if (shouldRefresh) {
				cixUser = authUser(null, null, null, userToken);
			}
		}

		return cixUser;
	}

	/**
	 * Delete a user token using the admin token obtained using admin
	 * credentials in settings file in Web-App
	 * 
	 * @param userToken
	 * @return Response Code
	 * @throws IOException
	 */
	public static int deleteToken(String userToken) throws IOException {
		return Auth.getKeystone().delete(
				Authentication.getAuthUtils().get_admin_token(), userToken).code;
	}

	/**
	 * Takes a Token object which contains an issued at timestamp, and and
	 * expires at timestamp, and a value, and checks if it is still valid
	 * 
	 * @param token
	 * @return
	 */
	public static boolean isTokenValid(Token token) {
		LOG.debug("token: e: " + token.getValue());
		SimpleDateFormat formatter = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
		boolean ret = false;
		Date expiry = null;
		try {
			expiry = formatter.parse(token.getExpires_at());
			Date now = DateTime.now(DateTimeZone.UTC).toDate();
			ret = now.compareTo(expiry) < 0 ? true : false;
		} catch (ParseException e) {
			LOG.debug("Error parsing date: " + e);
		}

		if (!ret) {
			LOG.debug("Auth failed on token: : " + token.toString());
		}

		return ret;
	}



	/**
	 * Takes a request containing either credentials or a token, and puts a
	 * refreshed token in the response header
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	public static CloudCIXUser authenticate(HttpServletRequest request,
			HttpServletResponse response) throws SocketTimeoutException {

		final String userName = request.getHeader(Constants.Headers.USER_NAME
				.getValue());
		final String password = request.getHeader(Constants.Headers.PWD
				.getValue());
		final String memberId = request.getHeader(Constants.Headers.MEMBER_ID
				.getValue());
		final String token = request.getHeader(Constants.Headers.TOKEN
				.getValue());
		final String domainId = request.getHeader(Constants.Headers.DOMAIN_ID
				.getValue());
		CloudCIXUser user = null;
		if (!StringUtils.isNullOrEmpty(token)) {
			long before = System.currentTimeMillis();
			user = readUserToken(token);// Refreshes if necessary
			long after = System.currentTimeMillis();
			LOG.debug("Refresh takes: " + (after - before) + " milliseconds");
		} else if (!StringUtils.isNullOrEmpty(userName)
				&& !StringUtils.isNullOrEmpty(password)) {// username and
															// password could be
															// null here too
			long before = System.currentTimeMillis();
			LOG.debug("username : " + userName + " pwd: " + password);
			user = authUser(userName, password, memberId, null);
			long after = System.currentTimeMillis();
			LOG.debug("Create using user pwd takes: " + (after - before)
					+ " milliseconds");
		} else {
			LOG.error("No token OR user name & pwd *****");
		}
		long before = System.currentTimeMillis();
		if (user != null && isTokenValid(user.getToken())) {

			response.setHeader("X-Subject-Token", user.getToken().getValue());
		} else {

			LOG.info("User null, auth failed");
			user = null;
		}
		long after = System.currentTimeMillis();
		LOG.debug("Rest takes: " + (after - before) + " milliseconds");
		return user;
	}



}
