package com.mohago.serialization;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class BaseItem {

	protected String name;
	protected Long datasetId;
	protected Long parentDatasetId;
	protected String project;
	protected String date;
	protected String hash;
	protected String filename;
	protected String database;
	protected String server;
	protected String deviceToken;
	protected int itemCountFromDB;
	protected HashMap<String, Object> metaInfo;

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public HashMap<String, Object> getMetaInfo() {
		return metaInfo;
	}

	public void setMetaInfo(HashMap<String, Object> metaInfo) {
		this.metaInfo = metaInfo;
	}

	public Long getParentUid() {
		return parentDatasetId;
	}

	public void setParentUid(Long parentUid) {
		this.parentDatasetId = parentUid;
	}

	private Meta metaData;
	private ArrayList<Pair<String, String>> fileHash;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getUid() {
		return datasetId;
	}

	public void setUid(Long uid) {
		this.datasetId = uid;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

//	public String getTask() {
//		return task;
//	}
//
//	public void setTask(String task) {
//		this.task = task;
//	}
//
//	public String getSource() {
//		return source;
//	}
//
//	public void setSource(String source) {
//		this.source = source;
//	}
//
//	public String getUser() {
//		return user;
//	}
//
//	public void setUser(String user) {
//		this.user = user;
//	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

//	public String getConnectionStringMySql() {
//		return connectionStringMySql;
//	}
//
//	public void setConnectionStringMySql(String connectionStringMySql) {
//		this.connectionStringMySql = connectionStringMySql;
//	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}
//
//	public String getGlobalId() {
//		return globalId;
//	}
//
//	public void setGlobalId(String global_id) {
//		this.globalId = global_id;
//	}

	public Meta getMetaData() {
		return metaData;
	}

	public void setMetaData(Meta metaData) {
		this.metaData = metaData;
	}

	public ArrayList<Pair<String, String>> getFileHash() {
		return fileHash;
	}

	public void setFileHash(ArrayList<Pair<String, String>> fileHash) {
		this.fileHash = fileHash;
	}

}
