package com.mohago.serialization;

public class DataObjectCollection extends DataObjectBase {

	public int doc_uid;
	public String name;
	public String date;
	public String project;
	public String task;
	public String user;
	public String source;
	public String fileSource;
	public int batch_uid;

	public DataObjectCollection(int b_uid, int d_uid, String name,
			String source, String date, int paramCount) {
		this.batch_uid = b_uid;
		this.doc_uid = d_uid;
		this.name = name;
		this.date = date;
		this.source = source;
		this.TotalParametersFixedFromStorage = paramCount;

	}

}
