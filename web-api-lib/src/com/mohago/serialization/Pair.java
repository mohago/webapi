package com.mohago.serialization;

public class Pair<T1, T2> {
	
	  private T1 First;
	    private T2 Second;

	    

		public Pair(T1 First, T2 Second) {//Ignoring java naming conventions to so that serialization with c# works
	    	super();
	    	this.First = First;
	    	this.Second = Second;
	    }

	    public int hashCode() {
	    	int hashFirst = First != null ? First.hashCode() : 0;
	    	int hashSecond = Second != null ? Second.hashCode() : 0;

	    	return (hashFirst + hashSecond) * hashSecond + hashFirst;
	    }

	    public boolean equals(Object other) {
	    	if (other instanceof Pair) {
	    		Pair otherPair = (Pair) other;
	    		return 
	    		((  this.First == otherPair.First ||
	    			( this.First != null && otherPair.First != null &&
	    			  this.First.equals(otherPair.First))) &&
	    		 (	this.Second == otherPair.Second ||
	    			( this.Second != null && otherPair.Second != null &&
	    			  this.Second.equals(otherPair.Second))) );
	    	}

	    	return false;
	    }

	    public String toString()
	    { 
	           return "(" + First + ", " + Second + ")"; 
	    }

	    public T1 getFirst() {
	    	return First;
	    }



	    public T2 getSecond() {
	    	return Second;
	    }
	    
	    public void setFirst(T1 first) {
			First = first;
		}

		public void setSecond(T2 second) {
			Second = second;
		}

	 

}
