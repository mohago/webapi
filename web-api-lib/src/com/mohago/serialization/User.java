package com.mohago.serialization;

public class User {
	
	private String userName;
	private boolean isAdmin;
	private boolean mustChangePwd;
	private String hashedPwd;
	
	public User()
    {
        userName = "";
        isAdmin = false;
        mustChangePwd = false;
        hashedPwd = "";
    }
    public User(String userName, boolean isAdmin)
    {
        this.userName = userName;
        this.isAdmin = isAdmin;
        this.mustChangePwd = false;
        this.hashedPwd = "";
    }

    public User(String userName, boolean isAdmin, boolean mustChangePwd)
    {
        this.userName = userName;
        this.isAdmin = isAdmin;
        this.mustChangePwd = mustChangePwd;
    }
    
    public User(String userName, String hashedPwd, boolean isAdmin, boolean mustChangePwd)
    {
        this.userName = userName;
        this.isAdmin = isAdmin;
        this.mustChangePwd = mustChangePwd;
        this.hashedPwd = hashedPwd;
    }

}
