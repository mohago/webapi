package com.mohago.serialization;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.mohago.lgf.TableIndex;
import com.mohago.request.DatabaseHelpers;
import com.mysql.jdbc.StringUtils;

///TODO: Continue work 08/sept/14 17:46
public class Parameter {
	
	private static final Logger LOG = Logger.getLogger(Parameter.class);

	private int datasetIndex; // /
	private int storageIndex;// /the parameters index in its storage table.
	//private int colIndex;
	//private int type;// /the parameters type; refers to global type enum/array.
	private LookingGlassType type;
	private String name;// / the parameters name.
	private TableIndex parent;

	private int pi;
	private double pd;
	private String ps;
	private Date pDate;

	public Parameter(int datasetIndex, String name, LookingGlassType type) {
		this.setDatasetIndex(datasetIndex);
		this.setName(name);
		this.setType(type);
		this.setStorageIndex(0);
	}
	public Parameter(int datasetIndex, String name, LookingGlassType type, int colIndex) {//used during stream
		this.setDatasetIndex(datasetIndex);
		this.setName(name);
		this.setType(type);
		this.setStorageIndex(colIndex);
	}

	public int getDatasetIndex() {
		return datasetIndex;
	}

	public void setDatasetIndex(int datasetIndex) {
		this.datasetIndex = datasetIndex;
	}

	public int getStorageIndex() {
		return storageIndex;
	}

	public void setStorageIndex(int storageIndex) {
		this.storageIndex = storageIndex;
	}

	public LookingGlassType getType() {
		return type;
	}

	public void setType(LookingGlassType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TableIndex getParent() {
		return parent;
	}

	public void setParent(TableIndex parent) {
		this.parent = parent;
	}

	public int getPi() {
		return pi;
	}

	public void setPi(int pi) {
		this.pi = pi;
	}

	public double getPd() {
		return pd;
	}

	public void setPd(double pd) {
		this.pd = pd;
	}

	public String getPs() {
		return ps;
	}

	public void setPs(String ps) {
		this.ps = ps;
	}

	public Date getpDate() {
		return pDate;
	}

	public void setpDate(Date pDate) {
		this.pDate = pDate;
	}

	public String getParsedValue() {
		return parsedValue;
	}

	public void setParsedValue(String parsedValue) {
		this.parsedValue = parsedValue;
	}

	private String parsedValue;

	public void parseStringAddToTableInsertQueue(String val) {
		//if (val != "" && val != null) {
		if (!StringUtils.isNullOrEmpty(val)) {
			if (type == LookingGlassType.double_precision) {
				try {
					pd = Double.parseDouble(val.toString());
					parsedValue = String.valueOf(pd);
					parent.getInsertPairs().add(
							new Pair<String, String>("p" + getStorageIndex(), parsedValue));///EDIT DB name to pseudonym
				} catch (NumberFormatException e) {
					LOG.error("Error parsing string for table insert: " + e);
				}
			} else if (type == LookingGlassType.integer) {
				try {
					pi = Integer.parseInt(val.toString());
					parsedValue = String.valueOf(pi);
					parent.getInsertPairs().add(
							new Pair<String, String>("p" + getStorageIndex(), parsedValue));
				} catch (NumberFormatException e) {
					LOG.error("Error parsing string for table insert: " + e);
				}

			} else if (type == LookingGlassType.string_value) {

				parsedValue = val.toString();
				parent.getInsertPairs()
						.add(new Pair<String, String>("p" + getStorageIndex(), "'" + parsedValue
								+ "'"));
			} else if (type == LookingGlassType.date_time) {
				try {
					DateFormat formatter = new SimpleDateFormat(
							"yyyyMMddHHmmssSSSS");//edit db from "yyyyMMddHHmmssSSSS"//edit agin to "yyyyMMdd'T'HHmmssSSSZ"//edit back
					Date date = formatter.parse(val.toString());
					parent.getInsertPairs().add(
							new Pair<String, String>("p" + getStorageIndex(), "'"
									//+ formatter.format(date) + "'"));
									+ new Timestamp(date.getTime()).toString() + "'"));
				} catch (ParseException e) {
					LOG.error("Error parsing string for table insert: " + e);
				}
			}
			else if (type == LookingGlassType.floating_point) {
				try {
					pd = Float.parseFloat(val.toString());
					parsedValue = String.valueOf(pd);
					parent.getInsertPairs().add(
							new Pair<String, String>("p" + getStorageIndex(), parsedValue));///EDIT DB name to pseudonym
				} catch (NumberFormatException e) {
					LOG.error("Error parsing string for table insert: " + e);
				}
			}
		} else {
			// Console.WriteLine("null @ "+name);
		}
	}

}
