package com.mohago.serialization;

import java.util.Date;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

public class Meta {
	
	private ArrayList<MetaDataBase<Double>> doubleData = new ArrayList<MetaDataBase<Double>>();
    private ArrayList<MetaDataBase<String>> stringData = new ArrayList<MetaDataBase<String>>();
    private ArrayList<MetaDataBase<Date>> dateTimeData = new ArrayList<MetaDataBase<Date>>();
	private ArrayList<MetaDataBase<Integer>> intData = new ArrayList<MetaDataBase<Integer>>();

	public ArrayList<MetaDataBase<Double>> getDoubleData() {
		return doubleData;
	}

	public void setDoubleData(ArrayList<MetaDataBase<Double>> doubleData) {
		this.doubleData = doubleData;
	}

	public ArrayList<MetaDataBase<String>> getStringData() {
		return stringData;
	}

	public void setStringData(ArrayList<MetaDataBase<String>> stringData) {
		this.stringData = stringData;
	}

	public ArrayList<MetaDataBase<Date>> getDateTimeData() {
		return dateTimeData;
	}

	public void setDateTimeData(ArrayList<MetaDataBase<Date>> dateTimeData) {
		this.dateTimeData = dateTimeData;
	}

	public ArrayList<MetaDataBase<Integer>> getIntData() {
		return intData;
	}

	public void setIntData(ArrayList<MetaDataBase<Integer>> intData) {
		this.intData = intData;
	}

	public Meta(){
		
	}
	
	public Meta(BaseItem parent){
		
	}


}
