package com.mohago.serialization;

import java.util.HashSet;

public class Project {
	
	public int id;
	public String name;
    public String createdBy;
    public String colourString;
    public String globalId;
    public HashSet<Pair<String,Integer>> membersAndAccess;
    
    public Project(int id, String name, String createdBy, String colourString, String globalId){
    	this.id = id;
    	this.name = name;
    	this.createdBy = createdBy;
    	this.colourString = colourString;
    	this.globalId = globalId;
    	this.membersAndAccess = new HashSet<Pair<String,Integer>>();
    }

}
