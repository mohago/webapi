package com.mohago.serialization;

public enum LookingGlassType {

	integer(0), double_precision(1), string_value(2), date_time(3), floating_point(4), unknown(5);
	
	private int value;
	
	private LookingGlassType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
	
}
