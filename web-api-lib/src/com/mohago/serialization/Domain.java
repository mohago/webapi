package com.mohago.serialization;

public class Domain {
	
	private int idMember;
	private boolean enabled;
	private String groupName;
	private boolean notificationsOff;
	private String name;
	private boolean selfManaged;
	
	public int getIdMember() {
		return idMember;
	}
	public void setIdMember(int idMember) {
		this.idMember = idMember;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public boolean isNotificationsOff() {
		return notificationsOff;
	}
	public void setNotificationsOff(boolean notificationsOff) {
		this.notificationsOff = notificationsOff;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isSelfManaged() {
		return selfManaged;
	}
	public void setSelfManaged(boolean selfManaged) {
		this.selfManaged = selfManaged;
	}
	
	public Domain(){
		
	}

}
