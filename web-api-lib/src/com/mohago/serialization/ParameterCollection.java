package com.mohago.serialization;

import java.util.ArrayList;

import com.mohago.serialization.ParameterCollection.ObjectOrigin;

public class ParameterCollection<T> extends PBase{
	
	private String name;
	private String mySqlCreateType;	
	private int buid;	
	private String ObfuscatedTableName;    
    private ArrayList<Pair<Integer, T>> indexedData;
    private final boolean isfromserver = true;    
    private LookingGlassType type;   
    private int inst;   
    private int parameterIndex;
    private ArrayList<Pair<DataObjectCollection, T>> linkedValues;
    private ObjectOrigin origin;
    
    public enum ObjectOrigin{
    	local(0),
    	server(1)
    	;
    	
    	private ObjectOrigin(int value) {
    		this.value = value;
    	}

    	private int value;

    	public int getValue() {
    		return value;
    	}

    	public void setValue(int value) {
    		this.value = value;
    	}
    
    }
    
    private ObjectOrigin op;
    
	
	public ParameterCollection(int b_uid, String table_name, String parameter_name, LookingGlassType type) {
		this.buid = b_uid;
		this.ObfuscatedTableName = table_name;
		this.name = parameter_name;
		this.setType(type);
		this.op = ObjectOrigin.server;
		
	}

	public ParameterCollection(String tableName, String parameterName,
			Integer pseudoIndex, LookingGlassType parameterType, Integer instance, ObjectOrigin origin) {
		//this.dataset = dataset;
		this.ObfuscatedTableName = tableName;
		this.name = parameterName;
		this.type = parameterType;
		this.setPseudonym("p" + pseudoIndex);
		this.inst = instance;
		this.origin = origin;
		this.parameterIndex = pseudoIndex;
	}

	public String getMySqlCreateType() {
//		String ret = "";
//		switch (type.toLowerCase()) {
//		case "int":
//			ret = "INT";
//			break;
//		case "double":
//			ret = "FLOAT";
//			break;
//		case "float":
//			ret = "FLOAT";
//			break;
//		case "string":
//			ret = "VARCHAR(40)";
//			break;
//		default:
//			ret = "FLOAT";
//			break;
//		}
		return mySqlCreateType;
	}

	public void setMySqlCreateType(String mySqlCreateType) {
		this.mySqlCreateType = mySqlCreateType;
	}

	public ArrayList<Pair<Integer, T>> getIndexedData() {
		return indexedData;
	}

	public void setIndexedData(ArrayList<Pair<Integer, T>> indexedData) {
		this.indexedData = indexedData;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LookingGlassType getType() {
		return type;
	}

	public void setType(LookingGlassType type) {
		this.type = type;
	}

	public int getParameterIndex() {
		return parameterIndex;
	}

	public void setParameterIndex(int parameterIndex) {
		this.parameterIndex = parameterIndex;
	}

	public ArrayList<Pair<DataObjectCollection, T>> getLinkedValues() {
		return linkedValues;
	}

	public void setLinkedValues(ArrayList<Pair<DataObjectCollection, T>> linkedValues) {
		this.linkedValues = linkedValues;
	}

	public boolean joinContacts(ParameterCollection<T> toAdd) {
		if (toAdd.name.equalsIgnoreCase(name) && linkedValues != null && toAdd.getLinkedValues() != null)
        {
            linkedValues.addAll(toAdd.getLinkedValues());
            return true;
        }
        else
        {
            return false;
        }
	}

	

}
