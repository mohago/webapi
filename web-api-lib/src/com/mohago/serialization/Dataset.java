package com.mohago.serialization;

import java.util.HashMap;





public class Dataset extends BaseItem{
	

    private CollectionBase cb;
    
    
    //private ArrayList<DataObjectCollection> docs;
    //private ArrayList<Item> items = new ArrayList<Item>();
    public Dataset(){
    	
    }
	public Dataset(Long ui, String nam, String dat, String proj,
			 int itemCount, String deviceToken) {
		metaInfo = new HashMap<String, Object>();
		datasetId = ui;
        name = nam;
        //user = use;
        project = proj;
        //task = tas;
        date = dat;//.substring(0, 25);
        this.itemCountFromDB = itemCount;
        //this.globalId = globalId;
        this.deviceToken = deviceToken;
	}

	public CollectionBase getCb() {
		return cb;
	}
	public void setCb(CollectionBase cb) {
		this.cb = cb;
	}

}
