package com.mohago.serialization;


public class MetaDataBase<T> {
	
	
	transient BaseItem parent;
	
	public String name;	
	public T value;
	public LookingGlassType type;
	
	public MetaDataBase(String name, T value) {
		this.name = name;
		this.value = value;
	}
	
	public BaseItem getParent() {
		return parent;
	}

	public void setParent(BaseItem parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public LookingGlassType getType() {
		return type;
	}

	public void setType(LookingGlassType type) {
		this.type = type;
	}

	public MetaDataBase(String name, T value, BaseItem parent) {
		this.name = name;
		this.value = value;
		this.parent = parent;
		
		
	}
	

}
