package com.mohago.serialization;

import java.util.ArrayList;

public class ParameterTableList {

	private ArrayList<ParameterTable> parameterTableList = new ArrayList<ParameterTable>();

	public ArrayList<ParameterTable> getParameterTableList() {
		return parameterTableList;
	}

	public void setParameterTableList(ArrayList<ParameterTable> parameterTableList) {
		this.parameterTableList = parameterTableList;
	}

	public boolean containsNameAndType(ParameterTable pt) {
		boolean doesContain = false;
		for (ParameterTable t : parameterTableList) {
			if (t.getParameter_type().equals(pt.getParameter_type())
					&& t.getParameter_name().equals(pt.getParameter_name()))// if
			{
				doesContain = true;
			}
		}
		return doesContain;
	}

	public boolean containsNameAndType(String name, String type)
    {
		for (ParameterTable parameterTable : parameterTableList) {
			if (parameterTable.getParameter_name().equals(name) && parameterTable.getParameter_type().equals(type))// if
			{
				return true;
			}
		}
		return false;
       
    }
	
	public boolean containsName(String name)
    {
		for (ParameterTable parameterTable : parameterTableList) {
			if (parameterTable.getParameter_name().equals(name))// if
			{
				return true;
			}
		}
		return false;
       
    }

	public boolean add(ParameterTable pt) {
		if (!containsNameAndType(pt)) {
			parameterTableList.add(pt);
			return true;
		} else {
			return false;
		}
	}

	public ParameterTable fetchByNameType(String name, String type)
    {
		if("default".equals(type)){///TODO: check this
			type = "double";
		}
		for (ParameterTable t : parameterTableList) {
			if (t.getParameter_type().equals(type)
					&& t.getParameter_name().equals(name))// if
			{
				return t;
			}
		}
		return null;
    }
}
