package com.mohago.serialization;

import java.util.ArrayList;

public class PBase {
	
	private String name; 
    private String units; 
    private  boolean anySelected; 
    private  int countSelected; 
    private  String countSelectedText;
    private  LookingGlassType type;
    private  String pseudonym;

    public  String GetParamType()
    {
        return "";
    }

    public String getPseudonym() {
		return pseudonym;
	}

	public void setPseudonym(String pseudonym) {
		this.pseudonym = pseudonym;
	}

	public  boolean GetDataIfNotDeferred()
    {
        return false;
    }

    public  String ValueAsString(DataObjectCollection d)
    {
        return "";
    }

    public  Object Value(DataObjectCollection d)
    {
        return "";
    }

    /// <summary>
    /// 9/7/2014
    /// </summary>
    private int parameterIndex;

    boolean selectp = false;



    private  ArrayList<DataObjectCollection> docArrayList;


    public  void buildIndexArrayList()
    {

    }

    public  boolean add(Object val, DataObjectCollection doc)
    {
        return false;
    }

    public  boolean add_LGDF_PARSE_JULY14(Object val, DataObjectCollection doc)
    {
        return false;
    }

    public  boolean add(Object val, int index)
    {
        return false;
    }


    public  void buildLinkedValuesExternal()
    {
        
    }

	public LookingGlassType getType() {
		return type;
	}

	public void setType(LookingGlassType type) {
		this.type = type;
	}

}
