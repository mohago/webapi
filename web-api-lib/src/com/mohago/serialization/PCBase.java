package com.mohago.serialization;

public class PCBase {
	private int count;
    private int maxIndex;
    private boolean newItemThisRound;
	
	private String p_name;
    public String getP_name() {
		return p_name;
	}
	public void setP_name(String p_name) {
		this.p_name = p_name;
	}
	public LookingGlassType getParameterType() {
		return parameterType;
	}
	public void setParameterType(LookingGlassType parameterType) {
		this.parameterType = parameterType;
	}
	public int getParameterIndex() {
		return parameterIndex;
	}
	public void setParameterIndex(int parameterIndex) {
		this.parameterIndex = parameterIndex;
	}
	private LookingGlassType parameterType;
    private int parameterIndex = -1;
    
    public PCBase(){
    	
    }

    

    public void addValue(int index, Object newValue){
    	
    }
    public boolean isNewItemThisRound() {
		return newItemThisRound;
	}
	public void setNewItemThisRound(boolean newItemThisRound) {
		this.newItemThisRound = newItemThisRound;
	}
	public int getCount() {
		return count;
	}
	public int getMaxIndex() {
		return maxIndex;
	}
	public void clearValues(){
    	
    }
    //
    // Summary:
    //     converts from IndexedValues to padded Values
    public void indexedToPadded(int padTo){
    	
    }
    public void paddedToIndexed(){
    	
    }
    public void padValue(){
    	
    }
    public Object valueAt(int index){
    	return null;
    }

}
