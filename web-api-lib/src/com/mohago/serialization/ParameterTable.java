package com.mohago.serialization;

public class ParameterTable {

	private int t_uid;
	private String table_name;
	private String parameter_name;
	private String parameter_type;
	private int instance;

	public ParameterTable(int t_uid, String table_name, String parameter_name,
			String parameter_type, int instance) {
		setT_uid(t_uid);
		setTable_name(table_name);
		setParameter_name(parameter_name);
		setParameter_type(parameter_type);
		setInstance(instance);

	}

	public ParameterTable(String table_name, String parameter_name,
			String parameter_type) {
		setTable_name(table_name);
		setParameter_name(parameter_name);
		setParameter_type(parameter_type);

	}

	public int getT_uid() {
		return t_uid;
	}

	public void setT_uid(int t_uid) {
		this.t_uid = t_uid;
	}

	public String getTable_name() {
		return table_name;
	}

	public void setTable_name(String table_name) {
		this.table_name = table_name;
	}

	public String getParameter_name() {
		return parameter_name;
	}

	public void setParameter_name(String parameter_name) {
		this.parameter_name = parameter_name;
	}

	public String getParameter_type() {
		return parameter_type;
	}

	public void setParameter_type(String parameter_type) {
		this.parameter_type = parameter_type;
	}

	public int getInstance() {
		return instance;
	}

	public void setInstance(int instance) {
		this.instance = instance;
	}

}
