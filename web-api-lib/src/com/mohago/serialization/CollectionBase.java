package com.mohago.serialization;

import java.util.ArrayList;
import java.util.Date;

public class CollectionBase {

	private ArrayList<ParameterCollection<String>> paramStrings;
	private ArrayList<ParameterCollection<Double>> paramDoubles;
	private ArrayList<ParameterCollection<Integer>> paramInts;
	private ArrayList<ParameterCollection<Boolean>> paramBools;
	private ArrayList<ParameterCollection<Date>> paramDates;
	
	
	private boolean mergedCB;
	
	public CollectionBase(){
		paramStrings = new ArrayList<ParameterCollection<String>>();
		paramDoubles = new ArrayList<ParameterCollection<Double>>();
		paramInts = new ArrayList<ParameterCollection<Integer>>();
		paramBools = new ArrayList<ParameterCollection<Boolean>>();
		paramDates = new ArrayList<ParameterCollection<Date>>();
	}

	public boolean isMergedCB() {
		return mergedCB;
	}

	public void setMergedCB(boolean mergedCB) {
		this.mergedCB = mergedCB;
	}

	public ArrayList<ParameterCollection<String>> getParamStrings() {
		return paramStrings;
	}

	public ArrayList<ParameterCollection<Double>> getParamDoubles() {
		return paramDoubles;
	}

	public ArrayList<ParameterCollection<Integer>> getParamInts() {
		return paramInts;
	}

	public ArrayList<ParameterCollection<Boolean>> getParamBools() {
		return paramBools;
	}

	public ArrayList<ParameterCollection<Date>> getParamDates() {
		return paramDates;
	}

	public void addParameterCollection(ParameterCollection pc) {
		
		String type = pc.getType().toString().toLowerCase();
		
		if (type.contains("double") || type.contains("float")) {

			//pc = (ParameterCollection<Double>) pc;
			boolean any = true;
			for (ParameterCollection<Double> pcd : paramDoubles) {
				any = pcd.joinContacts(pc);
				if (!any) {
					paramDoubles.add(pc);
					break;
				}
			}

		} else if (type.contains("int")) {

			//pc = (ParameterCollection<Integer>) pc;
			boolean any = true;
			for (ParameterCollection<Integer> pci : paramInts) {
				any = pci.joinContacts(pc);
				if (!any) {
					paramInts.add(pc);
					break;
				}
			}

		} else if (type.contains("string") || type.contains("varchar")) {

			//pc = (ParameterCollection<String>) pc;
			boolean any = true;
			for (ParameterCollection<String> pcs : paramStrings) {
				any = pcs.joinContacts(pc);
				if (!any) {
					paramStrings.add(pc);
					break;
				}
			}

		} else if (type.contains("date")) {

			//pc = (ParameterCollection<Date>) pc;
			boolean any = true;
			for (ParameterCollection<Date> pcd : paramDates) {
				any = pcd.joinContacts(pc);
				if (!any) {
					paramDates.add(pc);
					break;
				}
			}

		}

	}

//	public ArrayList<PBase> getParameterCollectionView() {
//		
//		pPCV.clear();
//		if (!mergedCB)
//        {
//            for(ParameterCollection<String> pcs : paramStrings){
//            	pPCV.add(pcs);
//            }
//            for(ParameterCollection<Double> pcd : paramDoubles){
//            	pPCV.add(pcd);
//            }
//            for(ParameterCollection<Integer> pci : paramInts){
//            	pPCV.add(pci);
//            }
//            for(ParameterCollection<Boolean> pcb : paramBools){
//            	pPCV.add(pcb);
//            }
//            for(ParameterCollection<Date> pcd : paramDates){
//            	pPCV.add(pcd);
//            }
//        }
//
//        return pPCV;
//	}

}
