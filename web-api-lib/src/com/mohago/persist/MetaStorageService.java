package com.mohago.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.mohago.serialization.Dataset;
import com.mohago.serialization.LookingGlassType;
import com.mohago.serialization.Meta;
import com.mohago.serialization.MetaDataBase;
import com.mohago.serialization.Pair;
import com.mohago.sql.SQLPreparedStatements;
import com.mohago.utilities.SQLHelpers;

public class MetaStorageService {

	private final static Logger LOG = Logger.getLogger(MetaStorageService.class);

	public static boolean insertfileHash(Dataset batchItem, Connection conn) {

		boolean result = false;
		PreparedStatement insertfileHash = null;
		try {
			insertfileHash = conn
					.prepareStatement(SQLPreparedStatements.Insert.INSERT_FILE_HASH
							.getValue());
		} catch (SQLException e1) {
			LOG.error("Insert file hash batch error: " + e1);
		}
		for (final Pair<String, String> fileHash : batchItem.getFileHash()) {
			try {

				insertfileHash.setLong(1, batchItem.getUid());
				insertfileHash.setString(2, fileHash.getSecond());
				insertfileHash.setString(3, fileHash.getFirst());
				insertfileHash.addBatch();

			} catch (SQLException e) {
				LOG.error("Error inserting batch file hash: " + e);
				SQLHelpers.rollBack(conn, "inserting batch file hash");
			}
		}
		try {
			insertfileHash.executeBatch();
			result = true;
		} catch (SQLException e) {
			LOG.error("Insert file hash batch error: " + e);
			SQLHelpers.rollBack(conn, "inserting batch file hash");
		}

		return result;

	}

	public static boolean deleteMeta(Long dId, LookingGlassType type, Connection conn) {

		boolean result = false;
		PreparedStatement prepStatement = null;
		try {
			if(type.equals(LookingGlassType.integer)){
				prepStatement = conn
						.prepareStatement(SQLPreparedStatements.Delete.META_INT_ALL
								.getValue());
			}else if(type.equals(LookingGlassType.double_precision)){
				prepStatement = conn
						.prepareStatement(SQLPreparedStatements.Delete.META_DOUBLE_ALL
								.getValue());
			}
			else if(type.equals(LookingGlassType.date_time)){
				prepStatement = conn
						.prepareStatement(SQLPreparedStatements.Delete.META_DATE_ALL
								.getValue());
			}
			else if(type.equals(LookingGlassType.string_value)){
				prepStatement = conn
						.prepareStatement(SQLPreparedStatements.Delete.META_STRING_ALL
								.getValue());
			}

			prepStatement.setLong(1, dId);
			//prepStatement.setString(2, "%");
			prepStatement.executeUpdate();
			result = true;
		} catch (SQLException e) {
			LOG.error("Insert file hash batch error: " + e);
			SQLHelpers.rollBack(conn, "inserting batch file hash");
		}

		return result;

	}

	private static void insertDatasetMetaDouble(final Long dUid, Meta meta,
			Connection conn) {
		PreparedStatement insertMetaDouble = null;

		try {
			insertMetaDouble = conn
					.prepareStatement(SQLPreparedStatements.Insert.META_DOUBLE
							.getValue());
		} catch (Exception e) {
			LOG.error("prepare stmt error: " + e);

		}

		for (MetaDataBase<Double> mdb : meta.getDoubleData()) {

			try {
				insertMetaDouble.setLong(1, dUid);// /Make sure this is correct
													// and is taken after
													// insert.
				insertMetaDouble.setString(2, mdb.name);
				insertMetaDouble.setDouble(3, mdb.value);
				insertMetaDouble.addBatch();

			} catch (SQLException e) {
				LOG.error("Error storing batch: " + e);
				SQLHelpers.rollBack(conn, "storing batch");
			}
		}
		try {
			insertMetaDouble.executeBatch();
		} catch (SQLException e1) {
			LOG.error("execute batch error: " + e1);
			SQLHelpers.rollBack(conn, "inserting batch");
		} finally {
			SQLHelpers.closeResources(insertMetaDouble, null);
		}
	}

	private static void insertDatasetMetaInt(final Long dUid, Meta meta,
			Connection conn) {
		PreparedStatement insertMetaInt = null;

		try {
			insertMetaInt = conn
					.prepareStatement(SQLPreparedStatements.Insert.META_INT
							.getValue());
		} catch (Exception e) {
			LOG.error("prepare stmt error: " + e);
		}

		for (MetaDataBase<Integer> mdb : meta.getIntData()) {

			try {
				insertMetaInt.setLong(1, dUid);// /Make sure this is correct
												// and is taken after
												// insert.
				insertMetaInt.setString(2, mdb.name);
				insertMetaInt.setInt(3, mdb.value);
				insertMetaInt.addBatch();

			} catch (SQLException e) {
				LOG.error("Error storing meta: " + e);
				SQLHelpers.rollBack(conn, "storing meta");
			}
		}
		try {
			insertMetaInt.executeBatch();
		} catch (SQLException e1) {
			LOG.error("execute batch error: " + e1);
			SQLHelpers.rollBack(conn, "inserting batch");
		} finally {
			SQLHelpers.closeResources(insertMetaInt, null);
		}
	}

	private static void insertDatasetMetaString(final Long dUid, Meta meta,
			Connection conn) {
		PreparedStatement insertMetaString = null;

		try {
			insertMetaString = conn
					.prepareStatement(SQLPreparedStatements.Insert.META_STRING
							.getValue());
		} catch (Exception e) {

			LOG.error("prepare stmt error: " + e);
		}

		for (MetaDataBase<String> mdb : meta.getStringData()) {

			try {
				insertMetaString.setLong(1, dUid);// /Make sure this is correct
													// and is taken after
													// insert.
				insertMetaString.setString(2, mdb.name);
				insertMetaString.setString(3, mdb.value);
				insertMetaString.addBatch();

			} catch (SQLException e) {
				LOG.error("Error storing meta: " + e);
				SQLHelpers.rollBack(conn, "storing meta");
			}
		}
		try {
			insertMetaString.executeBatch();
		} catch (SQLException e1) {
			LOG.error("execute batch error: " + e1);
			SQLHelpers.rollBack(conn, "inserting batch");
		} finally {
			SQLHelpers.closeResources(insertMetaString, null);
		}
	}

	private static void insertDatasetMetaDate(final Long dUid, Meta meta,
			Connection conn) {
		PreparedStatement insertMetaDate = null;

		try {
			insertMetaDate = conn
					.prepareStatement(SQLPreparedStatements.Insert.META_DATE
							.getValue());
		} catch (Exception e) {

			LOG.error("prepare stmt error: " + e);
		}

		for (MetaDataBase<Date> mdb : meta.getDateTimeData()) {

			Timestamp timeStamp = new Timestamp(mdb.value.getTime());
			try {
				insertMetaDate.setLong(1, dUid);// /Make sure this is correct
												// and is taken after
												// insert.
				insertMetaDate.setString(2, mdb.name);
				insertMetaDate.setTimestamp(3, timeStamp);
				insertMetaDate.addBatch();

			} catch (SQLException e) {
				LOG.error("Error storing meta: " + e);
				SQLHelpers.rollBack(conn, "storing meta");
			}
		}
		try {
			insertMetaDate.executeBatch();

		} catch (SQLException e1) {
			LOG.error("execute batch error: " + e1);
			SQLHelpers.rollBack(conn, "inserting batch");
		} finally {
			SQLHelpers.closeResources(insertMetaDate, null);
		}
	}

	public static void insertDatasetMeta(final Long dUid, Meta meta,
			Connection conn) {
		insertDatasetMetaDouble(dUid, meta, conn);
		insertDatasetMetaDate(dUid, meta, conn);
		insertDatasetMetaInt(dUid, meta, conn);
		insertDatasetMetaString(dUid, meta, conn);
	}

	public static LookingGlassType getLGType(Object value) {
		LookingGlassType type = null;
		if (value instanceof Integer) {
			type = LookingGlassType.integer;

		} else if (value instanceof Double) {
			type = LookingGlassType.double_precision;
		} else {
			try {
				new DateTime(value);
				type = LookingGlassType.date_time;
			} catch (IllegalArgumentException ie) {
				type = LookingGlassType.string_value;
			}
		}
		return type;
	}

}
