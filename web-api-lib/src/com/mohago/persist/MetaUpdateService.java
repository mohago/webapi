package com.mohago.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.mohago.request.DatabaseHelpers;
import com.mohago.request.RequestMeta;
import com.mohago.serialization.LookingGlassType;
import com.mohago.sql.SQLPreparedStatements;
import com.mohago.user.Authentication;
import com.mohago.utilities.Constants;
import com.mohago.utilities.ResultCodes;
import com.mohago.utilities.SQLHelpers;
import com.mysql.jdbc.StringUtils;

public class MetaUpdateService {

	private final static Logger LOG = Logger.getLogger(MetaUpdateService.class);

	private static boolean updateDatasetMetaInt(Connection conn, final Long dId,
			String paramName, String value, String updateType) {
		LOG.info("Updating meta data int");
		boolean result = false;
		PreparedStatement preparedStatement = null;
		try {
			if ("update".equalsIgnoreCase(updateType)) {
				preparedStatement = conn
						.prepareStatement(SQLPreparedStatements.Update.META_INT
								.getValue());
				preparedStatement.setInt(1, Integer.parseInt(value));
				preparedStatement.setLong(2, dId);
				preparedStatement.setString(3, paramName);
			} else if ("delete".equalsIgnoreCase(updateType)) {
				preparedStatement = conn
						.prepareStatement(SQLPreparedStatements.Delete.META_INT
								.getValue());
				preparedStatement.setLong(1, dId);
				preparedStatement.setString(2, paramName);
			} else if ("create".equalsIgnoreCase(updateType)) {
				preparedStatement = conn
						.prepareStatement(SQLPreparedStatements.Insert.META_INT
								.getValue());
				preparedStatement.setLong(1, dId);
				preparedStatement.setString(2, paramName);
				preparedStatement.setInt(3, Integer.parseInt(value));
			}
			int rowsAffected = 0;
			if (preparedStatement != null) {
				rowsAffected = preparedStatement.executeUpdate();
				conn.commit();
				result = (rowsAffected > 0);
			}
		} catch (SQLException | NumberFormatException e) {
			LOG.error("Error Updating meta data int: " + e);
			SQLHelpers.rollBack(conn, "Updating meta data int");
		} finally {
			SQLHelpers.closeResources(preparedStatement, null);
		}
		return result;
	}

	private static boolean updateDatasetMetaDouble(Connection conn, Long dId,
			String paramName, String value, String updateType) {
		LOG.info("Updating meta data double");
		boolean result = false;
		PreparedStatement preparedStatement = null;
		try {
			if ("update".equalsIgnoreCase(updateType)) {
				preparedStatement = conn
						.prepareStatement(SQLPreparedStatements.Update.META_DOUBLE
								.getValue());
				preparedStatement.setDouble(1, Double.parseDouble(value));
				preparedStatement.setLong(2, dId);
				preparedStatement.setString(3, paramName);
			} else if ("delete".equalsIgnoreCase(updateType)) {
				preparedStatement = conn
						.prepareStatement(SQLPreparedStatements.Delete.META_DOUBLE
								.getValue());
				preparedStatement.setLong(1, dId);
				preparedStatement.setString(2, paramName);
			} else if ("create".equalsIgnoreCase(updateType)) {
				preparedStatement = conn
						.prepareStatement(SQLPreparedStatements.Insert.META_DOUBLE
								.getValue());
				preparedStatement.setLong(1, dId);
				preparedStatement.setString(2, paramName);
				preparedStatement.setDouble(3, Double.parseDouble(value));
			}
			if (preparedStatement != null) {
				preparedStatement.execute();
				conn.commit();
				result = true;
			}
		} catch (SQLException e) {
			LOG.error("Error Updating meta data double: " + e);
			SQLHelpers.rollBack(conn, "Updating meta data double");
		} finally {
			SQLHelpers.closeResources(preparedStatement, null);
		}
		return result;
	}

	private static boolean updateDatasetMetaString(Connection conn, Long dId,
			String paramName, String value, String updateType) {
		LOG.info("Updating meta data string");
		boolean result = false;
		PreparedStatement preparedStatement = null;
		try {
			if ("update".equalsIgnoreCase(updateType)) {
				preparedStatement = conn
						.prepareStatement(SQLPreparedStatements.Update.META_STRING
								.getValue());
				preparedStatement.setString(1, value);
				preparedStatement.setLong(2, dId);
				preparedStatement.setString(3, paramName);
			} else if ("delete".equalsIgnoreCase(updateType)) {
				preparedStatement = conn
						.prepareStatement(SQLPreparedStatements.Delete.META_STRING
								.getValue());
				preparedStatement.setLong(1, dId);
				preparedStatement.setString(2, paramName);
			} else if ("create".equalsIgnoreCase(updateType)) {
				preparedStatement = conn
						.prepareStatement(SQLPreparedStatements.Insert.META_STRING
								.getValue());
				preparedStatement.setLong(1, dId);
				preparedStatement.setString(2, paramName);
				preparedStatement.setString(3, value);
			}
			if (preparedStatement != null) {
				preparedStatement.execute();
				conn.commit();
				result = true;
			}
		} catch (SQLException e) {
			LOG.error("Error Updating meta data string: " + e);
			SQLHelpers.rollBack(conn, "Updating meta data string");
		} finally {
			SQLHelpers.closeResources(preparedStatement, null);
		}
		return result;
	}

	private static boolean updateDatasetMetaDate(Connection conn, Long buid,
			String paramName, Timestamp value, String updateType) {
		LOG.info("Updating meta data date");
		boolean result = false;
		PreparedStatement preparedStatement = null;
		try {
			if ("update".equalsIgnoreCase(updateType)) {
				preparedStatement = conn
						.prepareStatement(SQLPreparedStatements.Update.META_DATE
								.getValue());
				preparedStatement.setTimestamp(1, value);
				preparedStatement.setLong(2, buid);
				preparedStatement.setString(3, paramName);
			} else if ("delete".equalsIgnoreCase(updateType)) {
				preparedStatement = conn
						.prepareStatement(SQLPreparedStatements.Delete.META_DATE
								.getValue());
				preparedStatement.setLong(1, buid);
				preparedStatement.setString(2, paramName);
			} else if ("create".equalsIgnoreCase(updateType)) {
				preparedStatement = conn
						.prepareStatement(SQLPreparedStatements.Insert.META_DATE
								.getValue());
				preparedStatement.setLong(1, buid);
				preparedStatement.setString(2, paramName);
				preparedStatement.setTimestamp(3, value);
			}
			if (preparedStatement != null) {
				preparedStatement.execute();
				conn.commit();
				result = true;
			}
		} catch (SQLException e) {
			LOG.error("Error Updating meta data date: " + e);
			SQLHelpers.rollBack(conn, "Updating meta data date");
		} finally {
			SQLHelpers.closeResources(preparedStatement, null);
		}
		return result;
	}

	public static ResultCodes.Data updateMeta(String metaParamName,
			LookingGlassType type, String metaParamValue,
			String metaParamUpdateType, Long dId, Connection conn) {
		//final int did = DatabaseHelpers.getDidFromGlobalId(globalId, conn);
		ResultCodes.Data result = ResultCodes.Data.FAIL;
		boolean success = false;
		if (dId != null) {

			if (!StringUtils.isNullOrEmpty(metaParamName) && type != null
					&& !StringUtils.isNullOrEmpty(metaParamUpdateType)) {

				if (type.equals(LookingGlassType.integer)) {

					success = updateDatasetMetaInt(conn, dId, metaParamName,
							metaParamValue, metaParamUpdateType);

				} else if (type.equals(LookingGlassType.double_precision)) {
					success = updateDatasetMetaDouble(conn, dId, metaParamName,
							metaParamValue, metaParamUpdateType);

				} else if (type.equals(LookingGlassType.string_value)
						&& !StringUtils.isNullOrEmpty(metaParamValue)) {
					success = updateDatasetMetaString(conn, dId, metaParamName,
							metaParamValue, metaParamUpdateType);

				} else if (type.equals(LookingGlassType.date_time)
						&& !StringUtils.isNullOrEmpty(metaParamValue)) {

					try {
						Timestamp timeStamp = null;
						if (!metaParamUpdateType.equals("delete")) {
							DateTime dt = new DateTime(metaParamValue);

							timeStamp = new Timestamp(dt.getMillis());
						}
						success = updateDatasetMetaDate(conn, dId,
								metaParamName, timeStamp, metaParamUpdateType);
					} catch (IllegalArgumentException e) {
						success = false;
						LOG.error("error parsing date: " + e);
					}

				}
				result = success ? ResultCodes.Data.SUCCESS
						: ResultCodes.Data.FAIL;
			} else {
				result = ResultCodes.Data.INVALID_ARGS;
			}

		} else {
			result = ResultCodes.Data.NOT_FOUND;
		}
		return result;
	}

}
