package com.mohago.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.mohago.sql.SQLPreparedStatements;
import com.mohago.utilities.SQLHelpers;

public class DatasetUpdateService {
	private final static Logger LOG = Logger.getLogger(DatasetUpdateService.class);



	public static boolean markDatasetAsDeleted(
			final Long dId, final Connection conn) {
		LOG.info("Deleting dataset");
		PreparedStatement preparedStatement = null;
		int rowsAffected = 0;
		try {
			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Update.MARK_DATASET_AS_DELETED
							.getValue());
			preparedStatement.setLong(1, dId);
			rowsAffected = preparedStatement.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOG.error("Error deleting dataset: " + e);
			SQLHelpers.rollBack(conn, "deleting dataset");
		} finally {
			SQLHelpers.closeResources(preparedStatement, null);
		}
		return rowsAffected > 0;

	}
	
	public static boolean moveToNewProject(final String project,
			final Long dId, final Connection conn) {
		LOG.info("Updating dataset project");
		boolean result = false;
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Update.CHANGE_PROJECT
							.getValue());
			preparedStatement.setString(1, project);
			preparedStatement.setLong(2, dId);
			preparedStatement.execute();
			conn.commit();
			result = true;
		} catch (SQLException e) {
			LOG.error("Error Updating dataset project: " + e);
			SQLHelpers.rollBack(conn, "Updating dataset project");
		} finally {
			SQLHelpers.closeResources(preparedStatement, null);
		}
		return result;

	}


}
