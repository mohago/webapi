package com.mohago.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;


public class HashUtils {
	
	private final static Logger LOG = Logger.getLogger(HashUtils.class);
	
	public static String getMD5(String filePath){
		

		String md5 = "";
		try(FileInputStream fis = new FileInputStream(new File(filePath))){
		md5 = DigestUtils.md5Hex(fis);
		fis.close();
		} catch (FileNotFoundException e) {
			LOG.error("File not found getting md5 hash: " + e);
		} catch (IOException e) {
			LOG.error("I/O error getting md5 hash: " + e);
		}
		return md5;
	}

}
