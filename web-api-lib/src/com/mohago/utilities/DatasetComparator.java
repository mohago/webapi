package com.mohago.utilities;

import java.util.Comparator;

import com.mohago.serialization.Dataset;

public class DatasetComparator implements Comparator<Dataset> {
	String property;
	boolean reverse;

	public DatasetComparator(boolean reverse, String property) {
		this.property = property;
		this.reverse = reverse;
	}

	@Override
	public int compare(Dataset o1, Dataset o2) {
		int result = 0;
		if (this.property.equals("name") && this.reverse) {
			result = o2.getName().compareTo(o1.getName());
		} else if (this.property.equals("name") && !this.reverse) {
			result = o1.getName().compareTo(o2.getName());
		} else if (this.property.equals("project") && this.reverse) {
			result = o2.getProject().compareTo(o1.getProject());
		} else if (this.property.equals("project") && !this.reverse) {
			result = o1.getProject().compareTo(o2.getProject());
		} else if (this.property.equals("date") && this.reverse) {
			result = o2.getDate().compareTo(o1.getDate());
		} else if (this.property.equals("date") && !this.reverse) {
			result = o1.getDate().compareTo(o2.getDate());
		}
		return result;
	}
}
