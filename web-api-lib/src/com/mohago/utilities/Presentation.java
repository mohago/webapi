package com.mohago.utilities;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.LongSerializationPolicy;
import com.google.gson.reflect.TypeToken;
import com.mohago.search.DatasetSearch;
import com.mohago.serialization.Dataset;
import com.mohago.serialization.Pair;
import com.mysql.jdbc.StringUtils;

public class Presentation {

	private static final Logger LOG = Logger.getLogger(Presentation.class);
	public static final Integer DEFAULT_LIMIT = 20;

	public static String generateLinksHeader(Integer offset, Integer limit,
			Integer totalRows, HttpServletRequest request) {

		StringBuilder linksHeader = new StringBuilder();
		String next = "";
		String prev = "";
		String first = "";
		String last = "";

		String reqUrl = request.getRequestURL().toString() + "?";
		if (StringUtils.isNullOrEmpty(request.getQueryString())) {
			reqUrl += "offset=" + String.valueOf(offset) + "&" + "limit="
					+ String.valueOf(limit);
		} else if (!StringUtils.isNullOrEmpty(request.getQueryString())) {

			reqUrl += request.getQueryString();
			if (request.getParameter("offset") == null) {
				reqUrl += "&offset=" + String.valueOf(offset);
			}
			if (request.getParameter("limit") == null) {
				reqUrl += "&limit=" + String.valueOf(limit);
			}
		}

		if ((offset + limit + limit) <= totalRows) {
			// have next
			next = reqUrl.replace("offset=" + String.valueOf(offset), "offset="
					+ String.valueOf(offset + limit));
		} else if ((offset + limit + limit) > totalRows
				&& (offset + limit) <= totalRows) {// have next but
													// limit should
													// be altered

			next = reqUrl.replace(
					"offset=" + String.valueOf(offset),
					"offset="
							+ String.valueOf(offset + limit).replace(
									"limit=" + String.valueOf(limit),
									"limit="
											+ String.valueOf(totalRows
													- (limit + offset))));
		}

		if (next.length() > 0) {
			next = "<" + next + ">; rel=\"next\"";
			linksHeader.append(next);
			linksHeader.append(",");
		}

		if ((offset - limit) >= 0) {
			prev = reqUrl.replace("offset=" + String.valueOf(offset), "offset="
					+ String.valueOf(offset - limit));
		}

		if (prev.length() > 0) {
			next = "<" + prev + ">; rel=\"prev\"";
			linksHeader.append(prev);
			linksHeader.append(",");
		}

		first = reqUrl.replace(
				"offset=" + String.valueOf(offset),
				"offset="
						+ String.valueOf(0).replace(
								"limit=" + String.valueOf(limit),
								"limit=" + String.valueOf(DEFAULT_LIMIT)));
		first = "<" + first + ">; rel=\"first\"";
		linksHeader.append(first);
		linksHeader.append(",");

		if (totalRows - DEFAULT_LIMIT > 0) {
			int lastLimit = totalRows % DEFAULT_LIMIT;
			int lastOffset = totalRows - lastLimit;
			last = reqUrl.replace(
					"offset=" + String.valueOf(offset),
					"offset="
							+ String.valueOf(lastOffset).replace(
									"limit=" + String.valueOf(limit),
									"limit=" + String.valueOf(lastLimit)));
			last = "<" + last + ">; rel=\"last\"";
			linksHeader.append(last);
			linksHeader.append(",");
		}

		

		return linksHeader.toString();
	}

	public static JsonArray filterFields(String filterBy,
			List<Dataset> topLevelDatasets) {

		JsonArray jArray = new JsonArray();
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setLongSerializationPolicy(LongSerializationPolicy.STRING);
		Gson gson = gsonBuilder.create();
		final Type datasetType = new TypeToken<Dataset>() {
		}.getType();

		if (!StringUtils.isNullOrEmpty(filterBy)) {
			String[] keepProps = filterBy.split(",");
			ArrayList<String> keepPropsList = new ArrayList<String>(
					Arrays.asList(keepProps));
			ArrayList<String> propsList = new ArrayList<String>();
			Dataset dataset = topLevelDatasets.get(0);
			JsonObject dsJson = gson.toJsonTree(dataset, datasetType)
					.getAsJsonObject();

			for (Map.Entry<String, JsonElement> entry : dsJson.entrySet()) {
				propsList.add(entry.getKey());
			}
			propsList.removeAll(keepPropsList);

			for (Dataset ds : topLevelDatasets) {
				JsonObject dsJs = gson.toJsonTree(ds, datasetType)
						.getAsJsonObject();
				for (String removeProp : propsList) {
					dsJs.remove(removeProp);
				}
				jArray.add(dsJs);
			}

		} else {// No filtering necessary
			for (Dataset ds : topLevelDatasets) {
				JsonObject dsJs = gson.toJsonTree(ds, datasetType)
						.getAsJsonObject();
				jArray.add(dsJs);
			}
		}
		return jArray;
	}

	public static List<Dataset> sortDatasets(String sortBy,
			List<Dataset> topLevelDatasets) {
		if (!StringUtils.isNullOrEmpty(sortBy)) {
			ArrayList<Pair<Boolean, String>> sortList = DatasetSearch
					.parseSortBy(sortBy);
			if (!sortList.isEmpty()) {
				LOG.debug("sort not empty: " + sortList.get(0).getSecond());
				Collections.sort(topLevelDatasets,
						new DatasetComparator(sortList.get(0).getFirst(),
								sortList.get(0).getSecond()));
			}
		}
		return topLevelDatasets;
	}
}
