package com.mohago.utilities;

public class Constants {
	
	private static final String USER_TABLE_NAME= "user";
	
public enum Headers {
		
		USER_NAME("user_name"),
		PWD("password"), MEMBER_ID("member_id"), TOKEN("X-Auth-Token"), DOMAIN_ID("domain_id"),
		;
		
		private Headers(String value) {
			this.value = value;
		}

		private String value;

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}

public enum Parameters {
	
	PROJECT_NAME("proj_name"), 
	PROJECT_NAME_NEW("proj_name_new"), 
	COLOUR("colour"), 
	USER_ID("user_id"),
	PASSWORD("password"),
	USER_NAME("user_name"),
	ACCESS("access"),
	USER_NAME_NEW("user_name_new"),
	MUST_CHANGE_PWD("must_change_pwd"), 
	COLOUR_NEW("colour_new"), 
	TARGET_ACCESS("access"), DELETED_NEW("deleted_new"), GLOBAL_ID_LIST("global_id_list"), GLOBAL_ID("global_id"), BY_CREATED_BY("by_created_by"),
	META("meta"), 
	META_PARAM_NAME("meta_param_name"),
	META_PARAM_VALUE("meta_param_value"), 
	META_PARAM_TYPE("meta_param_type"), META_PARAM_UPDATE_TYPE("meta_param_update_type"), GLOBAL_ID_NEW("global_id_new"), IS_PWD_HASHED("is_pwd_hashed"),
	;
	
	private Parameters(String value) {
		this.value = value;
	}

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}

public enum SessionAttributes {
	
	ACCESS("access"), 
	USER_ID("user_id"), IS_ADMIN("is_admin"), TIMEOUT("1200"/*20 minute inactivity timeout*/),NUM_LOGIN_FAILS("15"),
	;
	
	private SessionAttributes(String value) {
		this.value = value;
	}

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}

public enum SQL {
	
	ACCESS("access"),
	PWD("password"),
	PWD_LAST_CHANGED("pwd_last_changed"),
	USER_ID("id"),
	PROJ_ID("id"),
	IS_ADMIN("is_admin"),
	MUST_CHANGE_PWD("must_change_pwd"), PSQL_DATABASE_PORT("5432"),
	;
	
	private SQL(String value) {
		this.value = value;
	}

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}

public enum ErrorMessage {
	
	CLOSE_PREPARED_STATEMENT("Error closing prepared statement: "),
	GET_PRINT_WRITER("Error getting print writer: "),
	SQL("SQL error: ")
	;
	
	private ErrorMessage(String value) {
		this.value = value;
	}

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}

public enum ErrorCode {
	
	EMPTY(0),
	NOT_FOUND(-1),
	SQL(-2), AUTH_FAILED_OR_NO_SESSION(-3),
	;
	
	private ErrorCode(int value) {
		this.value = value;
	}

	private int value;

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}

public static String getUserTableName() {
	return USER_TABLE_NAME;
}

}
