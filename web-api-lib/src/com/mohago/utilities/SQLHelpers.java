package com.mohago.utilities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

import org.apache.log4j.Logger;
//import org.owasp.esapi.ESAPI;
//import org.owasp.esapi.codecs.Codec;

import com.mohago.sql.SQLPreparedStatements;
import com.mohago.utilities.Constants.ErrorMessage;

public class SQLHelpers {
	
	private static final Logger LOG = Logger.getLogger(SQLHelpers.class);
	private static final char[] ALPHA_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
	private static final char[] CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789".toCharArray();
	private static final Random RANDOM = new Random();

	public static int getRowCount(final PreparedStatement prepStatement, final String targetParam,
			final Connection conn) {
		ResultSet resultSet = null;
		PreparedStatement alteredPs = null;
		int count;
		try {
			String psString = String.valueOf(prepStatement.toString().split(" ORDER BY")[0]);
			final String split[] = psString.split("SELECT");
			psString = "SELECT" + split[1];
			prepStatement.clearBatch();
			alteredPs = conn.prepareStatement(psString
					.replace("SELECT " + targetParam, "SELECT COUNT(*)"));
			resultSet = alteredPs.executeQuery();
			conn.commit();
			resultSet.next();
			count = resultSet.getInt("count");
			
		} catch (SQLException e) {
			count = 0;
		}finally{
			closeResources(alteredPs, resultSet);//dont close prepStatement
			closeResources(prepStatement, null);//dont close prepStatement
		}
		return count;
	}
	
	public static String removeNonAlphaNumericOrUnderscoreChars(String sql){
		//Codec codec = Configuration.getCodec();
		//return ESAPI.encoder().encodeForSQL(codec, sql);
		return sql.replaceAll("[^a-zA-Z0-9_]", "");
	}
	
	
	public static String preparePlaceHolders(int length) {
	    StringBuilder builder = new StringBuilder();
	    for (int i = 0; i < length;) {
	        builder.append("?");
	        if (++i < length) {
	            builder.append(",");
	        }
	    }
	    return builder.toString();
	}

	public static void setValues(PreparedStatement preparedStatement, Object... values) throws SQLException {
	    for (int i = 0; i < values.length; i++) {
	        preparedStatement.setObject(i + 1, values[i]);
	    }
	}
	
	public static void rollBack(Connection conn, String transactionName){
		if (conn != null) {
			try {
				LOG.info("Transaction: " + transactionName + " is being rolled back");
				conn.rollback();

			} catch (SQLException excep) {
				LOG.error("Error rolling back \"" + transactionName + "\": " + excep);
			}
		}
	}
	public static void closeConn(Connection conn){
		try {
			if (conn != null) {
				//conn.commit();
				conn.close();
			}
		} catch (SQLException e1) {
			LOG.error("Error closing connection" + e1);
		}
	}
	
	public static void setAutoCommit(Connection conn, boolean autoCommit){
		try {
			if (conn != null) {
				conn.setAutoCommit(autoCommit);
			}
		} catch (SQLException e) {
			LOG.error("Error setting auto commit to " + String.valueOf(autoCommit) + ": " + e);
		}
	}
	
	public static void setSchema(final String schema, final Connection conn) {
		PreparedStatement statement = null;
		final String sql = String.format(SQLPreparedStatements.Select.SET_SCHEMA.getValue(), schema);
		if(conn != null){
		try {
			
			statement = conn.prepareStatement(sql);
			statement.execute();
		} catch (SQLException e1) {
			LOG.error("Error setting schema: " + e1);
		}finally{
			closeResources(statement, null);
		}
		}
	}
	
	public static String ranToken(int size) {
		final StringBuilder stringBuilder = new StringBuilder();
		final char a = ALPHA_CHARS[RANDOM.nextInt(ALPHA_CHARS.length)];
		stringBuilder.append(a);
		for (int i = 0; i < size - 1; i++) {
			final char c = CHARS[RANDOM.nextInt(CHARS.length)];
			stringBuilder.append(c);
		}
		return stringBuilder.toString().toUpperCase();
	}
	
	public static void closeResources(final PreparedStatement preparedStatement, final ResultSet reader){
		try {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (reader != null) {
				reader.close();
			}
		} catch (SQLException e1) {
			LOG.error(ErrorMessage.CLOSE_PREPARED_STATEMENT.getValue() + e1);
		}
	}
	
	

	

}
