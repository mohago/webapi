package com.mohago.utilities;

public class StringUtils {

	public static boolean isNullOrEmpty(String testString){
		boolean ret = false;
		if(testString == null || testString.length() < 1){
			ret = true;
		}
		return ret;
	}
}
