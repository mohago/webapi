package com.mohago.utilities;

import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import cloudcix.api.Membership;
import cloudcix.base.Response;

public class CIXApi {

	private static Membership mem = null;
	static {
		mem = new Membership();
	}

	public static HashSet<Integer> getLinkedMemberIdSet(String securityToken) throws SocketTimeoutException {

		Map a = new HashMap<String, String>();
		a.put("fields", "(idMember)");
		Response resp = mem.getMember().read(securityToken, null, null, a,
				null, null);
		
		HashSet<Integer> idMemberSet = new HashSet<Integer>();
		if (resp != null) {
			JSONObject body = resp.body;
			JSONArray idList = (JSONArray) body.get("content");

			for (Object entry : idList) {
				idMemberSet.add(((Long) ((JSONObject) entry).get("idMember"))
						.intValue());
			}
		}
		return idMemberSet;

	}

	public static HashSet<Integer> getLinkedAddressList(String securityToken) throws SocketTimeoutException {
		Map a = new HashMap<String, String>();
		a.put("fields", "(idAddress)");
		Response resp = mem.getAddress().read(securityToken, null, null, a,
				null, null);
		
		HashSet<Integer> idMemberSet = new HashSet<Integer>();
		if (resp != null) {
			JSONObject body = resp.body;
			JSONArray idList = (JSONArray) body.get("content");

			for (Object entry : idList) {
				idMemberSet.add(((Long) ((JSONObject) entry).get("idAddress"))
						.intValue());
			}
		}
		return idMemberSet;
	}
	
	
}
