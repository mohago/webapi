package com.mohago.utilities;

public class ResultCodes {

	public enum UpdateUser {
		SUCCESS("10"), USER_NAME_ALREADY_EXISTS("11"), NO_PERMISSION("12"), NO_SUCH_USER(
				"13"), INCORRECT_FORMAT_OR_NULL("14"), SQL_EXCEPTION("15");

		private UpdateUser(String value) {
			this.value = value;
		}

		private String value;

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}
	
	public enum UpdateProject {
		SUCCESS("40"), PROJ_NAME_ALREADY_EXISTS("41"), NO_PERMISSION("42"), NO_SUCH_PROJ(
				"43"), NO_SUCH_USER(
						"44"), INCORRECT_FORMAT_OR_NULL("45"), SQL_EXCEPTION("46"), USER_ALREADY_ADDED("47"), FAIL("48"), NOT_ALL_SUCCESS("49");

		private UpdateProject(String value) {
			this.value = value;
		}

		private String value;

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}
	
	public enum Authenticate {
		SUCCESS("20"), SUCCESS_PWD_EXPIRED("21"), USER_NAME_NOT_FOUND_OR_WRONG_PWD("22"),
		INCORRECT_FORMAT_OR_NULL("23"), 
		SQL_EXCEPTION("24"), 
		PROJ_NOT_FOUND("25"), 
		TRUE("26"), 
		FALSE("27"), 
		LOCKED_OUT("28");

		private Authenticate(String value) {
			this.value = value;
		}

		private String value;

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}
	
	public enum Config {
		SUCCESS("30"), FAIL("31"), TRUE("32"), FALSE("33"), CONNECTION_PARAMS_INCORRECT("34"), ACCESS_DENIED("35"), SQL_EXCEPTION("36"), NO_DB_CONNECTION("37");

		private Config(String value) {
			this.value = value;
		}

		private String value;

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}
	
	public enum Data {
		SUCCESS("50"), NOT_FOUND("51"), NO_PERMISSION("52"), FAIL("53"), ERROR("54"), INVALID_ARGS("55") ;

		private Data(String value) {
			this.value = value;
		}

		private String value;

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}

}
