package com.mohago.json;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "Item")
@ApiModel
public class Item {
	
	private ArrayList<Parameter<?>> parameters = new ArrayList<Parameter<?>>();

	@XmlElementWrapper(name = "parameters")
	@XmlElement(name = "param")
	@ApiModelProperty(required = true)
	public ArrayList<Parameter<?>> getParameters() {
		return parameters;
	}

	public void setParameters(ArrayList<Parameter<?>> parameters) {
		this.parameters = parameters;
	}
	
	private Item(){
		
	}

}
