package com.mohago.json;

public class Error {

	private String message, title;
	private int code;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Error(String message, String title, int code) {
		this.setCode(code);
		this.setMessage(message);
		this.setTitle(title);

	}

}
