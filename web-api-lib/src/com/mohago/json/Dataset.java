package com.mohago.json;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "Dataset")
public class Dataset {

	private List<Item> items = new ArrayList<Item>();
	private List<MetaData> metadata = new ArrayList<MetaData>();
	private String name, project;

	@XmlElementWrapper(name = "items")
	@XmlElement(name = "item")
	@ApiModelProperty(required = false, position = 3)
	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	@XmlElementWrapper(name = "metadata")
	@XmlElement(name = "meta")
	@ApiModelProperty(required = false, position = 4)
	public List<MetaData> getMetadata() {
		return metadata;
	}

	public void setMetadata(List<MetaData> metaList) {
		this.metadata = metaList;
	}

	@XmlElement(name = "name")
	@ApiModelProperty(required = true, position = 1)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name = "project")
	@ApiModelProperty(required = false, position = 2)
	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public Dataset() {

	}

}
