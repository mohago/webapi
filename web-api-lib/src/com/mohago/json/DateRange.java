package com.mohago.json;



//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlRootElement;

//import com.wordnik.swagger.annotations.ApiModel;
//import com.wordnik.swagger.annotations.ApiModelProperty;

//@XmlRootElement(name = "DateRange")
//@ApiModel
public class DateRange {
	private  java.sql.Date dateTimeStart;
	private java.sql.Date dateTimeEnd;
	private String dateParamName;

	//public DateRange(String dateStart, String dateEnd, String dateName) {
	public DateRange() {	
	//setDateTimeEnd(dateEnd);
		//setDateTimeStart(dateStart);
		//setDateParamName(dateName);
	}
	
	
	//@XmlElement(name = "dateParamName")
	//@ApiModelProperty(required=true)
	public String getDateParamName() {
		return dateParamName;
	}

	public void setDateParamName(String paramName) {
		this.dateParamName = paramName;
	}

	
	//@XmlElement(name = "startDate")
	public java.sql.Date getDateTimeStart() {
		return dateTimeStart;
	}

	public void setDateTimeStart(java.sql.Date startDate) {
		this.dateTimeStart = startDate;
	}

	
	//@XmlElement(name = "endDate")
	public java.sql.Date getDateTimeEnd() {
		return dateTimeEnd;
	}

	public void setDateTimeEnd(java.sql.Date endDate) {
		this.dateTimeEnd = endDate;
	}


}

