package com.mohago.json;

import java.util.ArrayList;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ParameterCollection {
	
	private ArrayList<Integer> paramInts;
	private ArrayList<Double> paramDoubles;
	private ArrayList<String> paramStrings;
	private ArrayList<Date> paramDates;
	
	@XmlElementWrapper
	@XmlElement
	public ArrayList<Integer> getParamInts() {
		return paramInts;
	}
	public void setParamInts(ArrayList<Integer> paramInts) {
		this.paramInts = paramInts;
	}
	@XmlElementWrapper
	@XmlElement
	public ArrayList<Double> getParamDoubles() {
		return paramDoubles;
	}
	public void setParamDoubles(ArrayList<Double> paramDoubles) {
		this.paramDoubles = paramDoubles;
	}
	@XmlElementWrapper
	@XmlElement
	public ArrayList<String> getParamStrings() {
		return paramStrings;
	}
	public void setParamStrings(ArrayList<String> paramStrings) {
		this.paramStrings = paramStrings;
	}
	@XmlElementWrapper
	@XmlElement
	public ArrayList<Date> getParamDates() {
		return paramDates;
	}
	public void setParamDates(ArrayList<Date> paramDates) {
		this.paramDates = paramDates;
	}
	
	private ParameterCollection(){
		
	}

}
