package com.mohago.json;

import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement
public class ChartRequest {

	private Set<String> globalIdSet;
	private String chartType, xName, yName;
	private int xType, yType, limit;

	@XmlElement(name = "chartType")
	@ApiModelProperty(required = true, allowableValues="xy")
	public String getChartType() {
		return chartType;
	}

	public void setChartType(String chartType) {
		this.chartType = chartType;
	}

	@XmlElement(name = "xName")
	@ApiModelProperty(required = true, notes="name of parameter on x-axis, e.g. \"time\"")
	public String getXname() {
		return xName;
	}

	public void setXname(String xName) {
		this.xName = xName;
	}

	@XmlElement(name = "yName")
	@ApiModelProperty(required = true, notes="name of parameter on y-axis, e.g. \"distance\"")
	public String getYname() {
		return yName;
	}

	public void setYname(String yName) {
		this.yName = yName;
	}

	@XmlElement(name = "xType")
	@ApiModelProperty(required = true, allowableValues="0,1,2,3")
	public int getXtype() {
		return xType;
	}

	public void setXtype(int xType) {
		this.xType = xType;
	}

	@XmlElement(name = "yType")
	@ApiModelProperty(required = true, allowableValues="0,1,2,3")
	public int getYtype() {
		return yType;
	}

	public void setYtype(int yType) {
		this.yType = yType;
	}

	@XmlElementWrapper(name = "globalIdSet")
	@XmlElement(name = "globalId")
	@ApiModelProperty(required = true)
	public Set<String> getGlobalIdSet() {
		return globalIdSet;
	}

	public void setGlobalIdSet(Set<String> globalIdSet) {
		this.globalIdSet = globalIdSet;
	}

	public ChartRequest() {

	}

	@XmlElement(name = "limit")
	@ApiModelProperty(required = false)
	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}
}
