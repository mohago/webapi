package com.mohago.json;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "Parameter")
@ApiModel
public class Parameter<T> {
	
	private String name;
	private T value;
	
	public Parameter(String string, int i, T d) {
		// TODO Auto-generated constructor stub
	}
	@XmlElement(name = "name")
	@ApiModelProperty(required=true)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name = "value", required=true)
	public T getValue() {
		return value;
	}
	public void setValue(T value) {
		this.value = value;
	}
	
	protected Parameter(){
		
	}

}
