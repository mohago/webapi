package com.mohago.json;

public class Token {

	private String issued_at;
	private String expires_at;
	private String value;
	
	public String getIssued_at() {
		return issued_at;
	}
	public void setIssued_at(String issued_at) {
		this.issued_at = issued_at;
	}
	public String getExpires_at() {
		return expires_at;
	}
	public void setExpires_at(String expired_at) {
		this.expires_at = expired_at;
	}
	
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
public Token(){
		
	}
}
