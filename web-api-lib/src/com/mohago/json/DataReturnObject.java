package com.mohago.json;

import java.util.Collection;

import org.json.JSONObject;

public class DataReturnObject {
	public String[] labels;
	public Collection data;
	public String  parameters; //Pagination next page if offset and limit are used. Otherwise null.
	
	public DataReturnObject(String[] labels, Collection data,String parameters) {
		this.labels = labels;
		this.data = data;
		this.parameters=parameters;
	}
	
	public void SetNextPage(String parameters){
		this.parameters=parameters;
	}
}
