package com.mohago.json;

import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Search {
	
	
	private Set<String> globalIdSet;
	private String start, end, project;
	
	@XmlElementWrapper(name = "globalIdSet")
	@XmlElement(name = "globalId")
	public Set<String> getGlobalIdSet() {
		return globalIdSet;
	}
	public void setGlobalIdSet(Set<String> globalIdSet) {
		this.globalIdSet = globalIdSet;
	}
	@XmlElement(name = "start")
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	@XmlElement(name = "end")
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	@XmlElement(name = "project")
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	
	public Search(){
		
	}

}
