package com.mohago.json;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement
public class MetaData<T> extends Parameter<T> {

	private String name;
	private T value;
	
	public MetaData(String string, int i, T d) {
		// TODO Auto-generated constructor stub
	}
	@XmlElement(name = "name")
	@ApiModelProperty(required=true)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@XmlElement(name = "value", required=true)
	public T getValue() {
		return value;
	}
	public void setValue(T value) {
		this.value = value;
	}
	
	protected MetaData(){
		
	}
}
