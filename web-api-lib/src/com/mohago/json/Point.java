package com.mohago.json;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Point")
public class Point<T1, T2> {
	
	private T1 x;
    private T2 y;

    

    private Point(){
    	
    }
	public Point(T1 x, T2 y) {//Ignoring java naming conventions to so that serialization with c# works
    	super();
    	this.x = x;
    	this.y = y;
    }

    public int hashCode() {
    	int hashx = x != null ? x.hashCode() : 0;
    	int hashy = y != null ? y.hashCode() : 0;

    	return (hashx + hashy) * hashy + hashx;
    }

    public boolean equals(Object other) {
    	if (other instanceof Point) {
    		Point otherPoint = (Point) other;
    		return 
    		((  this.x == otherPoint.x ||
    			( this.x != null && otherPoint.x != null &&
    			  this.x.equals(otherPoint.x))) &&
    		 (	this.y == otherPoint.y ||
    			( this.y != null && otherPoint.y != null &&
    			  this.y.equals(otherPoint.y))) );
    	}

    	return false;
    }

    public String toString()
    { 
           return "(" + x + ", " + y + ")"; 
    }

	@XmlElement(name = "x")
    public T1 getX() {
    	return x;
    }

	@XmlElement(name = "y")
    public T2 getY() {
    	return y;
    }
    
    public void setX(T1 x) {
		this.x = x;
	}

	public void setY(T2 y) {
		this.y = y;
	}

}
