package com.mohago.json;

import java.util.ArrayList;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "MetaDataCollection")
public class MetaDataCollection {

	private ArrayList<MetaData<Double>> doubleData = new ArrayList<MetaData<Double>>();
    private ArrayList<MetaData<String>> stringData = new ArrayList<MetaData<String>>();
    
    @XmlElementWrapper(name="doubleData")
    @XmlElement(name = "doubleMeta")
    public ArrayList<MetaData<Double>> getDoubleData() {
		return doubleData;
	}
	public void setDoubleData(ArrayList<MetaData<Double>> doubleData) {
		this.doubleData = doubleData;
	}
	
	@XmlElementWrapper(name="stringData")
    @XmlElement(name = "stringMeta")
	public ArrayList<MetaData<String>> getStringData() {
		return stringData;
	}
	public void setStringData(ArrayList<MetaData<String>> stringData) {
		this.stringData = stringData;
	}
	@XmlElementWrapper(name="dateTimeData")
    @XmlElement(name = "dateTimeMeta")
	public ArrayList<MetaData<Date>> getDateTimeData() {
		return dateTimeData;
	}
	public void setDateTimeData(ArrayList<MetaData<Date>> dateTimeData) {
		this.dateTimeData = dateTimeData;
	}
	@XmlElementWrapper(name="intData")
    @XmlElement(name = "intMeta")
	public ArrayList<MetaData<Integer>> getIntData() {
		return intData;
	}
	public void setIntData(ArrayList<MetaData<Integer>> intData) {
		this.intData = intData;
	}
	private ArrayList<MetaData<Date>> dateTimeData = new ArrayList<MetaData<Date>>();
	private ArrayList<MetaData<Integer>> intData = new ArrayList<MetaData<Integer>>();
}
