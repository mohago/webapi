package com.mohago.json;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Chart")
public class Chart {
	private ArrayList<Point<Number, Number>> points = new ArrayList<Point<Number, Number>>();

	@XmlElementWrapper(name = "points")
	@XmlElement(name = "point")
	public ArrayList<Point<Number, Number>> getPoints() {
		return points;
	}

	public void setPoints(ArrayList<Point<Number, Number>> points) {
		this.points = points;
	}
	
	public Chart(){
		
	}

}
