package com.mohago.json;

import com.mohago.serialization.Domain;

public class CloudCIXUser {
	
	private String startDate;
	private int idLanguage;
	private int idAddress;
	private boolean globalUser;
	private int idMember;
	private String firstName;
	private String surname;
	private int idDepartment;
	private int idUser;
	private String username;
	private String timezone;
	private String name;
	private boolean enabled;
	private boolean globalActive;
	private String expiryDate;
	private int domain_Id;
	private String lastLogin;
	private int idProfile;
	private String email;
	private boolean adminstrator;
	private Domain domain;
	private Address address;
	private Token token;


	public String getStartDate() {
		return startDate;
	}


	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}


	public int getIdLanguage() {
		return idLanguage;
	}


	public void setIdLanguage(int idLanguage) {
		this.idLanguage = idLanguage;
	}


	public int getIdAddress() {
		return idAddress;
	}


	public void setIdAddress(int idAddress) {
		this.idAddress = idAddress;
	}


	public boolean isGlobalUser() {
		return globalUser;
	}


	public void setGlobalUser(boolean globalUser) {
		this.globalUser = globalUser;
	}


	public int getIdMember() {
		return idMember;
	}


	public void setIdMember(int idMember) {
		this.idMember = idMember;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getSurname() {
		return surname;
	}


	public void setSurname(String surname) {
		this.surname = surname;
	}


	public int getIdDepartment() {
		return idDepartment;
	}


	public void setIdDepartment(int idDepartment) {
		this.idDepartment = idDepartment;
	}


	public int getIdUser() {
		return idUser;
	}


	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getTimezone() {
		return timezone;
	}


	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public boolean isEnabled() {
		return enabled;
	}


	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}


	public boolean isGlobalActive() {
		return globalActive;
	}


	public void setGlobalActive(boolean globalActive) {
		this.globalActive = globalActive;
	}


	public String getExpiryDate() {
		return expiryDate;
	}


	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}


	public int getDomain_Id() {
		return domain_Id;
	}


	public void setDomain_Id(int domain_Id) {
		this.domain_Id = domain_Id;
	}


	public String getLastLogin() {
		return lastLogin;
	}


	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}


	public int getIdProfile() {
		return idProfile;
	}


	public void setIdProfile(int idProfile) {
		this.idProfile = idProfile;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public boolean isAdminstrator() {
		return adminstrator;
	}


	public void setAdminstrator(boolean adminstrator) {
		this.adminstrator = adminstrator;
	}


	public Domain getDomain() {
		return domain;
	}


	public void setDomain(Domain domain) {
		this.domain = domain;
	}


	public Address getAddress() {
		return address;
	}


	public void setAddress(Address address) {
		this.address = address;
	}

	public CloudCIXUser(){
		
	}


	public Token getToken() {
		return token;
	}


	public void setToken(Token token) {
		this.token = token;
	}

}
