package com.mohago.node;

public class Node {
	private long id;
	private String ipAddress;

	public long getId() {
		return id;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public Node(final long id, final String ipAddress) {
		this.id = id;
		this.ipAddress = ipAddress;
	}
}
