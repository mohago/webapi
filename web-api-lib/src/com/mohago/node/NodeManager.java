package com.mohago.node;

import java.util.HashSet;
import java.util.Random;
import java.util.SortedMap;
import java.util.TreeMap;

public enum NodeManager {
	INSTANCE;
	private static final SortedMap<Long, String> CIRCLE = new TreeMap<Long, String>();
	private static final int V_NODE_COUNT = 1024;
	private static final long FIRST_HALF_RANGE = Integer.MAX_VALUE;
	private static final long SECOND_HALF_RANGE = Integer.MAX_VALUE;
	private static final long MAX_NUM_SHARDS = 1000;
	private static final Random RAND = new Random();
//	private static final int V_NODE_COUNT = 8;
//	private static final long FIRST_HALF_RANGE = 180;
//	private static final long SECOND_HALF_RANGE = 180;

	/**
	 * 
	 */
	public static void refresh(HashSet<Node> nodeSet) {
		CIRCLE.clear();
		for (Node node : nodeSet) {
			if (!CIRCLE.containsKey(node.getId())) {
				addNode(node);
			}
		}

	}

	private static void addNode(final Node node) {
		final long RANGE = FIRST_HALF_RANGE + SECOND_HALF_RANGE;
		for (long i = node.getId(); i < (node.getId() + (RANGE / 2)); i += (RANGE / V_NODE_COUNT)) {
			CIRCLE.put(i % RANGE, node.getIpAddress());
			CIRCLE.put((i + RANGE / 2) % RANGE, node.getIpAddress());
		}
	}

	public static String getNodeAddressByAddressId(String addressId) {

		long hashValue = MD5Hash.hash(addressId);
		if (!CIRCLE.containsKey(hashValue)) {
			SortedMap<Long, String> tailMap = CIRCLE.tailMap(hashValue);
			hashValue = tailMap.isEmpty() ? CIRCLE.firstKey() : tailMap
					.firstKey();
		}

		return CIRCLE.get(hashValue);
	}

	public static String getNodeAddressByDeviceToken(String deviceToken) {
		String addressId = deviceToken.split("a")[0];
		return getNodeAddressByAddressId(addressId);
	}
	
	public static long getDatasetUid(int userId){
		long ret = 0l;
		long ourEpoch = 1314220021721l;
		long now = System.currentTimeMillis();
		ret = now - ourEpoch << (64-41);
		ret |= (userId % MAX_NUM_SHARDS) << (64-41-9);
		ret |= RAND.nextInt(Integer.MAX_VALUE - 1) % 16384;
		return ret;
		
	}

}
