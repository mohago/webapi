package com.mohago.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.utils.DBConnectionManager;

import com.mohago.config.DatabaseConnection;
import com.mohago.request.MasterTable;
import com.mohago.serialization.Pair;
import com.mohago.sql.SQLPreparedStatements;
import com.mohago.utilities.SQLHelpers;

@DisallowConcurrentExecution
public class DeleteJob implements Job {

	private final static Logger LOG = Logger.getLogger(DeleteJob.class);

	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		// Connection conn = DatabaseConnection.getMasterConnection();//Mohago
		// system uses the master table to store file to be deleted info
		String ipAddress = (String) context.getJobDetail().getJobDataMap()
				.get("ip_address");
		try (Connection conn = DatabaseConnection
				.getPooledConnection(ipAddress)) {
			 SQLHelpers.setAutoCommit(conn, false);
			while (fileImportDeleteQueueCount(conn) > 0) {

				Pair<Integer, String> nameIdPair = getNextFileForDeletion(conn);
				String fileToBeDeleted = nameIdPair.getSecond();
				try {
					Files.delete(Paths.get(fileToBeDeleted));
					Files.delete(Paths.get(fileToBeDeleted.replace(".lgmf",
							".lgdf")));
				} catch (IOException e) {
					LOG.error("Error deleting lgf file: " + e);
				}

				removeFromFileDeleteTable(nameIdPair.getFirst(), conn);

				if (ScanService.getFilesSuccessfullyImported().contains(
						fileToBeDeleted)) {
					ScanService.getFilesSuccessfullyImported().remove(
							fileToBeDeleted);
				}

			}

			conn.commit();
		} catch (SQLException e) {
			LOG.error("Error from cleanup: " + e);
		}
	}

	private void removeFromFileDeleteTable(Integer integer, Connection conn) {
		PreparedStatement removeFileFromDeleteTable = null;
		try {
			removeFileFromDeleteTable = conn
					.prepareStatement(SQLPreparedStatements.Delete.REMOVE_FILE_FROM_DELETE_TABLE
							.getValue());
			removeFileFromDeleteTable.setInt(1, integer);
			removeFileFromDeleteTable.execute();
			conn.commit();
		} catch (SQLException e) {
			LOG.error("Error remove From File Delete Table: " + e);
			SQLHelpers.rollBack(conn, "remove From File Delete Table");
		} finally {
			SQLHelpers.closeResources(removeFileFromDeleteTable, null);
		}

	}

	private Pair<Integer, String> getNextFileForDeletion(Connection conn) {
		PreparedStatement getNextFileForDeletion = null;
		ResultSet reader = null;
		Pair<Integer, String> ret = new Pair<Integer, String>(-1, "");
		try {
			getNextFileForDeletion = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_NEXT_FILE_FOR_DELETION
							.getValue());
			reader = getNextFileForDeletion.executeQuery();
			reader.next();
			ret.setFirst(reader.getInt(1));
			ret.setSecond(reader.getString(2));
			conn.commit();

		} catch (SQLException e) {
			LOG.error("Error getting next file for deletion: " + e);
			SQLHelpers.rollBack(conn, "getting next file for deletion");
		} finally {
			SQLHelpers.closeResources(getNextFileForDeletion, reader);
		}
		return ret;
	}

	private int fileImportDeleteQueueCount(Connection conn) {
		int ret = -1;
		PreparedStatement getFileImportDeleteQueueLength = null;
		ResultSet reader = null;
		try {
			getFileImportDeleteQueueLength = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_FILE_DELETE_QUEUE_LENGTH
							.getValue());
			reader = getFileImportDeleteQueueLength.executeQuery();
			reader.next();
			ret = reader.getInt(1);
			conn.commit();

		} catch (SQLException e) {
			LOG.error("Error getting file delete queue length: " + e);
			SQLHelpers.rollBack(conn, "file delete queue length");
		} finally {
			SQLHelpers.closeResources(getFileImportDeleteQueueLength, reader);
		}
		return ret;
	}

}
