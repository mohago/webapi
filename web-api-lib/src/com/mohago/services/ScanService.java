package com.mohago.services;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.mohago.config.Configuration;
import com.mohago.sql.SQLPreparedStatements;
import com.mohago.utilities.SQLHelpers;

public class ScanService{

	private final static Logger LOG = Logger.getLogger(ScanService.class);
	private static final int INTERVAL = 1 * 5;
	private static boolean idle;
	private static boolean active;
	static final String LGDF_FOLDER = Configuration.getLgdfFolderPath();
	
	private static LinkedList<String> filesToProcess = new LinkedList<String>();
	private static HashSet<String> filesImportedSuccessfully = new HashSet<String>();
	
	static String currentFile;
	static Scheduler scheduler = null;
	

	public static Scheduler getScheduler() {
		return scheduler;
	}

	public static void setScheduler(Scheduler scheduler) {
		ScanService.scheduler = scheduler;
	}
	

	static boolean isIdle() {
		return idle;
	}

	static void setIdle(boolean idle) {
		ScanService.idle = idle;
	}

	public static HashSet<String> getFilesSuccessfullyImported() {
		return filesImportedSuccessfully;
	}

//	public void setImportedSuccessfully(HashSet<String> importedSuccessfully) {
//		ScanService.importedSuccessfully = importedSuccessfully;
//	}

	public static LinkedList<String> getFilesToProcess() {
		return filesToProcess;
	}

//	public static void setFilesToProcess(LinkedList<String> filesToProcess) {
//		ScanService.filesToProcess = filesToProcess;
//	}


	public static boolean isActive() {
		return active;
	}

	public static void setActive(boolean active) {
		ScanService.active = active;
	}

	public ScanService() {

		try {
			setScheduler(new StdSchedulerFactory().getScheduler());
			scheduler.start();
		} catch (SchedulerException se) {
			LOG.error("Error with scheduler: " + se);
		}

		JobDetail job = JobBuilder.newJob(PreScanJob.class)
				.withIdentity("pre_scan_job", "group1").build();

		// Trigger the job to run now, and then repeat every 40 seconds
		Trigger preScanTrigger = TriggerBuilder
				.newTrigger()
				.withIdentity("pre_scan_trigger", "group1")
				.startNow()
				.withSchedule(
						SimpleScheduleBuilder.simpleSchedule()
								.withIntervalInSeconds(INTERVAL).repeatForever())
				.build();
		try {
			scheduler.scheduleJob(job, preScanTrigger);
			
		} catch (SchedulerException e) {
			LOG.error("Scheduler exception: " + e);
		}
		setIdle(true);
		setActive(true);
	}

	static void addToFileDeleteTable(final String currentFile,
			final Connection conn) {
		PreparedStatement addFileToDeleteTable = null;
		try {
			addFileToDeleteTable = conn
					.prepareStatement(SQLPreparedStatements.Insert.ADD_FILE_TO_DELETE_TABLE
							.getValue());
			addFileToDeleteTable.setString(1, currentFile);
			addFileToDeleteTable.execute();
			conn.commit();
		} catch (SQLException e) {
			LOG.error("Error addToFileDeleteTable: " + e);
			SQLHelpers.rollBack(conn, "addToFileDeleteTable");
		} finally {
			SQLHelpers.closeResources(addFileToDeleteTable, null);
		}

	}


	static void setImportedToTrue(String deleteFileName) {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		try {
			db = dbf.newDocumentBuilder();
		} catch (ParserConfigurationException e3) {

			LOG.error("Parser config exception loading lgmf file, setting imported to true: "
					+ e3);
		}
		Document doc = null;
		try {
			doc = db.parse(new FileInputStream(deleteFileName));

		} catch (SAXException | IOException | IllegalArgumentException e3) {

			LOG.error("Error parsing lgmf: " + e3);
		}

		if (doc != null) {
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("Meta");
			for (int temp = 0; temp < nList.getLength(); temp++) {// This
																	// should
																	// all

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					Node jdbcTypeNode = eElement.getElementsByTagName(
							"Imported").item(0);
					Node contentNode = jdbcTypeNode.getFirstChild();
					contentNode.setTextContent("1");
				}

				TransformerFactory transformerFactory = TransformerFactory
						.newInstance();
				Transformer transformer = null;
				try {
					transformer = transformerFactory.newTransformer();
				} catch (TransformerConfigurationException e) {
					LOG.error("Error setting imported to true: " + e);
				}
				DOMSource source = new DOMSource(doc);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				StreamResult result = new StreamResult(baos);

				try {
					transformer.transform(source, result);
				} catch (TransformerException e) {
					LOG.error("Error setting imported to true: "
							+ baos.toString());
				}
				LOG.info("setted imported to true: " + " path: "
						+ doc.getDocumentURI());

			}

		}

	}


}
