package com.mohago.services;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import cloudcix.api.Auth;
import cloudcix.base.CloudCIXAuth;
import cloudcix.base.Response;

import com.mohago.user.Authentication;

public class RefreshTokenJob implements Job {

	private final static Logger LOG = Logger.getLogger(RefreshTokenJob.class);

	private CloudCIXAuth credentials = null;

	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		LOG.debug("Refreshing token.");
		refreshApiToken();

	}

	private void refreshApiToken() {
		Properties settings = Authentication.getAuthUtils().get_config();

		if (credentials == null) {

			credentials = new CloudCIXAuth(
					settings.getProperty("CLOUDCIX_API_USERNAME"),
					settings.getProperty("CLOUDCIX_API_PASSWORD"),
					settings.getProperty("CLOUDCIX_API_ID_MEMBER"), null);
		}
		Response response = null;
		try {
			response = Auth.getKeystone().create(credentials);
		} catch (IOException e) {
			LOG.error("Error getting token: " + e.getMessage());
		}

		if (response != null && response.code == 201) {
			Authentication.getAuthUtils().setApi_token(
					response.headers.get("X-Subject-Token").get(0));
		} else {
			try {
				Authentication.getAuthUtils().get_admin_token();
			} catch (SocketTimeoutException e) {
				LOG.error("Error getting token: ", e);
			}
		}
	}

}
