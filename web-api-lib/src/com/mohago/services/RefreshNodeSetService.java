package com.mohago.services;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

public class RefreshNodeSetService {
	private final static Logger LOG = Logger.getLogger(RefreshNodeSetService.class);
	private static final int INTERVAL = 1 * 60;//30 secs
	
private static Scheduler scheduler = null;
	

	public static Scheduler getScheduler() {
		return scheduler;
	}

	public static void setScheduler(Scheduler scheduler) {
		RefreshNodeSetService.scheduler = scheduler;
	}
	
	public RefreshNodeSetService(){
		try {
			setScheduler(new StdSchedulerFactory().getScheduler());
			scheduler.start();
		} catch (SchedulerException se) {
			LOG.error("Error with scheduler: " + se);
		}

		JobDetail job = JobBuilder.newJob(RefreshNodeSetJob.class)
				.withIdentity("refresh_node_set_job", "group1").build();

		Date now = new Date();
		Date nearestMinute = DateUtils.round(now, Calendar.MINUTE);
		Trigger refreshTokenTrigger = TriggerBuilder
				.newTrigger()
				.withIdentity("refresh_node_set_trigger", "group1")
				.startAt(nearestMinute)
				.withSchedule(
						SimpleScheduleBuilder.simpleSchedule()
								.withIntervalInSeconds(INTERVAL).repeatForever())
				.build();
		try {
			scheduler.scheduleJob(job, refreshTokenTrigger);
			
		} catch (SchedulerException e) {
			LOG.error("Scheduler exception: " + e);
		}
	}
}
