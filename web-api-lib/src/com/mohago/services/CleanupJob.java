package com.mohago.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.utils.DBConnectionManager;

import com.mohago.config.Configuration;
import com.mohago.config.DatabaseConnection;
import com.mohago.request.MasterTable;
import com.mohago.sql.SQLPreparedStatements;
import com.mohago.utilities.Constants;
import com.mohago.utilities.SQLHelpers;

import cloudcix.base.CloudCIXAuth;

public class CleanupJob implements Job {

	private final static Logger LOG = Logger.getLogger(CleanupJob.class);
	private final static int MAX_DAYS_OLD = 30;

	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		LOG.debug("Cleaning up");
		cleanup();
	}

	private void cleanup() {// Permanently delete Datasets that
							// are marked as
		// deleted and over a month old.

		for (String ip : MasterTable.getAllConnectionIPList()) {

			try (Connection conn = DatabaseConnection.getPooledConnection(ip)) {
				if (conn != null) {

					cleanupOldDatasets(MAX_DAYS_OLD, conn);

				}
			} catch (SQLException e) {
				LOG.error("e: " + e);
			}
		}

	}

	public static void cleanupOldDatasets(int daysOld, Connection conn) {
		if (deleteObfTablesRelatedToOldDatasets(daysOld, conn)) {
//			MasterTable
//					.deleteGlobalIdListFromMaster(getGlobalIdListForDeletionFromMaster(
//							daysOld, conn));
			removeOldDeletedEntriesFromDatasetTable(daysOld, conn);
		}
	}

	private static boolean deleteObfTablesRelatedToOldDatasets(int daysOld,
			Connection conn) {

		String tableList = "";
		String dropTableSql = "DROP TABLE IF EXISTS ";
		boolean ret = false;
		PreparedStatement preparedStatement = null;
		Statement statement = null;
		ResultSet reader = null;
		try {

			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Select.GET_TABLE_NAMES_FOR_DELETION
							.getValue());
			preparedStatement.setInt(1, daysOld);

			reader = preparedStatement.executeQuery();

			while (reader.next()) {

				tableList = reader.getString(1);
			}
			tableList = Configuration.Database.MOHAGO_DB_NAME.getValue() + "."
					+ tableList;// First entry on list needs prefix
			dropTableSql = dropTableSql + tableList;
			statement = conn.createStatement();
			statement.execute(dropTableSql);
			conn.commit();
			ret = true;
		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				LOG.error("Error cleaning up" + e);
			}

		}
		return ret;
	}



	private static int removeOldDeletedEntriesFromDatasetTable(int daysOld,
			Connection conn) {

		int ret = -1;
		PreparedStatement preparedStatement = null;
		ResultSet reader = null;
		try {

			preparedStatement = conn
					.prepareStatement(SQLPreparedStatements.Delete.DELETE_OLD_DATASETS
							.getValue());
			preparedStatement.setInt(1, daysOld);
			ret = preparedStatement.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOG.error(Constants.ErrorMessage.SQL.getValue() + e);
		} finally {
			SQLHelpers.closeResources(preparedStatement, reader);
		}
		return ret;
	}

}
