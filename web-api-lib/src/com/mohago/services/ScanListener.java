package com.mohago.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.JobListener;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.matchers.KeyMatcher;
import org.quartz.utils.DBConnectionManager;

import com.mohago.config.DatabaseConnection;
import com.mohago.node.NodeManager;
import com.mohago.utilities.SQLHelpers;
import com.mysql.jdbc.StringUtils;

@DisallowConcurrentExecution
public class ScanListener implements JobListener {
	private final static Logger LOG = Logger.getLogger(ScanListener.class);

	private static final String LISTENER_NAME = "ScanListener";

	@Override
	public String getName() {
		return LISTENER_NAME; // must return a name
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void jobToBeExecuted(JobExecutionContext context) {
		String jobName = context.getJobDetail().getKey().toString();
		LOG.info("Job about to be executed: " + jobName);

	}

	@Override
	public void jobWasExecuted(JobExecutionContext context,
			JobExecutionException arg1) {

		// /TODO: handle import
		// success/fail (will fail if
		// hashes don't match)
		String jobName = context.getJobDetail().getKey().toString();
		boolean success = (boolean) context.getJobDetail().getJobDataMap()
				.get("success");
		String fileName = (String) context.getJobDetail().getJobDataMap()
				.get("file_name");
		String deviceToken = (String) context.getJobDetail().getJobDataMap()
		.get("device_token");
		final String deleteFileName = fileName.replace(File.separator + "lgdf",
				File.separator + "lgdf" + File.separator + "delete");
		String dataFileName = fileName.replace(".lgmf", ".lgdf");
		final String deleteDataFileName = deleteFileName.replace(".lgmf",
				".lgdf");
		
		if (success) {// Delete the files if successful, no longer needed
			LOG.info("Job executed: " + jobName);

			ScanService.getFilesSuccessfullyImported().add(fileName);

			if (!StringUtils.isNullOrEmpty(fileName) && !StringUtils.isNullOrEmpty(deviceToken)) {
				String ip = NodeManager.getNodeAddressByDeviceToken(deviceToken);
				try (Connection conn = DatabaseConnection.getPooledConnection(ip)) {
					SQLHelpers.setAutoCommit(conn, false);
					ScanService.setImportedToTrue(deleteFileName);
					ScanService.addToFileDeleteTable(deleteFileName, conn);
					conn.commit();
				} catch (SQLException e) {
					LOG.error("Error from cleanup: " + e);
				}
				JobKey jobKey = new JobKey("delete_job_" + deleteFileName,
						"group3");
				JobDetail jobDetail = JobBuilder.newJob(DeleteJob.class)
						.withIdentity(jobKey).build();
				jobDetail.getJobDataMap().put("ip_address", ip);
				// Trigger the job to run now
				Trigger deleteTrigger = TriggerBuilder
						.newTrigger()
						.withIdentity("delete_trigger_" + deleteFileName,
								"group3")
						.startNow()
						.withSchedule(
								SimpleScheduleBuilder.simpleSchedule()
										.withIntervalInSeconds(0)).build();

				try {
					ScanService.getScheduler().scheduleJob(jobDetail,
							deleteTrigger);
				} catch (SchedulerException e) {
					LOG.error("Error with quartz scheduler: " + e);
				}
			} else {
				LOG.error("Empty file name.");
			}

		} else {// Import failed, do cleanup operation, i.e. move both lgf
				// files to error folder
			// handle fail situation, move files to error folder instead of
			// deleting
			try {// Move data file into error folder from main folder
				Files.move(
						Paths.get(dataFileName),
						Paths.get(dataFileName.replace(File.separator + "lgdf"
								+ File.separator, File.separator + "lgdf"
								+ File.separator + "error" + File.separator)),
						StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				LOG.error("Error moving mdata files after parse failure: " + e);
			}
			try {// Move meta file into error folder from main folder
				Files.move(
						Paths.get(fileName),
						Paths.get(fileName.replace(File.separator + "lgdf"
								+ File.separator, File.separator + "lgdf"
								+ File.separator + "error" + File.separator)),
						StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				LOG.error("Error moving mdata files after parse failure: " + e);
			}
			try {// Move data file into error folder from delete folder if
					// it is in there
				Files.move(
						Paths.get(deleteDataFileName),
						Paths.get(deleteDataFileName.replace(File.separator
								+ "lgdf" + File.separator + "delete"
								+ File.separator, File.separator + "lgdf"
								+ File.separator + "error" + File.separator)),
						StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				LOG.error("Error moving mdata files after parse failure: " + e);
			}
			try {// Move meta file into error folder from delete folder if
					// it is in there
				Files.move(
						Paths.get(deleteFileName),
						Paths.get(deleteFileName.replace(File.separator
								+ "lgdf" + File.separator + "delete"
								+ File.separator, File.separator + "lgdf"
								+ File.separator + "error" + File.separator)),
						StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				LOG.error("Error moving mdata files after parse failure: " + e);
			}
		}
		ScanService.getFilesToProcess().remove(fileName);

		// /Now Kick off processing of next file
		if (!ScanService.getFilesToProcess().isEmpty()
				&& ScanService.isActive()) {
			ScanService.currentFile = (String) ScanService.getFilesToProcess()
					.pop();

			JobKey jobKey = new JobKey("scan_job_" + ScanService.currentFile,
					"group2");
			JobDetail jobDetail = JobBuilder.newJob(ScanJob.class)
					.withIdentity(jobKey).build();
			jobDetail.getJobDataMap().put("lgmfFile", ScanService.currentFile);
			// Trigger the job to run now
			Trigger scanTrigger = TriggerBuilder
					.newTrigger()
					.withIdentity("scan_trigger_" + ScanService.currentFile,
							"group2")
					.startNow()
					.withSchedule(
							SimpleScheduleBuilder.simpleSchedule()
									.withIntervalInSeconds(0)).build();

			try {
				ScanService
						.getScheduler()
						.getListenerManager()
						.addJobListener(new ScanListener(),
								KeyMatcher.keyEquals(jobKey));
				ScanService.getScheduler().scheduleJob(jobDetail, scanTrigger);
			} catch (SchedulerException e) {
				LOG.error("Error with quartz scheduler: " + e);
			}
			ScanService.setIdle(false);

		} else {// Files to process is empty so set scan service to idle
			ScanService.setIdle(true);
		}

	}

}
