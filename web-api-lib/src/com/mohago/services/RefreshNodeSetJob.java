package com.mohago.services;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.mohago.node.NodeManager;
import com.mohago.request.MasterTable;

public class RefreshNodeSetJob implements Job {
	private final static Logger LOG = Logger.getLogger(RefreshNodeSetJob.class);


	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		LOG.debug("Refreshing node set");
		NodeManager.refresh(MasterTable.getNodeSet());

	}
	
}
