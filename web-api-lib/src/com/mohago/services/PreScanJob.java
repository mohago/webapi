package com.mohago.services;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.matchers.KeyMatcher;

import com.mohago.serialization.Parameter;

@DisallowConcurrentExecution
public class PreScanJob implements Job {

	private final static Logger LOG = Logger.getLogger(PreScanJob.class);
	
	@Override
	public void execute(JobExecutionContext arg0)
			throws JobExecutionException {
		//LOG.info("hello");
		//LOG.info("Timer tick!!");

		// boolean alive = scanWorker.
		if (ScanService.isIdle() /*&& ScanService.filesToProcess.isEmpty()*/ && ScanService.isActive()) {
			File dir = new File(ScanService.LGDF_FOLDER);
			File[] directoryListing = dir.listFiles();
			if (directoryListing != null) {
				for (File child : directoryListing) {

					if (child.getPath().contains(".lgmf")
							&& !ScanService.getFilesSuccessfullyImported().contains(child.getPath()) && !ScanService.getFilesToProcess().contains(child.getPath())) {
						ScanService.getFilesToProcess().add(child.getPath());
					}
				}
			} else {
				// Handle the case where dir is not really a directory.
				// Checking dir.isDirectory() above would not be sufficient
				// to avoid race conditions with another process that deletes
				// directories.
			}

			// Then perform processing.
			if (!ScanService.getFilesToProcess().isEmpty()) {
				
				//List<String> filesToProcessList = new ArrayList<String>(ScanService.filesToProcess);
				//
				//Do a sort
				Collections.sort(ScanService.getFilesToProcess(), 
						new Comparator<String>() {
					@Override
					public int compare(String param1,
							String param2) {

						return String.CASE_INSENSITIVE_ORDER.compare(
								param1,
								param2);
					}
				});
				//
				//ScanService.filesToProcess = new HashSet<String>(filesToProcessList);
				
				
				
				final String logMessage = "API import started for:";
				LOG.info(logMessage);
				ScanService.currentFile = (String) ScanService.getFilesToProcess().pop();
				ScanService.setIdle(false);
				JobKey jobKey = new JobKey("scan_job_" + ScanService.currentFile, "group2");
				JobDetail jobDetail = JobBuilder.newJob(ScanJob.class)
						.withIdentity(jobKey).build();
				jobDetail.getJobDataMap().put("lgmfFile", ScanService.currentFile);
				// Trigger the job to run now
				Trigger scanTrigger = TriggerBuilder
						.newTrigger()
						.withIdentity("scan_trigger" + ScanService.currentFile, "group2")//possibly name this with file?
						.startNow()
						.withSchedule(
								SimpleScheduleBuilder.simpleSchedule()
										.withIntervalInSeconds(0)).build();
 
				try {
					ScanService.getScheduler().getListenerManager()
							.addJobListener(new ScanListener(),
									KeyMatcher.keyEquals(jobKey));
					ScanService.getScheduler().scheduleJob(jobDetail, scanTrigger);
				} catch (SchedulerException e) {
					LOG.error("Error with quartz scheduler: " + e);
				}
			}
		}
	}
}