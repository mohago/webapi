package com.mohago.services;

import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;

import com.mohago.lgf.DataSetImportManager;
import com.mysql.jdbc.StringUtils;

@DisallowConcurrentExecution
public class ScanJob implements Job {
	private final static Logger LOG = Logger.getLogger(ScanJob.class);
	private String lgmfFile = "";

	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		JobDataMap map = context.getJobDetail().getJobDataMap();
		lgmfFile = map.getString("lgmfFile");
		boolean success;
		//Long dId = -1l;
		String deviceToken = "";
		try {
			success = false;
			DataSetImportManager dsi = new DataSetImportManager();
			deviceToken = dsi.importStoreLgmfLgdfPair(lgmfFile);
			
		} finally {
			ScanService.setIdle(true);
		}

		success = (!StringUtils.isNullOrEmpty(deviceToken)) ? true : false;
		map.put("success", success);
		map.put("file_name", lgmfFile);
		map.put("device_token", deviceToken);
		
		

		//Debug stuff below
		try {
			LOG.info("Trigger names : \n"
					+ ScanService.getScheduler().getTriggerGroupNames());
			LOG.info("Job names : \n"
					+ ScanService.getScheduler().getJobGroupNames());
		} catch (SchedulerException e) {

			// No log reqd
		}
		//End debug stuff

	}

}
