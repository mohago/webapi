package com.mohago.services;

import org.apache.log4j.Logger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

public class CleanupService {
	private final static Logger LOG = Logger.getLogger(CleanupService.class);
	private static final int INTERVAL = 1 * 60 * 60 * 24;//One day
	
private static Scheduler scheduler = null;
	

	public static Scheduler getScheduler() {
		return scheduler;
	}

	public static void setScheduler(Scheduler scheduler) {
		CleanupService.scheduler = scheduler;
	}
	
	public CleanupService(){
		try {
			setScheduler(new StdSchedulerFactory().getScheduler());
			scheduler.start();
		} catch (SchedulerException se) {
			LOG.error("Error with scheduler: " + se);
		}

		JobDetail job = JobBuilder.newJob(CleanupJob.class)
				.withIdentity("cleanup_job", "group1").build();

		Trigger refreshTokenTrigger = TriggerBuilder
				.newTrigger()
				.withIdentity("cleanup_trigger", "group1")
				.startNow()
				.withSchedule(
						SimpleScheduleBuilder.simpleSchedule()
								.withIntervalInSeconds(INTERVAL).repeatForever())
				.build();
		try {
			scheduler.scheduleJob(job, refreshTokenTrigger);
			
		} catch (SchedulerException e) {
			LOG.error("Scheduler exception: " + e);
		}
	}

}
