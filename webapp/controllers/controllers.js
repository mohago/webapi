(function () {
	'use strict';
	var app = angular.module('YourApp', ['ngMaterial']);
	
	app.config(function ($mdThemingProvider) {
		$mdThemingProvider.theme('blue').primaryPalette('blue').accentPalette('red');
		
	});
	
	app.controller('YourController', ['$scope', '$mdDialog', '$mdSidenav', '$rootScope', function ($scope, $mdDialog, $mdSidenav, $rootScope) {
		$rootScope.v1="mvar";
		$scope.myAuth={};
		$scope.participant={};
		$scope.participant.username = "declan.gordon@mohago.com";
		$scope.participant.password = "MohagoCix121!";
		
				$scope.openDialog = function ($event) {
					$mdDialog.show({
						targetEvent : $event,
						template : '<md-dialog>' + '  <md-content>Hello {{ userName }}!</md-content>' + '  <div class="md-actions">' + '    <md-button ng-click="login()">' + '      Login' + '    </md-button>' + '  </div>' + '</md-dialog>',
						controller : 'DialogController',
						onComplete : afterShowAnimation,
						clickOutsideToClose : false,
						escapeToClose : false,
						locals : {
							name : 'Bobby'
						}
					});
					function afterShowAnimation(scope, element, options) {}

				};
				$scope.openLogin = function ($event) {
					$mdDialog.show({
						targetEvent : $event,
						template : ' <md-dialog><form> \
												        <md-content> \
												            <md-input-container> \
												     <label>User name</label> \
												                <input ng-model="participant.username" value="declan.gordon@mohago.com"></input> \
												            </md-input-container> \
												   <md-input-container> \
												     <label>Password</label> \
												                <input type="password" ng-model="participant.password" value="MohagoCix121!"></input> \
												            </md-input-container> \
												        </md-content> \
												        <div class="md-actions"> \
												            <md-button class="md-primary" ng-click="login(answer)">OK</md-button> \
												        </div> \
												      </form></md-dialog>',
						controller : 'LoginController',
						onComplete : afterShowAnimation,
						clickOutsideToClose : false,
						escapeToClose : false,
						locals : {
							name : 'Bobby'
						}
					});
					function afterShowAnimation(scope, element, options) {}

				};
				$scope.showConfirm = function (ev) {
					var confirm = $mdDialog.confirm().title('Login').content('All of the banks have agreed to forgive you your debts.').ariaLabel('Lucky day').clickOutsideToClose(false).escapeToClose(false).targetEvent(ev);
					$mdDialog.show(confirm).then(function () {
						$scope.alert = 'You decided to get rid of your debt.';
						$scope.showConfirm(null);
					}, function () {
						$scope.alert = 'You decided to keep your debt.';
					});
				};
				$scope.toggleMenu = function () {
					$mdSidenav('left').toggle();
				};
				$scope.chunks = [{
						title : "Today",
						divider : true
					}, {
						title : "LG Dataset 3",
						description : "Process data aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
						expanded : false,
						content : {
							type : "ITEMS",
							quotes : ["A,B,C", "A"]
						}
					}, {
						title : "Yesterday",
						divider : true
					}, {
						title : "LG Dataset 2",
						description : "Process data",
						expanded : false,
						content : {
							type : "QUOTE",
							quotes : ["A,B,C"]
						}
					}, {
						title : "LG Dataset 1",
						description : "Process data",
						expanded : false,
						content : {
							type : "QUOTE",
							quotes : ["A,B,C"]
						}
					}
				];
				$scope.items = [{
						name : 'Share',
						icon : 'share'
					}, {
						name : 'Upload',
						icon : 'upload'
					}, {
						name : 'Copy',
						icon : 'copy'
					}, {
						name : 'Print this page',
						icon : 'print'
					}, ];
				$scope.listItemClick = function ($index) {
					var clickedItem = $scope.items[$index];
				};
			}
		]);
		
	app.controller('DialogController', function ($scope, $mdDialog, name) {
		$scope.userName = name;
		$scope.login = function () {
			$mdDialog.hide();
		};
	});
	
	app.controller('LoginController', function ($scope, $mdDialog, $mdToast, $http, $rootScope) {
		$scope.login = function () {
			if ($scope.participant != null) {
				if (($scope.participant.username != null || $scope.participant.username != '') && ($scope.participant.password != null || $scope.participant.password != '')) {
					
					$rootScope.v2="a";
					
					var auth = false;
						var authToken="a";
						var req = {
						 method: 'POST',
						 url: 'http://declaptop:8080/web-api/authenticate',
						 headers: {
						   'user_name':  $scope.participant.username.toString(),
						   'password':  $scope.participant.password.toString()
						 },
						 data: { test: 'test' },
						}
						$http(req).success(function(data, status, headers, config) {
						auth=true;
						$scope.myAuth=headers("X-Subject-Token");
						$mdToast.show($mdToast.simple().content("Successful login"));
						$mdDialog.hide();
					  }).
					  error(function(data, status, headers, config) {
					   $mdToast.show($mdToast.simple().content("Login service error"));
					  });
				}
			} else {
				$mdToast.show($mdToast.simple().content("Please enter your login details"));
			}
		};
		$scope.hide = function () {
			$mdDialog.hide($scope.participant);
		};
		$scope.cancel = function () {
			$mdDialog.cancel();
		};
	});
	app.controller('LeftCtrl', function ($scope, $timeout, $mdSidenav) {
		$scope.close = function () {
			$mdSidenav('left').close();
		};
	});
})();