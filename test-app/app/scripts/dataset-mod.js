'use strict';

/**
 * @ngdoc overview
 * @name datasetApp
 * @description
 * # datasetApp
 *
 * Main module of the application.
 */
angular
  .module('datasetApp', [
    'testAppApp'
  ])
  .config(function ($mdThemingProvider) {
     $mdThemingProvider.theme('blue').primaryPalette('blue').accentPalette('red');
	 
  });
