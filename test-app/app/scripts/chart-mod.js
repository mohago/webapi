'use strict';

/**
 * @ngdoc overview
 * @name chartApp
 * @description
 * # chartApp
 *
 * Chart module of the application.
 */
angular
  .module('chartApp', [
    'testAppApp'
  ]).config(function ($mdThemingProvider) {
     $mdThemingProvider.theme('blue').primaryPalette('blue').accentPalette('red');
  });
