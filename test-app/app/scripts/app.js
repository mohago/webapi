'use strict';

/**
 * @ngdoc overview
 * @name testAppApp
 * @description
 * # testAppApp
 *
 * Main module of the application.
 */
angular
  .module('testAppApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
	'ngMaterial',
	'ngDragDrop',
	'ngToast',
	'ui.bootstrap',
	'loginApp',
	'datasetApp',
	'chartApp'
  ]).service('datasetService', function() {
	this.selectedGlobalId = -1;
	this.globalIdList = [];
	 this.setGlobalIdList = function(data) {
	   this.globalIdList = data;
	 };
	 this.getGlobalIdList = function() {
	  return this.globalIdList;
	 };
	 this.setSelectedGlobalId = function(globalId) {
	   this.selectedGlobalId = globalId;
	 };
	 this.getSelectedGlobalId = function() {
	  return this.selectedGlobalId;
	 };
}).constant('CONSTANTS', {
    APP_NAME: 'Mohago Web Application',
    APP_VERSION: 1.0,
    API_URL: 'https://api.mohago.com/v1/'
}).config(['ngToastProvider', function(ngToast) {
    ngToast.configure({
      verticalPosition: 'bottom',
      horizontalPosition: 'center',
      maxNumber: 1
    });
  }]).config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
	  .when('/dataset', {
        templateUrl: 'views/dataset.html',
        controller: 'DatasetCtrl'
      })
	  .when('/chart', {
        templateUrl: 'views/chart.html',
        controller: 'ChartCtrl'
      })
	  .when('/settings', {
        templateUrl: 'views/settings.html',
        controller: 'SettingsCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }).run( function($rootScope, $location) {

    // register listener to watch route changes
    $rootScope.$on( '$locationChangeStart', function(event, next, current) {
      if (!$rootScope.myAuth) {//is auth token there?
        // no logged user, we should be going to #login
        if ( next.templateUrl === 'views/login.html') {
          // already going to #login, no redirect needed
        } else {
          // not going to #login, we should redirect now
          $location.path( '/' );
        }
      }         
    });
 });
