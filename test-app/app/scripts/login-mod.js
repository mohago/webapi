'use strict';

/**
 * @ngdoc overview
 * @name loginApp
 * @description
 * # loginApp
 *
 * Main module of the application.
 */
angular
  .module('loginApp', [
    'testAppApp'
  ])
  .config(function ($mdThemingProvider) {
     $mdThemingProvider.theme('blue').primaryPalette('blue').accentPalette('red');
	 
  });
