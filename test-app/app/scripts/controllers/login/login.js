angular
  .module('loginApp').controller('LoginCtrl', function ($http, $rootScope, $location, $mdToast, CONSTANTS) {
		this.login = function login() {
				if ((this.username) && (this.password)) {
					
					$rootScope.v2="";
					
						var req = {
						 method: 'POST',
						 url: CONSTANTS.API_URL.concat('authenticate'),
						 headers: {
						   'user_name':  this.username,
						   'password':  this.password
						 },
						 //data: { test: 'test' },
						}
						$http(req).success(function(data, status, headers, config) {
							$rootScope.myAuth=headers("X-Subject-Token");
							if($rootScope.myAuth){
								$location.path( '/dataset' );
							}else{
								$mdToast.show($mdToast.simple().content("Please try again"));
							}
					  }).
					  error(function(data, status, headers, config) {
					   $mdToast.show($mdToast.simple().content("Login service error"));
					  });
				}
		};
		this.hide = function () {
			$mdDialog.hide($scope.participant);
		};
		this.cancel = function () {
			$mdDialog.cancel();
		};
	});