angular
  .module('loginApp').controller('DialogController', function ($scope, $mdDialog, name) {
		$scope.userName = name;
		$scope.login = function () {
			$mdDialog.hide();
		};
	});