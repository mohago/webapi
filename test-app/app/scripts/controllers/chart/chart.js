'use strict';

/**
 * @ngdoc function
 * @name chartApp.controller:ChartCtrl
 * @description
 * # ChartCtrl
 * Controller of the chartApp
 */
angular.module('chartApp')
.controller('ChartCtrl', function ($rootScope, $scope, $compile, $http, $location, $modal, ngToast, datasetService, $q, CONSTANTS) {

	$scope.xParamList = [];
	$scope.yParamList = [];
	var selectedGlobalId = datasetService.getSelectedGlobalId();
	var req = {
		method : 'GET',
		url : CONSTANTS.API_URL.concat('datasets/').concat(selectedGlobalId).concat('/parameters'),
		headers : {
			'X-Auth-Token' : $rootScope.myAuth
		}
	}
	$http(req).success(function (result) {
		var paramList = [];
		var dataset = result[0]; //hardcode 0 for now
		for (var i = 0; i < dataset.paramInts.length; i++) {
			var name = dataset.paramInts[i];
			paramList.push([name, 0, false]);
		}
		for (var i = 0; i < dataset.paramDoubles.length; i++) {
			var name = dataset.paramDoubles[i];
			paramList.push([name, 1, false]);
		}
		for (var i = 0; i < dataset.paramStrings.length; i++) {
			var name = dataset.paramStrings[i];
			paramList.push([name, 2, false]);
		}
		for (var i = 0; i < dataset.paramDates.length; i++) {
			var name = dataset.paramDates[i];
			paramList.push([name, 3, false]);
		}
		$scope.parameterList = paramList;
	}).error(function () {
		$rootScope.myAuth = "";
		$location.path('/login');
	});

	$scope.chartClick = function (ev, attrs) {
		var aToast = ngToast.create({
				className : 'warning',
				content : 'create a base chart'
			});
		return;

	}

	$scope.lineChartClick = function (ev, attrs) { //$on('insertItem',function(ev,attrs){
		var cont = angular.element(document.createElement('line-control'));
		var el = $compile(cont)($scope);

		//where do you want to place the new element?
		angular.element($('#chartControlTarget')).replaceWith(cont);

		//$scope.insertHere = el;
	}

	$scope.scatterChartClick = function (ev, attrs) { //$on('insertItem',function(ev,attrs){
		var cont = angular.element(document.createElement('scatter-control'));
		var el = $compile(cont)($scope);

		//where do you want to place the new element?
		angular.element($('#chartControlTarget')).replaceWith(cont);

		//$scope.insertHere = el;
	}

	$scope.myChartClick = function myChartClick() {

		if (!($scope.xParamList[0][0]) && ($scope.yParamList[0][0])) { //If both x and y aren't there
			var aToast = ngToast.create({
					className : 'warning',
					content : 'Please add parameters'
				});
			return;
		}

		var chartData = {
			'chartType' : 'xy'
		};
		chartData.globalIdSet = [];
		chartData.globalIdSet.push(selectedGlobalId);
		chartData.limit = 500;
		chartData.xname = $scope.xParamList[0][0];
		chartData.xtype = $scope.xParamList[0][1];
		chartData.yname = $scope.yParamList[0][0];
		chartData.ytype = $scope.yParamList[0][1];
		var req2 = {
			method : 'POST',
			url : CONSTANTS.API_URL.concat('chart'),
			data : chartData,
			headers : {
				'X-Auth-Token' : $rootScope.myAuth
			}
		}
		$http(req2).success(function (result) {

			var chartData = result;
			$scope.data = chartData.points;
			$scope.renderer = 'scatterplot';

			var data_t = [{
					x : 1137537600000,
					y : 40
				}, {
					x : 1237537900000,
					y : 49
				}, {
					x : 1337547600000,
					y : 17
				}, {
					x : 1437637600000,
					y : 42
				}
			];
			var mapped = {};
			if ($scope.xParamList[0][1] == 3) {
				mapped = data_t.map(function (pt) { //test
						return {
							x : pt.x/1000,
							y : pt.y
						};
					});
			} else {
				mapped = chartData.points.map(function (pt) {
						return {
							x : pt.x,
							y : pt.y
						};
					});
			}

			var graph = new Rickshaw.Graph({
					element : document.querySelector("#chart"),
					width : 580,
					height : 250,
					renderer : 'scatterplot',
					series : [{
							color : 'steelblue',
							data : mapped
						}
					]
				});

			var axesX = new Rickshaw.Graph.Axis.Y({
					graph : graph
				});
			var axesY = new Rickshaw.Graph.Axis.Time({
					graph : graph
				});
			// var yaxis = new rickshaw.graph.axis.y({
			// graph: graph
			// });

			graph.render();

		}).error(function () {
			$rootScope.myAuth = "";
			$location.path('/login');
		});
	};

	$scope.hideMeX = function () {
		return $scope.xParamList.length > 0;
	}
	$scope.hideMeY = function () {
		return $scope.yParamList.length > 0;
	}
	$scope.beforeDropX = function (event, ui) {
		//var result = false;
		var param = $scope.dragParam;
		var deferred = $q.defer();
		if (param[1] == 2) {
			var aToast = ngToast.create({
					className : 'warning',
					content : 'String type parameters can not be used'
				});
			deferred.reject();
			console.log("I'm a string!");
		} else {
			deferred.resolve();
			$scope.xParamList.length = 0;
		}

		return deferred.promise;
	}

	$scope.beforeDropY = function (event, ui) {
		//var result = false;
		var param = $scope.dragParam;
		var deferred = $q.defer();
		if (param[1] == 2) {
			var aToast = ngToast.create({
					className : 'warning',
					content : 'String type parameters can not be used'
				});
			deferred.reject();
			console.log("I'm a string!");
		} else {
			deferred.resolve();
			$scope.yParamList.length = 0;
		}

		return deferred.promise;
	}

	$scope.startDrag = function (event, ui, param) {
		console.log('You started draggin: ' + param[0]);
		$scope.dragParam = param;
	};
}).controller('ModalInstanceCtrl', function ($scope, $modalInstance) {
	$scope.ok = function () {
		$modalInstance.close();
	};
	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};

});
