'use strict';

/**
 * @ngdoc function
 * @name chartApp.controller:LineChartCtrl
 * @description
 * # lineChartCtrl
 * Controller of the chartApp
 */

angular.module('chartApp').controller("LineChartCtrl", function ($rootScope, $scope, $compile, $http, $location, $modal, ngToast, datasetService, $q, CONSTANTS) {

	$scope.chartClick = function (ev, attrs) {
		var aToast = ngToast.create({
				className : 'warning',
				content : 'create a line chart'
			});
		return;
	}

	$scope.beforeDropY = function (event, ui) {
		//var result = false;
		var param = $scope.dragParam;
		var deferred = $q.defer();
		if (param[1] == 2) {
			var aToast = ngToast.create({
					className : 'warning',
					content : 'String type parameters can not be used'
				});
			deferred.reject();
			console.log("I'm a string!");
		} else {
			deferred.resolve();
			$scope.yParamList.length = 0;
		}

		return deferred.promise;
	}

	$scope.hideMeY = function () {
		return $scope.yParamList.length > 0;
	}
});
