'use strict';

/**
 * @ngdoc function
 * @name chartApp.controller:ScatterChartCtrl
 * @description
 * # catterChartCtrl
 * 
 */

angular.module('chartApp').controller("ScatterChartCtrl", function ($rootScope, $scope, $compile, $http, $location, $modal, ngToast, datasetService, $q, CONSTANTS) {

	$scope.chartClick = function (ev, attrs) {
		var aToast = ngToast.create({
				className : 'warning',
				content : 'create a scatter chart'
			});
		return;
	}

	$scope.beforeDropY = function (event, ui) {
		//var result = false;
		var param = $scope.dragParam;
		var deferred = $q.defer();
		if (param[1] == 2) {
			var aToast = ngToast.create({
					className : 'warning',
					content : 'String type parameters can not be used'
				});
			deferred.reject();
			console.log("I'm a string!");
		} else {
			deferred.resolve();
			$scope.yParamList.length = 0;
		}

		return deferred.promise;
	}

	$scope.hideMeY = function () {
		return $scope.yParamList.length > 0;
	}
});
