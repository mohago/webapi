'use strict';

/**
 * @ngdoc function
 * @name datasetApp.controller:DatasetCtrl
 * @description
 * # DatasetCtrl
 * Controller of the datasetApp
 */
angular.module('datasetApp')
  .controller('DatasetCtrl', function ($rootScope, $http, $scope, $location, datasetService, CONSTANTS) {
		
	var req =  {
					method: 'GET',
					url: CONSTANTS.API_URL.concat('datasets'),
					headers: {
					  'X-Auth-Token':  $rootScope.myAuth
					},
				}
			$http(req).success(function(result){
        $scope.datasetList = result;
    }).error(function(){
		$rootScope.myAuth = "";
        $location.path( '/login' );
    });
	
	$scope.myClick = function myClick(dataset){
		datasetService.setSelectedGlobalId(dataset.global_id);
		$location.path( '/chart' );
	}

  });
