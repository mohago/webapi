'use strict';

/**
 * @ngdoc directive
 * @name chartApp.directive:lineChartControl
 * @description
 * # lineChartControl
 */

angular.module('chartApp').directive('lineControl', function () {
	return {
		//template: '<div ng-controller="LineChartCtrl"><button ng-click="chartClick()">A</button></div>',
		templateUrl : 'scripts/directives/templates/lineControlTemplate.html',
		restrict : 'E',
		link : function postLink(scope, element, attrs) {
			//element.text('');
		}
	};
});
