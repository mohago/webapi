'use strict';

/**
 * @ngdoc directive
 * @name chartApp.directive:acatterChartControl
 * @description
 * # scatterChartControl
 */

angular.module('chartApp').directive('scatterControl', function () {
	return {
		//template: '<div ng-controller="LineChartCtrl"><button ng-click="chartClick()">A</button></div>',
		templateUrl : 'scripts/directives/templates/scatterControlTemplate.html',
		restrict : 'E',
		link : function postLink(scope, element, attrs) {
			//element.text('');
		}
	};
});