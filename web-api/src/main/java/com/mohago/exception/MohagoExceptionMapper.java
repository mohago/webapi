package com.mohago.exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class MohagoExceptionMapper implements ExceptionMapper<MohagoException> {

	@Override
	public Response toResponse(MohagoException exception) {
		
		

		 ResponseBuilder responseBuilder = Response.status(exception.getCode()).entity(exception.getJson())
					.type(MediaType.APPLICATION_JSON);
	        responseBuilder.header("Access-Control-Allow-Origin", "*");
	        responseBuilder.header("Access-Control-Expose-Headers", "Location,Content-Type,Accept,Origin,X-Auth-Token,X-Subject-Token,Status");
	        responseBuilder.header("Access-Control-Allow-Headers", "Content-Type,X-Requested-With,Accept,Origin,Access-Control-Request-Method,Access-Control-Request-Headers,Access-Control-Allow-Origin,user_name,member_id,X-Auth-Token,X-Subject-Token,domain_id,password,Status");
	        responseBuilder.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
	        responseBuilder.header("Access-Control-Max-Age", "1209600");
	        return responseBuilder.build();
	}

}
