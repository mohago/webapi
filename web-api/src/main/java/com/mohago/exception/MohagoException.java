package com.mohago.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

public class MohagoException extends WebApplicationException {
	private int code;
	private String json;

	public MohagoException(int code, String msg) {
		super(Response.status(code).entity(getJson(code, msg))
				.type(MediaType.APPLICATION_JSON).build());
		this.setCode(code);
		this.setJson(getJson(code, msg));
	}

	private static String getJson(int code, String msg) {
		Gson gson = new Gson();
		return gson.toJson(new com.mohago.json.Error(msg, "", code));
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}
}
