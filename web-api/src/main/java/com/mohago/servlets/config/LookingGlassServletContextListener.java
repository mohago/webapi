package com.mohago.servlets.config;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.quartz.SchedulerException;

import com.mohago.services.ScanService;

public class LookingGlassServletContextListener implements ServletContextListener{
	
	private final static Logger LOG = Logger.getLogger(LookingGlassServletContextListener.class);
	 
		@Override
		public void contextDestroyed(ServletContextEvent arg0) {
			LOG.info("Shutting down..");	
			try {
				ScanService.getScheduler().shutdown();
			} catch (SchedulerException e) {
				LOG.error("Error shutting down scheduler on app shutdown: " + e);
			}
			LOG.info("Shutting down...");	
		}
	 
	        //Run this before web application is started
		@Override
		public void contextInitialized(ServletContextEvent arg0) {
			LOG.info("Starting up...");	
		}

}
