package com.mohago.servlets.config;

import java.lang.annotation.*;

import javax.ws.rs.HttpMethod;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@HttpMethod("PATCH")
public @interface Patch {

}
