package com.mohago.servlets.config;



import javax.servlet.http.HttpServlet;

import com.wordnik.swagger.config.FilterFactory;

import java.util.List;
import java.util.ArrayList;

public class Bootstrap extends HttpServlet {
  static {
    // do any additional initialization here, such as set your base path programmatically as such:
    // ConfigFactory.config().setBasePath("http://www.foo.com/");

    // add a custom filter
    FilterFactory.setFilter(new CustomFilter());

    
  }
}
