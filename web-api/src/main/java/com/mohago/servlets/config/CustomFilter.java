/**
 *  Copyright 2013 Wordnik, Inc.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.mohago.servlets.config;

import java.util.List;
import java.util.Map;

import com.wordnik.swagger.core.filter.SwaggerSpecFilter;
import com.wordnik.swagger.model.*;
import com.wordnik.swagger.models.Model;
import com.wordnik.swagger.models.Operation;
import com.wordnik.swagger.models.parameters.Parameter;
import com.wordnik.swagger.models.properties.Property;

public class CustomFilter implements SwaggerSpecFilter {
	@Override
	public boolean isOperationAllowed(Operation operation, ApiDescription api,
			java.util.Map<String, java.util.List<String>> params,
			java.util.Map<String, String> cookies,
			java.util.Map<String, java.util.List<String>> headers) {
		return true;
	}

	@Override
	public boolean isParamAllowed(Parameter parameter, Operation operation,
			ApiDescription api,
			java.util.Map<String, java.util.List<String>> params,
			java.util.Map<String, String> cookies,
			java.util.Map<String, java.util.List<String>> headers) {
		return true;
	}

	@Override
	public boolean isPropertyAllowed(Model arg0, Property arg1, String arg2,
			Map<String, List<String>> arg3, Map<String, String> arg4,
			Map<String, List<String>> arg5) {
		// TODO Auto-generated method stub
		return false;
	}
}