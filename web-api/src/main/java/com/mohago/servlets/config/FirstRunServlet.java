package com.mohago.servlets.config;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import cloudcix.api.Auth;
import cloudcix.base.Utilities;

import com.mohago.config.Configuration;
import com.mohago.config.Configuration.Database;
import com.mohago.config.DatabaseConnection;
import com.mohago.request.MasterTable;
import com.mohago.services.CleanupService;
import com.mohago.services.RefreshNodeSetService;
import com.mohago.services.RefreshTokenService;
import com.mohago.services.ScanService;
import com.mohago.user.Authentication;
import com.mohago.utilities.SQLHelpers;

/**
 * Servlet implementation class FirstRunServlet This servlet kicks off api scan
 * thread and builds tables.
 */
public class FirstRunServlet extends HttpServlet {
	private static final long serialVersionUID = 2190074417587450650L;

	private final static Logger LOG = Logger.getLogger(FirstRunServlet.class);
	private static final String SAVE_DIR = "lgdf";
	private static final String DELETE_DIR = "lgdf" + File.separator + "delete";
	private static final String ERROR_DIR = "lgdf" + File.separator + "error";
	private static final String TRANSFER_DIR = "lgdf" + File.separator
			+ "transfer";

	/**
	 * Creates the required initial tables for the Mohago server web-app to run
	 * including Batch table, Doc table, User table etc.
	 * 
	 */
	public void init() throws ServletException {

		TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
		LOG.info("Entering init: ");
		final String appPath = this.getServletContext().getRealPath("");
		Configuration.setContextPath(appPath);
		try {
			configureAuthentication();
		} catch (Exception e) {
			LOG.info("erroooooooor init: ");
		}

		// constructs path of the directory to save uploaded file
		String savePath = appPath + File.separator + SAVE_DIR;
		String delPath = appPath + File.separator + DELETE_DIR;
		String errorPath = appPath + File.separator + ERROR_DIR;
		String transferPath = appPath + File.separator + TRANSFER_DIR;

		Configuration.setLgdfFolderPath(savePath);
		LOG.info("LGF folder: " + savePath);

		// creates the save directory if it does not exists
		File fileSaveDir = new File(savePath);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdir();
		}

		// creates the delete directory if it does not exists
		File fileDeleteDir = new File(delPath);
		if (!fileDeleteDir.exists()) {
			fileDeleteDir.mkdir();
		}

		// creates the transfer directory if it does not exists
		File fileTransferDir = new File(transferPath);
		if (!fileTransferDir.exists()) {
			fileTransferDir.mkdir();
		}

		// creates the error directory if it does not exists
		File fileErrorDir = new File(errorPath);
		if (!fileErrorDir.exists()) {
			fileErrorDir.mkdir();
		}

		Configuration.setTransferPath(transferPath);
		Configuration.setErrorPath(errorPath);

		DatabaseConnection.initDBConnectionParamsFromFile(this);
		DatabaseConnection.initJDBCDriver();
		try (Connection conn = DatabaseConnection.getMasterConnection();) {
			SQLHelpers.setAutoCommit(conn, false);
			if (conn != null) {
				DatabaseConnection.buildTables(conn);
			}

		} catch (SQLException e) {
			LOG.error("Error startup: " + e);
		}
		// *******************

		// *******************

		// ******************* RESTART SEVICES AFTER DEV

		// CleanupService cleanupService = new CleanupService();
		ScanService scanService = new ScanService();
		RefreshTokenService refreshTokenService = new RefreshTokenService();
		RefreshNodeSetService refreshNodesSetService = new RefreshNodeSetService();

	}

	private void configureAuthentication() {
		Authentication.setAuthUtils(new Utilities(Configuration
				.getContextPath()));
		Authentication.setClient(new Auth());
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

	}

}
