package com.mohago.resources.data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.security.InvalidParameterException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Encoded;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.xml.stream.XMLStreamException;

import org.apache.log4j.Logger;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartInput;
import org.joda.time.DateTime;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.LongSerializationPolicy;
import com.google.gson.reflect.TypeToken;
import com.mohago.DataObjects.DataSet;
import com.mohago.DataObjects.LookingGlassException;
import com.mohago.config.Configuration;
import com.mohago.config.DatabaseConnection;
import com.mohago.config.Configuration.Database;
import com.mohago.exception.MohagoException;
import com.mohago.json.CloudCIXUser;
import com.mohago.json.Item;
import com.mohago.json.MetaData;
import com.mohago.json.Parameter;
import com.mohago.json.Token;
import com.mohago.json.Value;
import com.mohago.lgf.MetaFileParser;
import com.mohago.node.NodeManager;
import com.mohago.persist.MetaStorageService;
import com.mohago.persist.DatasetUpdateService;
import com.mohago.persist.MetaUpdateService;
import com.mohago.request.DatabaseHelpers;
import com.mohago.request.MasterTable;
import com.mohago.request.RequestData;
import com.mohago.request.RequestMeta;
import com.mohago.search.DatasetSearch;
import com.mohago.serialization.Dataset;
import com.mohago.serialization.LookingGlassType;
import com.mohago.serialization.Meta;
import com.mohago.serialization.Pair;
import com.mohago.sql.SQLPreparedStatements;
import com.mohago.user.Authentication;
import com.mohago.user.Authorisation;
import com.mohago.utilities.CIXApi;
import com.mohago.utilities.Presentation;
import com.mohago.utilities.ResultCodes;
import com.mohago.utilities.SQLHelpers;
import com.mysql.jdbc.StringUtils;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path("/datasets")
@Api(value = "/datasets", description = "Operations relating to Datasets")
@Produces({ "application/json" })
public class DatasetResource {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2469547765162132945L;

	private static final Logger LOG = Logger.getLogger(DatasetResource.class);
	private static final Gson GSON = new Gson();


	@GET
	@ApiOperation(httpMethod = "GET", value = "Retrieve top-level Dataset information", notes = "Returns a list of Datasets (top-level only), according to optional query parameters. Will return all Datasets if no query parameters specified.")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Datasets not found"),
			@ApiResponse(code = 400, message = "Error parsing meta query condition, check format matches {meta-data name}{boolean comparator}{compare value}, e.g. \"voltage>=23.0\""),
			@ApiResponse(code = 401, message = "Auth failed") })
	public Response readDatasetList(
			@Context HttpServletRequest request,
			@Context HttpServletResponse response,
			@ApiParam(value = "Comma seperated list of Dataset Ids", required = false) @QueryParam(value = "datasetIdList") String datasetIdList,
			@ApiParam(value = "Start search time", required = false) @QueryParam(value = "startTime") String startTime,
			@ApiParam(value = "End search time", required = false) @QueryParam(value = "endTime") String endTime,
			@ApiParam(value = "Name", required = false) @QueryParam(value = "name") String name,
			@ApiParam(value = "Project", required = false) @QueryParam(value = "project") String project,
			// @ApiParam(value = "Comma seperated list of Dataset Member Ids",
			// required = false) @QueryParam(value = "memberIdList") String
			// memberIdList,
			@ApiParam(value = "Number of Datasets to skip in request", required = false) @QueryParam(value = "offset") Integer offset,
			@ApiParam(value = "Maximum number of Datasets to return in request (Default: 20)", required = false) @QueryParam(value = "limit") Integer limit,
			@ApiParam(value = "Sort term, e.g. \"+date\" (Sort results by 'date' ascending)", required = false) @QueryParam(value = "sortBy") String sortBy,
			@ApiParam(value = "Comma seperated list of filter terms, e.g. \"name,project\". This filters results to only include specified fields.", required = false) @QueryParam(value = "filterBy") String filterBy,
			@ApiParam(value = "Comma seperated list Meta-Data conditions of format {meta-name}{boolean comparator}{value}, e.g.: \"voltage>=3.56,temp_integer==42,operator==Stephen\". The comma implies OR", required = false) @QueryParam(value = "metaQuery") String metaQuery,
			@ApiParam(value = "Security token", required = true) @HeaderParam("X-Auth-Token") String token)
			throws ServletException, IOException, MohagoException {

		ArrayList<Dataset> topLevelDatasets = new ArrayList<Dataset>();
		HashMap<Long, Dataset> topLevelDatasetMap = new HashMap<Long, Dataset>();
		long now = System.currentTimeMillis();

		CloudCIXUser user = Authentication.authenticate(request, response);
		// CloudCIXUser user = new CloudCIXUser();
		// user.setIdAddress(205396);
		// user.setIdMember(11);
		// user.setIdUser(1);
		// user.setUsername("jim");
		// Token t = new Token();
		// t.setValue("");
		// user.setToken(t);
		long after = System.currentTimeMillis();

		if (user != null) {
			
			int rowCount = 0;
			int totalRows = 0;
			if (offset == null) {
				offset = 0;
				// preparedStatement.setInt(6, offset);
			}
			if (limit == null) {
				limit = Presentation.DEFAULT_LIMIT;
			}
			
			// do look up and get a list of pairs where first is a db
			// connection, second is an initialized empty array of d_ids

			now = System.currentTimeMillis();
			HashSet<Integer> addressIdSet = CIXApi.getLinkedAddressList(user
					.getToken().getValue());

			after = System.currentTimeMillis();
			LOG.error("Total CIX address check took: " + (after - now)
					+ " milliseconds");

			long before = System.currentTimeMillis();

			after = System.currentTimeMillis();
			LOG.error("Lookup ip list: " + (after - before) + " milliseconds");
			HashSet<String> searchedIpAddresses = new HashSet<String>();
			for (int addressId : addressIdSet) {
				before = System.currentTimeMillis();
				String ip = NodeManager.getNodeAddressByAddressId(String
						.valueOf(addressId));
				if (!searchedIpAddresses.contains(ip)) {
					try (Connection conn = DatabaseConnection
							.getPooledConnection(ip)) {
						SQLHelpers.setAutoCommit(conn, false);

						if (!StringUtils.isNullOrEmpty(datasetIdList)) {// First
																		// is
																		// specific
																		// global
																		// ids
							topLevelDatasets.addAll(RequestData
									.specifiedDatasets(datasetIdList,
											user.getIdMember(), conn));
						} else if (!StringUtils.isNullOrEmpty(metaQuery)) {// Second
																			// is
																			// metafilter
							topLevelDatasets.addAll(DatasetSearch
									.searchByMetaQuery(metaQuery,
											user.getIdMember(), conn));
						} else {// Third is use all the search options

							java.sql.Date start, end = null;
							if (!StringUtils.isNullOrEmpty(startTime)) {
								start = new java.sql.Date(new DateTime(
										startTime).getMillis());
							} else {
								start = new java.sql.Date(0);
							}
							if (!StringUtils.isNullOrEmpty(endTime)) {
								end = new java.sql.Date(
										new DateTime(endTime).getMillis());
							} else {
								DateTime dt = new DateTime(
										"9999-12-12T23:59:59.999+00");
								end = new java.sql.Date(dt.getMillis());
							}

							PreparedStatement preparedStatement = null;
							ResultSet reader = null;

							try {
								preparedStatement = conn
										.prepareStatement(SQLPreparedStatements.Select.SELECT_DATASET
												.getValue());
								preparedStatement.setInt(1, user.getIdMember());
								if (!StringUtils.isNullOrEmpty(name)) {
									preparedStatement.setString(2, name);
								} else {
									preparedStatement.setString(2, "%");
								}
								if (!StringUtils.isNullOrEmpty(project)) {
									preparedStatement.setString(3, project);
								} else {
									preparedStatement.setString(3, "%");
								}
								preparedStatement.setDate(4, start);
								preparedStatement.setDate(5, end);
								String sql = preparedStatement.toString();
								sql = sql.split(" OFFSET")[0];
								PreparedStatement countRows = conn
										.prepareStatement(sql);
								totalRows += SQLHelpers.getRowCount(countRows,
										"*", conn);
								

								reader = preparedStatement.executeQuery();
								

								conn.commit();
								while (reader.next()) {
									String deviceToken = null;
									if (reader.getInt("owner_m_id") == user
											.getIdMember()) {
										deviceToken = reader.getString(7);
									}// Only show device token to owner
									final Dataset dataset = new Dataset(
											reader.getLong(1), reader
													.getString(2).trim(),
											reader.getString(4), reader
													.getString(3).trim(),
											reader.getInt(5), deviceToken);
									if (dataset != null) {
										topLevelDatasetMap.put(
												reader.getLong(1), dataset);
										// dataset.setMetaInfo(RequestMeta
										// .getMetaNameValueMap(
										// reader.getLong(1), conn));
										// topLevelDatasets.add(dataset);
									}
									rowCount++;
								}
							} catch (Exception e) {
								LOG.error("SQL statement error: " + e);
								conn.rollback();
							} finally {
								SQLHelpers.closeResources(preparedStatement,
										reader);
							}
						}
						// Now add meta
						if (topLevelDatasetMap.size() > 0) {
							topLevelDatasets.addAll(RequestMeta.setMeta(
									topLevelDatasetMap, conn));
							topLevelDatasetMap.clear();
						}

						// end connection
					} catch (SQLException e) {
						throw new MohagoException(500, "No database connection");
					}
					searchedIpAddresses.add(ip);// right place for this?
					after = System.currentTimeMillis();
					LOG.error("Retrieve Datasets: " + (after - before)
							+ " milliseconds");
				}

			}

			if (!(topLevelDatasets == null) && !topLevelDatasets.isEmpty()) {
				topLevelDatasets = (ArrayList<Dataset>) Presentation
						.sortDatasets(sortBy, topLevelDatasets);

				try {
					if (offset + limit <= topLevelDatasets.size()) {
						topLevelDatasets = new ArrayList<Dataset>(
								topLevelDatasets
										.subList(offset, offset + limit));
					} else {
						topLevelDatasets = new ArrayList<Dataset>(
								topLevelDatasets.subList(offset,
										topLevelDatasets.size()));
					}
					rowCount = topLevelDatasets.size();
				} catch (IllegalArgumentException | IndexOutOfBoundsException e) {
					LOG.error("Error with offset: " + e);
					throw new MohagoException(400,
							"Offset value is greater than number of Datasets");

				}

				JsonArray jArray = Presentation.filterFields(filterBy,
						topLevelDatasets);
				String linksHeader = Presentation.generateLinksHeader(offset,
						limit, totalRows, request);

				return Response.ok().entity(GSON.toJson(jArray)).header("Links", linksHeader).header("X-Total-Count", String.valueOf(totalRows)).build();
			} else {
				throw new MohagoException(404, "No Datasets not found");
			}
		} else {
			throw new MohagoException(401, "Auth failed");
		}

	}

	@GET
	@Path("/{datasetId}")
	@ApiOperation(httpMethod = "GET", value = "Retrieve top-level Dataset information", notes = "Returns a Dataset (top-level), specified by it's Id")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Dataset not found"),
			@ApiResponse(code = 401, message = "Auth failed") })
	public Response readDataset(
			@Context HttpServletRequest request,
			@Context HttpServletResponse response,
			@ApiParam(value = "Id of Dataset", required = true) @PathParam(value = "datasetId") Long dId,
			@ApiParam(value = "Security token", required = true) @HeaderParam("X-Auth-Token") String token)
			throws ServletException, IOException, MohagoException {

		CloudCIXUser user = Authentication.authenticate(request, response);
		// CloudCIXUser user = new CloudCIXUser();
		// user.setIdAddress(205396);
		// user.setIdMember(11);
		// user.setIdUser(1);
		// user.setUsername("jim");
		if (user != null) {

			String ip = "";
			if (!StringUtils.isNullOrEmpty(ip = NodeManager
					.getNodeAddressByAddressId(String.valueOf(user
							.getIdAddress())))) {
				try (Connection conn = DatabaseConnection
						.getPooledConnection(ip)) {
					SQLHelpers.setAutoCommit(conn, false);// in case
					Dataset dataset = RequestData.retrieveDataset(dId,
							user.getIdMember(), conn);
					if (dataset != null) {
						GsonBuilder gsonBuilder = new GsonBuilder();
						gsonBuilder
								.setLongSerializationPolicy(LongSerializationPolicy.STRING);
						Gson gson = gsonBuilder.create();
						final Type datasetType = new TypeToken<Dataset>() {
						}.getType();
						return Response.ok()
								.entity(gson.toJson(dataset, datasetType))
								.build();
					} else {
						throw new MohagoException(404, "Dataset not found");
					}

				} catch (SQLException e1) {
					LOG.error("Error getting conn from pool: " + e1);
					throw new MohagoException(500, "No database connection");
				}

			} else {
				throw new MohagoException(500, "No database connection");
			}
		} else {
			throw new MohagoException(401, "Auth failed");
		}

	}

	@POST
	@Path("/{datasetId}/share/{memberId}")
	@ApiOperation(httpMethod = "POST", value = "Share Dataset with another member", notes = "Shares a Dataset (Read access), specified by Dataset Id, with another member, specified by Member Id")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Dataset not found"),
			@ApiResponse(code = 500, message = "Error sharing Dataset"),
			@ApiResponse(code = 400, message = "Dataset is not currently shared with specified member"),
			@ApiResponse(code = 400, message = "Dataset already shared with specified member"),
			@ApiResponse(code = 401, message = "Auth failed") })
	public Response shareDataset(
			@Context HttpServletRequest request,
			@Context HttpServletResponse response,
			@ApiParam(value = "Id of Dataset", required = true) @PathParam(value = "datasetId") Long dId,
			@ApiParam(value = "Member Id", required = true) @PathParam(value = "memberId") Integer memberId,
			@ApiParam(value = "Stop sharing Dataset with Member", allowableValues = "true,false", required = false, defaultValue = "false") @QueryParam("unshare") boolean unshare,
			@ApiParam(value = "Security token", required = true) @HeaderParam("X-Auth-Token") String token)
			throws ServletException, IOException, MohagoException {

		HashSet<Long> didSet = new HashSet<Long>();
		CloudCIXUser user = Authentication.authenticate(request, response);
		if (user != null) {

			String ip = "";
			if (!StringUtils.isNullOrEmpty(ip = NodeManager
					.getNodeAddressByAddressId(String.valueOf(user
							.getIdAddress())))) {
				try (Connection conn = DatabaseConnection
						.getPooledConnection(ip)) {
					SQLHelpers.setAutoCommit(conn, false);

					didSet.add(dId);

					// First check if member already has read access
					HashSet<Long> allowedDidListForMember = Authorisation
							.getAllowedReadDidList(memberId, conn);

					if (!allowedDidListForMember.contains(dId) && !unshare) {

						HashSet<Long> allowedShareDidList = Authorisation
								.getAllowedWriteDidList(user.getIdMember(),
										conn);
						didSet.retainAll(allowedShareDidList);// Removes any
																// buids
																// that
																// aren't in
																// the "allowed"
																// list

						if (!didSet.isEmpty()) {
							if (Authorisation.shareDataset(dId,
									user.getIdMember(), memberId, true, conn)) {
								JsonObject jElement = new JsonObject();
								jElement.add("result", new JsonPrimitive(
										"SUCCESS"));
								return Response.ok()
										.entity(jElement.toString()).build();
							} else {
								throw new MohagoException(500,
										"Error sharing Dataset");
							}

						} else {

							throw new MohagoException(404, "Dataset not found");
						}
					} else if (!allowedDidListForMember.contains(dId)
							&& unshare) {
						throw new MohagoException(400,
								"Dataset is not currently shared with specified member");
					} else if (allowedDidListForMember.contains(dId) && unshare) {
						if (Authorisation.shareDataset(dId, user.getIdMember(),
								memberId, false, conn)) {
							JsonObject jElement = new JsonObject();
							jElement.add("result", new JsonPrimitive("SUCCESS"));
							return Response.ok().entity(jElement.toString())
									.build();
						} else {
							throw new MohagoException(500,
									"Error un-sharing Dataset");
						}

					} else if (allowedDidListForMember.contains(dId)
							&& !unshare) {
						throw new MohagoException(400,
								"Dataset is already shared with specified member");
					} else {
						throw new MohagoException(500,
								"Error processing request");
					}
				} catch (SQLException e) {

					throw new MohagoException(500, "No Database connection");
				}
			} else {
				throw new MohagoException(404, "Dataset not found");
			}
		} else {
			throw new MohagoException(401, "Auth failed");
		}

	}

	@GET
	@Path("/{datasetId}/meta")
	@ApiOperation(httpMethod = "GET", value = "Retrieve Meta-Data", notes = "Returns Meta-Data in key-value form for a Dataset, specified by it's Dataset Id")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Dataset not found"),
			@ApiResponse(code = 401, message = "Auth failed") })
	public Response readMeta(
			@Context HttpServletRequest request,
			@Context HttpServletResponse response,
			@ApiParam(value = "Id of Dataset containing Meta-Data to be retrieved", required = true) @PathParam(value = "datasetId") Long dId,
			@ApiParam(value = "Security token", required = true) @HeaderParam("X-Auth-Token") String token)
			throws ServletException, IOException, MohagoException {

		HashSet<Long> didSet = new HashSet<Long>();
		// int did = -1;
		CloudCIXUser user = Authentication.authenticate(request, response);
		// CloudCIXUser user = new CloudCIXUser();
		// user.setIdAddress(205396);
		// user.setIdMember(11);
		// user.setIdUser(1);
		// user.setUsername("jim");
		if (user != null) {

			String ip = "";
			if (!StringUtils.isNullOrEmpty(ip = NodeManager
					.getNodeAddressByAddressId(String.valueOf(user
							.getIdAddress())))) {
				try (Connection conn = DatabaseConnection
						.getPooledConnection(ip)) {
					SQLHelpers.setAutoCommit(conn, false);
					// did = DatabaseHelpers.getDidFromGlobalId(globalId, conn);
					didSet.add(dId);

					HashSet<Long> allowedDidList = Authorisation
							.getAllowedReadDidList(user.getIdMember(), conn);
					didSet.retainAll(allowedDidList);// Removes any buids that
														// aren't in
														// the "allowed" list

					if (didSet.isEmpty()) {
						throw new MohagoException(404, "Dataset not found");
					}
					Gson gson = new Gson();
					final Type mapType = new TypeToken<HashMap<String, Object>>() {
					}.getType();
					HashMap<String, Object> meta = RequestMeta
							.getMetaNameValueMap(dId, conn);
					SQLHelpers.closeConn(conn);
					return Response.ok().entity(gson.toJson(meta, mapType))
							.build();
				} catch (SQLException e) {
					throw new MohagoException(500, "Dataset not found");
				}
			} else {
				throw new MohagoException(404, "Dataset not found");
			}
		} else {
			throw new MohagoException(401, "Auth failed");
		}
	}

	@GET
	@Path("/{datasetId}/parameters")
	@ApiOperation(httpMethod = "GET", value = "Retrieve parameters for a Dataset specified by Dataset Id", notes = "Returns parameters in name-type form for a Dataset, specified by it's Dataset Id where type (Integer) can be 0:Integer, 1:Double, 2:String, 3:DateTime")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Dataset not found"),
			@ApiResponse(code = 401, message = "Auth failed") })
	public Response readParameterCollections(
			@Context HttpServletRequest request,
			@Context HttpServletResponse response,
			@ApiParam(value = "Id of Dataset", required = true) @PathParam(value = "datasetId") Long dId,
			@ApiParam(value = "Security token", required = true) @HeaderParam("X-Auth-Token") String token)
			throws ServletException, IOException, MohagoException {

		CloudCIXUser user = Authentication.authenticate(request, response);
		// CloudCIXUser user = new CloudCIXUser();
		// user.setIdAddress(205396);
		// user.setIdMember(11);
		// user.setIdUser(1);
		// user.setUsername("jim");
		if (user != null) {

			String ip = "";
			if (!StringUtils.isNullOrEmpty(ip = NodeManager
					.getNodeAddressByAddressId(String.valueOf(user
							.getIdAddress())))) {
				try (Connection conn = DatabaseConnection
						.getPooledConnection(ip)) {
					SQLHelpers.setAutoCommit(conn, false);
					if (dId != null) {
						HashSet<Long> didSet = new HashSet<Long>();
						didSet.add(dId);
						HashSet<Long> allowedDidList = Authorisation
								.getAllowedReadDidList(user.getIdMember(), conn);
						didSet.retainAll(allowedDidList);// Removes any duids
															// that
															// aren't in
															// the "allowed"
															// list

						boolean isAuthorised = true;// Can re-add this layer
													// later

						Gson gson = new Gson();
						final Type mapType = new TypeToken<HashMap<String, Integer>>() {
						}.getType();
						HashMap<String, Integer> paramMap = com.mohago.request.RequestData
								.paramCollectionsMap(didSet, conn);
						SQLHelpers.closeConn(conn);
						if (isAuthorised) {
							return Response.ok()
									.entity(gson.toJson(paramMap, mapType))
									.build();

						} else {
							throw new MohagoException(401, "Auth failed");
						}
					} else {
						throw new MohagoException(404, "Dataset not found");
					}
				} catch (SQLException e) {
					throw new MohagoException(500, "No database connection");
				}
			} else {
				throw new MohagoException(404, "Dataset not found");
			}
		} else {
			throw new MohagoException(401, "Auth failed");
		}
	}

	@GET
	@Path("/{datasetId}/parameters/{name}")
	@ApiOperation(httpMethod = "GET", value = "Retrieve values for a Dataset for a given parameter", notes = "Returns a list of [itemIndex, value] entries where 'itemIndex' is the index of the item within the Dataset which contains the given value.")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Dataset not found"),
			@ApiResponse(code = 401, message = "Auth failed") })
	public Response readValues(
			@Context final HttpServletRequest request,
			@Context final HttpServletResponse response,
			@ApiParam(value = "Id of Dataset containing parameter values", required = true) @PathParam(value = "datasetId") Long dId,
			@ApiParam(value = "Name of parameter whose values are to be retrieved", required = true) @PathParam(value = "name") String name,
			@ApiParam(value = "Index of first value to be returned", required = false) @QueryParam(value = "startIndex") Integer startIndex,
			@ApiParam(value = "Index of final value to be returned", required = false) @QueryParam(value = "endIndex") Integer endIndex,
			@ApiParam(value = "Security token", required = true) @HeaderParam("X-Auth-Token") String token)
			throws IOException, MohagoException {

		CloudCIXUser user = Authentication.authenticate(request, response);
		// CloudCIXUser user = new CloudCIXUser();
		// user.setIdAddress(205396);
		// user.setIdMember(11);
		// user.setIdUser(1);
		// user.setUsername("jim");
		if (user != null) {
			String ip = "";
			if (!StringUtils.isNullOrEmpty(ip = NodeManager
					.getNodeAddressByAddressId(String.valueOf(user
							.getIdAddress())))) {
				try (Connection conn = DatabaseConnection
						.getPooledConnection(ip)) {
					SQLHelpers.setAutoCommit(conn, false);
					// int did = DatabaseHelpers
					// .getDidFromGlobalId(globalId, conn);
					if (dId != null) {
						LookingGlassType lgType = RequestData
								.getParamNameTypeMap(dId, conn).get(name);
						HashSet<Long> allowedDidList = Authorisation
								.getAllowedReadDidList(user.getIdMember(), conn);

						boolean isAuthorised = allowedDidList.contains(dId);
						if (isAuthorised) {
							JsonObject ret = DatabaseHelpers.getValues(name,
									lgType, dId, startIndex, endIndex, conn);
							SQLHelpers.closeConn(conn);
							return Response.ok().entity(ret.toString()).build();

						} else {
							throw new MohagoException(401, "No auth");
						}
					} else {
						throw new MohagoException(404, "Dataset not found");
					}
				} catch (SQLException e) {
					throw new MohagoException(500, "No Database connection");
				}
			} else {
				throw new MohagoException(404, "Dataset not found");
			}
		} else {
			throw new MohagoException(401, "Auth failed");
		}
	}

	@POST
	@ApiOperation(httpMethod = "POST", value = "Create a Dataset", notes = "DateTime parameter values must be in ISO-8601 format e.g.: \"2017-03-05T12:57:02.011\". Times should be in GMT Time-Zone. Time-Zones can be added via 'Meta' information if needed.")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Unable to create Dataset"),
			@ApiResponse(code = 401, message = "Auth failed") })
	public Response createDataset(
			@Context final HttpServletRequest request,
			@Context final HttpServletResponse response,
			@ApiParam(value = "Dataset object") com.mohago.json.Dataset dataset,
			@ApiParam(value = "Security token", required = true) @HeaderParam("X-Auth-Token") String token)
			throws IOException, MohagoException {

		CloudCIXUser user = Authentication.authenticate(request, response);
		// CloudCIXUser user = new CloudCIXUser();
		// user.setIdAddress(205396);
		// user.setIdMember(11);
		// user.setIdUser(1);
		// user.setUsername("jim");
		if (user != null) {

			JsonObject jElement = new JsonObject();
			jElement.add(
					"datasetId",
					new JsonPrimitive(storeDataset(dataset, user, null, null,
							null)));
			return Response.ok().entity(jElement.toString()).build();
		} else {
			throw new MohagoException(401, "Auth failed");
		}

	}

	@POST
	@Path("/{datasetId}/items")
	@ApiOperation(httpMethod = "POST", value = "Add a list of items to a Dataset", notes = "DateTime parameters must be in ISO-8601 format e.g.: \"2017-03-05T12:57:02.011\". Times should be in GMT Time-Zone.")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Unable to update Dataset"),
			@ApiResponse(code = 401, message = "Auth failed") })
	public Response addItemList(
			@Context final HttpServletRequest request,
			@Context final HttpServletResponse response,
			@ApiParam(value = "List of items", required = true) List<Item> items,
			@ApiParam(value = "Id of Dataset containing parameter values", required = true) @PathParam(value = "datasetId") Long dId,
			@ApiParam(value = "Device token, required if Security token is missing", required = false) @HeaderParam("device-token") String deviceToken,
			@ApiParam(value = "Security token, required if Device token is missing", required = false) @HeaderParam("X-Auth-Token") String token)
			throws IOException, MohagoException {

		String ip = "";
		CloudCIXUser user = Authentication.authenticate(request, response);
		// CloudCIXUser user = new CloudCIXUser();
		// user.setIdAddress(205396);
		// user.setIdMember(11);
		// user.setIdUser(1);
		// user.setUsername("jim");
		if (user != null) {

			if (!StringUtils.isNullOrEmpty(ip = NodeManager
					.getNodeAddressByAddressId(String.valueOf(user
							.getIdAddress())))) {
				try (Connection conn = DatabaseConnection
						.getPooledConnection(ip)) {
					SQLHelpers.setAutoCommit(conn, false);
					HashSet<Long> allowedDidList = Authorisation
							.getAllowedReadDidList(user.getIdMember(), conn);
					boolean isAuthorised = allowedDidList.contains(dId);
					if (isAuthorised) {

						String resp = addItems(dId, user, null, items, conn)
								.toString();
						SQLHelpers.closeConn(conn);

						return Response.ok().entity(resp).build();
					} else {
						throw new MohagoException(401, "Not authorised");
					}

				} catch (SQLException e) {

					throw new MohagoException(500, "No Database connection");
				}
			} else {
				throw new MohagoException(404, "Dataset not found");
			}
		} else if (!StringUtils.isNullOrEmpty(deviceToken)) {
			if (!StringUtils.isNullOrEmpty(ip = NodeManager
					.getNodeAddressByDeviceToken(deviceToken))) {
				try (Connection conn = DatabaseConnection
						.getPooledConnection(ip)) {
					SQLHelpers.setAutoCommit(conn, false);

					Long didFromDeviceToken = DatabaseHelpers
							.getDidFromDeviceToken(deviceToken, conn);
					if (dId.equals(didFromDeviceToken)) {
						return Response
								.ok()
								.entity(addItems(dId, null, deviceToken, items,
										conn).toString()).build();
					} else {
						throw new MohagoException(404, "Dataset not found");
					}

				} catch (SQLException e) {

					throw new MohagoException(500, "No Database connection");
				}
			} else {
				throw new MohagoException(404, "Dataset not found");
			}
		} else {
			throw new MohagoException(401, "Auth failed");
		}

	}

	@PUT
	@Path("/{datasetId}/meta/{name}")
	@ApiOperation(httpMethod = "PUT", value = "Update an element of Meta-Data")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Dataset not found"),
			@ApiResponse(code = 404, message = "Meta element not found"),
			@ApiResponse(code = 400, message = "Check parameters"),
			@ApiResponse(code = 401, message = "Auth failed") })
	public Response updateMeta(
			@Context final HttpServletRequest request,
			@Context final HttpServletResponse response,
			@ApiParam(value = "Id of Dataset", required = true) @PathParam(value = "datasetId") Long dId,
			@ApiParam(value = "Name of Meta to be updated", required = true) @PathParam(value = "name") String name,
			@ApiParam(value = "New value of Meta element in String representation, e.g. : \"23.476\" or \"2017-03-05T12:57:02.011\"", required = true) Value newValue,
			@ApiParam(value = "Security token", required = true) @HeaderParam("X-Auth-Token") String token)
			throws IOException, MohagoException {

		CloudCIXUser user = Authentication.authenticate(request, response);
		// CloudCIXUser user = new CloudCIXUser();
		// user.setIdAddress(205396);
		// user.setIdMember(11);
		// user.setIdUser(1);
		// user.setUsername("jim");
		if (user != null) {
			String ip = "";
			if (!StringUtils.isNullOrEmpty(ip = NodeManager
					.getNodeAddressByAddressId(String.valueOf(user
							.getIdAddress())))) {
				try (Connection conn = DatabaseConnection
						.getPooledConnection(ip)) {
					SQLHelpers.setAutoCommit(conn, false);

					if (dId != null) {
						LookingGlassType lgType = RequestMeta
								.getMetaNameTypeMap(dId, conn).get(name);
						HashSet<Long> allowedDidList = Authorisation
								.getAllowedReadDidList(user.getIdMember(), conn);

						boolean isAuthorised = allowedDidList.contains(dId);
						ResultCodes.Data metaUpdated = null;
						if (isAuthorised) {
							metaUpdated = MetaUpdateService.updateMeta(name, lgType,
									newValue.getValue().toString(), "update",
									dId, conn);
							SQLHelpers.closeConn(conn);
							if (metaUpdated.equals(ResultCodes.Data.SUCCESS)) {
								JsonObject jElement = new JsonObject();
								jElement.add("result", new JsonPrimitive(
										"SUCCESS"));
								return Response.ok()
										.entity(jElement.toString()).build();
							} else if (metaUpdated
									.equals(ResultCodes.Data.FAIL)) {

								throw new MohagoException(404,
										"Meta element not found");

							} else if (metaUpdated
									.equals(ResultCodes.Data.INVALID_ARGS)) {

								throw new MohagoException(400,
										"Check parameters");

							} else {
								throw new MohagoException(500,
										"Error updating meta");
							}

						} else {
							throw new MohagoException(401, "No auth");
						}
					} else {
						throw new MohagoException(500, "No Database connection");
					}
				} catch (SQLException e) {
					throw new MohagoException(500, "No Database connection");
				}
			} else {
				throw new MohagoException(404, "Dataset not found");
			}
		} else {
			throw new MohagoException(401, "No auth");
		}

	}

	@PUT
	@Path("/{datasetId}/{project}")
	@ApiOperation(httpMethod = "PUT", value = "Move Dataset to a new project")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Dataset not found"),
			@ApiResponse(code = 400, message = "Check parameters"),
			@ApiResponse(code = 401, message = "Auth failed") })
	public Response updateProject(
			@Context final HttpServletRequest request,
			@Context final HttpServletResponse response,
			@ApiParam(value = "Id of Dataset", required = true) @PathParam(value = "datasetId") Long dId,
			@ApiParam(value = "New Project name ( < 250 characters )", required = true) @PathParam(value = "project") String project,
			@ApiParam(value = "Security token", required = true) @HeaderParam("X-Auth-Token") String token)
			throws IOException, MohagoException {

		CloudCIXUser user = Authentication.authenticate(request, response);
		// CloudCIXUser user = new CloudCIXUser();
		// user.setIdAddress(205396);
		// user.setIdMember(11);
		// user.setIdUser(1);
		// user.setUsername("jim");
		if (user != null) {
			String ip = "";
			if (!StringUtils.isNullOrEmpty(ip = NodeManager
					.getNodeAddressByAddressId(String.valueOf(user
							.getIdAddress())))) {
				try (Connection conn = DatabaseConnection
						.getPooledConnection(ip)) {
					SQLHelpers.setAutoCommit(conn, false);
					if (dId != -1) {
						HashSet<Long> allowedDidList = Authorisation
								.getAllowedReadDidList(user.getIdMember(), conn);
						if (allowedDidList.contains(dId)) {

							if (!StringUtils.isNullOrEmpty(project)
									&& project.length() < 250) {
								if (DatasetUpdateService.moveToNewProject(project, dId,
										conn)) {
									JsonObject jElement = new JsonObject();
									jElement.add("result", new JsonPrimitive(
											"SUCCESS"));
									return Response.ok()
											.entity(jElement.toString())
											.build();

								} else {
									throw new MohagoException(500,
											"Unable to update project");
								}
							} else {
								throw new MohagoException(400,
										"Check parameters, project name must be < 250 parameters");

							}

						} else {
							throw new MohagoException(401, "No auth");
						}
					} else {
						throw new MohagoException(500, "No Database connection");
					}
				} catch (SQLException e) {
					throw new MohagoException(500, "No Database connection");
				}
			} else {
				throw new MohagoException(404, "Dataset not found");
			}
		} else {
			throw new MohagoException(401, "No auth");
		}

	}

	@POST
	@Path("/lgf")
	@Consumes("multipart/form-data")
	public Response uploadLgfPair(@Context final HttpServletRequest request,
			@Context final HttpServletResponse response, MultipartInput input)
			throws IOException, MohagoException, ServletException {
		// TODO: Add layer for permissions. does this member have permission to
		// add to this Dataset if it is a stream type?
		// TODO: Also we can check file hash here
		CloudCIXUser user = Authentication.authenticate(request, response);
		// CloudCIXUser user = new CloudCIXUser();
		// user.setIdAddress(205396);
		// user.setIdMember(11);
		// user.setIdUser(1);
		// user.setUsername("jim");
		if (user != null) {
			// final Connection conn = DatabaseConnection.getMasterConnection();
			// if (conn != null) {
			// SQLHelpers.setAutoCommit(conn, false);
			String savePath = Configuration.getLgdfFolderPath();
			Pair<InputPart, InputPart> metaAndData = new Pair<InputPart, InputPart>(
					null, null);
			String metaSavePath = "";
			String dataSavePath = "";
			for (InputPart part : input.getParts()) {

				String fileName = extractFileName(part);
				if (fileName.contains(".lgmf")) {
					metaAndData.setFirst(part);
					metaSavePath = savePath + File.separator + fileName
							+ File.separator;
				} else if (fileName.contains(".lgdf")) {
					metaAndData.setSecond(part);
					dataSavePath = savePath + File.separator + fileName
							+ File.separator;
				}
			}

			File file = new File(dataSavePath);

			if (!file.exists()) {

				file.createNewFile();
				try (BufferedWriter bw = new BufferedWriter(
						new FileWriter(file), 8 * 1024);
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(metaAndData.getSecond()
										.getBody(InputStream.class, null)))) {
					String line = "";
					while ((line = reader.readLine()) != null) {
						bw.write(line + "\r\n");
					}

				}

			}

			// TODO: Add layer for permissions. does this member have
			// permission to add to this Dataset if it is a stream type?
			MetaFileParser.parseMetaNodes(metaAndData.getFirst(), metaSavePath,
					user.getIdMember(), user.getIdUser(), user.getUsername(),
					user.getIdAddress());// Parses
			// xml,
			// edits
			// and
			// writes
			// to
			// file

			// SQLHelpers.setAutoCommit(conn, true);
			// SQLHelpers.closeConn(conn);
			return Response.ok().build();

		} else {
			throw new MohagoException(401, "No auth");
		}
	}

	@POST
	@Path("/csv")
	@Consumes("multipart/form-data")
	public Response uploadCsv(@Context final HttpServletRequest request,
			@Context final HttpServletResponse response, MultipartInput input)
			throws IOException, MohagoException, ServletException,
			NoSuchAlgorithmException, LookingGlassException, XMLStreamException {
		// /TODO: test
		CloudCIXUser user = Authentication.authenticate(request, response);
		if (user != null) {

			for (InputPart part : input.getParts()) {

				String fileName = extractFileName(part);
				parseCSV(part, fileName, user);
			}

			return Response.ok().build();
		} else {
			throw new MohagoException(401, "No auth");
		}
	}

	/**
	 * Extracts file name from HTTP header content-disposition
	 */
	private String extractFileName(InputPart part) {
		String contentDisp = part.getHeaders().get("content-disposition")
				.get(0);
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {// Remove??
				return s.substring(s.indexOf("=") + 2, s.length() - 1);
			} else if (s.trim().startsWith("name")) {
				String[] split = s.split("=");
				s = split[1].replace("\"", "");
				if (s.equals("lgf")) {
					return ".lgdf";

				} else if (s.equals("lmf")) {
					return ".lgmf";
				}
			}
		}
		return "";
	}

	@DELETE
	@Path("/{datasetId}/meta/{name}")
	@ApiOperation(httpMethod = "DELETE", value = "Delete an element of Meta-Data")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Dataset not found"),
			@ApiResponse(code = 404, message = "Meta element not found"),
			@ApiResponse(code = 400, message = "Check parameters"),
			@ApiResponse(code = 401, message = "Auth failed") })
	public Response deleteMeta(
			@Context final HttpServletRequest request,
			@Context final HttpServletResponse response,
			@ApiParam(value = "Id of Dataset", required = true) @PathParam(value = "datasetId") Long dId,
			@ApiParam(value = "Name of Meta to be deleted", required = true) @PathParam(value = "name") String name,
			@ApiParam(value = "Security token", required = true) @HeaderParam("X-Auth-Token") String token)
			throws IOException, MohagoException {

		CloudCIXUser user = Authentication.authenticate(request, response);
		// CloudCIXUser user = new CloudCIXUser();
		// user.setIdAddress(205396);
		// user.setIdMember(11);
		// user.setIdUser(1);
		// user.setUsername("jim");
		if (user != null) {
			String ip = "";
			if (!StringUtils.isNullOrEmpty(ip = NodeManager
					.getNodeAddressByAddressId(String.valueOf(user
							.getIdAddress())))) {
				try (Connection conn = DatabaseConnection
						.getPooledConnection(ip)) {
					SQLHelpers.setAutoCommit(conn, false);
					if (dId != -1) {
						LookingGlassType lgType = RequestMeta
								.getMetaNameTypeMap(dId, conn).get(name);
						HashSet<Long> allowedDidList = Authorisation
								.getAllowedReadDidList(user.getIdMember(), conn);

						boolean isAuthorised = allowedDidList.contains(dId);
						ResultCodes.Data metaUpdated;
						if (isAuthorised) {
							metaUpdated = MetaUpdateService.updateMeta(name, lgType,
									"stub", "delete", dId, conn);

							if (metaUpdated.equals(ResultCodes.Data.SUCCESS)) {
								JsonObject jElement = new JsonObject();
								jElement.add("result", new JsonPrimitive(
										"SUCCESS"));
								return Response.ok()
										.entity(jElement.toString()).build();
							} else if (metaUpdated
									.equals(ResultCodes.Data.FAIL)) {

								throw new MohagoException(404,
										"Meta element not found");

							} else if (metaUpdated
									.equals(ResultCodes.Data.INVALID_ARGS)) {

								throw new MohagoException(400,
										"Check parameters");

							} else {
								throw new MohagoException(500,
										"Error updating meta");
							}

						} else {
							throw new MohagoException(401, "No auth");
						}
					} else {
						throw new MohagoException(500, "No Database connection");
					}
				} catch (SQLException e) {
					throw new MohagoException(500, "No Database connection");
				}
			} else {
				throw new MohagoException(404, "Dataset not found");
			}
		} else {
			throw new MohagoException(401, "No auth");
		}

	}

	@PUT
	@Path("/{datasetId}/meta")
	@ApiOperation(httpMethod = "PUT", value = "Update Meta-Data of a Dataset", notes = "DateTime parameters must be in ISO-8601 format e.g.: \"2017-03-05T12:57:02.011\". Times should be in GMT Time-Zone.")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Dataset not found"),
			@ApiResponse(code = 400, message = "Check parameters"),
			@ApiResponse(code = 401, message = "Auth failed") })
	public Response updateMetaList(
			@Context final HttpServletRequest request,
			@Context final HttpServletResponse response,
			@ApiParam(value = "List of Meta-Data", required = true) List<MetaData> metaList,
			@ApiParam(value = "Id of Dataset Meta-Data is being replaced", required = true) @PathParam(value = "datasetId") Long dId,
			@ApiParam(value = "Security token", required = true) @HeaderParam("X-Auth-Token") String token)
			throws IOException, MohagoException {

		CloudCIXUser user = Authentication.authenticate(request, response);
		// CloudCIXUser user = new CloudCIXUser();
		// user.setIdAddress(205396);
		// user.setIdMember(11);
		// user.setIdUser(1);
		// user.setUsername("jim");
		if (user != null) {

			String ip = "";
			if (!StringUtils.isNullOrEmpty(ip = NodeManager
					.getNodeAddressByAddressId(String.valueOf(user
							.getIdAddress())))) {
				try (Connection conn = DatabaseConnection
						.getPooledConnection(ip)) {
					SQLHelpers.setAutoCommit(conn, false);
					if (dId != null) {
						HashSet<Long> allowedDidList = Authorisation
								.getAllowedReadDidList(user.getIdMember(), conn);

						boolean isAuthorised = allowedDidList.contains(dId);
						ResultCodes.Data metaUpdated = null;
						if (isAuthorised) {

							// First delete existing meta
							MetaStorageService.deleteMeta(dId,
									LookingGlassType.integer, conn);
							MetaStorageService.deleteMeta(dId,
									LookingGlassType.double_precision, conn);
							MetaStorageService.deleteMeta(dId,
									LookingGlassType.date_time, conn);
							MetaStorageService.deleteMeta(dId,
									LookingGlassType.string_value, conn);
							for (MetaData meta : metaList) {
								LookingGlassType lgType = MetaStorageService
										.getLGType(meta.getValue());
								if (!RequestMeta.getMetaNameTypeMap(dId, conn)
										.containsKey(meta.getName())) {
									metaUpdated = MetaUpdateService.updateMeta(meta
											.getName(), lgType, meta.getValue()
											.toString(), "create", dId, conn);
									if (!metaUpdated
											.equals(ResultCodes.Data.SUCCESS)) {
										throw new MohagoException(400,
												"Check parameters");
									}
								} else {
									throw new MohagoException(400,
											"Meta parameter: \""
													+ meta.getName()
													+ "\" already exists");
								}
							}

							JsonObject jElement = new JsonObject();
							jElement.add("result", new JsonPrimitive("SUCCESS"));
							return Response.ok().entity(jElement.toString())
									.build();

						} else {
							throw new MohagoException(401, "No auth");
						}
					} else {

						throw new MohagoException(404, "Dataset not found");
					}
				} catch (SQLException e) {
					throw new MohagoException(500, "No Database connection");
				}
			} else {
				throw new MohagoException(404, "Dataset not found");
			}

		} else {
			throw new MohagoException(401, "No auth");
		}

	}

	@POST
	@Path("/{datasetId}/meta")
	@ApiOperation(httpMethod = "POST", value = "Add a list of Meta-Data to a Dataset", notes = "DateTime parameters must be in ISO-8601 format e.g.: \"2017-03-05T12:57:02.011\". Times should be in GMT Time-Zone.")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Dataset not found"),
			@ApiResponse(code = 400, message = "Check parameters"),
			@ApiResponse(code = 401, message = "Auth failed") })
	public Response addMetaList(
			@Context final HttpServletRequest request,
			@Context final HttpServletResponse response,
			@ApiParam(value = "List of Meta-Data", required = true) List<MetaData> metaList,
			@ApiParam(value = "Id of Dataset Meta-Data is being added to", required = true) @PathParam(value = "datasetId") Long dId,
			@ApiParam(value = "Security token", required = true) @HeaderParam("X-Auth-Token") String token)
			throws IOException, MohagoException {

		CloudCIXUser user = Authentication.authenticate(request, response);
		// CloudCIXUser user = new CloudCIXUser();
		// user.setIdAddress(205396);
		// user.setIdMember(11);
		// user.setIdUser(1);
		// user.setUsername("jim");
		if (user != null) {

			String ip = "";
			if (!StringUtils.isNullOrEmpty(ip = NodeManager
					.getNodeAddressByAddressId(String.valueOf(user
							.getIdAddress())))) {
				try (Connection conn = DatabaseConnection
						.getPooledConnection(ip)) {
					SQLHelpers.setAutoCommit(conn, false);
					if (dId != null) {
						HashSet<Long> allowedDidList = Authorisation
								.getAllowedReadDidList(user.getIdMember(), conn);

						boolean isAuthorised = allowedDidList.contains(dId);
						ResultCodes.Data metaUpdated = null;
						if (isAuthorised) {
							for (MetaData meta : metaList) {
								LookingGlassType lgType = MetaStorageService
										.getLGType(meta.getValue());
								if (!RequestMeta.getMetaNameTypeMap(dId, conn)
										.containsKey(meta.getName())) {
									metaUpdated = MetaUpdateService.updateMeta(meta
											.getName(), lgType, meta.getValue()
											.toString(), "create", dId, conn);
									if (!metaUpdated
											.equals(ResultCodes.Data.SUCCESS)) {
										throw new MohagoException(400,
												"Check parameters");
									}
								} else {
									throw new MohagoException(400,
											"Meta parameter: \""
													+ meta.getName()
													+ "\" already exists");
								}
							}

							JsonObject jElement = new JsonObject();
							jElement.add("result", new JsonPrimitive("SUCCESS"));
							return Response.ok().entity(jElement.toString())
									.build();

						} else {
							throw new MohagoException(401, "No auth");
						}
					} else {

						throw new MohagoException(404, "Dataset not found");
					}
				} catch (SQLException e) {
					throw new MohagoException(500, "No Database connection");
				}
			} else {
				throw new MohagoException(404, "Dataset not found");
			}

		} else {
			throw new MohagoException(401, "No auth");
		}

	}

	@DELETE
	@Path("/{datasetId}")
	@ApiOperation(httpMethod = "DELETE", value = "Delete a Dataset by Dataset Id")
	public Response deleteDataset(
			@Context final HttpServletRequest request,
			@Context final HttpServletResponse response,
			@ApiParam(value = "Id of Dataset to be deleted", required = true) @PathParam("datasetId") Long dId,
			@ApiParam(value = "Security token", required = true) @HeaderParam("X-Auth-Token") String token)
			throws IOException, MohagoException {
		javax.ws.rs.core.Response resp = null;
		CloudCIXUser user = Authentication.authenticate(request, response);
		// CloudCIXUser user = new CloudCIXUser();
		// user.setIdAddress(205396);
		// user.setIdMember(11);
		// user.setIdUser(1);
		// user.setUsername("jim");
		if (user != null) {
			String ip = "";
			if (!StringUtils.isNullOrEmpty(ip = NodeManager
					.getNodeAddressByAddressId(String.valueOf(user
							.getIdAddress())))) {
				try (Connection conn = DatabaseConnection
						.getPooledConnection(ip)) {
					SQLHelpers.setAutoCommit(conn, false);
					HashSet<Long> allowedSet = Authorisation
							.getAllowedWriteDidList(user.getIdMember(), conn);

					if (allowedSet.contains(dId)) {

						if (DatasetUpdateService.markDatasetAsDeleted(dId, conn)) {
							JsonObject jElement = new JsonObject();
							jElement.add("result", new JsonPrimitive("SUCCESS"));
							resp = Response.ok().entity(jElement.toString())
									.build();

						} else {
							throw new MohagoException(404,
									"Unable to delete Dataset.");
						}
					} else {
						throw new MohagoException(401, "Forbidden");
					}

					SQLHelpers.setAutoCommit(conn, true);
					SQLHelpers.closeConn(conn);
				} catch (SQLException e) {
					throw new MohagoException(401, "Auth failed");
				}
			} else {

				throw new MohagoException(500, "No Database connection");
			}
		} else {
			throw new MohagoException(404, "Dataset not found");
		}
		return resp;
	}

	@OPTIONS
	public Response handleCORSRequest() {
		final ResponseBuilder retValue = Response.ok();

		return retValue.build();
	}

	@OPTIONS
	@Path("/{path:.*}")
	public Response handleCORSRequest2() {
		final ResponseBuilder retValue = Response.ok();

		return retValue.build();
	}

	private boolean parseCSV(InputPart part, String fileName, CloudCIXUser user)
			throws IOException, LookingGlassException,
			NoSuchAlgorithmException, XMLStreamException {
		boolean ret = false;
		DataSet apiDataset = new DataSet();
		apiDataset.setFolderPath(Configuration.getLgdfFolderPath());
		apiDataset.setName(fileName);
		apiDataset.attachMeta("cloudCIXIDMember", user.getIdMember());
		apiDataset.attachMeta("cloudCIXIDUser", user.getIdUser());
		apiDataset.attachMeta("cloudCIXUserName", user.getUsername());
		apiDataset.attachMeta("cloudCIXIDAddress", user.getIdAddress());

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(
				part.getBody(InputStream.class, null)))) {
			String line = "";
			ArrayList<Pair<String, LookingGlassType>> params = new ArrayList<Pair<String, LookingGlassType>>();
			line = reader.readLine();
			if (line != null) {
				for (String name : line.split(",")) {
					params.add(new Pair(name, null));
				}
			}
			line = reader.readLine();
			if (line != null) {
				for (int i = 0; i < params.size(); i++) {
					Object value = null;
					String[] items = line.split(",");
					try {
						value = Integer.parseInt(items[i]);
						params.get(i).setSecond(LookingGlassType.integer);
						apiDataset.addParameterInt(params.get(i).getFirst(),
								(Integer) value);
					} catch (NumberFormatException e) {
						try {
							value = Double.parseDouble(items[i]);
							params.get(i).setSecond(
									LookingGlassType.double_precision);
							apiDataset.addParameterDouble(params.get(i)
									.getFirst(), (Double) value);
						} catch (NumberFormatException ne) {
							try {
								value = new DateTime(items[i]);
								params.get(i).setSecond(
										LookingGlassType.date_time);
								apiDataset.addParameterDate(params.get(i)
										.getFirst(), ((DateTime) value)
										.toDate());
							} catch (IllegalArgumentException ie) {
								params.get(i).setSecond(
										LookingGlassType.string_value);
								apiDataset.addParameterString(params.get(i)
										.getFirst(), items[i]);
							}
						}
					}
				}
			}
			apiDataset.endItem();
			while ((line = reader.readLine()) != null) {
				String[] items = line.split(",");
				for (int i = 0; i < params.size(); i++) {
					try {
						if (params.get(i).getSecond()
								.equals(LookingGlassType.integer)) {
							apiDataset.addParameterInt(
									params.get(i).getFirst(),
									Integer.parseInt(items[i]));
						} else if (params.get(i).getSecond()
								.equals(LookingGlassType.double_precision)) {
							apiDataset.addParameterDouble(params.get(i)
									.getFirst(), Double.parseDouble(items[i]));
						} else if (params.get(i).getSecond()
								.equals(LookingGlassType.date_time)) {
							apiDataset.addParameterDate(params.get(i)
									.getFirst(), new DateTime(items[i])
									.toDate());
						} else if (params.get(i).getSecond()
								.equals(LookingGlassType.string_value)) {
							apiDataset.addParameterString(params.get(i)
									.getFirst(), items[i]);
						}
					} catch (NumberFormatException e) {
						// Do nothing
					}
				}
				apiDataset.endItem();
			}
			apiDataset.endWrite();
		}

		ret = true;
		return ret;
	}

	private String storeDataset(com.mohago.json.Dataset dataset,
			CloudCIXUser user, Long parentUid, String deviceToken,
			Connection conn) throws IOException, MohagoException {

		HashMap<String, LookingGlassType> nameValueMap = new HashMap<String, LookingGlassType>();
		HashMap<String, LookingGlassType> nameValueMapMeta = new HashMap<String, LookingGlassType>();
		// First get HashMap from DB

		DataSet apiDataset = new DataSet();

		apiDataset.setFolderPath(Configuration.getLgdfFolderPath());
		if (parentUid != null) {
			try {

				nameValueMap.putAll(RequestData.getParamNameTypeMap(parentUid,
						conn));
				nameValueMapMeta.putAll(RequestMeta.getMetaNameTypeMap(
						parentUid, conn));
				apiDataset.setParentDatasetId(parentUid);// needs to be last
				// call in this catch clause!!!!!!!!!! (Because of exception
				// thrown)
			} catch (LookingGlassException e) {
				// Nothing to do here
				// part of self consuming public API
			}
		}
		if (!StringUtils.isNullOrEmpty(deviceToken)) {
			apiDataset.setDeviceToken(deviceToken);
			apiDataset.setDatasetId(NodeManager.getDatasetUid(0));// Use magic
																	// number
																	// instead
																	// of user
																	// id, need
																	// any did
																	// as long
																	// as diff
		} else {
			apiDataset.setDatasetId(NodeManager.getDatasetUid(user
					.getIdAddress()));
		}
		apiDataset.setName(dataset.getName());
		apiDataset.setProject(dataset.getProject());

		try {

			for (MetaData<?> meta : dataset.getMetadata()) {

				LookingGlassType type = MetaStorageService.getLGType(meta.getValue());
				if (!nameValueMapMeta.containsKey(meta.getName())) {

					nameValueMapMeta.put(meta.getName(), type);
					if (type.equals(LookingGlassType.integer)) {
						String name = meta.getName();
						if (!name.equalsIgnoreCase("cloudCIXIDMember")
								|| !name.equalsIgnoreCase("cloudCIXIDAddress")
								|| !name.equalsIgnoreCase("cloudCIXIDUser")
								|| !name.equalsIgnoreCase("cloudCIXUserName")) {
							apiDataset.attachMeta(name,
									(Integer) meta.getValue());
						}
						// apiDataset.attachMeta(meta.getName(),
						// (Integer) meta.getValue());
					} else if (type.equals(LookingGlassType.double_precision)) {
						apiDataset.attachMeta(meta.getName(),
								(Double) meta.getValue());
					} else if (type.equals(LookingGlassType.string_value)) {
						apiDataset.attachMeta(meta.getName(),
								(String) meta.getValue());
					} else if (type.equals(LookingGlassType.date_time)) {

						DateTime dt = new DateTime((String) meta.getValue());
						apiDataset.attachMeta(meta.getName(), dt.toDate());
					}
				} else {
					throw new MohagoException(400, "Meta item \""
							+ meta.getName() + "\" already exists.");
				}

			}
			if (user != null) {
				apiDataset.attachMeta("cloudCIXIDMember", user.getIdMember());
				apiDataset.attachMeta("cloudCIXIDUser", user.getIdUser());
				apiDataset.attachMeta("cloudCIXUserName", user.getUsername());
				apiDataset.attachMeta("cloudCIXIDAddress", user.getIdAddress());
			}
			for (Item item : dataset.getItems()) {
				for (Parameter<?> param : item.getParameters()) {

					LookingGlassType type = null;
					Object value = param.getValue();
					if (!nameValueMap.containsKey(param.getName())) {

						if (value instanceof Integer) {
							type = LookingGlassType.integer;
							apiDataset.addParameterInt(param.getName(),
									(Integer) param.getValue());
						} else if (value instanceof Double) {
							type = LookingGlassType.double_precision;
							apiDataset.addParameterDouble(param.getName(),
									(Double) param.getValue());
						} else {
							try {
								value = new DateTime(value.toString());
								type = LookingGlassType.date_time;
								DateTime dt = new DateTime(
										(String) param.getValue());
								apiDataset.addParameterDate(param.getName(),
										dt.toDate());
							} catch (IllegalArgumentException ie) {
								value = value.toString();
								type = LookingGlassType.string_value;
								apiDataset.addParameterString(param.getName(),
										(String) param.getValue());
							}
						}
						nameValueMap.put(param.getName(), type);
					} else {
						type = nameValueMap.get(param.getName());
						if (type.equals(LookingGlassType.integer)) {
							if (value instanceof Integer) {
								apiDataset.addParameterInt(param.getName(),
										(Integer) param.getValue());
							} else {
								throw new MohagoException(400,
										"Check type of parameter \""
												+ param.getName() + "\"");
							}
						} else if (type
								.equals(LookingGlassType.double_precision)) {
							if (value instanceof Double) {
								apiDataset.addParameterDouble(param.getName(),
										(Double) param.getValue());
							} else {
								throw new MohagoException(400,
										"Check type of parameter \""
												+ param.getName() + "\"");
							}
						} else if (type.equals(LookingGlassType.date_time)) {
							try {
								DateTime dt = new DateTime(
										(String) param.getValue());
								apiDataset.addParameterDate(param.getName(),
										dt.toDate());
							} catch (IllegalArgumentException ie) {
								throw new MohagoException(400,
										"Check type of parameter \""
												+ param.getName() + "\"");
							}
						} else if (type.equals(LookingGlassType.string_value)) {
							try {
								apiDataset.addParameterString(param.getName(),
										(String) param.getValue());
							} catch (ClassCastException e) {
								throw new MohagoException(400,
										"Check type of parameter \""
												+ param.getName() + "\"");
							}
						}
					}

				}
				apiDataset.endItem();
			}

		} catch (LookingGlassException e) {
			LOG.error("Error adding meta info: " + e);
		}

		try {
			apiDataset.endWrite();
		} catch (NoSuchAlgorithmException | XMLStreamException
				| LookingGlassException e) {
			LOG.error("Error writing to lgdf folder: " + e);
		}

		String ret = "";
		if (parentUid != null) {
			ret = String.valueOf(parentUid);
		} else {
			ret = String.valueOf(apiDataset.getDatasetId());
		}
		return ret;

	}

	private JsonObject addItems(Long dId, CloudCIXUser user,
			String deviceToken, List<Item> items, Connection conn)
			throws MohagoException, IOException {
		if (dId != null) {

			com.mohago.json.Dataset dataset = new com.mohago.json.Dataset();
			dataset.setItems(items);
			JsonObject jElement = new JsonObject();
			jElement.add(
					"datasetId",
					new JsonPrimitive(storeDataset(dataset, user, dId,
							deviceToken, conn)));
			return jElement;
		} else {
			throw new MohagoException(404, "Dataset not found");
		}
	}

}
