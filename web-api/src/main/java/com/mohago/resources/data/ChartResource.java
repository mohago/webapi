package com.mohago.resources.data;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.mohago.charting.DownSampling;
import com.mohago.config.DatabaseConnection;
import com.mohago.exception.MohagoException;
import com.mohago.json.Chart;
import com.mohago.json.ChartRequest;
import com.mohago.json.CloudCIXUser;
import com.mohago.json.Point;
import com.mohago.json.Search;
import com.mohago.json.DateRange;
import com.mohago.node.NodeManager;
import com.mohago.request.*;
import com.mohago.serialization.LookingGlassType;
import com.mohago.serialization.Pair;
import com.mohago.user.Authentication;
import com.mohago.user.Authorisation;
import com.mohago.utilities.SQLHelpers;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path("/chart")
@Api(value = "/chart", description = "Operations relating to charting")
@Produces({ "application/json" })
public class ChartResource extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5079623370030461855L;

	private static final Logger LOG = Logger.getLogger(ChartResource.class);

	public void init() throws ServletException {
	}

	// @POST
	// @ApiOperation(httpMethod = "POST", value =
	// "Returns a json array of chartable points", response = Chart.class, notes
	// =
	// "Time data is returned as the number of milliseconds since 1970-01-01T00:00:00Z")
	// @Produces(MediaType.APPLICATION_JSON)
	// @Consumes(MediaType.APPLICATION_JSON)
	// @ApiResponses(value = { @ApiResponse(code = 404, message = "Not found"),
	// @ApiResponse(code = 401, message = "Auth failed") })
	// public Response chart(
	// @Context HttpServletRequest request,
	// @Context HttpServletResponse response,
	// @ApiParam(value =
	// "Json object containing parameters required to build a chart. Allowable values for 'types', e.g. xtype, ytype (Integer): (0:'Integer', 1:'Double', 2:'String', 3:'DateTime'). 'names' e.g. xname, yname are parameter names.",
	// required = true) ChartRequest chartRequest,
	// @ApiParam(value = "Security token", required = true)
	// @HeaderParam("X-Auth-Token") String token)
	// throws MohagoException {
	// CloudCIXUser user;
	// try {
	// user = Authentication.authenticate(request, response);
	// } catch (SocketTimeoutException e) {
	// throw new MohagoException(500, "Dependant service timeout");
	// }
	// if (user != null) {
	// Connection conn = null;
	// if ((conn = DatabaseConnection.getConnection(MasterTable
	// .getConnectionIPForDataset((String) chartRequest
	// .getGlobalIdSet().toArray()[0]))) != null) {
	// SQLHelpers.setAutoCommit(conn, false);// in case
	// int did = DatabaseHelpers.getDidFromGlobalId(
	// (String) chartRequest.getGlobalIdSet().toArray()[0],
	// conn);
	//
	// LookingGlassType lgTypeX = LookingGlassType.values()[chartRequest
	// .getXtype()];
	// LookingGlassType lgTypeY = LookingGlassType.values()[chartRequest
	// .getYtype()];
	// HashSet<Integer> allowedDidList = Authorisation
	// .getAllowedReadDidList(user.getIdMember(), conn);
	// boolean isAuthorised = allowedDidList.contains(did);
	// if (isAuthorised) {
	// Chart downSampled = new Chart();
	// if (chartRequest.getChartType().equals("xy")) {
	// ArrayList<Point<Number, Number>> ret = DatabaseHelpers
	// .getXYValues(chartRequest.getXname(), lgTypeX,
	// chartRequest.getYname(), lgTypeY, did,
	// conn);
	//
	// downSampled = DownSampling.largestTriangleThreeBuckets(
	// ret, chartRequest.getLimit());
	// }
	//
	// SQLHelpers.setAutoCommit(conn, true);
	// SQLHelpers.closeConn(conn);
	// final Gson gson = new Gson();
	// final Type chartTypeToken = new TypeToken<Chart>() {
	// }.getType();
	// return Response.ok()
	// .entity(gson.toJson(downSampled, chartTypeToken))
	// .build();
	//
	// } else {
	// throw new MohagoException(401, "Auth failed");
	// }
	// } else {
	// throw new MohagoException(500, "No Database connection");
	// }
	// } else {
	// throw new MohagoException(401, "Auth failed");
	// }
	// }

	@GET
	@Path("/{datasetId}")
	@ApiOperation(httpMethod = "GET", value = "Retrieve data for charting", notes = "")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Dataset not found"),
			@ApiResponse(code = 401, message = "Auth failed") })
	public Response chartV2(
			@Context HttpServletRequest request,
			@Context HttpServletResponse response,
			@ApiParam(value = "Dataset Id", required = true) @PathParam(value = "datasetId") Long dId,
			@ApiParam(value = "Comma seperated list of requested parameter names", required = true) @QueryParam("parameters") String parametersList,
			@ApiParam(value = "The name of a parameter of type datetime to be used to filter on date .", required = false) @QueryParam(value = "dateName") String dateName,
			@ApiParam(value = "Specify the minimum date for the filter on the dataset parameter dateName", required = false) @QueryParam(value = "dateStart") String dateStart,
			@ApiParam(value = "Specify the maximum date for the filter on the dataset paramater dateName", required = false) @QueryParam(value = "dateEnd") String dateEnd,
			@ApiParam(value = "Offset values on index", required = false) @QueryParam(value = "offset") Integer offset,
			@ApiParam(value = "Number of values to return", required = false) @QueryParam(value = "limit") Integer limit,
			@ApiParam(value = "Return the index value with your requested parameters", required = false, defaultValue="false") @QueryParam(value = "returnId") boolean returnId,
			@ApiParam(value = "Security token", required = true) @HeaderParam("X-Auth-Token") String token)
			throws ServletException, IOException, MohagoException {
		CloudCIXUser user = Authentication.authenticate(request, response);
		if (user != null) {

			String ip = NodeManager.getNodeAddressByAddressId(String
					.valueOf(user.getIdAddress()));
			try (Connection conn = DatabaseConnection.getPooledConnection(ip)) {
				SQLHelpers.setAutoCommit(conn, false);// in case
				HashSet<Long> allowedDidList = Authorisation
						.getAllowedReadDidList(user.getIdMember(), conn);
				boolean isAuthorised = allowedDidList.contains(dId);

				if (isAuthorised) {

					Object ret = null;

					// parse the parameter list.
					DateRange dateRange = null;
					if (parametersList != null) {

						int offsetInt = -1, limitInt = -1;
						try {

							if (offset != null) {// /only offset
								offsetInt = offset;
							}
							if (limit != null) {// /only limit
								limitInt = limit;
							}
						} catch (NumberFormatException ex) {
							return null;// /int format exception
						}
						if (limitInt == -1 ^ offsetInt == -1) {
							// /must be both or neither
							throw new MohagoException(400,
									"Offset and limit must both be set if they are used.");
						}

						if (dateName != null) {
							dateRange = new DateRange();
							dateRange.setDateParamName(dateName);

							java.sql.Date start, end = null;
							if (!com.mysql.jdbc.StringUtils
									.isNullOrEmpty(dateStart)) {
								start = new java.sql.Date(new DateTime(
										dateStart).getMillis());
							} else {
								start = new java.sql.Date(0);
							}
							if (!com.mysql.jdbc.StringUtils
									.isNullOrEmpty(dateEnd)) {
								end = new java.sql.Date(
										new DateTime(dateEnd).getMillis());
							} else {
								DateTime dt = new DateTime(
										"9999-12-12T23:59:59.999+00");
								end = new java.sql.Date(dt.getMillis());
							}
							dateRange.setDateTimeEnd(end);
							dateRange.setDateTimeStart(start);
						}

						// String[] params =
						// parametersList.split(",(?![^()]*+\\))");


						String sl = "(";
						String sr = ")";
						String[] params = parametersList.split(",(?=\\()");

						if (params.length > 0 && params[0] != parametersList) {// /split
																				// by
																				// (A,0),(B,2)
																				// has
																				// worked
							ArrayList<Pair<String, LookingGlassType>> cleanParams = new ArrayList<Pair<String, LookingGlassType>>();
							int n = 0;
							for (String param : params) {
								if (param.contains(sl) && param.contains(sr)) {
									param = param.replaceAll("[\\(\\)]", "");
									String[] nameType = param.split(",");
									if (!(StringUtils
											.isAlphanumericSpace(nameType[0]))) {
										return null; // /illegal param nalme,
														// bad request
									}
									Pair<String, LookingGlassType> p = new Pair<String, LookingGlassType>(
											nameType[0],
											LookingGlassType.values()[Integer
													.parseInt(nameType[1])]);
									cleanParams.add(p);
									n += 1;
								} else {
									return null; // /parse error, bad request
								}
							}
							ret = DatabaseHelpers.getValues(cleanParams,
									dateRange, offsetInt, limitInt,
									returnId, dId, conn);
							// /check returned object for errors or is json

						} else { // /try split by just ","

							if (!(parametersList.contains(sl))
									&& !(parametersList.contains(sr))) {
								params = parametersList.split(",");
								if (params.length > 0) {
									for (String param : params) {
										if (!(StringUtils.isAlphanumericSpace(param))) {
											throw new MohagoException(
													404,
													"Illegal parameter names requested. Parameter names must comprise solely alphanumeric characters.");// /param
																																						// names
																																						// are
																																						// illegal,
																																						// bad
																																						// request
										}
									}
									ret = DatabaseHelpers.getValues(params,
											dateRange, offsetInt,
											limitInt, returnId, dId, conn);
									// /check returned object for errors or is
									// json
								} else {
									throw new MohagoException(404,
											"Invalid parameter string.");
								}
							}
						}
					} else {
						throw new MohagoException(404,
								"A comma seperated parameter name or (name,type) list must be specified.");
					}
					SQLHelpers.setAutoCommit(conn, true);
					SQLHelpers.closeConn(conn);
					final Gson gson = new Gson();
					// final Type chartTypeToken = new TypeToken<Chart>()
					// {}.getType();
					if (ret != null && ret.getClass().equals(JsonObject.class)) {
						// return
						// Response.ok().entity(((JsonObject)ret)).build();
						return Response.ok()
								.entity(gson.toJson(((JsonObject) ret)))
								.build();
					} else {
						throw new MohagoException(404, "Request failed");
						// return Response.ok().build();
					}
				} else {
					throw new MohagoException(401, "Auth failed");
				}
			} catch (SQLException e) {
				throw new MohagoException(500, "No Database connection");
			}
		} else {
			throw new MohagoException(401, "Auth failed");
		}
	}

	@OPTIONS
	public Response handleCORSRequest() {
		final ResponseBuilder retValue = Response.ok();

		return retValue.build();
	}

	@OPTIONS
	@Path("/{path:.*}")
	public Response handleCORSRequest2() {
		final ResponseBuilder retValue = Response.ok();

		return retValue.build();
	}
}
