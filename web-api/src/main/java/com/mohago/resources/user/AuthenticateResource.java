package com.mohago.resources.user;

import java.io.IOException;

import javax.enterprise.inject.Produces;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;

import com.mohago.exception.MohagoException;
import com.mohago.json.CloudCIXUser;
import com.mohago.request.MasterTable;
import com.mohago.user.Authentication;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path("/authenticate")
@Api(value = "/authenticate", description = "Authentication resource")
@javax.ws.rs.Produces({ "application/json" })
public class AuthenticateResource {

	private static final long serialVersionUID = 3038182437936521085L;
	private static final Logger LOG = Logger
			.getLogger(AuthenticateResource.class);

	@POST
	@ApiOperation(httpMethod = "POST", value = "Authenticate credentials", notes = "Returns a security token header 'x-subject-token' in the response if successful")
	@ApiResponses(value = { @ApiResponse(code = 401, message = "Auth failed") })
	@Produces()
	public Response authenticate(
			@Context HttpServletRequest request,
			@Context HttpServletResponse response,
			@ApiParam(value = "Security token", required = false) @HeaderParam("X-Auth-Token") String token,
			@ApiParam(value = "User name", required = false) @HeaderParam("user_name") String userName,
			@ApiParam(value = "Password", required = false) @HeaderParam("password") String password)
			throws IOException, MohagoException {
		long before = System.currentTimeMillis();
		CloudCIXUser user = Authentication.authenticate(request, response);
//		CloudCIXUser user = new CloudCIXUser();
//		user.setIdAddress(205396);
//		user.setIdMember(11);
//		user.setIdUser(1);
//		user.setUsername("jim");
		long after = System.currentTimeMillis();
		LOG.error("Total AUth auth resource took: " + (after - before) + " milliseconds");
		if (user != null) {
			return Response.ok().build();
		} else {
			throw new MohagoException(401, "Not authorised");
		}
	}
	
	@OPTIONS
	public Response handleCORSRequest() {
	    final ResponseBuilder retValue = Response.ok();


	    return retValue.build();
	}
	@OPTIONS
	@Path("/{path:.*}")
	public Response handleCORSRequest2() {
	    final ResponseBuilder retValue = Response.ok();


	    return retValue.build();
	}

}
